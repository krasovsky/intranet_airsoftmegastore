var menu = new Object();

menu.slyArr = [];


menu.init = function(name){	
	var $frame  = $('#'+name);
	var $slidee = $frame.children('ul').eq(0);
	var $wrap   = $frame.parent();

	// Call Sly on frame
	menu.slyArr[name] = new Sly($frame,{
		itemNav: 'basic',
		smart: false,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: null,
		scrollBar: $wrap.find('.scrollbar'),
		scrollBy: 1,
		pagesBar: $wrap.find('.pages'),
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,

		// Buttons
		forward: $wrap.find('.forward'),
		backward: $wrap.find('.backward'),
		prev: $wrap.find('.prev'),
		next: $wrap.find('.next'),
		prevPage: $wrap.find('.prevPage'),
		nextPage: $wrap.find('.nextPage')
	}, {
		active: menu.elementActivated,
	});

	//menu.slyArr[name].on("active",menu.elementActivated);

}

menu.elementActivated = function(eventName,element){
	console.log("ON" +element);
}

$(document).ready(function () {
	menu.init("dropdown-model");
    menu.init("dropdown-fps");

    var jsonData = JSON.parse($("#menuJson").text());
    console.log(jsonData);

    for(var key in jsonData){
    	if(jsonData.hasOwnProperty(key)){
    		menu.init("dropdown-"+ key);
    	}
    }
	$(".menu-item").hover(function () {
        $(this).siblings().find('.wrap').slideUp(200);
        $(this).find('.wrap').slideDown(200);
        $(this).find(".menuTitle").css(["background-color","orange"]);
        menu.slyArr["dropdown-" + $(this).attr('id')].init();

    }, function () {
       $(this).find('.wrap').slideUp(200);
       menu.slyArr["dropdown-" + $(this).attr('id')].destroy();
    });


    
});