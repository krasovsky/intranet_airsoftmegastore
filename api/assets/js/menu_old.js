jQuery(function($){
	'use strict';

	// -------------------------------------------------------------
	//   Non Item Based Navigation
	// -------------------------------------------------------------
	(function () {
		var $frame = $('#nonitembased');
		var $wrap  = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			speed: 300,
			easing: 'easeOutExpo',
			pagesBar: $wrap.find('.pages'),
			activatePageOn: 'click',
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 100,
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
		});
	}());

	// -------------------------------------------------------------
	//   Smart Navigation
	// -------------------------------------------------------------
	(function () {
		var $frame  = $('#smart');
		var $slidee = $frame.children('ul').eq(0);
		var $wrap   = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			itemNav: 'basic',
			smart: 1,
			activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 3,
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 1,
			pagesBar: $wrap.find('.pages'),
			activatePageOn: 'click',
			speed: 300,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,

			// Buttons
			forward: $wrap.find('.forward'),
			backward: $wrap.find('.backward'),
			prev: $wrap.find('.prev'),
			next: $wrap.find('.next'),
			prevPage: $wrap.find('.prevPage'),
			nextPage: $wrap.find('.nextPage')
		});
	}());
});