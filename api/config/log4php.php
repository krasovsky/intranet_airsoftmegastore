<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * config for layout pattern
 */

$config["log4php"] =

    array(
        'appenders' => array(
            'default' => array(
                'class' => 'LoggerAppenderRollingFile',
                'layout' => array(
                    'class' => 'LoggerLayoutPattern',
                    'params' => array(
                        'conversionPattern' => '%date{Y-m-d H:i:s} %logger  %msg%n',
                    )
                ),
                "params" => array(
                    'file' => APPPATH . "\\assets\\data\\logs\\prices.log" ,
                    'maxFileSize' => '1MB',
                    'maxBackupIndex' => 5,
                )
            )
        ),
        'rootLogger' => array(
            'appenders' => array('default')
        ),
    )

?>