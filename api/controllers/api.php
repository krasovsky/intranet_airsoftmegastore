<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

const SECRET_KEY = "8a6e92e13972938d96f3961506b380f356311cb6a482c";
const API_KEY = "8a6e92e13972938d96f3961506b380f369c209a19d43625a21fb3e4f86c11ca2";

class Api extends CI_Controller{
    public function index(){
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $this->load->model('apiModel');
//        $request = json_encode(array(
//            "website" => "http://www.airsoft.sandbox.orderdynamics.net/11072-#12689,14689,478,7064,468,9693|webshot",
//	    ));

        $request = file_get_contents('php://input');

        if(empty($request)) $request = '{"method":"alsoViewed","params":{"product_ids":["4094"]}}';

        try{
        	$viewData = $this->apiModel->decodeRequest($request);
        }
        catch (Exception $e) {
        	$viewData = array("error" => $e->getMessage());
        }

        $this->load->view('apiview',array('output' =>$viewData));
    }

    public function getBuild(){
        #header('content-type: application/json; charset=utf-8');
        $this->load->model('apiModel');
        $buildId = $this->uri->segment(3);
        
        $request = '{"method":"getConfigJs","params":{"build_id":"'.$buildId.'"}}';

        try{
            $viewData = $this->apiModel->decodeRequest($request);
        }
        catch (Exception $e) {
            $viewData = array("error" => $e->getMessage());
        }

        $this->load->view('apiview',array('output' =>$viewData,"encode" => false));

    }

    public function createWebshotTestRequest(){
        $url = "http://webshot.airsoftmegastore.net";
        $data = array("website" => "http://www.airsoft.sandbox.orderdynamics.net/1844-#1844,13555,461,2357|webshot");

        $jsonData = json_encode($data);

        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, "data=".$jsonData);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }

    public function apiMenu(){
        $this->load->model('apiModel');
        $data = $this->apiModel->generateMenuData(["model","muzzle_velocity"]);

        $this->load->view('menuview',array("data" => $data));
    }
}