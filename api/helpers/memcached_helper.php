<?php

class Memcached_helper {
	public static function getCachedTime($key){
		$ci=& get_instance();

		$ci->load->driver("cache");
		$time = $ci->cache->memcached->get($key);
		if(! $time){
			$time = time();
			$ci->cache->memcached->save($key, $time,rand(1200,2700));
		}

		return $time;

	}
}