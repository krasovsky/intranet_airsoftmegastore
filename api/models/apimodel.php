<?php

class apiModel extends CI_Model{

	const OPTION_REQUEST_METHOD_NAME = "method";
	const OPTION_REQUEST_PARAMS_NAME = "params";

	const VALUE_METHOD_NAME_GET_SIMILAR_ITEMS = "find";
	const VALUE_METHOD_NAME_GET_ITEMS_FOR_CAGETORY = "alsoViewed";
	const VALUE_METHOD_NAME_GET_ITEMS = "getItems";
	const VALUE_METHOD_NAME_GET_FROM_BRAND = "getFromBrand";
	
	const VALUE_METHOD_SAVE_CONFIG_JS = "saveConfigJs";
	const VALUE_METHOD_GET_CONFIG_JS = "getConfigJs";

	const VALUE_METHOD_SAVE_NOTIFY_REQUEST = "saveNotifyRequest";
	const VALUE_METHOD_GET_ACTIVE_COUPONS = "getActiveCoupons";


	const VALUE_IMAGE_CACHE_TIME = 14400; //4*60*60 4 hours
    const VALUE_CONFIG_CACHE_TIME = 1200; // 20*60 20 mins

    const VALUE_TYPE_DOORBASTER = 1;
    const VALUE_TYPE_MAP = 2;

   function __construct()
    {
        parent::__construct();

       	//array of existing models
        $this->models = array(292,198,293,294,291,295,297,298,301,197,2,302,296,299,303,304,305,306);
        $this->mainParentCategories = array(1,63,174,22,41,36,55,239);

        $allCategoriesRes = $this->db->query("SELECT GROUP_CONCAT(c.id) AS child_categories,c.`parent_id` FROM categories c GROUP BY c.`parent_id`");
		$this->allCategoriesArr = $allCategoriesRes->result_array();
    }

	public function decodeRequest($jsonData){
		$dataArr = json_decode($jsonData,true);
		if (empty($dataArr) || $dataArr == false) throw new Exception("Wrong json data");

		if (empty($dataArr[self::OPTION_REQUEST_METHOD_NAME])) throw new Exception("method is not defined");

		$result = array();

		$paramsArr = @$dataArr[self::OPTION_REQUEST_PARAMS_NAME]["product_ids"];
		$isItemSearch = false;
		if(empty($paramsArr)) {
			$paramsArr = @$dataArr[self::OPTION_REQUEST_PARAMS_NAME]["item_ids"];
			$isItemSearch = true;
		}

		switch ($dataArr[self::OPTION_REQUEST_METHOD_NAME]) {
			case self::VALUE_METHOD_NAME_GET_SIMILAR_ITEMS:
				break;
			case self::VALUE_METHOD_SAVE_NOTIFY_REQUEST:
				$productId = $paramsArr[0];
				$email = $dataArr[self::OPTION_REQUEST_PARAMS_NAME]["user_email"];
				$ip = get_client_ip();
				$locationJson = file_get_contents("http://ipinfo.io/{$ip}/json");
				$result = $this->db->query("insert ignore into auto_notify_requests values(?,?,?,?)", array(
					$productId,
					$email,
					date('Y-m-d h:m:s'),
					$locationJson
					)
				);
				return array("sucess" => $result);
				break;
			case self::VALUE_METHOD_NAME_GET_ITEMS:
				if (empty($paramsArr)) throw new Exception("No parametrs found");
				$result = $this->getCategoryItemsFromDb($paramsArr,$isItemSearch);

				$returnArr = array();
				foreacH($result as $element){
					$returnArr[$element["item_id"]] = $element;
				}
				return $returnArr;

				break;
			case self::VALUE_METHOD_NAME_GET_FROM_BRAND:
				if (empty($paramsArr)) throw new Exception("No parametrs found");
				$result = $this->getProductsWithBrand($paramsArr,$isItemSearch);
				break;
			case self::VALUE_METHOD_NAME_GET_ITEMS_FOR_CAGETORY:
				if (empty($paramsArr)) throw new Exception("No parametrs found");
				$result = $this->getSimilarProducts($paramsArr,array(),$isItemSearch);				
				break;
			case self::VALUE_METHOD_GET_ACTIVE_COUPONS:
				$result = $this->getActiveCoupons();
				break;
			case self::VALUE_METHOD_GET_CONFIG_JS:
				$result = $this->getConfigJs($dataArr[self::OPTION_REQUEST_PARAMS_NAME]["build_id"]);
				return $result;
				break;
			case self::VALUE_METHOD_SAVE_CONFIG_JS:
				
				$userKey = $dataArr[self::OPTION_REQUEST_PARAMS_NAME]["user_key"];
				if($this->validateUser($userKey) == false){
					throw new Exception("Wrong API Key");
				}

				$positions = $dataArr[self::OPTION_REQUEST_PARAMS_NAME]["positions"];
				$buildId = $dataArr[self::OPTION_REQUEST_PARAMS_NAME]["build_id"];

				if(! $this->saveBuildJson($buildId,$positions)){
					throw new Exception("Request Failed!");
				}
				else{
					return array("success" => "Request successed!");
				}
				break;
			default:
				throw new Exception($dataArr[self::OPTION_REQUEST_METHOD_NAME]. "not found");
				
				break;
		}

		return $result;
		
		
	}

	public function getConfigJs($buildId){
		if(empty($buildId)) return false;

		$this->db->cleanMemcached("SELECT * FROM customizer_builds where build_id = ?", array($buildId));

		$rez = $this->db->query("SELECT * FROM customizer_builds where build_id = ?", array($buildId));
		$result = $rez->result_array()[0];
		return "var Platform = ". $result["config_json"];
	}
	public function saveBuildJson($buildId, $positions){
		if(empty($buildId) || empty($positions))
			return false;

		$rez = $this->db->query("INSERT into customizer_builds (build_id,config_json,last_update_date,last_user_updated)  values (?,?,?,?) ON DUPLICATE KEY UPDATE config_json = ?, last_update_date = ?, last_user_updated = ?", 
			array(
				$buildId,
				json_encode($positions),
				date('Y-m-d h:m:s'),
				$this->user["user_id"],
				json_encode($positions),
				date('Y-m-d h:m:s'),
				$this->user["user_id"],
				)
			);

		if(! $rez) return false; // echo ($this->db->_error_message());

		return true;
	}

	public function validateUser($userKey){
		if(empty($userKey))
			return false;

		$rez = $this->db->query("SELECT * from customizer_users where user_key like ? LIMIT 1", array($userKey));
		$user = $rez->result_array();
		if(count($user) == 0)
			return false;
		
		$this->user = $user[0];
		return true;
	}

	public function getActiveCoupons($option = array()){

		$cachedTime = date("Y-m-d h:m:s", Memcached_helper::getCachedTime("getActiveCoupons"));

		$rez = $this->db->query("SELECT o.* FROM od_coupons o WHERE o.`end_date` > ? and o.active = 1" , array($cachedTime));
		$result = $rez->result_array();

		$data = array();
		$data["coupons"] = $result;

		$query = $this->db->query("SELECT * FROM od_product_mapping  WHERE product_type IN (1,2)");
		$result = $query->result_array();
		foreach($result as $element){
			if($element["product_type"] == self::VALUE_TYPE_DOORBASTER){
				$data["doorbusters"][] = intval($element["product_id"]);
			}
			else if($element["product_type"] == self::VALUE_TYPE_MAP){
				$data["map"][] = intval($element["product_id"]);
			}
		}
		return $data;
	}

	public function getChildCategories($parent_id){
		foreach($this->allCategoriesArr as $cat){
			if($cat["parent_id"] == $parent_id){
				$childsArr = explode(',', $cat["child_categories"]);
				return $childsArr;
			}
		}
	}

	function getCurrectCategoryVal($catsArr,$exclude = array()){
		//dump($catsArr);
		//search through models
		// foreach($catsArr as $cat ){
		// 	$key = array_search($cat, $this->models);
		// 	if($key !== false && is_numeric($key)){
		// 		//if($cat != 298)
		// 			return $cat;
		// 	}
		// }

		foreach($catsArr as $cat) {
			//within main category!
			$key = array_search($cat, $this->mainParentCategories);
			if($key !== false && is_numeric($key)){
				$childrenArr = $this->getChildCategories($cat);
				if(! empty($childrenArr))
					break;
			}
		}
		
		//search within grandchild
		if(! empty($childrenArr)){
			foreach($childrenArr as $child){
				$grandChild = $this->getChildCategories($child);
				foreach($grandChild as $grand){
					$key = array_search($grand, $catsArr);
					if($key !== false && is_numeric($key)){
						//if($grand != 10)
							return $grand;
					}
				}
			}

			//search within parent
			foreach($catsArr as $cat) {
				$key = array_search($cat, $childrenArr);
				if($key !== false && is_numeric($key)){
					//if($cat != 10)
						return $cat;
				}
			}
		}

		return false;
	}

	public function getProductsWithBrand($paramsArr,$isItemSearch = false){
		$productInfo = $this->getCategoryItemsFromDb($paramsArr,$isItemSearch);
		$productInfo = $productInfo[0];	
		$cats = explode(',', $productInfo["categories"]);

		$queryStr = <<<HEREDOC
			SELECT 
			  cx.product_id,
			  GROUP_CONCAT(cx.category_id) AS categories,
			  i.id AS item_id,
			  p.name,
			  p.brand_backend_name AS brand,
			  p.img AS image,
			  p.on_sale,
			  TRUNCATE(
			    pp.price - (pp.price * (p.on_sale / 100)),
			    2
			  ) AS price,
		  	CONCAT('/', p.id, '-', p.vanity_url, '.aspx') AS url 
			FROM
			  categories_xref cx 
			INNER JOIN products p ON p.`id` = cx.`product_id` AND p.`brand_backend_name` LIKE ?
			INNER JOIN items i ON i.product_id = cx.product_id 
			INNER JOIN productpricings pp ON pp.id = cx.product_id
			WHERE cx.`category_id` IN ({$productInfo["categories"]}) and cx.product_id <> ?
			and p.merchandisable = 1 AND pp.`price` <> 0.00 AND i.inventory > 0
			GROUP BY cx.product_id 
			ORDER BY CHAR_LENGTH(GROUP_CONCAT(cx.category_id)) DESC
			LIMIT 100
HEREDOC;
		$rez = $this->db->query($queryStr,array($productInfo["brand"], $productInfo["product_id"]));
		$result = $rez->result_array();
		//get 6 different prices
		if(count($result) > 6){
			 $returnArr = array();

			$tNum = floor(count($result) / 6);
			$i = 0; $num = 0;
			while($i != 6){
				$returnArr[$i] = $result[$num];
				//dump($num);
				$num += $tNum;
				$i++;
			}
			return $returnArr;
		}
		return $result;
	}

	public function getSimilarProducts($paramsArr,$exclude = array(),$isItemSearch = false){
		$productInfo = $this->getCategoryItemsFromDb($paramsArr,$isItemSearch);
		//dump($productInfo);
		$productInfo = $productInfo[0];
		$cats = $productInfo["categories"];

		$catsArr = explode(',', $cats);

		$category = $this->getCurrectCategoryVal($catsArr);

		//$orderStr = "(TRUNCATE(pp.price - (pp.price * (p.on_sale / 100)),2) - {$productInfo["price"]})";
		// it not it models -> take min category
		if($category == false){
			$category = $catsArr[0];
			$orderStr = "ABS(TRUNCATE(pp.price - (pp.price * (p.on_sale / 100)),2))";
		}
		$orderStr = "TRUNCATE(pp.price - (pp.price * (p.on_sale / 100)),2)";

		$whereStr = "";
		$joinStr = "";
		//if its a gun
		if($catsArr[0] == 1){
			$joinStr = " inner join productspecs ps on p.id = ps.product_id  ";
			if(! empty($productInfo["model"]) && empty($exclude["model"])){	
				$whereStr .= " AND ps.model like '{$productInfo["model"]}'";
			}

			if(! empty($productInfo["material"]) && empty($exclude["material"])){	
				$whereStr .= " AND ps.material like '{$productInfo["material"]}'";
			}

			if(! empty($productInfo["muzzle_velocity"]) && $productInfo["muzzle_velocity"] != 0 && empty($exclude["muzzle_velocity"])){
				$start = $productInfo["muzzle_velocity"]-15;
				$end = $productInfo["muzzle_velocity"]+15;	
				$whereStr .= " AND ps.muzzle_velocity BETWEEN {$start} AND {$end}";
			}
		}

		if(empty($category)) throw new Exception("Categories are not set!", 1);

		$queryStr = <<<HEREDOC
		SELECT 
		  p.id AS product_id,
		  i.id AS item_id,
		  p.name,
		  p.brand_backend_name AS brand,
		  p.img AS image,
		  p.on_sale,
		  TRUNCATE(
		    pp.price - (pp.price * (p.on_sale / 100)),
		    2
		  ) AS price,
		  CONCAT('/', p.id, '-', p.vanity_url, '.aspx') AS url 
		FROM
		  products p 
		  INNER JOIN productpricings pp 
		    ON pp.id = p.id 
		  INNER JOIN items i 
		    ON i.product_id = p.id 
		  INNER JOIN categories_xref cx ON cx.`product_id` = p.`id`
		  {$joinStr}
		  WHERE cx.`category_id` = {$category} AND p.id <> {$productInfo["product_id"]} and p.merchandisable = 1 AND pp.`price` <> 0.00 AND i.inventory > 0 {$whereStr}
		  ORDER BY {$orderStr} ASC
		  LIMIT 100
HEREDOC;

		//dump($queryStr);
	
		$rez = $this->db->query($queryStr);
		$result = $rez->result_array();

		//get 6 different prices
		if(count($result) > 6){
			 $returnArr = array();

			$tNum = floor(count($result) / 6);
			$i = 0; $num = 0;
			while($i != 6){
				$returnArr[$i] = $result[$num];
				//dump($num);
				$num += $tNum;
				$i++;
			}
			return $returnArr;
		}
		else if(count($result) < 3 && empty($exclude)){
			$result = $this->getSimilarProducts($paramsArr,array("model" => true),$isItemSearch);	
		}


		return($result);
	}

	/**
	 * Returns default product info
	 */
	public function getCategoryItemsFromDb($paramsArr,$isItemSearch = false){
		//dump($paramsArr);
		$inStr = implode(',',$paramsArr);
		if($isItemSearch == false){
			$whereStr = "p.id in ({$inStr})";
		}
		else{
			$whereStr = "i.id in ({$inStr})";
		}

		$queryStr = <<<HEREDOC
				SELECT 
				  p.id as product_id,
				  i.id as item_id,
				  i.sku,
				  p.name,p.brand_backend_name as brand,
				  p.img as image,
				  p.categories,
				  IF((i.inventory - i.allocated_inventory) > 0 AND (i.inventory - i.allocated_inventory) > i.`outofstock_threshold`, i.inventory - i.allocated_inventory , 0 )  AS inventory,
				  p.on_sale,
				  ps.model, ps.material,ps.muzzle_velocity,ps.weight,
				  TRUNCATE(
				    pp.price - (pp.price * (p.on_sale / 100)),2 ) AS price,
				    concat('/' , p.id , '-' , p.vanity_url,'.aspx') as url 
				from
				  products p 
				 inner join productpricings pp on pp.id = p.id
				 inner join items i on i.product_id = p.id
				 inner join productspecs ps on p.id = ps.product_id 
				where {$whereStr}
HEREDOC;
		
		$rez = $this->db->query($queryStr);
		$result = $rez->result_array();
		//dump($result);
		return $result;
	}

	public function generateMenuData($filters = array()){
		$returnArr = array();

		foreach($filters as $filter){
			$returnArr[$filter] = array();
			$queryStr = <<<HEREDOC
				SELECT {$filter} FROM productspecs
				WHERE {$filter} IS NOT NULL AND {$filter} <> ""
				GROUP BY {$filter}
				ORDER BY {$filter} ASC
HEREDOC;
			$rez = $this->db->query($queryStr);
			foreach($rez->result_array() as $value){
				//dump($value);
				$returnArr[$filter][] = $value[$filter];
			}
		}

		return $returnArr;
	}
}