<?php 
	if(isset($encode) && $encode == false){
		header("Cache-Control: no-cache, no-store, must-revalidate");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $output;
	}
	else{
		$this->load->view('headers.php');
		echo json_encode($output,JSON_HEX_QUOT | JSON_HEX_TAG);
	}
?>