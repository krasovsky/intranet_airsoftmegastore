<div class="menu-item" id="<?php echo $key; ?>">
	<div class="menuTitle"><?php echo $key; ?></div>
	<div class="wrap" style="display:none">
		<div class="scrollbar">
			<div class="handle" style="transform: translateZ(0px) translateY(60px); height: 119px;">
				<div class="mousearea"></div>
			</div>
		</div>
		<div class="frame smart" id="dropdown-<?php echo $key; ?>" style="overflow: hidden;">
			<ul class="items" style="transform: translateZ(0px) translateY(-303px); height: 3030px;">
			<?php FOREACH($element as $value): ?>
				<li><?php echo $value; ?></li>
			<?php ENDFOREACH; ?>
			</ul>
		</div>

		<ul class="pages"><li class="">1</li><li class="active">2</li></ul>
	</div>
</div>