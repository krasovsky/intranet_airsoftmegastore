<!DOCTYPE html>
<html>
<head>
	<title>MENU!</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="assets/js/pluggins.js"></script>
	<script src="assets/js/sly.min.js"></script>
	<script src="assets/js/menu.js"></script>
	<link rel="stylesheet" href="assets/css/menu.css">
	
</head>
<body>
	<div class="pagespan container">
		<?php FOREACH($data as $key => $element) {
			$this->load->view('menu/menu_item',array("key" => $key, "element" => $element));
			//include("/views/menu/menu_item.php");
		}
		?>
	</div>
	<div class="clear"></div>
	<div id="menuJson" class="hidden"><?php echo json_encode($data); ?> </div>
</body>
</html>