<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Airsoftgi extends CI_Controller {

    public function index(){
        $this->products();
    }

    public function products()
    {
        $this->load->model('airsoftgi_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsoftgi_spider->getProducts($page,$itemsPerPage);

        $pages = $this->airsoftgi_spider->getPagination($itemsPerPage);

        $this->load->view('airsoftgi/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Airsoftgi Products'));
    }


    public function product(){

        $this->load->model('airsoftgi_spider');
        $this->load->model('ams');
        $this->load->helper('rollup_inventory');
        $this->load->helper('rollup_prices');

        $id = $this->uri->segment(3);

        $product = $this->airsoftgi_spider->getProduct($id);
        $ams_product = ($product->ams_id)? $this->ams->getProductCompare($product->ams_id) : array() ;

        //get prices
        $prices = $this->airsoftgi_spider->getYearOfProductPrices($id);
//
//        $rollup_prices['year'] = rollup_prices_year($prices);
//        $rollup_prices['month'] = rollup_prices_month($prices);
//        $rollup_prices['week'] = rollup_prices_week($prices);
//        $rollup_prices['yesterday'] = rollup_prices_yesterday($prices);

        //get inventory
        $inventory = $this->airsoftgi_spider->getYearOfProductInventory($id);

//        $rollup_inventory['year'] = rollup_inventory_year($inventory);
//        $rollup_inventory['month'] = rollup_inventory_month($inventory);
//        $rollup_inventory['week'] = rollup_inventory_week($inventory);
//        $rollup_inventory['yesterday'] = rollup_inventory_yesterday($inventory);

        $this->load->view('airsoftgi/product', array('product'=>$product,'prices'=>$prices, 'inventory'=>$inventory, 'ams_product'=>$ams_product));
    }

    public function outofstock()
    {
        $this->load->model('airsoftgi_spider');
        $this->load->helper('rollup_inventory');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsoftgi_spider->getOutOfStock($page,$itemsPerPage);

        $pages = $this->airsoftgi_spider->getPagination($itemsPerPage);

        $this->load->view('airsoftgi/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Airsoftgi Out of Stock'));
    }

    public function discontinued()
    {
        $this->load->model('airsoftgi_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsoftgi_spider->getDiscontinued($page,$itemsPerPage);

        $pages = $this->airsoftgi_spider->getPagination($itemsPerPage);

        $this->load->view('airsoftgi/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Airsoftgi Discontinued'));
    }

    public function exclusive(){
        $this->load->model('airsoftgi_spider');

        $itemsPerPage = $this->uri->segment(4)?:200;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsoftgi_spider->getExclusiveCompetitorProducts($page,$itemsPerPage);
        $brands = $this->airsoftgi_spider->getCommonBrands();

        $pages = $this->airsoftgi_spider->getPagination($itemsPerPage);

        $this->load->view('airsoftgi/product_exclusive', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Airsoftgi Products We Do Not Have'));
    }

    public function identical(){
        $this->load->model('airsoftgi_spider');

        $itemsPerPage = $this->uri->segment(4)?:200;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsoftgi_spider->getIdenticalCompetitorProducts($page,$itemsPerPage);
        $brands = $this->airsoftgi_spider->getCommonBrands();

        $pages = $this->airsoftgi_spider->getPagination($itemsPerPage);

        $this->load->view('airsoftgi/product_exclusive', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Airsoftgi Products Have'));
    }


    public function exclusiveBrands(){
        $this->load->model('airsoftgi_spider');
        $this->load->model('ams');

        $brands = $this->ams->getAllBrands();
        $data = $this->airsoftgi_spider->getExclusiveCompetitorBrands();
        $this->load->view('airsoftgi/brand_xref', array('result'=>$data, 'brands'=>$brands, 'h1'=>'Airsoftgi Brands We Do Not Have'));
    }

    public function updateBrand(){
        $this->load->model('airsoftgi_spider');
        if($this->input->post('id'))
            $this->airsoftgi_spider->updateBrandXref($this->input->post('id'), $this->input->post('ams_id'));
        return true;
    }

    public function brands(){
        $this->load->model('airsoftgi_spider');

        $data = $this->airsoftgi_spider->getAllBrandTotals();
        $this->load->view('airsoftgi/brand_all', array('result'=>$data, 'h1'=>'Airsoftgi All Brand Stats'));
    }

    public function topBrands(){
        $this->load->model('airsoftgi_spider');

        $data = $this->airsoftgi_spider->getTopBrands();
        $this->load->view('airsoftgi/brand_top', array('result'=>$data, 'h1'=>'Airsoftgi Top Brands'));
    }

    public function brand()
    {
        $this->load->model('airsoftgi_spider');
        $brand_id = $this->uri->segment(3);

        $data = array();
        $result = $this->airsoftgi_spider->getProductsFromBrandId($brand_id);
        $totals = $this->airsoftgi_spider->getBrandTotals($brand_id);
        $this->load->view('airsoftgi/brand_list', array('result'=>$result, 'brand_id'=>$brand_id, 'totals'=>$totals, 'h1'=>'Airsoftgi\'s '.$result[0]->brand.' Product line'));
    }

    public function parse(){
        $this->load->model('ams');
        $id = $this->uri->segment(3);

        $data = $this->ams->parse($id);
    }

    public function allInventory(){
        $this->load->model('airsoftgimanager');
        $data = array();
        $data['products'] = $this->airsoftgimanager->getAllProducts();

        foreach($data['products'] as $p){
            $p->inventory = ( $p->inventory == 100 ) ? -2 : $p->inventory;
        }

        $this->load->view('inventory/list', $data );
    }

    //command line function only
    public function crawl(){
        set_time_limit(1800);

        $this->load->model('airsoftgi_spider');

        $count = 0;
        while(($id = $this->airsoftgi_spider->getNextIdToSpider($count++)) != '' ){
            $this->airsoftgi_spider->getEverythingInBrand("http://www.airsoftgi.com/index.php?manufacturers_id=$id&max_display_search_results=48&listing=side&sort=2a", $id);
        }
        exit(0);
    }

    //command line function only
    public function getMassInventory(){

        $start = microtime(true);
        $this->load->model('airsoftgi_spider');
        $this->airsoftgi_spider->deleteCookie();

        $ids = $this->airsoftgi_spider->getNextProductIdMulti(300);

        $stock = $this->airsoftgi_spider->getInventoryMulti($ids);

        foreach($stock as $id=>$values){
            if($values['quantity']<5000){
                $this->airsoftgi_spider->setInventory($id, $values['quantity']);
            }else{
                $this->airsoftgi_spider->setInventory($id, 0);
            }
        }

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        exit(0);
    }


    public function brandProducts(){
        $this->load->model('airsoftgi_spider');
        $this->load->model('ams');
        $Airsoftgi_brand_id = $this->uri->segment(3);

        $ams_brand_id = $this->airsoftgi_spider->getAMSBrand($Airsoftgi_brand_id);

        $products = $this->ams->getAllProductsInBrandId($ams_brand_id);
        $this->load->view('airsoftgi/product_dropdown', array('products'=>$products));
    }


    public function updateXref(){
        $this->load->model('airsoftgi_spider');
        $this->airsoftgi_spider->updateXref();
        exit(0);
    }
}
