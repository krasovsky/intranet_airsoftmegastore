<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Airsplat extends CI_Controller {

    public function index(){
        $this->products();
    }

    public function products()
    {
        $this->load->model('airsplat_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsplat_spider->getProducts($page,$itemsPerPage);

        $pages = $this->airsplat_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products'));
    }


    public function product(){

        $this->load->model('airsplat_spider');
        $this->load->model('ams');
        $this->load->helper('rollup_inventory');
        $this->load->helper('rollup_prices');

        $id = $this->uri->segment(3);

        $product = $this->airsplat_spider->getProduct($id);
        $ams_product = ($product->ams_id)? $this->ams->getProductCompare($product->ams_id) : array() ;

        //get prices
        $prices = $this->airsplat_spider->getYearOfProductPrices($id);
//
//        $rollup_prices['year'] = rollup_prices_year($prices);
//        $rollup_prices['month'] = rollup_prices_month($prices);
//        $rollup_prices['week'] = rollup_prices_week($prices);
//        $rollup_prices['yesterday'] = rollup_prices_yesterday($prices);

        //get inventory
        $inventory = $this->airsplat_spider->getYearOfProductInventory($id);

//        $rollup_inventory['year'] = rollup_inventory_year($inventory);
//        $rollup_inventory['month'] = rollup_inventory_month($inventory);
//        $rollup_inventory['week'] = rollup_inventory_week($inventory);
//        $rollup_inventory['yesterday'] = rollup_inventory_yesterday($inventory);

        $this->load->view('evike/product', array('product'=>$product,'prices'=>$prices, 'inventory'=>$inventory, 'ams_product'=>$ams_product));
    }

    public function outofstock()
    {
        $this->load->model('airsplat_spider');
        $this->load->helper('rollup_inventory');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsplat_spider->getOutOfStock($page,$itemsPerPage);

        $pages = $this->airsplat_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Out of Stock'));
    }

    public function discontinued()
    {
        $this->load->model('airsplat_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsplat_spider->getDiscontinued($page,$itemsPerPage);

        $pages = $this->airsplat_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Discontinued'));
    }

    public function exclusive(){
        $this->load->model('airsplat_spider');

        $itemsPerPage = $this->uri->segment(4)?:200;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsplat_spider->getExclusiveCompetitorProducts($page,$itemsPerPage);
        $brands = $this->airsplat_spider->getCommonBrands();

        $pages = $this->airsplat_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_exclusive', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products We Do Not Have'));
    }

    public function identical(){
        $this->load->model('airsplat_spider');

        $itemsPerPage = $this->uri->segment(4)?:200;
        $page = $this->uri->segment(3)?:1;
        $data = $this->airsplat_spider->getIdenticalCompetitorProducts($page,$itemsPerPage);
        $brands = $this->airsplat_spider->getCommonBrands();

        $pages = $this->airsplat_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_exclusive', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products Have'));
    }

    public function brands(){
        $this->load->model('airsplat_spider');

        $data = $this->airsplat_spider->getAllBrandTotals();
        $this->load->view('evike/brand_all', array('result'=>$data, 'h1'=>'Evike All Brand Stats'));
    }

    public function brand()
    {
        $this->load->model('airsplat_spider');
        $brand_id = $this->uri->segment(3);

        $data = array();
        $result = $this->airsplat_spider->getProductsFromBrandId($brand_id);
        $totals = $this->airsplat_spider->getBrandTotals($brand_id);
        $this->load->view('evike/brand_list', array('result'=>$result, 'brand_id'=>$brand_id, 'totals'=>$totals, 'h1'=>'Evike\'s '.$result[0]->brand.' Product line'));
    }

    public function brandProducts(){
        $this->load->model('airsplat_spider');
        $this->load->model('ams');
        $brand_id = $this->uri->segment(3);

        $ams_brand_id = $this->airsplat_spider->getAMSBrand($brand_id);

        $products = $this->ams->getAllProductsInBrandId($ams_brand_id);
        $this->load->view('evike/product_dropdown', array('products'=>$products));
    }

    public function topBrands(){
        $this->load->model('airsplat_spider');

        $data = $this->airsplat_spider->getTopBrands();
        $this->load->view('evike/brand_top', array('result'=>$data, 'h1'=>'Evike Top Brands'));
    }

    public function exclusiveBrands(){
        $this->load->model('airsplat_spider');
        $this->load->model('ams');

        $brands = $this->ams->getAllBrands();
        $data = $this->airsplat_spider->getExclusiveCompetitorBrands();
        $this->load->view('evike/brand_xref', array('result'=>$data, 'brands'=>$brands, 'h1'=>'Evike Brands We Do Not Have'));
    }


    public function updateBrand(){
        $this->load->model('airsplat_spider');
        if($this->input->post('id'))
            $this->airsplat_spider->updateBrandXref($this->input->post('id'), $this->input->post('ams_id'));
        return true;
    }

    //command line function only
    public function crawl(){
        set_time_limit(3600);

        $this->load->model('airsplat_spider');

        $count = 0;
        while(($id = $this->airsplat_spider->getNextIdToSpider($count++)) != '' ){
            $this->airsplat_spider->getEverythingInBrand("http://www.airsplat.com/manufacturers/$id?limit=60", $id);
        }
        exit(0);
    }

 public function updateXref(){
        $this->load->model('airsplat_spider');
        $this->airsplat_spider->updateXref();

        $this->updateDuplicates();//update duplicates calls updatexref and dupicates always need to be checked
        exit(0);
    }

    public function updateDuplicates(){
        $this->load->model('airsplat_spider');
        $this->airsplat_spider->updateDuplicateSkus();
    }

}
