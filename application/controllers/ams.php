<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ams extends CI_Controller {

    public function index(){

//        $this->getMail();
//        $this->downloadZip();
//        $this->downloadUsers();
//        $this->downloadOrders();
//        $this->downloadOrderItems();
//        $this->unzip();
//        $this->process();
//        $this->clean();

    }

    public function step1()
    {
        $this->load->model('mailmanager');
        echo "<h3>Getting Exports</h3>";
        $this->mailmanager->getMessage();
    }

    public function step2()
    {
        $this->load->model('downloadmanager');
        echo "<h3>Downloading Zips</h3>";
        $this->downloadmanager->download();
    }
    public function step2_1()
    {
        $this->load->model('downloadmanager');
        echo "<h3>Downloading Users</h3>";
        $this->downloadmanager->downloadUsers();
    }
    public function step2_2()
    {
        $this->load->model('downloadmanager');
		
        echo "<h3>Downloading Orders</h3>";
        $this->downloadmanager->downloadOrders();
    }
    public function step2_3()
    {
        $this->load->model('downloadmanager');
		
        echo "<h3>Downloading Order Items</h3>";
        $this->downloadmanager->downloadOrderItems();
    }

    public function step3()
    {
        $this->load->model('downloadmanager');
        echo "<h3>Unzipping CSV</h3>";
        $this->downloadmanager->unzip();
    }
    public function step4()
    {
        $this->load->model('csv');
        echo "<h3>Processing CSV</h3>";
        $this->csv->processAll();
    }

    public function step5(){
        $this->load->model('csv');
        echo "<h3>Removing CSV</h3>";
        $this->csv->cleanAllProcessed();
    }

    public function download(){
        $this->step1();
        $this->step2();
        try{
            $this->step2_1();
            $this->step2_2();
            $this->step2_3();
        }
        catch (Exception $e){
            
        }

        $this->step3();
        //$this->mailOutput('Export Download', ob_get_clean());
    }

    public function install(){
        $this->step4();
        $this->step5();
        //$this->mailOutput('Export Install', ob_get_clean());
    }

    public function setPeopleAlsoOrdered(){
        $this->load->model('dataminer');
        $count = $this->uri->segment(3);
        $end = $count + 50;
        while($count++ < $end ){
            $this->dataminer->setAlsoOrdered($count);
        }
        $this->load->view('refresh', array('count'=>$count));
    }

    public function mailOutput($subject,$content){
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: Aaron <aaron@airsoftmegastore.com>' . "\r\n";
        $headers .= 'From: AMS Robot <odautoams@airsoftmegastore.com>' . "\r\n";

        mail('aaron@airsoftmegastore.com',$subject,$content, $headers);
    }
	
	public function testMail(){
		$this->mailOutput('works','nice');
	}
}
