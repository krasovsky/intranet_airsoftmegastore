<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Controller {

    public function index()
    {
        $this->load->model('image_manager');
        $this->image_manager->processCsv();
    }
    public function update()
    {
        $this->load->model('image_manager');
        $this->image_manager->saveDirectory();
    }

}
