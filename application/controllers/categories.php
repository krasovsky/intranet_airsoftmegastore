<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function index(){

    }

    public function process(){
        $this->load->model('csv');
        $this->csv->setAllCategoriesXref();
    }
    public function download(){
        $this->load->view('categories/download');
    }

    public function getDownload(){
        $this->load->model('categorymanager');
        $this->load->library('ParseCSV');

        $ids = $this->input->post('product_ids');

        if($ids!=''){
            $categories = $this->categorymanager->downloadCategories($ids);
        }else{
            $categories = $this->categorymanager->downloadAllCategories();
        }

        $this->parsecsv->enclosure='~';
        $file = $this->parsecsv->unparse($categories,array(),true);


        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=ODCategory_upload.csv");
        header("Content-Type: application/octet-stream; ");
        header("Content-Transfer-Encoding: binary");

        echo $file;
    }

    public function update(){

        $data = $this->input->post();

        if($data['category_ids'] && $data['product_ids']){
            $data['category_ids'] = explode(',', $data['category_ids']);
            $data['product_ids'] = explode(',', $data['product_ids']);

            $data['message'] = $this->processUpdate( $data );
        }else{
            $data = array();
        }

        $this->load->view('categories/update', $data);
    }

    private function processUpdate($post){
        $this->load->model('categorymanager');
        foreach($post['category_ids'] as $cid){
            if($post['action'] == 'add')
                $this->categorymanager->addProductsToCategory( $cid, $post['product_ids'] );
            elseif($post['action'] == 'remove')
                $this->categorymanager->removeProductsFromCategory( $cid, $post['product_ids'] );
        }
    }

}
