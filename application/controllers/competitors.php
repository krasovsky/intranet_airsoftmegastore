<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Competitors extends CI_Controller {

    public function old(){
        $this->load->model('competitormanager');
        $data['skus'] = $this->competitormanager->getAllProducts();
        $this->load->view('prices/listProducts', $data);
    }

    public function index(){
        $postData = $this->input->post();
        $this->viewAll($postData);
    }

    public function viewAll($optionsArr = array()){
        $this->load->model('competitormanager');
        $this->load->model('ams');

        $page = $this->uri->segment(3)?:1;
        $itemsPerPage = 1000 ;
        $pages = $this->ams->getPagination($itemsPerPage);

          $data = $this->competitormanager->getAllProducts($page,$itemsPerPage,$optionsArr);
//        $data = $this->competitormanager->getAllProducts($page,$itemsPerPage);

        foreach($data as $key=>$row){
            $data[$key] = $this->getLowest($row);
        }

        $dataList = $this->competitormanager->generateDataList();

        $this->load->view('prices/listProducts', array('skus'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, "postData" => $optionsArr , "dataList" => $dataList));


    }

    private function getLowest($row){
        $min = array();

        //if there is inventory then they can be a part of the competition
        if(@$row->inventory>0)
            $min['price'] = $row->price;
        if(@$row->airrattle_inventory>0)
            $min['airrattle'] = $row->airrattle;
        if(@$row->evike_inventory>0)
            $min['evike'] = $row->evike;
//        if(@$row->airsplat_inventory>0)
//            $min['airsplat'] = $row->airsplat;
//        if(@$row->airsoftgi_inventory>0)
//            $min['airsoftgi'] = $row->airsoftgi;

        asort($min);
        $row->lowest = key(array_filter($min));

        return $row;
    }

    private function getProductLowest($row){
        $min = array();

        //if there is inventory then they can be a part of the competition
        if(@$row['ams']->inventory>0)
            $min['price'] = $row['ams']->price;
//        if(@$row['airrattle']->inventory>0)
//            $min['airrattle'] = $row['airrattle']->price;
        if(@$row['evike']->inventory>0)
            $min['evike'] = $row['evike']->price;
//        if(@$row['airsplat']->inventory>0)
//            $min['airsplat'] = $row['airsplat']->price;
        if(@$row['airsoftgi']->inventory>0)
            $min['airsoftgi'] = $row['airsoftgi']->price;

        asort($min);

        return key(array_filter($min));
    }

    public function viewProduct(){

        $this->load->model('competitormanager');
        $this->load->model('products');
        $this->load->model('evike_spider');
        $this->load->model('airsoftgi_spider');
//        $this->load->model('airsplat_spider');
//        $this->load->model('airrattle_spider');
        $this->load->model('ams');
        $id = $this->uri->segment(3);

        $data = array();

        //$data['competitors'] = $this->competitormanager->getCompetitorInfoForProduct($id);

        $data['ams'] =  $this->products->getLatestInventoryAndPrice($id);
        $data['evike'] =  $this->evike_spider->getLatestInventoryAndPrice($id);
        $data['airsoftgi'] =  $this->airsoftgi_spider->getLatestInventoryAndPrice($id);
//        $data['airsplat'] =  $this->airsplat_spider->getLatestInventoryAndPrice($id);
//        $data['airrattle'] =  $this->airrattle_spider->getLatestInventoryAndPrice($id);

        $sku = $this->ams->getSkuFromId($id);

        $data['urls'] = $this->competitormanager->getUrls($sku);
        $data['itemId'] = $this->competitormanager->getItemIdFromSku($sku);

        $data['wholesale'] = $this->competitormanager->getWholesalePriceById($id);
        $data['gross'] = ($data['ams']->price)?$this->competitormanager->getGrossMarginPercentage($data['wholesale'], $data['ams']->price):0;
        $data['fourtyPercent'] = $this->competitormanager->getFourtyPercent($data['wholesale']);
        $data['lowestPossible'] = $this->competitormanager->getLowestPossible($data['wholesale']);
        $data['lowest'] = $this->getProductLowest($data);

        $data['sku'] = $sku;

        $this->load->view('prices/product', $data);
    }

    public function newViewProduct(){

        $this->load->model('competitormanager');
        $sku = $this->uri->segment(3);


        $data['ams'] = $this->competitormanager->getCompetitorPrices($sku,'ams');
        $data['evike'] = $this->competitormanager->getCompetitorPrices($sku,'evike');
        $data['airsoftgi'] = $this->competitormanager->getCompetitorPrices($sku,'airsoftgi');
        $data['airsplat'] = $this->competitormanager->getCompetitorPrices($sku,'airsplat');
        $data['hobbytron'] = $this->competitormanager->getCompetitorPrices($sku,'hobbytron');
//        $data['hitguns'] = $this->competitormanager->getCompetitorPrices($sku,'hitguns');
//        $data['airrattle'] = $this->competitormanager->getCompetitorPrices($sku,'airrattle');

        asort($data);
        $data['lowest']= key(array_filter($data));

        $data['urls'] = $this->competitormanager->getUrls($sku);
        $data['itemId'] = $this->competitormanager->getItemIdFromSku($sku);

        $data['wholesale'] = $this->competitormanager->getWholesalePrice($sku);
        $data['gross'] = ($data['ams'])?$this->competitormanager->getGrossMarginPercentage($data['wholesale'], $data['ams']):0;
        $data['inventory'] = $this->competitormanager->getInventory($sku);
        $data['fourtyPercent'] = $this->competitormanager->getFourtyPercent($data['wholesale']);
        $data['lowestPossible'] = $this->competitormanager->getLowestPossible($data['wholesale']);

        $this->load->view('prices/product', $data);
    }

    public function updatePrices(){
        $this->load->helper('url');
        $this->update();
        redirect('/competitors/viewProduct/'. $this->uri->segment(3));
    }

    public function update(){
        $this->load->model('competitormanager');
        $this->load->model('ams');

        //get all brand ids

        $this->load->model('evike_spider');

        $sku = $this->ams->getSkuFromId($this->uri->segment(3)) ?: $this->competitormanager->getNextSku();
        $this->competitormanager->updatePrices($sku);//model update
        $this->load->view('prices/update_bot');
    }

    public function saveUrls(){
        $this->load->model('competitormanager');
        $this->load->model('ams');

        $this->load->model('evike_spider');
        $this->load->model('airrattle_spider');
        $this->load->model('airsoftgi_spider');
        $this->load->model('airsplat_spider');

        $this->competitormanager->saveUrls($this->input->post());

        //update the xrefs
        $this->evike_spider->updateXref();
        $this->airsoftgi_spider->updateXref();
        $this->airsplat_spider->updateProductXref();
        $this->airrattle_spider->updateXref();


        $sku = $this->input->post('sku');
        $id = $this->ams->getIdFromSku($sku);

        $this->competitormanager->updateProductSummary($id);

        $this->load->helper('url');
        redirect("/competitors/viewproduct/$id");
    }

    public function remove(){
        $this->load->model('competitormanager');
        $sku = $this->uri->segment(3);

        if($sku){
            $this->competitormanager->removeSku($sku);
        }
        $this->load->helper('url');
        redirect('/competitors/');
    }

    private function getCartQuantity($body){
        $this->page = htmlqp($body);
        //if($quantity = $this->page->find('.cartlistqty input[type=text]')->attr('value')){
        if($quantity = $this->page->find('input[name^=cart_quantity]')->attr('value')){

            $outofstock = $this->page->is('.markProductOutOfStock');

            if(!$outofstock){
                //instock
                return false;
            }else{
                //out of stock message
                return true;
            }
        }
    }

    public function guessNumber($guess){
        $num = 45;
        if($guess<$num){
            //instock
            return false;
        }else{
            //out of stock message
            return true;
        }
    }

    public function getInventory(){
        $obj = new stdClass();
        $obj->competitor = 'airsoftgi';
        $obj->cookie = $this->getCookie($obj);
        $obj->product_id = $this->uri->segment(3);
        $this->addtocart($obj);

        $min = 0;
        $max = 1000;
        $lowest = 0;

        $guess = $max; //start by guessing max, should show failure and begin the matching process

        for($i=1; $i<20; ++$i){ //do ten iterations

            if($this->getCartQuantity($this->updateCart($obj,$guess))){ //outofstock too high of a number
                $max = $guess;
            }else{ //instock too low of a number
                $min = $guess;
            }
            $guess = ceil(($max-$min)/2)+$min;
            echo "Guess $i max = $max min = $min: ". $guess . "</br>";

            if($min + 1 == $guess){
                break;
            }
        }

        echo "Final Guess: " . $guess . "</br>";

        $this->removeItemFromCart($obj);

        echo $guess;
    }


    public function removeItemFromCart($obj){

        $postDataString = "cart_delete[]={$obj->product_id}&products_id[]={$obj->product_id}";

        $ch = curl_init("https://www.{$obj->competitor}.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.{$obj->competitor}.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function updateCart($obj, $quantity){

        $postDataString = "cart_quantity[]=$quantity&products_id[]={$obj->product_id}";

        $ch = curl_init("https://www.{$obj->competitor}.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.{$obj->competitor}.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function addtocart($obj){

        $postDataString = "products_id={$obj->product_id}";

        $ch = curl_init("http://www.{$obj->competitor}.com/product_info.php?products_id={$obj->product_id}&action=add_product");
        curl_setopt($ch, CURLOPT_REFERER, "https://www.{$obj->competitor}.com/products/{$obj->product_id}");

        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function getCookie($obj){

        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\{$obj->competitor}.txt" ;

        if(file_exists($file)){
            return $file;
        }else{

            file_put_contents($file,'');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"http://www.{$obj->competitor}.com/");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

            ob_start();      // prevent any output
            curl_exec ($ch); // execute the curl command
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            return $file;
        }
    }

    public function updateIndex(){
        $this->load->model('competitormanager');
        $this->competitormanager->updateSummary();
    }

    public function getDownload(){
        $this->load->model('competitormanager');
        $this->load->library('ParseCSV');

        $ids = $this->input->post('product_ids');

        if($ids!=''){
            $categories = $this->competitormanager->getAllProducts($ids);
        }else{
            $categories = $this->competitormanager->getAllProducts(1,100000);
        }

        $this->parsecsv->enclosure='"';
        $file = $this->parsecsv->unparse($categories,array('product_id','sku','price','inventory','evike_inventory','evike','airsoftgi_inventory','airsoftgi'),false);

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=ODCategory_upload.csv");
        header("Content-Type: application/octet-stream; ");
        header("Content-Transfer-Encoding: binary");

        echo $file;
    }
}
