<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Couchdb extends CI_Controller {

    public function replicate(){
        $this->load->model('couch');
        $this->couch->replicate();
    }
    public function addProduct2(){
        $this->load->model('couch');

        $products = $this->couch->getAllProducts2();

        foreach($products as $p){
            $this->couch->insertDocument($p);
            //var_dump($p);
        }
    }
    public function addProduct(){
        $this->load->model('couch');

        $products = $this->couch->getAllProducts();

        foreach($products as $p){
            $p['categories'] = explode(',',$p['categories']);
            foreach($p['categories'] as $key=>$val){
                $p['categories'][$key]=(int)$val;
            }
            $p['img'] = explode('|',$p['img']);

            foreach($p as $key=>$val){
                if(in_array($key,array('product_id','merchandisable','site_channelAdv','site_AMS','site_AMS_mobile','site_AMS_call','on_sale'))){
                    $p[$key] = (int)$val;
                }
            }

            foreach($p as $key=>$val){
                if(in_array($key,array('height','wholesale','retail','width','weight'))){
                    $p[$key] = round((float)$val,2);
                }
            }
            //$this->couch->insertDocument($p);
            var_dump($p);
        }

       // echo implode(array_keys($products[0]),', ');
    }

}
