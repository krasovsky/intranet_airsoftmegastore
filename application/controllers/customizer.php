<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$generation_1_0 = array(1844,1906,1933,1988,2855,4067,5124,7664,7988,8064,8118,8272,8273,8276,8724,
			8737,9263,9265,9377,9379,10107,10108,10430,10763,10936,11074,11076,11370,11486,
			11507,11508,11510);

$generation_2_0 = array(1665,1841,1654,1662,1881,4071,4695,8153,10255,10413,10736,11078,11087,11038,11075,11468,11578,11237);

class Customizer extends CI_Controller {

	const OPTION_GENERATION_NAME = "ogn";
	const OPTION_GENERATION_BUILDS_ARR = "ogba";

	const RELEASE_FORMAT_VALUE = "m-d-y-H-i"; //because stupid trace and iamgemagic doesnt see folders with spaces!!!!;

	private $options = [];

	public function __construct(){
		parent::__construct();
		$this->load->library("gunbuilder/gunbuilder");
		$this->load->library("customizer/configurator/master");
		$this->load->library("customizer/configurator/configurator");

		//
		//core bug issues:
		//9676
		//10701
		//11359 (edited)
		//
		//amsmatt [1:25 PM]
		//not in sandbox db:
		//11676
		//11682
		//11832
		//11881
		//12304

		$this->generation_2_0 = array(1665,1841,1654,1662,1881,4071,4695,8153,10255,10413,10736,11078,11087,11038,11075,11468,11578,11237);

		$this->options = [
			self::OPTION_GENERATION_NAME => "GENERATION_2",
			self::OPTION_GENERATION_BUILDS_ARR => $this->generation_2_0,
		];
	}
    public function index(){

		$mstr = new Master($this->options[self::OPTION_GENERATION_NAME]);

		/**
		 * need to create configFile first! and only once!.
		 */
		//$mstr->createGenerationConfigFile($this->options[self::OPTION_GENERATION_BUILDS_ARR]);

		//Master::createLiveBuildsFile(["GENERATION_1"]);

		/**
		 * will release only current generation
		 */
		$releaseOptions = array(
		    Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
		    //Master::OPTION_BUILD_PARTS3_RELEASE => true,
		    //MASTER::OPTION_PREVIOUS_RELEASE_STRING => "Oct-12",
		    Master::OPTION_BUILD_FULL_RELEASE => true
		    );
		$mstr->release("Oct-12",$releaseOptions);
    }

    public function rebuildGeneration(){
    	$generation = $this->uri->segment(3);
    	$currentRelease = date(self::RELEASE_FORMAT_VALUE);

    	$mstr = new Master($generation);
    	$releaseOptions = array(
    		Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
    		Master::OPTION_BUILD_FULL_RELEASE => true,
    		Master::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE => true,
    		);

    	$mstr->release($currentRelease,$releaseOptions);
    }

    /**
     * will rebuild selected builds for current generation
     */
    public function rebuildOnly(){
    	$mstr = new Master("12-30-2015",$this->db); // current generation_2
    	$currentRelease = "12-101-15-12-00";

    	$releaseOptions = array(
    		Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
    		Master::OPTION_BUILD_ARRAY_ONLY => array($this->uri->segment(3)),
    		//Master::OPTION_BUILD_PARTS3_RELEASE => true,
    		//Master::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE => true,
    		);

    	$mstr->release($currentRelease,$releaseOptions);

    }

	public function build2Generation(){
		//$currentRelease = date(self::RELEASE_FORMAT_VALUE);

		$mstr = new Master("GENERATION_2");

		//$mstr->createGenerationConfigFile($this->options[self::OPTION_GENERATION_BUILDS_ARR]);

		$releaseOptions = array(
			Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
			//Master::OPTION_BUILD_PARTS3_RELEASE => true,
			Master::OPTION_BUILD_FULL_RELEASE => true,
			Master::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE => true,
		);
		$currentRelease = "10-13-15-21-36";
		$mstr->release($currentRelease,$releaseOptions);

		//Master::createLiveBuildsFile(["GENERATION_1","GENERATION_2"]);
	}

	public function buildTodaysGeneration(){
		$currentRelease = date(self::RELEASE_FORMAT_VALUE);

		$mstr = new Master("12-30-2015",$this->db);

		//$mstr->createGenerationConfigFile(array(10435,10429,10428,6601,11376,11399));
	
		$releaseOptions = array(
			Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
			//Master::OPTION_BUILD_PARTS3_RELEASE => true,
			Master::OPTION_BUILD_FULL_RELEASE => true,
			Master::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE => true,
		);
		$currentRelease = "12-40-15-12-00";
		$mstr->release($currentRelease,$releaseOptions);

	    //Master::createLiveBuildsFile(["1-14-2016"]);
	}

    public function checkUpdates(){
    	//$count = $this->uri->segment(3);

    	$currentRelease = date(self::RELEASE_FORMAT_VALUE);

    	$mstr = new Master("GENERATION_1");
    	$mstr2 = new Master("GENERATION_2");

    	$releaseOptions = array(
		    //Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
		    Master::OPTION_BUILD_PARTS3_RELEASE => true,
		    Master::OPTION_PREVIOUS_RELEASE_STRING => $mstr->lastReleaseDate,
		    //Master::OPTION_BUILD_FULL_RELEASE => true
		    );
    	$mstr->release($currentRelease,$releaseOptions);
    	$mstr2->release($currentRelease,$releaseOptions);
    }

    public function updateAttribute(){
    	$currentRelease = date(self::RELEASE_FORMAT_VALUE);

    	$mstr = new Master("GENERATION_1");

    	$releaseOptions = array(
			Master::OPTION_UPDATE_ATTRIBUTE_COLUMN => "hidden",
		    );
    	$mstr->release($currentRelease,$releaseOptions);
    }

    public function compressAllImages(){
    	Master::compressAllImages();
    }


    public function gunbuilderBuild(){ 
		$mstr = new Master("GUNBUILDER2",$this->db);
		//$mstr->createGenerationConfigFile(array("zvd-arms-rpk","zvd-arms-ak","zvd-arms-spetsnaz"));

		$releaseOptions = array(
			//Master::OPTION_CREATE_BUILDS_FROM_CATEGORIES => true,
			Master::OPTION_BUILD_PARTS3_RELEASE => true,
			//Master::OPTION_BUILD_FULL_RELEASE => true,
			//Master::OPTION_BUILD_ARRAY_ONLY => array("zvd-arms-spetsnaz"),
			//MASTER::OPTION_DONT_RESET_CONFIG_FILE => true,
			Master::OPTION_IS_GUNBUILDER => true,
			Master::OPTION_PREVIOUS_RELEASE_STRING => $mstr->lastReleaseDate,
		);
		$currentRelease = "11-13-15-09-00";
		$mstr->release($currentRelease,$releaseOptions);

		//Master::createLiveBuildsFile(["GENERATION_1","GENERATION_2"]);
    }
}

