<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller {

    public function index(){
        $this->load->view('data/default');
    }

    public function discount(){

        $this->load->model('datamanager');

        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));


        $data = array();
        $data['without'] = $this->datamanager->getDiscountedOrdersWithoutProfit($start,$end);

        $data['without_total'] = 0.0;
        foreach($data['without'] as $d){
            $data['without_total']+=$d['profit'];
        }

        $data['with'] = $this->datamanager->getDiscountedOrdersWithProfit($start,$end);

        $data['with_total'] = 0.0;
        foreach($data['with'] as $d){
            $data['with_total']+=$d['profit'];
        }

        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);


        $this->load->view('data/list', $data);
    }

    public function orders(){
        $this->load->model('datamanager');

        if(! empty($_POST["start"]) && ! empty($_POST["end"])){
            $startTime = $_POST["start"];
            $endTime = $_POST["end"];
        }
        else{
            $startTime = $this->uri->segment(3);
            $endTime = $this->uri->segment(4);
        }

        if(empty($startTime) || empty($endTime)){
            $this->load->view('data/default');
            return;
        }

        $filter = @$_GET["filter"];
        $options = array();
        if(! empty($filter)) $options["filter"] = $filter;

        $start = date('Y-m-d', strtotime(str_replace('-', '/', $startTime)));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $endTime)));

        $data = array();
        $data['without'] = $this->datamanager->getOrdersWithoutProfit($start,$end,$options);
        $data['total_shipping']=0.0;
        $data['freebie_shipping']=0.0;
        $data["total_shipping_revenue"]=0.0;
        $data['total_freebies']=0;
        $data['without_total'] = 0.0;
        foreach($data['without'] as $d){
            $data['without_total']+=$d['profit'];
            $data['total_shipping']+=$d["items"]['shipping_price'];
            $data['total_shipping_revenue']+= ($d["items"][0]->ship_charged - $d["items"]['shipping_price']);
            if(array_key_exists('freebie_shipping',$d)){
                $data['total_freebies']++;
                @$data['freebie_cost']+=$d['freebie_cost'];
                @$data['freebie_shipping']+=$d['freebie_shipping'];
            }
        }

        $data['with'] = $this->datamanager->getOrdersWithProfit($start,$end,$options);

        $data['with_total'] = 0.0;
        foreach($data['with'] as $d){
            $data['with_total']+=$d['profit'];
            $data['total_shipping']+=$d["items"]['shipping_price'];
            $data['total_shipping_revenue']+= ($d["items"][0]->ship_charged - $d["items"]['shipping_price']);
            if(array_key_exists('freebie_shipping',$d)){
                $data['total_freebies']++;
                @$data['freebie_cost']+=$d['freebie_cost'];
                @$data['freebie_shipping']+=$d['freebie_shipping'];
            }
        }

        if(empty($data['freebie_shipping'])) $data['freebie_shipping'] = 0;
        if(empty($data['freebie_cost'])) $data['freebie_cost'] = 0;
        $data['start'] = $start;
        $data['end'] = $end;
        $data['show_with'] = $this->uri->segment(5)?:false;
        $data["filters"] = $options;

        // dump($data);
        $this->load->view('data/list', $data);
    }

    public function sku(){

        $this->load->model('datamanager');

        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));
        $sku = $this->uri->segment(5);

        $data  = array();
        $data['items'] = $this->datamanager->getOrdersBySku($sku,$start,$end);
        //dump($data);
        //$data['total_quantity'] = count($data['items']);

        foreach($data['items'] as $i){
            @$data['total_quantity'] += $i->quantity - $i->returned;
            @$data['total_revenue'] += $i->revenue;
            @$data['total_shipping'] += $i->shipping;
            @$data['total_profit'] += $i->profit;
        }

        $data['sku'] = $sku;
        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);
        //dump($data);
        $this->load->view('data/sku', $data);
    }


    public function autopsy(){

        $this->load->model('datamanager');
        if(! empty($_POST["orderId"])){
            $id = $_POST["orderId"];

            if(! is_numeric($id)){
                $this->load->view('data/default',["errors" => ["orderId" => "Order should be numeric"]]);
                return;
            }
        }
        else {
            $id = $this->uri->segment(5);
        }

        if(empty($id)){
             $this->load->view('data/default');
             return;
        }
        

        $data  = $this->datamanager->getOrdersByOrderId($id);
//        $data['items'] = $this->datamanager->getOrdersBySku($sku,$start,$end);
//
//        $data['total_quantity'] = count($data['items']);
//
//        foreach($data['items'] as $i){
//            @$data['total_revenue'] += $i->revenue;
//        }
//        foreach($data['items'] as $i){
//            @$data['total_shipping'] += $i->shipping;
//        }
//        foreach($data['items'] as $i){
//            @$data['total_profit'] += $i->profit;
//        }
//
        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);
        $data['id'] = $id;
        $data['order'] = $data[$id];

        $data['shipping_rate'] = $data['order']["items"]['shipping_price']/$data['order']['total_weight'];
        //dump($data);
        $this->load->view('data/autopsy', $data);
    }

    public function setSkuShipping(){

        $this->load->model('datamanager');

        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));

        $data = array();
        $skus = $this->datamanager->getAllSkusOrdered($start,$end);

        foreach($skus as $row){
            $orders = $this->datamanager->getOrdersBySku($row->sku,$start,$end);
            $shipping = 0.0;
            $quantity = 0;
            foreach($orders as $o){
                $shipping += $o->shipping;
                $quantity += $o->quantity;
            }
            $shipping = $quantity ? round($shipping/$quantity,2):0;
            echo "editing ". $row->sku;
            $this->datamanager->setSkuShipping($row->sku, $shipping , $quantity);
        }
    }

    public function skuList($start,$end){

        $this->load->model('datamanager');


        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));


        $data = array();
        $skus = $this->datamanager->getAllSkusOrdered($start,$end);

        foreach($skus as $row){
            $data['skus'][] = $this->datamanager->getOrdersBySku($row->sku,$start,$end);
        }
//
//        $data['total_shipping']=0.0;
//        $data['freebie_shipping']=0.0;
//        $data['total_freebies']=0;
//
//        $data['skus_total'] = 0.0;
//        foreach($data['skus'] as $d){
//            $data['skus_total']+=$d['profit'];
//            $data['total_shipping']+=$d['shipping'];
//        }

        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);

        $this->load->view('data/list', $data);
    }

    public function user(){
        $this->load->model('usermanager');
        $this->load->model('datamanager');

        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));
        $email = urldecode($this->uri->segment(5));

        $data['orders']  = $this->datamanager->getCustomerOrders($email,$start,$end);
        $data['user']  = $this->usermanager->getCustomerInfo($email);
        $data['order_total']=0;
        $data['total_shipping']=0;

        foreach($data['orders'] as $d){
            $data['order_total']+=$d['profit'];
            $data['total_shipping']+=$d["items"]['shipping_price'];
            if(array_key_exists('freebie_shipping',$d)){
                $data['total_freebies']++;
                @$data['freebie_cost']+=$d['freebie_cost'];
                @$data['freebie_shipping']+=$d['freebie_shipping'];
            }
        }

        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);
        $data['email'] = $email;
        if(empty($data['freebie_shipping'])) $data['freebie_shipping'] = 0;
        if(empty($data['freebie_cost'])) $data['freebie_cost'] = 0;
 
        $this->load->view('user/orders', $data);
    }

    public function userList(){
        $this->load->model('usermanager');
        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));

        $data['users'] = $this->usermanager->getCustomers($start,$end);
        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);

        $this->load->view('user/list', $data);
    }

    public function state(){
        $this->load->model('usermanager');
        $start = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(3))));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $this->uri->segment(4))));

        $data['users'] = $this->usermanager->getCustomersByState($this->uri->segment(5),$start,$end);
        $data['start'] = $this->uri->segment(3);
        $data['end'] = $this->uri->segment(4);

        $this->load->view('user/list', $data);
    }
}
