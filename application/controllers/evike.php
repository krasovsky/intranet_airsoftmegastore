<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evike extends CI_Controller {

    public function index(){
        $this->products();
    }

    public function products()
    {
        $this->load->model('evike_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->evike_spider->getProducts($page,$itemsPerPage);

        $pages = $this->evike_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products'));
    }


    public function product(){

        $this->load->model('evike_spider');
        $this->load->model('ams');
        $this->load->helper('rollup_inventory');
        $this->load->helper('rollup_prices');

        $id = $this->uri->segment(3);

        $product = $this->evike_spider->getProduct($id);
        $ams_product = ($product->ams_id)? $this->ams->getProductCompare($product->ams_id) : array() ;

        //get prices
        $prices = $this->evike_spider->getYearOfProductPrices($id);
//
//        $rollup_prices['year'] = rollup_prices_year($prices);
//        $rollup_prices['month'] = rollup_prices_month($prices);
//        $rollup_prices['week'] = rollup_prices_week($prices);
//        $rollup_prices['yesterday'] = rollup_prices_yesterday($prices);

        //get inventory
        $inventory = $this->evike_spider->getYearOfProductInventory($id);

//        $rollup_inventory['year'] = rollup_inventory_year($inventory);
//        $rollup_inventory['month'] = rollup_inventory_month($inventory);
//        $rollup_inventory['week'] = rollup_inventory_week($inventory);
//        $rollup_inventory['yesterday'] = rollup_inventory_yesterday($inventory);

        $this->load->view('evike/product', array('product'=>$product,'prices'=>$prices, 'inventory'=>$inventory, 'ams_product'=>$ams_product));
    }

    public function outofstock()
    {
        $this->load->model('evike_spider');
        $this->load->helper('rollup_inventory');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->evike_spider->getOutOfStock($page,$itemsPerPage);

        $pages = $this->evike_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Out of Stock'));
    }

    public function discontinued()
    {
        $this->load->model('evike_spider');

        $itemsPerPage = $this->uri->segment(4)?:1000;
        $page = $this->uri->segment(3)?:1;
        $data = $this->evike_spider->getDiscontinued($page,$itemsPerPage);

        $pages = $this->evike_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Discontinued'));
    }

    public function exclusive(){
        $this->load->model('evike_spider');

        $itemsPerPage = $this->uri->segment(4)?:400;
        $page = $this->uri->segment(3)?:1;
        $data = $this->evike_spider->getExclusiveCompetitorProducts($page,$itemsPerPage);
        $brands = $this->evike_spider->getCommonBrands();

        $pages = $this->evike_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_exclusive', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products We Do Not Have'));
    }

    public function identical(){
        $this->load->model('evike_spider');

        $itemsPerPage = $this->uri->segment(4)?:400;
        $page = $this->uri->segment(3)?:1;
        $data = $this->evike_spider->getIdenticalCompetitorProducts($page,$itemsPerPage);
        $brands = $this->evike_spider->getCommonBrands();

        $pages = $this->evike_spider->getPagination($itemsPerPage);

        $this->load->view('evike/product_identical', array('result'=>$data, 'brands'=>$brands, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, 'h1'=>'Evike Products We Have'));
    }


    public function exclusiveBrands(){
        $this->load->model('evike_spider');
        $this->load->model('ams');

        $brands = $this->ams->getAllBrands();
        $data = $this->evike_spider->getExclusiveCompetitorBrands();
        $this->load->view('evike/brand_xref', array('result'=>$data, 'brands'=>$brands, 'h1'=>'Evike Brands We Do Not Have'));
    }

    public function updateBrand(){
        $this->load->model('evike_spider');
        if($this->input->post('id'))
            $this->evike_spider->updateBrandXref($this->input->post('id'), $this->input->post('ams_id'));
        return true;
    }

    public function brands(){
        $this->load->model('evike_spider');

        $data = $this->evike_spider->getAllBrandTotals();
        $this->load->view('evike/brand_all', array('result'=>$data, 'h1'=>'Evike All Brand Stats'));
    }

    public function topBrands(){
        $this->load->model('evike_spider');

        $data = $this->evike_spider->getTopBrands();
        $this->load->view('evike/brand_top', array('result'=>$data, 'h1'=>'Evike Top Brands'));
    }

    public function brand()
    {
        $this->load->model('evike_spider');
        $brand_id = $this->uri->segment(3);

        $data = array();
        $result = $this->evike_spider->getProductsFromBrandId($brand_id);
        $totals = $this->evike_spider->getBrandTotals($brand_id);
        $this->load->view('evike/brand_list', array('result'=>$result, 'brand_id'=>$brand_id, 'totals'=>$totals, 'h1'=>'Evike\'s '.$result[0]->brand.' Product line'));
    }

    public function parse(){
        $this->load->model('ams');
        $id = $this->uri->segment(3);

        $data = $this->ams->parse($id);
    }

    public function allInventory(){
        $this->load->model('evikemanager');
        $data = array();
        $data['products'] = $this->evikemanager->getAllProducts();

        foreach($data['products'] as $p){
            $p->inventory = ( $p->inventory == 100 ) ? -2 : $p->inventory;
        }

        $this->load->view('inventory/list', $data );
    }

    //command line function only
    public function crawl(){
        set_time_limit(1800);

        $this->load->model('evike_spider');

        $count = 0;
        while(($id = $this->evike_spider->getNextIdToSpider($count++)) != '' ){
            $this->evike_spider->getEverythingInBrand($id);
        }
        exit(0);
    }

    public function updateXref(){
        $this->load->model('evike_spider');
        $this->evike_spider->updateXref();
        exit(0);
    }

    //command line function only
    public function getMassInventory(){
        set_time_limit(1800);

        $start = microtime(true);
        $this->load->model('evike_spider');
        $this->evike_spider->deleteCookie();

        $ids = $this->evike_spider->getNextProductIdMulti(300);

        $stock = $this->evike_spider->getInventoryMulti($ids);

        foreach($stock as $id=>$values){
            $this->evike_spider->setInventory($id, $values['quantity']);
        }

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        exit(0);
    }


    public function brandProducts(){
        $this->load->model('evike_spider');
        $this->load->model('ams');
        $evike_brand_id = $this->uri->segment(3);

        $ams_brand_id = $this->evike_spider->getAMSBrand($evike_brand_id);

        $products = $this->ams->getAllProductsInBrandId($ams_brand_id);
        $this->load->view('evike/product_dropdown', array('products'=>$products));
    }

}
