<?php


class Fishbowl extends CI_Controller {

    public function generateInventoryChangeCsv(){
        $this->load->model('fishbowlmodel');

        var_dump("pulling data from Datebase.");
        $data = $this->fishbowlmodel->getInventoryChangedData();
        if (empty($data)){
            var_dump("Nothing to update! Exiting. ".date('c'));
            return;
        }

        var_dump("generating csv File.");
        $this->fishbowlmodel->createCsvFileFromData($data);

        var_dump("updating fishbowl inventory column in datebase.");
        $this->fishbowlmodel->updateInventoryValues($data);

        var_dump("complete. ".date("c"));
        var_dump("file saved in : ". APPPATH ."assets/data/fishbowlData/Dropbox");

    }
}