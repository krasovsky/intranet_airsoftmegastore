<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

    public function index()
    {
        $this->load->model('csv');
        $this->csv->processAll();
    }

    public function clean(){
        $this->load->model('csv');
        $this->csv->cleanAllProcessed();
    }
}
