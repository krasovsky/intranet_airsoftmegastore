<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends CI_Controller {

    public function evike(){
        $start = microtime(true);

        $this->load->model('evike_spider');

        $count = $this->uri->segment(3);
        $id = $this->evike_spider->getNextProductId($count);

        if(!isset($id)) exit(); //loop is over

        $stock = $this->evike_spider->getInventory($id);

        $this->evike_spider->setInventory($id, $stock);

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        $this->load->view('competitor_spider',array('next' =>'/index.php/inventory/evike/'.($count+1)));

    }

    public function evike_multi(){
        $start = microtime(true);

        $this->load->model('evike_spider');

        $this->evike_spider->deleteCookie();

        $count = $this->uri->segment(3);
        $ids = $this->evike_spider->getNextProductIdMulti(1,300);

        if(!count($ids)) exit(); //loop is over

        $stock = $this->evike_spider->getInventoryMulti($ids);

        foreach($stock as $id=>$values){
            $this->evike_spider->setInventory($id, $values['quantity']);
        }

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        $this->load->view('competitor_spider',array('next' =>'/index.php/inventory/evike_multi/'.($count+1)));

    }

    public function airsoftgi_multi(){
        $start = microtime(true);

        $this->load->model('airsoftgi_spider');

        $count = $this->uri->segment(3);
        $ids = $this->airsoftgi_spider->getNextProductIdMulti(1,300);

        if(!count($ids)) exit(); //loop is over

        $stock = $this->airsoftgi_spider->getInventoryMulti($ids);

        foreach($stock as $id=>$values){
            $this->airsoftgi_spider->setInventory($id, $values['quantity']);
            $this->airsoftgi_spider->setDailyInventory($id, $values['quantity']);
        }

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        $this->load->view('competitor_spider',array('next' =>'/index.php/inventory/airsoftgi_multi/'.($count+1)));

    }


    public function airsoftgi(){

        $start = microtime(true);

        $this->load->model('airsoftgi_spider');

        $this->airsoftgi_spider->deleteCookie();

        $count = $this->uri->segment(3);
        $id = $this->airsoftgi_spider->getNextProductId($count);

        if(!isset($id)) exit(); //loop is over

        $stock = $this->airsoftgi_spider->getInventory($id);

        $this->airsoftgi_spider->setInventory($id, $stock);

        $finish = microtime(true);
        echo 'total time = '. ($finish - $start) . 'ms';

        $this->load->view('competitor_spider',array('next' =>'/index.php/inventory/airsoftgi/'.($count+1)));

    }

    public function yesterday(){
        $company = $this->uri->segment(3);
        if($company == 'airsoftgi')
            $this->load->model('airsoftgi_spider', 'yesterday');
        elseif($company=='evike')
            $this->load->model('evike_spider', 'yesterday');
        else
            exit(0);

        $this->yesterday->getYesterdaysInventory();



    }
}
