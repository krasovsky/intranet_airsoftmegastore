<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Json extends CI_Controller {

    public function index(){

    }
    public function item(){
        $this->load->model('products');

        $id = $this->uri->segment(3);

        $data = $this->products->getProduct($id);

        echo json_encode($data);

    }
    public function items(){}
}
