<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Controller {

    public function index()
    {
        $this->load->model('mailmanager');
        $this->mailmanager->getMessage();
    }
}
