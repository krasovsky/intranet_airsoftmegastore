<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH .'libraries/nusoap/nusoap.php');

class Merchant extends CI_Controller
{
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->process();
        $this->upload();
    }

    public function process()
    {
        $this->load->model('merchantmanager');

        echo "<h3>Processing Google Merchant Index</h3>";

        $this->merchantmanager->makeCSV();
    }

    public function upload(){
        $this->load->library('ftp');

        $config['hostname'] = 'ftp.airsoftmegastore.net';
        $config['username'] = 'aaronams';
        $config['password'] = '2abstrakt';
        $config['port']     = 21;
        $config['passive']  = TRUE;
        $config['debug']    = TRUE;

        $this->ftp->connect($config);

        echo "<h3>Uploading Google Merchant Index</h3>";
        $timestamp = new DateTime();
        if($this->ftp->upload(APPPATH."assets/data/googleMerchant.csv", '/catalog.airsoftmegastore.net/application/assets/data/googleMerchant.csv', 'ascii', 0775)){
            $uploaded = new DateTime();
            $diff = $uploaded->diff($timestamp);
            echo "Upload completed in ". $diff->format('%I:%S') ;
        }
        $this->ftp->close();

    }

    public function download(){
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="OD-ASM-GOOGLE-FEED.txt"');
        echo file_get_contents(APPPATH."assets/data/googleMerchant.csv");
    }
}
