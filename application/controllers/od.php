<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Od extends CI_Controller {

    public function login(){
        $start = microtime(true);

        $this->load->model('odmanager');

        $this->odmanager->addProduct();

//        $count = $this->uri->segment(3);
//        $id = $this->evike_spider->getNextProductId($count);

//        if(!isset($id)) exit(); //loop is over
//
//        $stock = $this->evike_spider->getInventory($id);
//
//        $this->evike_spider->setInventory($id, $stock);
//
//        $finish = microtime(true);
//        echo 'total time = '. ($finish - $start) . 'ms';
//
//        $this->load->view('competitor_spider',array('next' =>'/index.php/inventory/evike/'.($count+1)));

    }

    public function scraper(){
        $start = microtime(true);

        $this->load->model('odmanager');

        $this->odmanager->addProduct();
    	
    }
    public function getOrders(){
    	$this->load->model('odmanager');

    	// $start = "2014-06-15";
    	// $end = "2014-07-15";
        $start = $this->uri->segment(3);
        $end = $this->uri->segment(4); 
        if(empty($start) && empty($end)){
            $start = date("Y-m-d" , strtotime("10 days ago"));
            $end = date("Y-m-d" , time());
        }
        var_dump($start);
        var_dump($end);

    	$this->odmanager->getOrdersReport($start,$end);

    }

    public function scrapeOrder(){
    	$this->load->model('odmanager');
    	$limit = 40;

    	$ordersArr = $this->odmanager->getOrdersToScrape($limit);
    	//dump($ordersArr);
    	foreach($ordersArr as $order){
            try{
                $this->odmanager->scrapeOrder($order);
            }
            catch(Exception $e){
                echo $e->getMessage();
            }
    		
    		//die(1);
    	}
    }

    public function generateProductRating(){
    	$this->load->model('odmanager');
    	$oldTime = $this->uri->segment(3);
    	if(! empty($oldTime)){
    		$currentTime = strtotime($oldTime);
    	}
    	else{
			$currentTime = time();
    	}
    	
    	$this->odmanager->generateRatings($currentTime);
    }

}
