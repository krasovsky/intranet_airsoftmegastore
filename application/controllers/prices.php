<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prices extends CI_Controller {


    public function evike(){
        $this->load->model('evike_spider');

        $count = $this->uri->segment(3);
        $id = $this->evike_spider->getNextIdToSpider($count);

        if(!isset($id)) exit(); //loop is over

        $url = "http://www.evike.com/brands/$id";

        $this->evike_spider->getAllProductsPricesInBrand($url);

        $this->load->view('competitor_spider',array('next' =>'/index.php/prices/evike/'.($count+1)));

    }

    public function airsoftgi(){
        $this->load->model('airsoftgi_spider');

        $count = $this->uri->segment(3);
        $id = $this->airsoftgi_spider->getNextIdToSpider($count);

        if(!isset($id)) exit(); //loop is over

        $url = "http://www.airsoftgi.com/index.php?manufacturers_id=$id&max_display_search_results=200&listing=side";

        $this->airsoftgi_spider->getAllProductsPricesInBrand($url, $id);

        $this->load->view('competitor_spider',array('next' =>'/index.php/prices/airsoftgi/'.($count+1)));

    }
}
