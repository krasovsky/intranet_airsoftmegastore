<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

    public function index(){
        $this->page();
    }

    public function page()
    {
        $this->load->model('ams');

        $itemsPerPage = $this->uri->segment(4)?:350;
        $page = $this->uri->segment(3)?:1;
        $data = $this->ams->getProducts($page,$itemsPerPage);

        $pages = $this->ams->getPagination($itemsPerPage);

        $this->load->view('product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage));
    }

    public function item(){
        $this->load->model('ams');

        $id = $this->uri->segment(3);

        $data = $this->ams->getItem($id);
        $gridArr = array();
        $gridHeader = array();
        foreach($data as $element){
            //dump($element);
            $gridArr[] = $element["30_days_sales"];
            $gridHeader[] = date('F', strtotime($element["create_date"]));
        }
        $this->load->view('product', array('item'=>$data[0], "gridData" => $gridArr, "gridHeader" => $gridHeader));
    }

    public function parse(){
        $this->load->model('ams');
        $id = $this->uri->segment(3);

        $data = $this->ams->parse($id);
    }

    public function embedPage($item_id, $view){

        $url = "http://www.airsoftmegastore.com/$item_id-spider.aspx";

        $this->page = htmlqp($url);

        $this->page->top();
        $links = $this->page->find('[href]');

        foreach($links as $link){
            $value = $link->attr('href');
            preg_match('/(^javascript)|(^#)|(^\/\/)|(^mailto)|(^http)/',$value,$matches);
            if(!count($matches)){
                if(substr($value,0,1)=='/')
                    $link->attr('href', 'http://www.airsoftmegastore.com'.$value);
                else
                    $link->attr('href', 'http://www.airsoftmegastore.com/'.$value);
            }
        }

        $this->page->top();
        $links = $this->page->find('[src]');

        foreach($links as $link){
            $value = $link->attr('src');
            preg_match('/(^javascript)|(^#)|(^\/\/)|(^mailto)|(^http)/',$value,$matches);
            if(!count($matches)){
                if(substr($value,0,1)=='/')
                    $link->attr('src', 'http://www.airsoftmegastore.com'.$value);
                else
                    $link->attr('src', 'http://www.airsoftmegastore.com/'.$value);
            }
        }

        $this->page->top();
        $container = $this->page->find('[itemprop=description]');

        $container->text('##view##');

        $this->page->top();
        $strPage = $this->page->html();

        $strPage = substr_replace($strPage, '', stripos($strPage, '<!--compatible products module-->'),stripos($strPage, '<!--end compatible products module-->'));
       // $strPage = substr_replace($strPage, '', stripos($strPage, '<!--SIMILAR Products-->'),stripos($strPage, '<!--END SIMILAR Products-->'));



        $replaced =preg_replace('/##view##/',$view, $strPage);

        echo $replaced;
        //$replaced = substr_replace($replaced, '', stripos($replaced, '<!--mini cart-->'),stripos($replaced, '<!--mini cart end-->'));
        //$replaced = substr_replace($replaced, '', stripos($replaced, '<!--jstemplates-->'),stripos($replaced, '<!--end jstemplates-->'));

        //echo $replaced;

    }

    public function showDescription($item_id = 0){

        $item_id = ($item_id)? $item_id : $this->uri->segment(3);
        $compatible = $this->compatible($item_id);
        //var_dump($compatible);
        $this->embedPage($item_id, $compatible);
    }


	public function showCompatible($item_id = 0){
	
        $item_id = ($item_id)? $item_id : $this->uri->segment(3);
		$compatible = $this->compatible($item_id);
		
		//var_dump($compatible);
		$this->embedPage($item_id, $compatible);
	}
	
    public function compatible($item_id=0){

        $this->load->model('ams');

        $item_id = ($item_id)? $item_id : $this->uri->segment(3);

        $compatible = $this->ams->getRandomCompatibleProducts($item_id);
        $topCompatible = $this->ams->getRandomReverseCompatibleProducts($item_id);
        $similar = $this->ams->getSimilarProducts($item_id);
        $categories = $this->ams->getProductCategories($item_id);

        $compatible = array_merge($topCompatible,$compatible);

        $compatible = array_slice($compatible,0,5);
        $item = $this->ams->getProductById($item_id);

        $item->long_description = $this->removeDescriptionLinks($item->long_description);
		//var_dump($compatible);

        return $this->load->view('compatible_lists', array('item'=>$item, 'compatible'=>$compatible, 'similar'=>$similar, 'categories'=>$categories), true);
    }


    public function removeDescriptionLinks($text){
        //find end of the product description
        $startFlag = '<!--product description--> ';
        $endFlag = '<!--end product description-->';

        $desc = str_ireplace($startFlag, '', $text);
        $desc = substr($desc, 0, stripos($desc, $endFlag));
        return $desc;
    }

    public function descriptionsToCSV(){

        $this->load->model('ams');
        $filename = realpath( APPPATH . "\\assets\\data\\csv" ) ."\\descriptions.csv" ;

        $page = $this->uri->segment(3);

        // create a file pointer connected to the output stream
        $file = fopen($filename , 'a');

        $Delimiter = '~';
        $Separator = ',';
        // fetch the data
        $products = $this->ams->getAllProducts($page);
        foreach($products as $product){

            $id = $product->id;
            $compatible = $this->compatible($id);

            if($compatible!==false){
                $line = array($id, preg_replace('/\s+/', ' ', $compatible ));
                fwrite($file, $Delimiter.implode($Delimiter.$Separator.$Delimiter, $line).$Delimiter."\n");
            }
        }

        fclose($file);


        $this->load->view('competitor_spider',array('next' =>'/index.php/products/descriptionsToCSV/'.($page+1)));
    }

    public function descriptionsToDB(){

        $this->load->model('ams');

        $page = $this->uri->segment(3);

        $products = $this->ams->getAllProducts($page);
        foreach($products as $product){

            $id = $product->id;
            $compatible = $this->compatible($id);

            if($compatible!==false){
                $this->ams->setDescription($id, $compatible);
            }
        }

        $this->load->view('competitor_spider',array('next' =>'/index.php/products/descriptionsToDB/'.($page+1)));
    }


    public function descriptionsCSV(){
        $this->load->model('ams');
        $filename = realpath( APPPATH . "\\assets\\data\\csv" ) ."\\descriptions.csv" ;

        $page = $this->uri->segment(3);

        // create a file pointer connected to the output stream
        $file = fopen($filename , 'w');

        $Delimiter = '|';
        $Separator = ',';
        // fetch the data
        $products = $this->ams->getDescriptions($page);
        foreach($products as $product){

            $line = array($product->id, preg_replace('/\s+/', ' ', $product->description ));
            fwrite($file, $Delimiter.implode($Delimiter.$Separator.$Delimiter, $line).$Delimiter."\n");

        }

        fclose($file);
    }

    public function crawl(){
        $this->load->model('ams');

        $this->ams->setAllProductPrices();
        $this->ams->setAllProductInventory();
    }

    public function inStock(){
        $this->load->model('ams');

        $itemsPerPage = $this->uri->segment(4)?:350;
        $page = $this->uri->segment(3)?:1;
        //dump($page);
        //dump($itemsPerPage);
        $data = $this->ams->getProducts($page,$itemsPerPage,array("inStock" => true));

        $pages = $this->ams->getPagination($itemsPerPage);

        $this->load->view('product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, "control" => "inStock"));
    }

    public function onSale(){
        $this->load->model('ams');

        $itemsPerPage = $this->uri->segment(4)?:350;
        $page = $this->uri->segment(3)?:1;
        //dump($page);
        //dump($itemsPerPage);
        $data = $this->ams->getProducts($page,$itemsPerPage,array("onSale" => true));

        $pages = $this->ams->getPagination($itemsPerPage);

        $this->load->view('product_list', array('result'=>$data, 'pages'=>$pages, 'current'=>$page, 'itemsPerPage' => $itemsPerPage, "control" => "inStock"));
    }
}
