<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotions extends CI_Controller {

    public function index($saved = false){
       //promotions/main
       $this->load->model('datamanager');

       $data = $this->datamanager->getAllCoupons();
       $this->load->view('/promotions/main' , array("data" => $data,"saved" => $saved));
    }

    public function coupon(){
    	$this->load->model('datamanager');
        
        $couponId = $this->uri->segment(3);
        $data = "";
        if(! empty($couponId)){
            $data = $this->datamanager->getCouponInfo($couponId);
            //dump($data);
        }

        $this->load->view('/promotions/coupon' , array("data" => $data));
    }

    public function save(){
        $errors = array();
    	if(empty($_POST)){
            $errors["empty"] = true;
    	}

        if(! is_numeric($_POST["discount_value"])){
            $errors["discount_value"] = "Should be numeric";
        }
        else if($_POST["discount_value"] < 0 || $_POST["discount_value"] > 100){
            $errors["discount_value"] = "Cannot be negative or more than 100";
        }

        $production_list = explode(',', trim($_POST["product_list"]));
        foreach($production_list as $prod){
            if(! is_numeric($prod)){
                $errors["product_list"] = "List of products is incorrect!";
            }
        }


        //validation failed
        if(! empty($errors)){
            $this->load->view('/promotions/coupon' , array("errors" => $errors, "data" => $_POST));
            return;
        }


        $this->load->model('datamanager');
        $dataArr = $_POST;
        $dataArr["product_list"] = $production_list;
        $this->datamanager->saveCupon($dataArr);
        //$this->load->view('/promotions/coupon' , array("saved" => true, "data" => $_POST));
        $this->index(true);
    }
}