<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Redirects extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->sitemap = APPPATH.'assets/data/xml/301Redirects2.xml';
    }

    public function index(){
        $this->load->model('seo');
        $this->seo->importXML($this->sitemap);
    }

    public function create(){
        $this->load->model('seo');
        $this->seo->createXML(APPPATH.'assets/data/xml/301Redirects4.xml');
    }

    public function find404(){
        $this->load->model('seo');
        $result = array('result' => $this->seo->find404());
        $this->load->view('refresh',$result);
    }
}
