<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function index(){
        $this->load->view('reports/main');
    }

    public function masterReport(){
        $this->load->model('datamanager');
        $data = $this->datamanager->generateMasterReport();

        $this->load->view("reports/master-report", array("data" => $data));
    }

    public function process(){
    	if(! empty($_POST["start"]) && ! empty($_POST["end"])){
            $startTime = $_POST["start"];
            $endTime = $_POST["end"];
        }
        else{
            $startTime = $this->uri->segment(3);
            $endTime = $this->uri->segment(4);
        }

        if(empty($startTime) || empty($endTime)){
            $this->load->view('data/default');
            return;
        }
        $start = date('Y-m-d', strtotime(str_replace('-', '/', $startTime)));
        $end = date('Y-m-d', strtotime(str_replace('-', '/', $endTime)));

        $this->load->model('datamanager');
        $data = $this->datamanager->getTop20($start,$end);

        $this->load->view('reports/result' , array("top20ByMoney" => $data , "start" => $start, "end" => $end));
    }
}