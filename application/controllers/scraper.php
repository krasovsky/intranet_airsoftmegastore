<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scraper extends CI_Controller {

    public function index(){
        $this->load->model('scraperobot');

        $links = $this->scraperobot->getPage('http://www.airsplat.com', '/' );

        foreach($links as $link){
            $this->scraperobot->saveLink($link, 'airsplat');
        }
    }

    public function page(){
        $this->load->model('scraperobot');

        $url = $this->scraperobot->getLink('airsplat');
        $item = $this->scraperobot->getItemData($url);
        $this->scraperobot->saveItem($item, $url, 'airsplat');

        $links = $this->scraperobot->getLinks('http://www.airsplat.com');

        foreach($links as $link){
            $this->scraperobot->saveLink($link, 'airsplat');
        }

        return $url;
    }


}
