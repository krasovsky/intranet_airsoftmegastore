<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seo extends CI_Controller {

    public function index(){
        //getTopTrends
        $this->load->model('analytics');

        $days = $this->uri->segment(3) ?: 30;

        $dtime = new DateTime(date('Y-m-d', strtotime('-1 days')));
        $start = $dtime->format("Y-m-d H:i:s");

        $dtime = new DateTime(date('Y-m-d', strtotime('-'.$days.' days')));
        $end = $dtime->format("Y-m-d H:i:s");


        $data = $this->analytics->getTopTrends($start,$end);

        $this->load->view('seo/list', array('data'=> $data));
    }

    public function trending(){
        $this->load->model('analytics');

        $product_id = $this->uri->segment(3) ?: 0;
        $days = $this->uri->segment(4) ?: 30;

        $dtime = new DateTime(date('Y-m-d', strtotime('-1 days')));
        $start = $dtime->format("Y-m-d H:i:s");

        $dtime = new DateTime(date('Y-m-d', strtotime('-'.$days.' days')));
        $end = $dtime->format("Y-m-d H:i:s");


        $data = $this->analytics->getProductTrends($product_id,$start,$end);
        $orders = $this->analytics->getOrders($product_id,$start,$end);
        $orders = $this->analytics->ordersToTrendsRemap($orders,$data);

        $product = $this->analytics->getProduct($product_id);

        $this->load->view('seo/trendingProducts', array('data'=> $data, 'product'=>$product, 'orders' => $orders ));
    }

    public function organic(){
        $this->load->model('analytics');
    }
    public function ppc(){}

    public function reset(){
        $this->load->library('session');
        $this->session->set_userdata('day',1);
    }

    public function update(){
        $this->load->library('session');
        $this->topProducts();
    }

    public function organicUpdate(){
        $this->load->model('analytics');

        $day = $this->session->userdata('day') ?: 1;

        $this->analytics->setup(date('Y-m-d', strtotime('-'.$day.' days')),date('Y-m-d', strtotime('-'.$day.' days')));

        if($this->session->userdata('oauth_access_token')){
            $this->session->set_userdata('day',$day+1);
        }
        $data = $this->analytics->getTopOrganicKeywords();

        if(count($data['rows'])){
            $this->analytics->setOrganicKeywords($data);
            $this->load->view('seo/rank', array('data'=> $data ));
        }
    }

    public function ppcUpdate(){
        $this->load->model('analytics');

        $day = $this->session->userdata('day') ?: 1;

        if($this->input->get('day')){
            $day = $this->input->get('day');
        }

        $this->analytics->setup(date('Y-m-d', strtotime('-'.$day.' days')),date('Y-m-d', strtotime('-'.$day.' days')));

        if($this->session->userdata('oauth_access_token')){
            $this->session->set_userdata('day',$day+1);
        }

        $data = $this->analytics->getTopPPCKeywords();

        if(count($data['rows'])){
            $this->analytics->setTopPPCKeywords($data);
            $this->load->view('seo/rank', array('data'=> $data ));
        }

    }

    public function topProducts(){
        $this->load->model('analytics');

        $day = $this->session->userdata('day') ?: 1;

        if($this->input->get('day')){
            $day = $this->input->get('day');
        }

        $this->analytics->setup(date('Y-m-d', strtotime('-'.$day.' days')),date('Y-m-d', strtotime('-'.$day.' days')));

        if($this->session->userdata('oauth_access_token')){
            $this->session->set_userdata('day',$day+1);
        }

        $data = $this->analytics->getTrendingProducts();

        if(count($data['rows'])){
            $this->analytics->setTrendingProducts($data);
            $this->load->view('seo/rank', array('data'=> $data ));
        }

    }

    public function sitemap(){
        require_once(APPPATH.'/libraries/GoogleSitemapper.php');
        $this->load->model('analytics');

        $products = $this->analytics->getProductLinks();
        $categories = $this->analytics->getCategoryLinks();

        $sitemap = new GoogleSitemapper();
        $sitemap->setSiteAddress('http://www.airsoftmegastore.com');

        $sitemap->addUrl('http://www.airsoftmegastore.com', GoogleSitemapper::CHANGEFREQ_DAILY, date('c'), '1');

        foreach($products as $p){

            $node = $sitemap->addUrl($p->url, GoogleSitemapper::CHANGEFREQ_DAILY, date('c'), $p->score);

            $images = explode(';',$p->img);
            $imgCount = 1;
            foreach($images as $i){
                $sitemap->addImage($node, array(
                    'loc' => $i,
                    'caption' => 'http://www.airsoftmegastore.com',
                    'title' => $p->name .' image '. $imgCount++,
                    'license' => 'http://www.airsoftmegastore.com'
                ));
            }
        }

        foreach($categories as $p){
            $sitemap->addUrl($p->url, GoogleSitemapper::CHANGEFREQ_DAILY, date('c'), $p->score);
        }

        $prc = scandir('C:/_server/projects/AMS/www.airsoftmegastore.com/prc');
        $videos = scandir('C:/_server/projects/AMS/www.airsoftmegastore.com/prc/videos');

        foreach($prc as $c){
            if(substr($c,0,1) != '.'){
                $sitemap->addUrl('/prc/'.$c ,GoogleSitemapper::CHANGEFREQ_WEEKLY, date('c'), '0.4');
            }
        }

        foreach($videos as $c){
            if(substr($c,0,1) != '.'){
                $sitemap->addUrl('/prc/videos/'.$c,GoogleSitemapper::CHANGEFREQ_WEEKLY, date('c'), '0.4');
            }
        }

        $sitemap->saveXml(false);
    }

}
