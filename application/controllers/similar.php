<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Similar extends CI_Controller {

    public function index()
    {
        $this->load->model('similaritymanager');
        $data = array();

        $data['subject'] = $this->similaritymanager->get404();
        $data['redirects'] = $this->similaritymanager->compare404sToProducts($data['subject']);

        $this->load->view('redirects', $data);
    }
    public function airsplat()
    {
        $this->load->model('similaritymanager');
        $data = array();

        $next = $this->uri->segment(3)?:1;

        $subject = $this->input->post('subject');
        $typeOfSearch = $this->input->post('type');

        $data['subject'] = ($subject) ? $subject : $this->similaritymanager->getProduct($next);

        if($typeOfSearch == 'alike'){
            $data['redirects'] = $this->similaritymanager->alikeProducts($subject);
        }elseif($typeOfSearch == 'similar text'){
            $data['redirects'] = $this->similaritymanager->similarTextProducts($subject);
        }else{
            $data['redirects'] = $this->similaritymanager->levenshteinProducts($subject);
        }
        $this->load->view('similar_products', $data);
    }

    public function requery(){
        $search = $this->input->get('search');
        return json_encode($this->similaritymanager->compare404sToProducts($search));
    }

}
