<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH .'libraries/nusoap/nusoap.php');

class Sli extends CI_Controller
{
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->process();
        $this->upload();

        $this->load->model('mailmanager');
        $this->mailmanager->mailOutput('SLI Index', ob_get_clean());
    }

    public function process()
    {
        $this->load->model('slimanager');

        echo "<h3>Processing SLI Index</h3>";

        $this->slimanager->makeXML();
    }

    public function test(){
        $this->load->model('slimanager');
        $this->slimanager->test();
    }

    public function upload(){
        $this->load->library('ftp');

        $config['hostname'] = 'ftp.airsoftmegastore.net';
        $config['username'] = 'aaronams';
        $config['password'] = '2abstrakt';
        $config['port']     = 21;
        $config['passive']  = TRUE;
        $config['debug']    = TRUE;

        $this->ftp->connect($config);

        echo "<h3>Uploading SLI Index</h3>";
        $timestamp = new DateTime();
        if($this->ftp->upload(APPPATH."assets/data/products.xml", '/catalog.airsoftmegastore.net/application/assets/data/products.xml', 'ascii', 0775)){
            $uploaded = new DateTime();
            $diff = $uploaded->diff($timestamp);
            echo "Upload completed in ". $diff->format('%I:%S') ;
        }
        $this->ftp->close();

    }

    public function download(){
        header('Content-Type: text/xml');
        echo file_get_contents(APPPATH."assets/data/products.xml");
    }

    public function download_sales(){
        header('Content-Type: text/xml');
        echo file_get_contents(APPPATH."assets/data/products_sales.xml");
    }


    public function gzip(){
        echo gzcompress(file_get_contents(APPPATH."assets/data/products.xml"),9);
    }
}
