<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH .'libraries/nusoap/nusoap.php');

class Soap extends CI_Controller
{
    var $storeId = '6296BC81-12C2-4530-9BE1-258F3A08EB6A';
    var $accessKey = 'K0J+kbCajcOJV1uwh/OnH38CqpMnLUJ5X4vJ0mQvGWjooXla3PX9Zyqij87aZNXuhP5x/h6wMSGwfqzsvfkespXY';
    var $ODAuthenticationService = 'https://webservices.orderdynamics.net/AuthenticationService.svc?wsdl';
    var $ODInventoryService = 'https://webservices.orderdynamics.net/InventoryService.svc?wsdl';
    var $mongo_config = "mongodb://localhost:27017/AMS";


    function __construct(){
        parent::__construct();
        $this->load->model('ams');
    }

    public function index()
    {
        $client = new nusoap_client($this->ODAuthenticationService, true);
        $result = $client->call('Authenticate', array('StoreId'=>$this->storeId,'Access Key'=>$this->accessKey));
        var_dump($result);
       // $client = new soapclient('http://localhost/phphack/helloworld.php');

//        $result = $client->call('hello', array('name' => 'Scott'));
//// Display the result
//        print_r($result);
    }

    public function export(){

    }

    public function csvToMySql(){
        $row = 1;
        $header = array();
        if (($handle = fopen(APPPATH."assets/data/products.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", "~")) !== FALSE) {
                if($row == 1){
//                    $header = str_replace('[]','_arr',$data);
//                    $header = str_replace(array('.','#',':',' ', '&'),'_',$header);
//                    $header = array_map('strtolower', $header);
                }else{
                    $combined = array_combine($header, $data);
                    $transformed = $this->transform($combined);
                    //var_dump($combined);
                    $this->saveToMongo($transformed);
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function csvProducts(){
        $row = 1;
        $header = array();
        if (($handle = fopen(APPPATH."assets/data/products.csv" , "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", "~")) !== FALSE) {
                if($row == 1){
                    $header = str_replace('[]','_arr',$data);
                    $header = str_replace(array('.','#',':',' ', '&'),'_',$header);
                    $header = array_map('strtolower', $header);
                    $header[0] = '_id';
                }else{
                   $combined = array_combine($header, $data);
                   $transformed = $this->transform($combined);
                   $this->saveProducts($transformed);
                }
                $row++;
            }
            fclose($handle);
        }
    }
    public function csvCategories(){
        $row = 1;
        $header = array();
        if (($handle = fopen(APPPATH."assets/data/categories.csv" , "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",", "~")) !== FALSE) {
                if($row == 1){
                    $header = str_replace('[]','_arr', $data);
                    $header = str_replace(array('.','#',':',' ', '&'),'_',$header);
                    $header = array_map('strtolower', $header);
                    $header[0] = '_id';
                }else{
                   $transformed = array_combine($header, $data);
                   $this->saveCategories($transformed);
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function transform($data){

        //filter out bad keys
//        $data = array_filter_key($data, function($key) {
//            return strpos($key, 'attribute_product_banner') !== 0;
//        });


        //turn product categories into an array
        $data['imageurl'] = explode(';', $data['imageurl']);

        $categories = explode(',', $data['product_categories']);
        foreach($categories as $key=>$val){
            $categories[$key] = $this->ams->getCategory($val);
        }
        $data['product_categories'] = $categories;

//        //remove all blank values
//        foreach($data as $key=>$val){
//            if($val==""){
//                unset($data[$key]);
//            }
//        }

        return $data;
    }

    public function saveProducts($data){
        $m = new Mongo($this->mongo_config);
        $collection = $m->AMS->od_products;
        return $collection->insert($data);
    }

    public function saveCategories($data){
        $m = new Mongo($this->mongo_config);
        $collection = $m->AMS->od_categories;
        return $collection->insert($data);
    }

    public function xmlFeed(){
        $m = new Mongo($this->mongo_config);
        $collection = $m->AMS->od_products;

        $cursor = $collection->find();
        $xml = new SimpleXMLElement('<catalog/>');

        $allowed = array("id", "attribute_general_sku", "default_retail_price", "imageurl", "long_description", "name", "product_categories",
                            "attribute_specifications_battery_type_","attribute_specifications_build_material_","attribute_specifications_gearbox_",
                            "attribute_specifications_magazine_capacity_","attribute_specifications_muzzle_velocity_","attribute_specifications_propellant_","brand_back-end_name"
        );
        //$keys = array('name','short_desc','full_desc','url', 'category','price','availability', 'product_id', 'manufacturer_id');

        foreach($cursor as $item){

            $filtered = array_intersect_key( $item , array_flip($allowed));

            $child = $xml->addChild('product');
            $child ->addAttribute('id', $item['_id']);

            foreach($filtered as $key=>$val){

                if($key == "product_categories"){
                    $categories = $child->addChild('categories');

                    foreach($val as $category){
                         $cat = $categories->addChild('category',htmlspecialchars($category['name']));
                         $cat->addAttribute('id', $category['_id']);
                    }

                }elseif($key =='brand_back-end_name'){
                    $c = $child->addChild('brand',$val);

                }elseif(stripos($key,'attribute_general_sku')===0){
                    $c = $child->addChild('sku',$val);

                }elseif(stripos($key,'attribute_specifications')===0){

                    if($val){
                        $stripped = str_ireplace('attribute_specifications_', '', $key);
                        $stripped  = trim($stripped, "_");
                        $c = $child->addChild($stripped,$val);
                    }else{
                        continue;
                    }
                }elseif($key == "name"){
                   $c = $child->addChild($key, htmlspecialchars($val));
                }elseif($key == "imageurl"){

                    $images = $child->addChild('images');

                    foreach($val as $img){
                        $images->addChild('image',$img);
                    }
                }elseif($key == "long_description"){
                    $desc = $child->addChild('long_description');
                    $this->sxml_cdata($desc, $val);
                }else{
                    $c = $child->addChild($key,$val);
                }
            }
        }

        echo $xml->asXML(APPPATH."assets/data/products.xml");
    }

    public function sxml_cdata($path, $string){
        $dom = dom_import_simplexml($path);
        $cdata = $dom->ownerDocument->createCDATASection($string);
        $dom->appendChild($cdata);
    }

}
