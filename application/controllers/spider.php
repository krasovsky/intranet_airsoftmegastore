<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spider extends CI_Controller {

    public function crawl(){
        $this->load->model('ams_spider');
        $id = $this->uri->segment(3);
        $id = $this->ams_spider->getNextIdToSpider($id);
        $items = $this->ams_spider->getCompatibleItems($id);
        $this->ams_spider->saveItems($id, $items);

        $this->load->view('spider',array('data'=>$items, 'id'=>$id));
    }

    public function evike(){
        $this->load->model('evike_spider');

        $count = $this->uri->segment(3);

        $id = $this->evike_spider->getNextIdToSpider($count);

        if(!isset($id)) exit(); //loop is over

        $url = "http://www.evike.com/brands/$id";

        $this->evike_spider->getAllProductsInBrand($url, $id);

        $this->load->view('competitor_spider', array('next' =>'/index.php/spider/evike/'.($count+1)));

    }

    public function evike_homepage(){
        $this->load->model('evike_spider');

        $this->evike_spider->getAllProductsOnHomepage();


    }

    public function airsoftgi_homepage(){
        $this->load->model('airsoftgi_spider');

        $this->airsoftgi_spider->getAllProductsOnHomepage();


    }


    public function airsoftgi(){
        $this->load->model('airsoftgi_spider');

        $count = $this->uri->segment(3);

        $id = $this->airsoftgi_spider->getNextIdToSpider($count);

        if(!isset($id)) exit(); //loop is over

        $url = "http://www.airsoftgi.com/index.php?manufacturers_id=$id&max_display_search_results=200&listing=side";

        $this->airsoftgi_spider->getAllProductsInBrand($url, $id);

        $this->load->view('competitor_spider',array('next' =>'/index.php/spider/airsoftgi/'.($count+1)));

    }
}
