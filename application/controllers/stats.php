<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stats extends CI_Controller {

    public function index(){
        //getTopTrends
        $this->load->model('analytics');

        $days = $this->uri->segment(3) ?: 30;

        $dtime = new DateTime(date('Y-m-d', strtotime('-1 days')));
        $start = $dtime->format("Y-m-d H:i:s");

        $dtime = new DateTime(date('Y-m-d', strtotime('-'.$days.' days')));
        $end = $dtime->format("Y-m-d H:i:s");


        $data = $this->analytics->getTopTrends($start,$end);

        $this->load->view('seo/list', array('data'=> $data));
    }

    public function trending(){
        $this->load->model('analytics');
        $this->load->helper('url');

        $product_id = $this->uri->segment(3) ?: 0;

        if($product_id==0) redirect('/stats');

        $days = $this->uri->segment(4) ?: 30;

        $start_time = new DateTime(date('Y-m-d', strtotime('-1 days')));
        $start = $start_time->format("Y-m-d H:i:s");

        $end_time = new DateTime(date('Y-m-d', strtotime('-'.$days.' days')));
        $end = $end_time->format("Y-m-d H:i:s");


        $data = $this->analytics->getProductTrends($product_id,$start,$end);
        $data = $this->remap($data,$days,'events');

        $orders = $this->analytics->getOrders($product_id,$start,$end);
        //$orders = $this->analytics->ordersToTrendsRemap($orders,$data);

        $orders = $this->remap($orders, $days,'quantity');

        $product = $this->analytics->getProduct($product_id);

        $this->load->view('seo/trendingProducts', array('data'=> $data, 'product'=>$product, 'orders' => $orders ));
    }

    public function organic(){
        $this->load->model('analytics');
    }
    public function ppc(){}

    private function remap($data, $days, $label){
        $remap = array();
        for($i = 1; $i<=$days; ++$i){
            $date = new DateTime(date('Y-m-d', strtotime('-'.$i.' days')));
            $day = $date->format("m/d/y");

            foreach($data as $row){
                if($day == $row['timestamp']){
                    $remap[$day] = $row;
                    break;
                }
                $remap[$day] = array($label=>0,'timestamp'=>$day);
            }
        }
        return array_reverse($remap);
    }

}
