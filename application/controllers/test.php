<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	public function index(){
		$this->load->view('test');
	}

	public function curl(){
		$iim1 = new COM("imacros");
 	    $s = $iim1->iimOpen("-fx");
     
     	$fileName = date("m-d-y-H-i");
 	    $s = $iim1->iimSet("FILENAME", $fileName);
 	    $s = $iim1->iimPlay("IMPORT.iim");
     
 	    // echo "iimplay=";
 	    // echo $s;
 	    // echo "extract=";  
   	    //echo $iim1->iimGetExtract;
     
 	    $s = $iim1->iimClose();
	}

	public function csv(){
		$row = array(10,20,30);
		$keys = array("aaa","bbb","ccc","eee");
        $hash = array_combine($keys, $row);
        dump($hash);
		//$this->load->model('csv');
		//$success = $this->csv->processCSV("categories.csv", "categories");
	}

	public function generateFileList(){
		$path = "C:\\xampp\\htdocs\\intranet\\application\\assets\\data\\sandbox\\www.airsoft.sandbox.orderdynamics.net\\";
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $name => $object){
			if(strpos($name,'.aspx')!== false){
				$fileName = str_replace("C:\\xampp\\htdocs\\intranet\\application\\assets\\data\\sandbox\\www.airsoft.sandbox.orderdynamics.net","http://www.airsoftmegastore.com",$name);
				$fileName = str_replace("\\", "/", $fileName);
				echo "{$fileName}</br>".PHP_EOL;
			}
		}
	}

	public function webshot(){
		$webshotPath = realpath(APPPATH."assets/data/webshots/");

		$command = "wkhtmltoimage.exe --javascript-delay 5000 http://www.airsoft.sandbox.orderdynamics.net/2855-admin#2855,1000005,9214,1000002,1000004,1000003,1000001 {$webshotPath}\\test.jpg";
		chdir('C:\\Program Files (x86)\\wkhtmltopdf\\bin');
		system($command,$retval);
		dump($retval);
	}
	public function getSimilar(){
		$similarStr = implode(array(1881,1671,1935),',');
		$queryStr = "select p.*, 
			TRUNCATE(pp.price - (pp.price * (p.on_sale / 100)),   2  ) AS price
			from products p  
			INNER JOIN productpricings pp ON pp.id = p.id  
			where p.id in ({$similarStr})";
		$rez = $this->db->query($queryStr);

		$buildsArr = $rez->result_array();


		foreach($buildsArr as $build){
			dump($build);
			$queryStr = <<<HEREDOC
			SELECT 
			  p1.id AS similar_id,
			  p1.sku AS sku1,
			  p1.`categories`,
			  TRUNCATE(
			    pp.price - (pp.price * (p1.on_sale / 100)),
			    2
			  ) AS similar_price,
			  MATCH(p1.`categories`) AGAINST (
			    '{$build["categories"]}' IN NATURAL LANGUAGE MODE
			  ) AS asd 
			FROM
			  products p1 
			  INNER JOIN productpricings pp 
			    ON pp.id = p1.id 
			WHERE p1.`merchandisable` = 1 
			  AND MATCH(p1.`categories`) AGAINST (
			    '{$build["categories"]}' IN NATURAL LANGUAGE MODE
			  ) 
			  AND p1.id <> {$build["id"]} 
			ORDER BY ABS(
			    TRUNCATE(
			      pp.price - (pp.price * (p1.on_sale / 100)),
			      2
			    ) - {$build["price"]}
			  ) ASC 
			LIMIT 5
HEREDOC;
			dump($queryStr);
		}
	}

	public function phpinfo(){
		echo phpinfo();
		echo date('Y-m-d H:i:s',time() + 2 * 86400);
	}

	public function memcache(){
		$this->load->driver('cache',array('adapter'=>'cache_memcached'));
		dump($this->cache);
		//die();

		$tmp_object = new stdClass;
		$tmp_object->str_attr = 'test';
		$tmp_object->int_attr = 123;

		$this->cache->memcached->save('test',$tmp_object, 10);

		$get_result = $this->cache->memcached->get('test');
		echo "Data from the cache:<br/>\n";

		var_dump($get_result);
	}

	public function createWebshotTestRequest(){
		$url = "http://www.webshot.airsoftmegastore.net/webshot_script.php";
		$data = array("website" => "http://www.airsoft.sandbox.orderdynamics.net/1844-#1844,13555,461,2357|webshot");

		$jsonData = json_encode($data);
		
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, "data=".$jsonData);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
	}

	public function generateFpsProductsFile(){
		$queryStr = <<<HEREDOC
		select * from products where p.id in ()
HEREDOC;

		$rez = $this->db->query($queryStr);
		$rezultArr = $rez->result_array();
		var_dump(count($rezultArr));
		file_put_contents('gunConfigs.js', 'var gunConfigs = ' . json_encode($rezultArr) . ';');
	}

	public function ups(){
		$this->load->helper("ups");
		$options = array(
			UpsConfig::OPTION_RECIEVER_ADDRESS_VALUE => "1817 2nd street",
			UpsConfig::OPTION_RECIEVER_ZIPCODE_VALUE => "91010",
			UpsConfig::OPTION_PACKAGE_WEIGHT_VALUE => "5",
			// UpsConfig::OPTION_PACKAGE_DIMENTIONS_WIDTH_VALUE => "10",
			// UpsConfig::OPTION_PACKAGE_DIMENTIONS_HEIGHT_VALUE => "19",
			// UpsConfig::OPTION_PACKAGE_DIMENTIONS_LENGTH_VALUE => "10"
			);
		$rate = calculate_ups_rate($options);
		var_dump($rate);
	}
}