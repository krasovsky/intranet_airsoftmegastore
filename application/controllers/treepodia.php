<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH .'libraries/nusoap/nusoap.php');

class Treepodia extends CI_Controller
{
    function __construct(){
        parent::__construct();
    }

    public function process()
    {
        $this->load->model('treepodia_manager');
        $this->treepodia_manager->makeXMl();
    }

    public function download(){
        header('Content-Type: text/xml');
        echo file_get_contents(APPPATH."assets/data/treepodia.xml");
    }

    public function gzip(){
        echo gzcompress(file_get_contents(APPPATH."assets/data/treepodia.xml"),9);
    }

}
