<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trends extends CI_Controller {

    public function index(){
        $this->load->model('trendmanager');
        $this->trendmanager->topTenProducts();
    }

    public function topTen(){
        $this->load->model('trendmanager');
        $data = array();

        $data['categories'] = $this->trendmanager->getCategories();
        $this->load->view('trends/topten',$data);
    }
}
