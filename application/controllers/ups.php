<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ups extends CI_Controller {

    public function index()
    {
    	DIE("NOTHING HERE");
    }

    public function getExport(){
        $this->load->model('ups_model');

        $options = array();
        $value = $this->uri->segment(3);
        if(! empty($value) && is_numeric($value)){
        	//$options[Ups_model::OPTION_DAYS_BACK_VALUE] = $value;
            $options[Ups_model::OPTION_SCRAPE_BACK] = true;
        }
        $this->ups_model->downloadExport($options);
    }

    public function runExports(){
    	$this->load->model('ups_model');

        $options = array();
        $i = 30;
        while($i > 10){
        	$options[Ups_model::OPTION_DAYS_BACK_VALUE] = $i;
        	$this->ups_model->downloadExport($options);
        	$i--;
        }

    }
    public function processExport(){
        $this->load->model('ups_model');
        $this->ups_model->processExport();
    }
}
