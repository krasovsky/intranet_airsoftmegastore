<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zip extends CI_Controller {

    public function download()
    {
        $this->load->model('downloadmanager');
        $this->downloadmanager->download();
    }
    public function downloadUsers()
    {
        $this->load->model('downloadmanager');
        $this->downloadmanager->downloadUsers();
    }
    public function downloadOrders()
    {
        $this->load->model('downloadmanager');
        $this->downloadmanager->downloadOrders();
    }
    public function downloadOrderItems()
    {
        $this->load->model('downloadmanager');
        $this->downloadmanager->downloadOrderItems();
    }
    public function unzip()
    {
        $this->load->model('downloadmanager');
        $this->downloadmanager->unzip();
    }
}
