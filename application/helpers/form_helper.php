<?php


function showErrorMessage($message){
	$errorMessage = <<<HEREDOC
		<div class="alert alert-danger">{$message}</div>
		<div class="clear"></div>
HEREDOC;

	return $errorMessage;
}