<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/11/15
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */


    function rollup_inventory_year($data){
        $rollup = array('sold'=>0,'restocked'=>0);
        foreach($data as $d){
            if($d['days'] < 365){
                if(isset($last) && $last - $d['inventory'] <= 0 ) $rollup['sold'] +=  $last - $d['inventory'];
                if(isset($last) && $last - $d['inventory'] > 0 ) $rollup['restocked'] += $last - $d['inventory'];
                $last = $d['inventory'];
            }
        }
        return $rollup;
    }

    function rollup_inventory_month($data){

    }

    function rollup_inventory_week($data){

    }

    function rollup_inventory_yesterday($data){

    }
