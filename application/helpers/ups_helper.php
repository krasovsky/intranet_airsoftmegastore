<?php

class UpsConfig{
	const OPTION_RECIEVER_ADDRESS_VALUE = "oav";
	const OPTION_RECIEVER_ZIPCODE_VALUE = "orzv";
	const OPTION_PACKAGE_WEIGHT_VALUE = "opwv";
	const OPTION_PACKAGE_DIMENTIONS_HEIGHT_VALUE = "opdhv";
	const OPTION_PACKAGE_DIMENTIONS_WIDTH_VALUE = "opdwv";
	const OPTION_PACKAGE_DIMENTIONS_LENGTH_VALUE = "opdlv";

	const OPTION_CHECK_BY_ORDER = "ocbo";
	const OPTION_CHECK_BY_PRODUCT = "ocbpa";

	const VALUE_ORDER = "vo";
	const VALUE_ORDER_ARR = "voa";
	const VALUE_PRODUCT = "vp";
	const VALUE_PRODUCT_ARR = "vpa";

}

/**
 * USE THIS TO CALCULATE RATE
 * HAS DB CHECK FEATURE
 */
function calculate_rate($options){
	if(! empty($options[UpsConfig::OPTION_CHECK_BY_ORDER])){
		if(! empty($options[UpsConfig::VALUE_ORDER_ARR])){
			$ordersToCheck = $options[UpsConfig::VALUE_ORDER_ARR];
		}

	}
	elseif(! empty($options[UpsConfig::OPTION_CHECK_BY_PRODUCT])){
		DIE("asd");
	}
}

/**
 * USED TO CALL API
 * @param  array  $options [description]
 * @return [type]          [description]
 */
function calculate_ups_rate($options = array()){
		
		if(empty($options))
			return false;
		
		$ci=& get_instance();
		
		$config = $ci->config->item('upsconfig');

		$userId = $config["userid"];
		$password = $config["password"];
		$accessKey = $config["accesskey"];
		

		$rate = new Ups\Rate(
		    $accessKey,
		    $userId,
		    $password
		);

		try {
		    $shipment = new \Ups\Entity\Shipment();

		    $shipperAddress = new \Ups\Entity\Address();
		    $shipperAddress->setAddressLine1($config["address"]);
		    $shipperAddress->setPostalCode($config["zipcode"]);

		    $shipFrom = new \Ups\Entity\ShipFrom();
		    $shipFrom->setAddress($shipperAddress);

		    $shipment->setShipFrom($shipFrom);

		    $shippingTo = new \Ups\Entity\Address();
		    if(! empty($options[UpsConfig::OPTION_RECIEVER_ADDRESS_VALUE])){
		    	$shippingTo->setAddressLine1($options[UpsConfig::OPTION_RECIEVER_ADDRESS_VALUE]);
		    }
		    if(! empty($options[UpsConfig::OPTION_RECIEVER_ZIPCODE_VALUE])){
		    	$shippingTo->setPostalCode($options[UpsConfig::OPTION_RECIEVER_ZIPCODE_VALUE]);
			}

		    $shipTo = new \Ups\Entity\ShipTo();
		    $shipTo->setAddress($shippingTo);
		    $shipment->setShipTo($shipTo);


		    $package = new \Ups\Entity\Package();
		    $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
		    $package->getPackageWeight()->setWeight($options[UpsConfig::OPTION_PACKAGE_WEIGHT_VALUE]);

		    if(! empty($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_HEIGHT_VALUE]) && ! empty($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_WIDTH_VALUE]) && ! empty($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_LENGTH_VALUE])){
				$dimensions = new \Ups\Entity\Dimensions();
			    $dimensions->setHeight($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_HEIGHT_VALUE]);
			    $dimensions->setWidth($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_WIDTH_VALUE]);
			    $dimensions->setLength($options[UpsConfig::OPTION_PACKAGE_DIMENTIONS_LENGTH_VALUE]);

			    $unit = new \Ups\Entity\UnitOfMeasurement;
		    	$unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

		    	$dimensions->setUnitOfMeasurement($unit);
		    	$package->setDimensions($dimensions);
			}

		    $shipment->addPackage($package);

		    $result = $rate->getRate($shipment);
		    $rez = $result->RatedShipment[0]->TotalCharges->MonetaryValue;
		} catch (Exception $e) {
			//var_dump($e);
		    return false;
		}

		return $rez;
		//return $result->RelatedShipment[0]->TotalCharges->MonetaryValue;
}

function regroup_array($arr,$key){
	$rez = array();

	foreach($arr as $element){
		$rez[$element[$key]] = $element;
	}

	return $rez;
}


