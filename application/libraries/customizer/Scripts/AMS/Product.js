/**
 * Created by abstraktron on 10/5/2015.
 */
var AMS = AMS || {};

AMS.AlsoPurchased = {
    createSlider: function(){
        //Compile HTML Template
        var source   = $('#also-purchased-template').html();
        this.template = Handlebars.compile(source);

        //Query also purchased items from SLI
        var that = this;
        var skuNumber = $('#skuNumber').html();
        $.getJSON("http://2050-1.sli-r.com/r-api/1/r.json?sid=53348d6e84ae4d6481c52a23&sku=" + skuNumber + "&c=10",
            function(feed) {
                that.data = feed.results;
                that.renderSlider();
                $('.also-purchased').css({'display':'block'});
            });
    },
    renderSlider: function(){
        // Cycle through array of products and output its contents into html
        var html = '';
        for (var i=0;i<this.data.length;i++){
            html += this.template(this.data[i]);
        }
        //Build the slider
        $('#also-purchased-list').html(html);
        $('#also-purchased-list').bxSlider({
            minSlides: 3,
            maxSlides: 4,
            slideWidth: 220,
            slideMargin: 10,
            pager: false,
            nextSelector: '#slider-next',
            prevSelector: '#slider-prev',
            nextText: '<img src="/Images/small-bx-button-right.png">',
            prevText: '<img src="/Images/small-bx-button-left.png">'
        });
    }
};

AMS.Configurator = {
    initialized:false,
    isOpen:false,
    overrideConfigRelease:false,
    ready:function(){
        //is this id to be configurated
        console.log("asdasd");
        var that = this;
        //looking for config-release.js variable
        if(( !! configRelease && !~configRelease.indexOf(parseInt(AMS.Product.productId))) || this.overrideConfigRelease) return false;

        AMS.LazyLoad.injectDependancies();

        $('.buttonBar, .configure').removeClass('superHide');

        $.getScript("/configurator/assets/js/gb2.2.js",function(data){
            $('.configure').on('click',function(){
                that.showConfigurator();
                if(!that.isOpen) that.showProshop();
                else that.hideProshop();
            }.bind(this));
            that.autoShowProShop();
        });

        var source = $('#_tmpl_configurator').html();
        $('#configurator .container').html(source);

        $('#main').on('gb2.initialized',function(){
            $('#configurator #main').animate({'opacity':1},500,'linear');
        });

        Handlebars.registerHelper('getProductId',function() { return AMS.Product.productId; });
    },
    showProshop:function(){
        $('#productContainer').animate({'opacity':0},1500,function(){
            $('#configurator').animate({'height':'630px'}, 1000,'easeOutBounce' ,function(){
                this.isOpen = true;
                $('.buy').css({'visibility':'hidden'});
            }.bind(this));
        }.bind(this));
    },
    hideProshop:function(){
        $('#configurator').animate({'height':0}, 1000,'easeInQuint' ,function(){
            $('#productContainer').animate({'opacity':1},1000);
            this.isOpen = false;
            $('.buy').css({'visibility':'visible'});
        }.bind(this));
    },
    autoShowProShop:function(){
        if(location.hash){
            setTimeout(function(){
                this.showConfigurator();
                if(!this.isOpen) this.showProshop();
                else this.hideProshop();
            }.bind(this),1500);
        }
    },
    showConfigurator:function(){
        if(!this.initialized)
            this.initialize();
    },
    initialize:function(){
        this.initialized = true;
        AMS.GB2.startup();
        AMS.Admin.ready();

        $('#main').on('gb2.closeApp', function (e) {
            this.hideProshop();
        }.bind(this));
    }
};

AMS.FreeShip = {
    ready:function(){
        this.check()
    }
    ,check:function(){
        if($('#hero').hasClass('sale')){
            var price = parseFloat($('meta[itemprop=price]').attr('content'));
            if(!!price && price > 150){
                this.show();
            }
        }else{
            this.show();//free shipping automatically for all non-sale items.
        }
    }
    ,show:function(){
        $('.freeship').show();
    }

};

AMS.ImageSwap = {
    items:[],
    current:0,
    ready:function(){
        var that = this;
        AMS.Videos.initialize();
        this.$container = $('.mainImage .contentContainer');
        this.$images = $('.polaroids a[data-config]');

        this.$action = $('.imgAction');

        this.$images.on('click',function(e){
            e.preventDefault();
            that.current = that.$images.index(this);
            that.showImage(that.current);
            that.displayCurrentThumb(this);
        });

        this.$action.on('click',function(){
            $('.mainImage').magnificPopup('open');
        });
        this.buildItems(
            function(){
                return $('.mainImage').magnificPopup({
                    items:that.items,
                    gallery:{
                        enabled:true
                    },
                    callbacks:{
                        open:function(){
                            this.goTo(that.current);
                            var str = that.items[that.current].src;
                            if(str.indexOf('youtube.com')){
                                var id = 'video-'+ str.substring(str.indexOf('watch?v=')+8);
                            }else{
                                var id = 'highlight-'+ str.substring(str.indexOf('imageId')+8,str.indexOf('&'));
                            }
                            AMS.Analytics.logClick(['hero',id]);
                        },
                        close:function(){
                            that.current = this.index;
                            that.showImage();
                        }
                    }
                });
            }
        );

        $('.photos').on('click',function(e){
            $('.buttonBar div').removeClass('active');
            $(e.delegateTarget).addClass('active');
            $('#photo_0').trigger('click');
        }.bind(this));

        //this.carousel();
    },
    buildItems:function(callback){

        this.$images.each(function(index,el){

            var config = JSON.parse($(el).attr('data-config'));
            var type = (!config.type)?"image":config.type;

            this.items.push({
                src:$(el).attr('href'),
                type:type
            });
        }.bind(this));
        this.lightbox = callback();
    },
    displayCurrentThumb:function(el){
        this.$images.removeClass('active');
        $(el).addClass('active');
    },
    showImage:function(){
        var el = this.$images[this.current];
        var img = new Image();
        var config = JSON.parse($(el).attr('data-config'));
        var type = !config.type?'image':config.type;
        var source = $('#'+type+'_tmpl').html();
        var template = Handlebars.compile(source);

        if(!config.loaded){

            this.$container.fadeOut(200,function(){
                img.src = config.img;
            }.bind(this));

            img.onload = function(){
                this.$container.html(template(config));
                this.$container.fadeIn(100);
                config.loaded = true;
                $(el).attr('data-config',JSON.stringify(config));
            }.bind(this);
        }else{
            this.$container.html(template(config));
            this.$container.show();
        }

        this.displayCurrentThumb(el);
    },
    carousel: function(){
        //Determine how many images are there in this Carousel
        var i = 0;
        $( ".polaroids li" ).each(function( index ) {
            i++;
        });
        i = (-62 * i) + 620;

        //Hide "previous" button
        if($('.polaroids').css("left") == "0px") {
            $('.carousel-nav.left').hide();
        }
        //Hide "Next" button if needed
        if(i > 0) {
            $('.carousel-nav.right').hide();
        }

        //Do the magic
        $('.carousel-nav.left').click(function(){
            if($('.polaroids').css("left") == "-62px") {
                $('.carousel-nav.left').hide();
            }

            $('ul.polaroids').animate({left: '+=62px'}, 'fast');
            $('.carousel-nav.right').show();
        });

        $('.carousel-nav.right').click(function(){
            if($('.polaroids').css("left") == i + "px") {
                $('.carousel-nav.right').hide();
            }

            $('ul.polaroids').animate({left: '-=62px'}, 'fast');
            $('.carousel-nav.left').show();
        });
    }
};

AMS.OutOfStock={
    notready:function(){
        if ($('.btnAddToCart-OutOfStock').length > 0) {
            $('.notificationBar').text('Out of Stock : On Order');
            $('#hero').removeClass('sale').addClass('oos');
            this.outOfStockMsg();
        }
    },
    outOfStockMsg: function(){
        var msg = 'This item is out of stock. ';
        var op = $('a[data-name^=similar]')[0];

        if (op){
            msg = msg + 'Check out the ' + op.outerHTML;
        }

        $('.alert-yellow').html(msg).removeClass('hide');
    }
};

AMS.ProductPageTest = {
    state:'hidden',
    ready:function(){
        //remove must haves for firefox and ie in place of the styled at to cart button
        if($('#relatedAccordion div div').length == 0) return;
//                if(AMS.Browser.isFirefox() || AMS.Browser.isIE()){
//                    var $configure = $('<a href="#" id="configure" data-name="ConfigurePackage.toggle">Configure Product Package</a>');
//                    $configure.click(function(e){
//                        e.preventDefault();
//
//                        this.toggle();
//                    }.bind(this));
//
//                    $("#relatedAccordion").before($configure)
//                    this.height = $("#relatedAccordion").height()+5;
//                    $("#relatedAccordion").css({"height":"0px", "overflow":"hidden"});
//
//
//                    $(".column2").css({"border-style":"solid"});
//                    $(".column2").css({"border-width":"1px", "border-color":"#cccccc", "padding":"10px", "width":"300px", "text-align":"left"});
//                    $("#mainContentPlaceHolder_ctl00_buyArea_btnAddToCart").css({"position":"relative", "left":"40px"});
//                    $("#mainContentPlaceHolder_ctl00_buyArea").css({"position":"relative", "left":"40px"});
//                    $("#mainContentPlaceHolder_ctl00_buyArea_btnAddToCart").css({"left":""});
//                }
//
        $('.relDropDown').change(function(e){
            var value = e.target.value;
            AMS.Analytics.logClick(['ConfigureAddProduct',value]);
        });

        $('.gotoReviews').click(function(){AMS.Analytics.logClick(['hero','readReviews'])});
        $('.btn-write-review').click(function(){AMS.Analytics.logClick(['hero','writeReviews'])});

        //this.setLargestFacebookShareImage();
    },
    setLargestFacebookShareImage:function(){
        $('meta[property$=image]').attr('content', $('#photo_0').attr('href'));
    },
    toggle:function(){
        if(this.state == 'hidden'){
            this.showOptions();
            this.state = 'visible';
        }else{
            this.hideOptions();
            this.state = 'hidden';
        }
    },
    hideOptions:function(){
        $("#relatedAccordion").animate({"height":"0px", "overflow":"hidden"});
    },
    showOptions:function(){
        $("#relatedAccordion").animate({"height":this.height, "overflow":"hidden"});
    }
}

AMS.ProductPage = {
    ready:function(){
        $('.buyarea').on('click', 'input[type=submit]', function(){
            AMS.Analytics.logClick(['AddToCart',AMS.Product.id]);
        })
        $('#sidebar .btn-add').each(function(){
            var id = $(this).attr('href').replace('/AddItem.aspx?itemId=','');
            $(this).attr('data-name','AddToCart.'+id+'.YouMayAlsoLike');
        });

        if($('.btnAddToCart-OutOfStock').length){
            AMS.Analytics.logClick(['OutStock',AMS.Product.id]);
        }else{
            AMS.Analytics.logClick(['InStock',AMS.Product.id]);
        }

    }
};

AMS.Tabs = {
    ready:function(){
        var that = this;
        this.$tabs = $('.tabs li[data-ref]');
        this.$panes = $('.pane');
        this.$tabs.on('click',function(){
            var pane = $(this).attr('data-ref');
            that.removeActiveStates();
            that.setActiveState(this,pane);
        });
    },
    removeActiveStates:function(){
        this.$tabs.removeClass('active');
        this.$panes .removeClass('active');
    },
    setActiveState:function(tab,pane){
        $(tab).addClass('active');
        $('#'+pane).addClass('active');
    }
};

AMS.Videos = {
    //http://stackoverflow.com/questions/2068344/how-do-i-get-a-youtube-video-thumbnail-from-the-youtube-api
    img:'http://img.youtube.com/vi/##id##/hqdefault.jpg',
    thumb:'http://img.youtube.com/vi/##id##/2.jpg',
    video:'http://www.youtube.com/watch?v=##id##',
    initialize:function(){
        var str = $.trim($('#video_variable').text());

        if(!!str){
            $('.buttonBar, .videos').show();
        }else{// if no video stop.
            return false;
        }

        var videos = str.split('|');
        $(videos).each(function(index, el){
            var $thumb = this.assemble(el,index);
            this.addThumb($thumb);
        }.bind(this));

        $('.videos').on('click',function(){
            $('.buttonBar div').removeClass('active')
            $(this).addClass('active');
            $('#video_0').trigger('click');
        }); //hide all associated with videos
    }
    ,noVideos:function(){
        //message to the screen that there are no videos for this product.
        //message subscribe to our youtube channel
    }
    ,assemble:function(video,index){
        var thumb = this.idSwap(this.thumb,video);
        var img = this.idSwap(this.img,video);
        var link = this.idSwap(this.video,video);
        var html = '<li><a id="video_'+index+'" data-name="hero.videoThumb-'+index+'" href="'+link+'" alt="video" title="video" data-config=\'{"type":"iframe","img":"'+img+'"}\'><img src="'+thumb+'" height="60" class="miniImg" alt="video"></a></li>'

        return $(html);
    }
    ,idSwap:function(str,id){
        return str.replace('##id##', id);
    }
    ,addThumb:function($thumb){
        $('.polaroids').append($thumb);
    }
};
