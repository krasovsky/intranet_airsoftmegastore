﻿/* File Created: August 16, 2012 */

function ratingValidator_Validate(sender, args) {
    //var ctrl = $find('<%= this.ratingControl.BehaviorID %>');
    var ctrl = $find('mainContentPlaceHolder_ratingControl_RatingExtender');
	args.IsValid = (ctrl.get_Rating() > 0);
}


$(function () {
	//ProductImageViewerInit();

	$('.galleryImgs li:first').addClass('active');

	// Related Products Accordion
	$('#relatedAccordion > div').accordion({
		animated: 'slide',
		autoHeight: false
	});

	$('#relatedAccordion h3:first').addClass('activeAccordionHeading');
	$('#relatedAccordion h3').click(function () {
		$('#relatedAccordion h3').removeClass('activeAccordionHeading');
		$(this).addClass('activeAccordionHeading');
	});

	// Make sure default ShareThis unless ie7/6
	//if (BrowserDetect.browser == 'Explorer') {
	//	if (parseInt(BrowserDetect.version) === 7 || parseInt(BrowserDetect.version) === 6) {
	//		$('.plugin .shareThis').css({ 'top': '-2px' });
	//		// Share This Add-On Control
	//		stLight.options({
	//			publisher: '12345',
	//			popup: true,
	//			onhover: true
	//		});
	//	} else {
	//		// Share This Add-On Control
	//		stLight.options({
	//			publisher: '12345'
	//		});
	//	}
	//} else {
	//	// Share This Add-On Control
	//	stLight.options({
	//		publisher: '12345'
	//	});
	//}

	//bvc - new
	InitiateItemArrayLogic();
});

function InitiateItemArrayLogic() {
    $('.buyOptions').prepend("<option selected value='0'>(select)</option>");
    $('.buyOptions').change(function () {
        var itemId = GetCurrentItemId();
        if (itemId > 0) {
            var sku = GetProductItemInfo(itemId, "SKU");
            $(".itemCodeDisplay").html(sku);
            var inv = GetProductItemInfo(itemId, "Inventory");
            if (inv > 0) {
                $(".itemStockMsg").html("<span class='in'>This item is in stock<span>");
            } else {
                $(".itemStockMsg").html("<span class='out'>Sorry, this item is out of stock<span>");
            }
        }
        $(this).find("option:contains('(select)')").hide();
    }); 
}

function GetCurrentItemId() {
    var selectedMaps = new Array();
    $(".buyOptions option:selected").each(function (index) {
        var n = $(this).val();
        selectedMaps[index] = n;
    });

    if (selectedMaps.length > 0) {
        var mapCount = itemArray[0].Mappings.split(";").length;
        var mapArray = new Array();
        if (mapCount == 2) {
            if (selectedMaps.length < 2) {
                //
            } else {
                mapArray[0] = selectedMaps[0] + ";" + selectedMaps[1];
                mapArray[1] = selectedMaps[1] + ";" + selectedMaps[0];
            }
        } else {
            mapArray[0] = selectedMaps[0];
        }

        for (var i = 0; i < itemArray.length; i++) {
            if (itemArray[i] != null) {
                var mappings = itemArray[i].Mappings;
                for (var j = 0; j < mapArray.length; j++) {
                    if (mapArray[j] == mappings) {
                        return itemArray[i].Id;
                    }
                }
            }
        }

    }
    return -1;
}

function GetProductItemInfo(itemId, type) {
    for (var i = 0; i < itemArray.length; i++) {
        if (itemArray[i].Id == itemId) {
            switch (type) {
                case "SKU":
                    return itemArray[i].SKU;
                   case "Inventory":
                   	var aMyUTF8Output = base64DecToArr(itemArray[i].Inventory);
                    return UTF8ArrToStr(aMyUTF8Output);
                case "RetailPrice":
                    return itemArray[i].RetailPrice;
                case "Price":
                    return itemArray[i].Price;
            }
        }
    }
    return -1;
}