<%@ page language="C#" masterpagefile="~/MasterPages/Default.master" autoeventwireup="true" inherits="ASM.Web.Airsoft.ViewProduct, Airsoft.Web.Airsoft" enableEventValidation="false" %>

<%@ Register Src="~/WebControls/YouMayAlsoLike2.ascx" TagName="YouMayAlsoLike" TagPrefix="ASM" %>
<%@ Reference Control="~/WebControls/RelatedDropdowns.ascx" %>
<asp:Content ID="headContent" ContentPlaceHolderID ="headPlaceHolder" runat="server">
	<meta property="fb:app_id" content="583914284983195" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">

<!--[if lte IE 9]>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.0/jquery.xdomainrequest.min.js'></script>
<![endif]-->

<script type="text/javascript" src="/configurator/assets/js/vendor/underscore-min.js"></script>

<link type="text/css" rel="stylesheet" href="/configurator/assets/css/common.css" />
<link type="text/css" rel="stylesheet" href="/configurator/assets/tools/selector/css/imgareaselect-default.css" />
<link href="/Styles/AMS/product.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<%= this.ResolveClientUrl("~/Scripts/ViewProduct.js") %>"></script>
<!--GB2 UPSELL-->
<div class="backdrop hide">
    <div id="popup" class="hide"></div>
    <div id="upsell" class="hide">
        <div class="content clearfix">
        <div>
            <div class="pull-right checkout btn">Finish Build</div>
            <h2>Add Essentials</h2>
       </div>
            <p>Select from the compatible essentials you need from the list below.</p>
            <div class="upsell-list"></div>
            <div class="upsell-details hide"></div>
            <div class="upsell-evaluation"></div>
        </div>
    </div>
</div>
<!--END GB2 UPSELL-->
	<div id="main" class="alt-container">
    
	<od:PersonalizedContentArea ID="MainPixelTrackingPCA" runat="server" AreaId="7" OutputContainerDiv="true"></od:PersonalizedContentArea>
		<div class="w1"  itemscope itemtype="http://schema.org/Product">
<!-- OOS MESSAGING-->
            <div class="alert-red hide"></div>
            <div class="alert-yellow hide"></div>
<!-- END OOS MESSAGING-->
			<od:ProductDisplay runat="server" ID="productDisplay" AutoAddMetaTags="true" AutoAddTitleTag="true">
				<Template>
				<script>
				    var AMS = AMS || {};
				    AMS.Page = {
				        name:'Product',
				        dependencies:["/configurator/builds/<%# Container.ProductId %>/parts3.js"
                                     ,"/configurator/builds/<%# Container.ProductId %>/config.js"
                                     ,"/configurator/assets/tools/selector/scripts/jquery.imgareaselect.min.js"]
				    };

                    AMS.Product = {
                        id:'<%#Container.CurrentProduct.Items[0].Id %>',
                        productId:'<%# Container.ProductId %>',
                        name:'<%# Container.ProductName %>',
                        url:'<%# HttpUtility.HtmlEncode(Container.CurrentProduct.ProductUrl) %>'
                    };
				</script>
				<script type="text/javascript" src="/Scripts/AMS/Product.js"></script>
				<script type="text/javascript" src="/configurator/config-release.js"></script>
                <script type="text/variable" id="video_variable">
                    <%# Container.CustomText3 %>
                </script>
<!--SCRIPT DECLARATIONS-->
<!--PRODUCT CONFIGURATOR-->
                <div id="configurator">
                    <div class="container"></div>
                    <div id="pageTransition" class="row">
                            <div class="first"></div>
                    </div>
                </div>
<!--PRODUCT CONFIGURATOR-->
                <div id="productContainer">
<!--BREADCRUMBS-->
                    <div class="shareLinks">
                        <asp:PlaceHolder ID="WishlistDisplay" runat="server">
                            <a href="<%# String.Format("javascript:AddToWishList({0})", Container.CurrentProduct.Items[0].Id) %>" data-name="AddToWishlist.<%#Container.CurrentProduct.Items[0].Id %>">Add To Wish List</a><br />
                        </asp:PlaceHolder>
                    </div>
                    <div id="configureThis" class="configure superHide"><i class="fa fa-wrench"></i> Customize It!</div>
					<ul class="breadcrumbs">
						<li><a href="<%= this.ResolveClientUrl("~/Default.aspx") %>" title="Home">Home</a></li>
						<asp:Label ID="productBrand" runat="server" />
						<asp:Label ID="productCategory" runat="server" />
						<li><%# Container.CurrentProduct["General.SKU"] %></li>
					</ul>
<!--END BREADCRUMBS-->
<!--HERO-->
                        <div id="hero" class="photo<%# Container.SingleUnitPrice == OriginalPrice ? "": " sale"%>" >
                        <div class="notificationBar">On Sale: Ending Soon!</div>
                        <div class="mainImage">
                            <div class="contentContainer">
                                <img src="<%# ImageFactory.GetImageUrl("Products", Container.ProductId, 560, 374, true, ImageConstraintType.Both) %>" class="highlight" alt="product image" />
                            </div>
                        </div>
                        <div class="title">
                            <h1 itemprop="name" id="productName"><%# Container.ProductName %></h1>
                            <div class="rate">
                                <ASM:ProductRatingDisplay ID="prodRating" runat="server" FullStarPath="~/Images/starFull.gif" EmptyStarPath="~/Images/starEmpty.gif"
                                    HalfStarPath="~/Images/starHalf.gif" DisplayString="<span class='prodrating'> {0}/5<br /><em>Based on {1} {2}</em></span>"
                                    ShowBeTheFirstToRate="true" ShowExtraContent="true" EnableViewState="false" />
                            </div>
                        </div>
                        <div id="freeShip-flag" class="hide freeship">
                            <div class="explain">
                                <h4>3 Ways to get Free Shipping</h4>
                                <ol>
                                    <li>1. Buy Any Regular Priced Item</li>
                                    <li>2. or, Buy Any Sale Item + Any Non-Sale Item</li>
                                    <li>3. or, Any Order Over $150</li>
                                </ol>
                                <span class="arrow"></span>
                            </div>
                            <strong>Free Shipping</strong>
                        </div>
                        <div class="pricing">
                            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">

                                <meta itemprop="priceCurrency" content="USD" />
                                 <asp:PlaceHolder runat="server" Visible="<%# OriginalPrice>Container.SingleUnitPrice %>">
                                    <div>
                                       <span class="old-price"><del>WAS : <%# OriginalPrice.ToString("C2")%></del></span>
                                    </div>
                                 </asp:PlaceHolder>
                                
                                <strong class="price" itemprop="price"><%# Container.SingleUnitPrice.ToString("C2") %></strong><span class="lowest">Guaranteed Lowest</span>
                                <asp:PlaceHolder runat="server" Visible="<%# Container.SingleUnitPrice == OriginalPrice%>">
                                    <div>
                                        <span style="color: #666;" class="old-price" title="AMS RETAIL PRICE is based upon average market prices which calculates from multiple factors such as MSRP and acutal printed retail prices.">
                                            <del><%# (OriginalPrice * (decimal)1.12).ToString("C0")%>.99 RETAIL PRICE</del>
                                        </span>
                                    </div>
                                </asp:PlaceHolder>
                                    <div style="display: <%# OriginalPrice > Container.SingleUnitPrice ? "block" : "none"%>">
                                        <asp:PlaceHolder runat="server" Visible="<%# ( OriginalPrice - Container.SingleUnitPrice ) > 5 %>">
                                            <span class="save percentage" style="display:none;">YOU SAVE <%# OriginalPrice > 0 ?((OriginalPrice - Container.SingleUnitPrice) / OriginalPrice * 100 ).ToString("N0") : "" %>% !</span>
                                            <span class="save dollar" >YOU SAVE <%# (OriginalPrice - Container.SingleUnitPrice).ToString("C2") %> !</span>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" Visible="<%# OriginalPrice > 0 && ( OriginalPrice - Container.SingleUnitPrice ) < 5 && ((OriginalPrice - Container.SingleUnitPrice) / OriginalPrice * 100 ) > 5 %>">
                                            <span class="save">YOU SAVE <%# OriginalPrice > 0 ?((OriginalPrice - Container.SingleUnitPrice) / OriginalPrice * 100 ).ToString("N0") : "" %>% !</span>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" Visible="<%# OriginalPrice > 0 &&( OriginalPrice - Container.SingleUnitPrice ) < 5  && ((OriginalPrice - Container.SingleUnitPrice) / OriginalPrice * 100 ) < 5 %>">
                                            <span class="save">SAVE!!!</span>
                                        </asp:PlaceHolder>
                                    </div>

                                    <meta itemprop="price" content="<%#Container.SingleUnitPrice.ToString("F2")	%>" />
                                    <meta itemprop="itemCondition" content="new" />
                                    <meta itemprop="name" content="<%#Container.ProductName%>" />
                                    <meta itemprop="brand" content="<%#Container.Brand%>" />
                                    <meta itemprop="sku" content="<%#Container.CurrentProduct.Items[0].ItemCode%>" />
                                    <meta itemprop="availability" content="<%# ASM.Web.Airsoft.Utility.Instance.GetItemRealtimeInventory(Container.CurrentProduct.Items[0]) > 0 ? "InStock":"OutOfStock" %>" itemtype="http://schema.org/ItemAvailability"/>
                            </span>
                        </div>
                        <div class="buyBg"></div>
                        <!--<div class="imgNav">
                            <i class="icon-angle-right"></i> 
                            <i class="icon-angle-left"></i> 
                        </div>-->
                        <div class="shadow"></div>
                        <div class="imgAction"></div>
                        <div class="buttonBar superHide">
                            <div class="photos active"><a href="#" data-name="hero.tab-photo"><i class="fa fa-camera"></i></a></div>
                            <div class="videos hide"><a href="#" data-name="hero.tab-video"><i class="fa fa-youtube-play"></i></a></div>
                            <div class="configure superHide"><a href="#" data-name="hero.customize"><i class="fa fa-wrench"></i></a></div>
                        </div>
                    </div>
                    <div id="actionArea">
                        <!--<img class="carousel-nav left" src="/images/polaroid-back-button.png">-->
                        <div class="thumbs">
                            <asp:ListView ID="lvImageRotator" runat="server">
                                <layouttemplate>
                                    <ul class="polaroids">
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </ul>
                                </layouttemplate>
                                <itemtemplate>
                                    <li>
                                        <a id="photo_<%#Container.DisplayIndex %>" data-name="hero.thumb-<%#Container.DisplayIndex %>" href="<%# ImageFactory.GetImageUrl((int) this.Eval("Id"), (int) this.Eval("Width"), (int) this.Eval("Height"), true, ImageConstraintType.Both) %>" title="<%# this.Eval("PublicDescription") %>"
                                        data-config='{"img":"<%# ImageFactory.GetImageUrl((int) this.Eval("Id"), 560, 374, true) %>"}' alt="<%# this.Eval("PublicDescription") %>" <%#Container.DisplayIndex == 0? "class='active'":"" %>>
                                            <%# ImageFactory.GenerateImageTag("<img src=\"{0}\" height=\"60\" class=\"miniImg\" alt=\"{1}\" />", (int) this.Eval("Id"),60,  60, true) %>
                                        </a>
                                    </li>
                                </itemtemplate>
                            </asp:ListView>
                        </div>
                        <!--<img class="carousel-nav right" src="/images/polaroid-next-button.png">-->
                    </div>
                    </div>
<!--End NEW Hero-->
                    <div class="specifications">
                        <asp:ListView runat="server" ID="SpecificationsList">
                             <layouttemplate>
                                <ul>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </ul>
                            </layouttemplate>
                            <itemtemplate>
                                <li>
                                    <strong><%# this.Eval("Key") %></strong> <%# this.Eval("Value") %>
                                </li>
                            </itemtemplate>
                        </asp:ListView>
                    </div>
                    
                        <div class="others">
                            <div class="buy">
                                <div class="buyarea">
                                    <div class="configure btn superHide"><i class="fa fa-wrench"></i> customize it!</div>
                                    <ASM:BuyArea ID="buyArea" runat="server" />
                                </div>
                            </div>
                            <div id="freeShip-addToCart" class="hide freeship">
                            <div class="explain">
                                <h4>3 Ways to get Free Shipping</h4>
                                <ol>
                                    <li>1. Buy Any Regular Priced Item</li>
                                    <li>2. or, Buy Any Sale Item + Any Non-Sale Item</li>
                                    <li>3. or, Any Order Over $150</li>
                                </ol>
                                <span class="arrow"></span>
                            </div>

                            <strong>Free Shipping</strong>This item qualifies for Free Shipping
                            </div>
                            <div id="relatedAccordion">
                                <asp:Panel ID="relatedDropdownsPanel" runat="server" />
                            </div>
                           </Template>
                    </od:ProductDisplay>
                            <ASM:YouMayAlsoLike runat="server" ID="YouMayAlsoLike" Size="3" />
                            <div style="padding: 20px 0; text-align:center; clear: both;">
                                <a href="/gunbuilder/" data-name="SideBar.gunBuilderBanner"><img src="/images/banners/gun-builder-banner1.jpg"/></a>
                            </div>
                        </div>
                    <div class="description">
                    <od:ProductDisplay ID="ProductDisplay4" runat="server">
							<Template>
								<ul class="tabs">
									<li class="active" data-ref="pane1"><span>Details</span></li>
									<li data-ref="pane2"><span>Ratings &amp; Reviews</span></li>
                                    <li data-ref="pane3" onclick="FB.XFBML.parse();"><i class="fa fa-facebook-square"></i><span> Comments</span></li>
								</ul>
							</Template>
						</od:ProductDisplay>
					<od:ProductDisplay runat="server" ID="productDisplay1" AutoAddMetaTags="true" AutoAddTitleTag="true">
							<Template>
							    <script type="text/tmpl" id="videos">
                                    <asp:Label runat="server" ID="VideoList" />
                                </script>
                                <div class="pane active" id="pane1">
                                    <div style="margin-bottom: 15px; display: <%# ASM.Web.Airsoft.Utility.Instance.GetDynamicPromotion(Container.CurrentProduct.Id) != null ? "block" : "none"%>">
                                        <%# ASM.Web.Airsoft.Utility.Instance.GetDynamicPromotion(Container.CurrentProduct.Id)%>
                                    </div>
                                    <p itemprop="description" og:description><%# Container.LongDescription %></p>
                                </div>
							</Template>
						</od:ProductDisplay>

                    <div class="reviews pane" id="pane2">
                            <od:PersonalizedContentArea ID="PCAProductReview" runat="server" AreaId="25" OutputContainerDiv="true"></od:PersonalizedContentArea>
							<asp:UpdatePanel runat="server" ID="reviewsUpdatePanel" UpdateMode="Conditional">
								<triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSubmitReview" EventName="Click" />
                                </triggers>
								<contenttemplate>
                                    <asp:Panel runat="server" ID="ratingsOverview">
                                        <asp:PlaceHolder runat="server" ID="hasReviewsPanel">
                                            <div class="header">
                                                <h3>Overview</h3>
                                                <div class="avgRatingStars">
                                                    <od:ProductRatingDisplay ID="ProductRatingDisplay1" runat="server" CssClass="star-rating" FullStarPath="~/Images/starFull.gif" EmptyStarPath="~/Images/starEmpty.gif" HalfStarPath="~/Images/starHalf.gif" EnableViewState="false" />
                                                    <span>
                                                        <%= this.RatingPercTotal %> / 5</span>
                                                </div>
                                                <div class="totalRatings">
                                                    Based on <%= this.TotalRatingCount %> Review(s)</div>
                                            </div>
                                            <asp:ListView runat="server" ID="lstRatingBreakDown" EnableViewState="false">
                                                <LayoutTemplate>
                                                    <div class="ratingBreakdown">
                                                        <ul class="ratingbreakdown">
                                                            <li runat="server" id="itemPlaceHolder" />
                                                        </ul>
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li style="list-style: none outside none; padding: 0;">
                                                        <label>
                                                            <%# this.Eval("NumberOfStars") %>
                                                            star</label>
                                                        <div class="ratingbargraph">
                                                            <div class="ratingbar" style="width: <%# ((double) this.Eval("RatingPercentage")).ToString() %>%;">
                                                            </div>
                                                        </div>
                                                        <em>(<%# this.Eval("NumberOfRatings") %>)</em> </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                            <asp:LinkButton runat="server" ID="btnWriteAReview" CssClass="btnWriteReview" OnClick="btnWriteAReview_Click" Text="Write Your Own Review"></asp:LinkButton>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" ID="noReviewsPanel">
                                            <p>No one has reviewed this product yet,
                                                <asp:LinkButton runat="server" ID="btnWriteAReview1" OnClick="btnWriteAReview_Click" Text="why don't you be the first" Font-Bold="true"></asp:LinkButton>?
                                            </p>
                                        </asp:PlaceHolder>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="ratingSubmission" Visible="false" CssClass="writeareview">
                                        <div class="separator">
                                        </div>
                                        <h3>Write A Review</h3>
                                        <div style="float: left; margin-top: 20px; width: 200px;">
                                            <p>
                                                <label> Your Display Name<span class="required">* (required)</span></label>
                                                    <asp:TextBox runat="server" ID="txtDisplayName" Width="165" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="rating" ControlToValidate="txtDisplayName" Display="Dynamic" ErrorMessage="*" CssClass="required" />
                                            </p>
                                            <div>
                                                <label>Your Rating<span class="required">* (required)</span></label>
                                                <ajaxToolkit:Rating runat="server" ID="ratingControl" RatingAlign="Horizontal" RatingDirection="LeftToRightTopToBottom" AutoPostBack="false" MaxRating="5" StarCssClass="ratingstar" WaitingStarCssClass="waitingstar" FilledStarCssClass="filledstar" EmptyStarCssClass="waitingstar" />
                                                <asp:CustomValidator runat="server" ID="ratingValidator" ClientValidationFunction="ratingValidator_Validate" ErrorMessage="*" CssClass="required" Display="Dynamic" OnServerValidate="ratingValidator_ServerValidate" ValidationGroup="rating" />
                                            </div>
                                            <br/>
                                            <p style="margin-top: 30px;"><asp:LinkButton runat="server" ID="btnCancelRating" CausesValidation="false" OnClick="btnCancelRating_Click" Text="Return to Ratings Overview"></asp:LinkButton></p>
                                        </div>
                                        <div style="float: left; margin-top: 20px;">
                                            <p>
                                                <label>Your Review Title</label>
                                                <asp:TextBox runat="server" ID="txtReviewTitle" Width="280px" />
                                            </p>
                                            <p>
                                                <label>Your Review</label>
                                                <asp:TextBox runat="server" ID="txtReview" Width="350px" Height="175" TextMode="MultiLine" />
                                            </p>
                                            <p>
                                                <asp:LinkButton runat="server" ID="btnSubmitReview" ValidationGroup="rating" OnClick="btnSubmitReview_Click" CssClass="btnSubmitReview">Submit Review</asp:LinkButton>
                                            </p>
                                        </div>
                                        <br class="clear" />
                                    </asp:Panel>
                                </contenttemplate>
							</asp:UpdatePanel>
							<div class="separator">
							</div>
							<asp:UpdatePanel runat="server" ID="reviewListUpdatePanel" UpdateMode="Conditional">
								<triggers>
                                    <asp:AsyncPostBackTrigger ControlID="reviewsList" EventName="ItemCommand" />
                                </triggers>
								<contenttemplate>
                                    <od:ProductReviewsListView runat="server" ID="reviewsList" EnableViewState="true">
                                        <LayoutTemplate>
                                            <div class="row"></div>
                                            <h3>Product Reviews</h3>
                                            <div id="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <!-- review -->
                                            <div class="productreview" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                <div class="wrapper">
                                                    <div class="author">
                                                        <div>
                                                            <span class="hide" itemprop="reviewRating"><%# Convert.ToDecimal((int) this.Eval("Rating")) %></span>
                                                            <od:ProductDisplay runat="server" ID="productDisplay2" AutoAddMetaTags="true" AutoAddTitleTag="true">
                                                                <Template>
                                                                   <span class="hide" itemprop="itemReviewed"><%# Container.ProductName %></span>
                                                                </Template>
                                                            </od:ProductDisplay>
                                                            <od:ProductRatingDisplay ID="ProductRatingDisplay2" runat="server" Rating='<%# Convert.ToDecimal((int) this.Eval("Rating")) %>'
                                                                FullStarPath="~/Images/starFull.gif" HalfStarPath="~/Images/starHalf.gif" EmptyStarPath="~/Images/starEmpty.gif"  />
                                                        </div>
                                                        <div class="title" itemprop="name">
                                                            <%# !string.IsNullOrEmpty((string) this.Eval("Title")) ? this.Eval("Title") : "&nbsp;" %>
                                                        </div>
                                                        <div class="date">
                                                            <em itemprop="datePublished">
                                                                <%# ((DateTime) this.Eval("PostedOn")).ToString("dd/MM/yy") %></em>
                                                        </div>
                                                        <div itemprop="author" >
                                                            <strong>By</strong>:
                                                            <%# this.Eval("DisplayName") %>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div class="text">
                                                        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# !string.IsNullOrEmpty((string) this.Eval("Review")) %>'>
                                                            <p itemprop="reviewBody">"<%# this.Eval("Review") %>"</p>
                                                        </asp:PlaceHolder>
                                                    </div>
                                                </div>
                                                <div class="review-footer">
                                                    <p>
                                                        <%# this.Eval("HelpfulCount") %>
                                                        <%# ((int) this.Eval("HelpfulCount")) == 0 || ((int) this.Eval("HelpfulCount")) > 1 ? "people" : "person" %>
                                                        found this review helpful.<br />
                                                    </p>
                                                </div>
                                                <div>
                                                    <div style="float: left;">
                                                        Was this review helpful to you?
                                                        <asp:ImageButton ID="ImageButton1" runat="server" CommandName="markhelpful" ImageUrl="~/Images/HelpfulYes.png"
                                                            AlternateText="I found this review helpful" ImageAlign="middle" />
                                                        <asp:ImageButton ID="ImageButton2" runat="server" CommandName="marknothelpful" ImageUrl="~/Images/HelpfulNo.png"
                                                            AlternateText="I did not find this review helpful" ImageAlign="middle" />
                                                    </div>
                                                    <div style="float: right;">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="inappropriateLink" CommandName="report"
                                                            Text="Report As Inappropriate" />
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </od:ProductReviewsListView>
                                </contenttemplate>
							</asp:UpdatePanel>
						</div>
                        </div>
						<od:ProductDisplay runat="server" ID="productDisplay2" AutoAddMetaTags="true" AutoAddTitleTag="true">
							<Template>
								<div class="tab-content" id="tab-4">
									<p>
										<%# Container.CustomText1 %></p>
								</div>
							</Template>
						</od:ProductDisplay>
		</div>
		<od:PersonalizedContentArea ID="productPixelTrackingPCA" runat="server" AreaId="21" OutputContainerDiv="false">
		</od:PersonalizedContentArea>
	</div>

<!--JS TEMPLATES-->
    <script type="text/tmpl" id="_tmpl_configurator">
        <div id="main">
            <div id="wizard">
                <ul>
                    <li><div class="btn black" id="restart">Restart</div></li>
                    <li><div class="btn black" id="remove">Remove</div></li>
                    <li><div class="btn black" id="next" class="pulse">Next</div></li>
                    <li><div class="btn black" id="closeApp">Close</div></li>
                </ul>
                <div class="pull-right path-controls"><label>Hide Paths <input type="checkbox" name="paths"/></label></div>
            </div>
            <div id="title">
                <div class="h1">Custom ProShop</div>
                <span style="font-weight: bold;">Customize your gun like a Pro! Compatiblity Guaranteed.</span>
            </div>
            <div id="menu">
            </div>
            <div id="details" style="display:none"></div>
            <div id="gun"></div>
            <div id="stats">
                <div id="graph" class="hide">
                    <h4>Specs</h4>
                    <ul>
                        <li>Weight: 2.8 lbs</li>
                        <li>Length: 128"</li>
                        <li>FPS: 440</li>
                        <li>Capacity: 300</li>
                    </ul>
                </div>
                <div id="price">Total Price: $0.00</div>
            </div>

            <div id="checkout" class="checkout">
                <!-- <button id="share">share</button>-->
                <div class="btn" id="cart">Add To cart</div>
            </div>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_svg">
      <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
           width="990.000000px" height="315.000000px" viewBox="0 0 990.000000 315.000000" preserveAspectRatio="xMinYMin meet">
          <g transform="translate(0.000000,315.000000) scale(0.100000,-0.100000)">
              <path d="" data-rel="noUse"/><!--default path to avoid js defaulting to first path below-->
              {{#each this}}
                <path d="{{#if imgResized }}{{svg.resized}}{{/if}}{{#unless imgResized }}{{svg.full}}{{/unless}}" data-id="{{item_id}}" id="id-{{item_id}}" data-rel="{{category}}" class="{{#if isBase}}base{{/if}}"/>
              {{/each}}
          </g>
      </svg>
    </script>

    <script type="text/tmpl" id="_tmpl_img">
        <div id="images">
            {{#each this}}
                {{#unless isBase}}
                <img src="/configurator/builds/{{getProductId}}/media/{{#if imgResized }}resized/{{/if}}{{render_img}}" data-id="{{item_id}}" data-rel="{{category}}"/>
                {{/unless}}
            {{/each}}
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_menu_list">
       <ul id="list">
        {{#with selected}}
           <li data-id="{{item_id}}" class="selected {{#if isDisabled}}disabled{{/if}}">
               <div class="img">
                   <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=90&height=90&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
               </div>
               <h3>{{product_name}}</h3>
               <div class="brand">brand: {{brand}}</div>
               <em class="price">${{price}}</em>
               <div data-rel="{{item_id}}" class="remove">unselect</div>
               <span class="more">info</span>
           </li>
        {{/with}}
        {{#each list}}
           <li data-id="{{item_id}}" class="{{#if isDisabled}}disabled{{/if}} {{#if out_of_stock}}oos{{/if}}">
               <div class="img">
                   <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=90&height=90&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
               </div>
               <h3>{{product_name}}</h3>
               <div class="brand">brand: {{brand}}</div>
               <em class="price">${{price}}</em>
               <span class="more">info</span>
           </li>
        {{/each}}
       </ul>
    </script>

    <script type="text/tmpl" id="_tmpl_menu_popuplist">
        <ul id="list">
        {{#each this}}
        {{#unless out_of_stock}}
        {{#unless isDisabled}}
        <li data-id="{{item_id}}">
            <div class="img">
                <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=90&height=90&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
            </div>
            <h3>{{product_name}}</h3>
            <div class="brand">brand: {{brand}}</div>
            <em class="price">${{price}}</em>
            <span class="more">info</span>
        </li>
        {{/unless}}
        {{/unless}}
        {{/each}}
        </ul>
    </script>

    <script type="text/tmpl" id="_tmpl_menu">
        <ul>
            {{#each this}}
            <li data-id="{{id}}">
                {{#if requiredForCheckout}}<label>*</label>{{/if}} {{category_name}}
            </li>
            {{/each}}
        </ul>
    </script>

    <script type="text/tmpl" id="_tmpl_menu_details">
            <div class="img">
                <div class="shadow"></div>
                <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=350&height=350&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
            </div>
            <div class="content">
                <h2>{{product_name}}</h2>
                <h4>Brand: {{brand}}</h4>
                <h4>{{sku}}</h4>
                <p>{{{short_description}}}</p>
                <strong>${{price}}</strong>
                {{#unless out_of_stock}}<div class="btn"  data-rel="{{item_id}}">Select</btn>{{/unless}}
                {{#if out_of_stock}}<strong class="pull-right">Out of Stock</strong>{{/if}}
            </div>
    </script>

    <script type="text/tmpl" id="_tmpl_menu_incompatible">
        <div class="img">
            <div class="incompatible">
                {{#each incompatible}}
                    <strong class="remove" title="Remove this item" data-rel="{{item_id}}">X</strong>
                    <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=90&height=90&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
                    <div>incompatible item</div>
                {{/each}}
            </div>

            <div class="shadow red"></div>
            <img src="http://www.airsoftmegastore.com/ImageHandler.axd?ownerTable=Products&ownerId={{product_id}}&width=350&height=350&constraint=3&interpolate=7&async=True&culture=en-US&category="/>
        </div>
        <div class="content">
            <h2>{{product_name}}</h2>
            <h4>{{sku}}</h4>
            <p>{{{short_description}}}</p>
            <strong>${{price}}</strong>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_menu_evaluation">
        <div>
            <h2>Custom Gun <span>${{gunPrice}}</span></h2>
        </div>
        <ul>
            {{#each list}}
            <li data-id="{{item_id}}">
                <h3>{{name}}</h3>
                <em class="price">${{price}}</em>
            </li>
            {{/each}}
        </ul>
        <div>
            <h2>Subtotal <span>${{subTotal}}</span></h2>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_message_confirm">
        <div id="confirm">
            <div class="content">
                <p>{{message}}</p>
                <div class="btn"  data-id="true">yes</div>
                <div class="btn"  data-id="false">no</div>
            </div>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_message_prompt">
        <div id="prompt">
            <div class="content">
                <p>{{message}}</p>
                <input type="text" name="user-data" placeholder="Name Your Gun" />
                <div class="btn"  data-id="true">Done</div>
                <div class="btn"  data-id="false">Cancel</div>
            </div>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_message_alert">
        <div class="content">
            <p>{{message}}</p>
            <div class="btn" data-id="true">ok</div>
        </div>
    </script>

    <script type="text/tmpl" id="_tmpl_message_checkout">
        <div class="content checkout">
            <p>Please give us a second while we prepare your cart for you.</p>
            <div style="text-align:center;"><img src="/configurator/assets/images/ajax-loader.gif" /> </div>
        </div>
    </script>

     <script type="text/tmpl" id="_tmpl_admin">
         <div id="admin">
            <div>
                <div class="btn" data-id="master">New Master Position</div>
                <div class="btn" data-id="saveMaster">Save Master Positions</div>
            <div>
            <div class="hide">
                <div class="btn" data-id="item">part</div>
                <div class="btn" data-id="align">align</div>
                <div class="btn" data-id="move">move</div>
            <div>
        </div>
    </script>

    <script type="text/tmpl" id="image_tmpl">
            <img src="{{img}}" class="highlight" alt="product image" />
    </script>

    <script type="text/tmpl" id="iframe_tmpl">
            <div class="video-container">
                <img src="{{img}}" class="highlight" alt="product image" />
                <i class="fa fa-play"></i>
            </div>
    </script>
<!--END JS TEMPLATES-->
</asp:Content>

