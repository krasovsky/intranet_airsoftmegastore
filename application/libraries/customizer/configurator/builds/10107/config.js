var Platform = {
	"name": "10107",
	"upsellCategoryId": 747,
	"Build": [
		{
			"id": 789,
			"base_id": 11513,
			"category_name": "Core",
			"item_id": 11513,
			"requiredForCheckout": false
		},
		{
			"id": 663,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"663": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 619,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"619": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 707,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"707": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"11513": {
			"619": {
				"x1": 267,
				"y1": 75,
				"x2": 395,
				"y2": 157,
				"width": 128,
				"height": 82,
				"item_id": 11513,
				"position": "619"
			},
			"663": {
				"x1": 270,
				"y1": 152,
				"x2": 398,
				"y2": 234,
				"width": 128,
				"height": 82,
				"item_id": 11513,
				"position": "663"
			},
			"707": {
				"x1": 280,
				"y1": 18,
				"x2": 408,
				"y2": 100,
				"width": 128,
				"height": 82,
				"item_id": 11513,
				"position": "707"
			}
		}
	},
	"Position": {
		"28": {
			"663": {
				"align": "topCenter",
				"x": 20
			}
		}
	}
};