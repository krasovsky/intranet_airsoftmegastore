var Platform = {
	"name": "10108",
	"upsellCategoryId": 717,
	"Build": [
		{
			"id": 757,
			"base_id": 11514,
			"category_name": "Core",
			"item_id": 11514,
			"requiredForCheckout": false
		},
		{
			"id": 631,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"631": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 587,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"587": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 675,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"675": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"11514": {
			"587": {
				"x1": 287,
				"y1": 73,
				"x2": 415,
				"y2": 155,
				"width": 128,
				"height": 82,
				"item_id": 11514,
				"position": "587"
			},
			"631": {
				"x1": 269,
				"y1": 154,
				"x2": 397,
				"y2": 236,
				"width": 128,
				"height": 82,
				"item_id": 11514,
				"position": "631"
			},
			"675": {
				"x1": 238,
				"y1": 27,
				"x2": 366,
				"y2": 109,
				"width": 128,
				"height": 82,
				"item_id": 11514,
				"position": "675"
			}
		}
	},
	"Position": {
		"28": {
			"631": {
				"align": "topCenter",
				"x": 20
			}
		},
		"72": {
			"631": {
				"align": "topCenter",
				"x": 20
			}
		}
	}
};