var Platform = {
	"name": "10255",
	"upsellCategoryId": 802,
	"Build": [
		{
			"id": 1110,
			"base_id": 11703,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 929,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 928,
			"base_id": 9214,
			"category_name": "Handguards",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 930,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1238,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};