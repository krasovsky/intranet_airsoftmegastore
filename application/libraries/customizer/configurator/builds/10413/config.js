var Platform = {
	"name": "10413",
	"upsellCategoryId": 823,
	"Build": [
		{
			"id": 1115,
			"base_id": 11875,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 932,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1151,
			"base_id": 2074,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 933,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 934,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};