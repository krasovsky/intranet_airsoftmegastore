var Platform = {
	"name": "10430",
	"upsellCategoryId": 748,
	"Build": [
		{
			"id": 790,
			"base_id": 11892,
			"category_name": "Core",
			"item_id": 11892,
			"requiredForCheckout": false
		},
		{
			"id": 664,
			"base_id": 1000000,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000000,
			"requiredForCheckout": false,
			"position": {
				"664": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 620,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"620": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 708,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"708": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"11892": {
			"620": {
				"x1": 291,
				"y1": 72,
				"x2": 419,
				"y2": 154,
				"width": 128,
				"height": 82,
				"item_id": 11892,
				"position": "620"
			},
			"664": {
				"x1": 269,
				"y1": 153,
				"x2": 397,
				"y2": 235,
				"width": 128,
				"height": 82,
				"item_id": 11892,
				"position": "664"
			},
			"708": {
				"x1": 273,
				"y1": 16,
				"x2": 401,
				"y2": 98,
				"width": 128,
				"height": 82,
				"item_id": 11892,
				"position": "708"
			}
		}
	},
	"Position": {
		"28": {
			"664": {
				"align": "topCenter",
				"x": 20
			}
		},
		"72": {
			"664": {
				"align": "topCenter",
				"x": 20
			}
		}
	}
};