var Platform = {
	"name": "10701",
	"upsellCategoryId": 718,
	"Build": [
		{
			"id": 758,
			"base_id": 15551,
			"category_name": "Core",
			"item_id": 15551,
			"requiredForCheckout": false
		},
		{
			"id": 632,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 544,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 588,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};