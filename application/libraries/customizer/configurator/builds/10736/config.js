var Platform = {
	"name": "10736",
	"upsellCategoryId": 824,
	"Build": [
		{
			"id": 1120,
			"base_id": 12316,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 940,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 937,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 939,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 941,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 936,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 938,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};