var Platform = {
	"name": "10763",
	"upsellCategoryId": 730,
	"Build": [
		{
			"id": 772,
			"base_id": 12355,
			"category_name": "Core",
			"item_id": 12355,
			"requiredForCheckout": false
		},
		{
			"id": 558,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"558": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"12355": {
			"558": {
				"x1": 466,
				"y1": 9,
				"x2": 594,
				"y2": 91,
				"width": 128,
				"height": 82,
				"item_id": 12355,
				"position": "558"
			}
		}
	},
	"Position": {
		"1339": {
			"558": {
				"align": "bottomCenter",
				"x": -20
			}
		}
	}
};