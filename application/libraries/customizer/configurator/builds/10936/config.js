var Platform = {
	"name": "10936",
	"upsellCategoryId": 720,
	"Build": [
		{
			"id": 760,
			"base_id": 12547,
			"category_name": "Core",
			"item_id": 12547,
			"requiredForCheckout": false
		},
		{
			"id": 546,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"546": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 590,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"590": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"12547": {
			"546": {
				"x1": 453,
				"y1": 4,
				"x2": 581,
				"y2": 86,
				"width": 128,
				"height": 82,
				"item_id": 12547,
				"position": "546"
			},
			"590": {
				"x1": 362,
				"y1": 48,
				"x2": 490,
				"y2": 130,
				"width": 128,
				"height": 82,
				"item_id": 12547,
				"position": "590"
			}
		}
	},
	"Position": {
		"1339": {
			"546": {
				"align": "bottomCenter",
				"x": -20
			}
		}
	}
};