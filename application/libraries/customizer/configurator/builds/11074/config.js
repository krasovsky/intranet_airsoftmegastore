var Platform = {
	"name": "11074",
	"upsellCategoryId": 715,
	"Build": [
		{
			"id": 755,
			"base_id": 12691,
			"category_name": "Core",
			"item_id": 12691,
			"requiredForCheckout": false
		},
		{
			"id": 629,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"629": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 541,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"541": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 673,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"673": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"12691": {
			"541": {
				"x1": 432,
				"y1": 18,
				"x2": 560,
				"y2": 100,
				"width": 128,
				"height": 82,
				"item_id": 12691,
				"position": "541"
			},
			"629": {
				"x1": 337,
				"y1": 148,
				"x2": 465,
				"y2": 230,
				"width": 128,
				"height": 82,
				"item_id": 12691,
				"position": "629"
			},
			"673": {
				"x1": 330,
				"y1": 31,
				"x2": 458,
				"y2": 113,
				"width": 128,
				"height": 82,
				"item_id": 12691,
				"position": "673"
			}
		}
	},
	"Position": {
		"28": {
			"629": {
				"align": "topCenter",
				"x": 40
			}
		},
		"72": {
			"629": {
				"align": "topCenter",
				"x": 40
			}
		},
		"2366": {
			"541": {
				"align": "bottomCenter",
				"x": -10
			}
		},
		"1339": {
			"541": {
				"align": "bottomCenter",
				"x": -20
			}
		}
	}
};