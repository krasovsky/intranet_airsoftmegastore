var Platform = {
	"name": "11075",
	"upsellCategoryId": 828,
	"Build": [
		{
			"id": 1130,
			"base_id": 12692,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 958,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 957,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 959,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 956,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};