var Platform = {
	"name": "11076",
	"upsellCategoryId": 742,
	"Build": [
		{
			"id": 784,
			"base_id": 12693,
			"category_name": "Core",
			"item_id": 12693,
			"requiredForCheckout": false
		},
		{
			"id": 658,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"658": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 570,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"570": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 614,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"614": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 702,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"702": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"12693": {
			"570": {
				"x1": 499,
				"y1": 31,
				"x2": 627,
				"y2": 113,
				"width": 128,
				"height": 82,
				"item_id": 12693,
				"position": "570"
			},
			"614": {
				"x1": 299,
				"y1": 63,
				"x2": 427,
				"y2": 145,
				"width": 128,
				"height": 82,
				"item_id": 12693,
				"position": "614"
			},
			"658": {
				"x1": 296,
				"y1": 148,
				"x2": 424,
				"y2": 230,
				"width": 128,
				"height": 82,
				"item_id": 12693,
				"position": "658"
			},
			"702": {
				"x1": 300,
				"y1": 30,
				"x2": 428,
				"y2": 112,
				"width": 128,
				"height": 82,
				"item_id": 12693,
				"position": "702"
			}
		}
	},
	"Position": {
		"28": {
			"658": {
				"align": "topCenter",
				"x": 40
			}
		},
		"72": {
			"658": {
				"align": "topCenter",
				"x": 40
			}
		},
		"12803": {
			"614": {
				"align": "bottomCenter",
				"x": -5,
				"y": -7
			}
		},
		"12804": {
			"614": {
				"align": "bottomCenter",
				"x": -5,
				"y": -7
			}
		}
	}
};