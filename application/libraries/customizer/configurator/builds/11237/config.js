var Platform = {
	"name": "11237",
	"upsellCategoryId": 798,
	"Build": [
		{
			"id": 1161,
			"base_id": 12932,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 971,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 973,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 970,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 972,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};