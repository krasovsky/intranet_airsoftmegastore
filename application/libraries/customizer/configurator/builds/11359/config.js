var Platform = {
	"name": "11359",
	"upsellCategoryId": 712,
	"Build": [
		{
			"id": 752,
			"base_id": 15552,
			"category_name": "Core",
			"item_id": 15552,
			"requiredForCheckout": false
		},
		{
			"id": 626,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 538,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 582,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};