var Platform = {
    "name": "11370",
    "upsellCategoryId": 734,
    "Build": [{
        "id": 776,
        "base_id": 13065,
        "category_name": "Core",
        "item_id": 13065,
        "requiredForCheckout": false
    }, {
        "id": 650,
        "base_id": 28,
        "category_name": "Bipods and Foregrips",
        "item_id": null,
        "requiredForCheckout": false,
        "position": {"650": {"align": "topCenter"}},
        "imgResized": true
    }, {
        "id": 562,
        "base_id": 483,
        "category_name": "Optics",
        "item_id": null,
        "requiredForCheckout": false,
        "position": {"562": {"align": "bottomCenter"}},
        "imgResized": true
    }, {
        "id": 1236,
        "base_id": 311,
        "category_name": "Flashlights and Lasers",
        "item_id": null,
        "requiredForCheckout": false,
        "position": {"1236": {"align": "bottomCenter"}},
        "imgResized": true
    }],
    "MasterPosition": {
        "13065": {
            "562": {
                "x1": 432,
                "y1": 7,
                "x2": 560,
                "y2": 89,
                "width": 128,
                "height": 82,
                "item_id": 13065,
                "position": "562"
            },
            "650": {
                "x1": 266,
                "y1": 160,
                "x2": 394,
                "y2": 242,
                "width": 128,
                "height": 82,
                "item_id": 13065,
                "position": "650"
            },
            "1236": {
                "x1": 232,
                "y1": 70,
                "x2": 360,
                "y2": 152,
                "width": 128,
                "height": 82,
                "item_id": 13065,
                "position": "1236"
            }
        }
    },
    "Position": {}
};