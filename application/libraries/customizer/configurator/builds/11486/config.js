var Platform = {
	"name": "11486",
	"upsellCategoryId": 746,
	"Build": [
		{
			"id": 788,
			"base_id": 13181,
			"category_name": "Core",
			"item_id": 13181,
			"requiredForCheckout": false
		},
		{
			"id": 662,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"662": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 574,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"574": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 618,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"618": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 706,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"706": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"13181": {
			"574": {
				"x1": 465,
				"y1": 29,
				"x2": 593,
				"y2": 111,
				"width": 128,
				"height": 82,
				"item_id": 13181,
				"position": "574"
			},
			"618": {
				"x1": 282,
				"y1": 63,
				"x2": 410,
				"y2": 145,
				"width": 128,
				"height": 82,
				"item_id": 13181,
				"position": "618"
			},
			"662": {
				"x1": 281,
				"y1": 147,
				"x2": 409,
				"y2": 229,
				"width": 128,
				"height": 82,
				"item_id": 13181,
				"position": "662"
			},
			"706": {
				"x1": 290,
				"y1": 30,
				"x2": 418,
				"y2": 112,
				"width": 128,
				"height": 82,
				"item_id": 13181,
				"position": "706"
			}
		}
	},
	"Position": {
	}
};