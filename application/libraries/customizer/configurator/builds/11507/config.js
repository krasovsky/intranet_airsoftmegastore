var Platform = {
	"name": "11507",
	"upsellCategoryId": 737,
	"Build": [
		{
			"id": 779,
			"base_id": 13202,
			"category_name": "Core",
			"item_id": 13202,
			"requiredForCheckout": false
		},
		{
			"id": 653,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"653": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 565,
			"base_id": 1000002,
			"category_name": "Optics",
			"item_id": 1000002,
			"requiredForCheckout": false,
			"position": {
				"565": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 609,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"609": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 697,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"697": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"13202": {
			"565": {
				"x1": 500,
				"y1": 29,
				"x2": 628,
				"y2": 111,
				"width": 128,
				"height": 82,
				"item_id": 13202,
				"position": "565"
			},
			"609": {
				"x1": 232,
				"y1": 68,
				"x2": 360,
				"y2": 150,
				"width": 128,
				"height": 82,
				"item_id": 13202,
				"position": "609"
			},
			"653": {
				"x1": 231,
				"y1": 148,
				"x2": 359,
				"y2": 230,
				"width": 128,
				"height": 82,
				"item_id": 13202,
				"position": "653"
			},
			"697": {
				"x1": 200,
				"y1": 28,
				"x2": 328,
				"y2": 110,
				"width": 128,
				"height": 82,
				"item_id": 13202,
				"position": "697"
			}
		}
	},
	"Position": {
		"28": {
			"653": {
				"align": "topCenter",
				"x": 40
			}
		},
		"72": {
			"653": {
				"align": "topCenter",
				"x": 40
			}
		},
		"9693": {
			"697": {
				"align": "bottomCenter",
				"x": -10
			}
		},
		"1000002":{
			"565":{
				"align": "bottomCenter",
				"x":-20,
				"y":-4
			}
		}
	}
};