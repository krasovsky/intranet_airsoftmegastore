var Platform = {
	"name": "11508",
	"upsellCategoryId": 725,
	"Build": [
		{
			"id": 767,
			"base_id": 13203,
			"category_name": "Core",
			"item_id": 13203,
			"requiredForCheckout": false
		},
		{
			"id": 641,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"641": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 553,
			"base_id": 1000002,
			"category_name": "Optics",
			"item_id": 1000002,
			"requiredForCheckout": false,
			"position": {
				"553": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 597,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"597": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 685,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"685": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"13203": {
			"553": {
				"x1": 528,
				"y1": 29,
				"x2": 656,
				"y2": 111,
				"width": 128,
				"height": 82,
				"item_id": 13203,
				"position": "553"
			},
			"597": {
				"x1": 330,
				"y1": 63,
				"x2": 458,
				"y2": 145,
				"width": 128,
				"height": 82,
				"item_id": 13203,
				"position": "597"
			},
			"641": {
				"x1": 323,
				"y1": 143,
				"x2": 451,
				"y2": 225,
				"width": 128,
				"height": 82,
				"item_id": 13203,
				"position": "641"
			},
			"685": {
				"x1": 308,
				"y1": 28,
				"x2": 436,
				"y2": 110,
				"width": 128,
				"height": 82,
				"item_id": 13203,
				"position": "685"
			}
		}
	},
	"Position": {
		"28": {
			"641": {
				"align": "topCenter",
				"x": 40
			}
		},
		"72": {
			"641": {
				"align": "topCenter",
				"x": 40
			}
		},
		"9693": {
			"685": {
				"align": "bottomCenter",
				"x": -10
			}
		}
	}
};