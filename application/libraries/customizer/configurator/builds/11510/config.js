var Platform = {
	"name": "11510",
	"upsellCategoryId": 731,
	"Build": [
		{
			"id": 773,
			"base_id": 13205,
			"category_name": "Core",
			"item_id": 13205,
			"requiredForCheckout": false
		},
		{
			"id": 647,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"647": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 559,
			"base_id": 1000003,
			"category_name": "Optics",
			"item_id": 1000003,
			"requiredForCheckout": false,
			"position": {
				"559": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 603,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"603": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 691,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"691": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"13205": {
			"559": {
				"x1": 493,
				"y1": 32,
				"x2": 621,
				"y2": 114,
				"width": 128,
				"height": 82,
				"item_id": 13205,
				"position": "559"
			},
			"603": {
				"x1": 336,
				"y1": 63,
				"x2": 464,
				"y2": 145,
				"width": 128,
				"height": 82,
				"item_id": 13205,
				"position": "603"
			},
			"647": {
				"x1": 339,
				"y1": 147,
				"x2": 467,
				"y2": 229,
				"width": 128,
				"height": 82,
				"item_id": 13205,
				"position": "647"
			},
			"691": {
				"x1": 340,
				"y1": 30,
				"x2": 468,
				"y2": 112,
				"width": 128,
				"height": 82,
				"item_id": 13205,
				"position": "691"
			}
		}
	},
	"Position": {
		"28": {
			"647": {
				"align": "topCenter",
				"x": 50
			}
		},
		"72": {
			"647": {
				"align": "topCenter",
				"x": 60
			}
		}
	}
};