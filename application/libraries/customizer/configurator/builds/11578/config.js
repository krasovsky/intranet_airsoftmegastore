var Platform = {
	"name": "11578",
	"upsellCategoryId": 816,
	"Build": [
		{
			"id": 1174,
			"base_id": 13273,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1004,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1006,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1003,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1005,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};