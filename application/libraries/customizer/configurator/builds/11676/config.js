var Platform = {
	"name": "11676",
	"upsellCategoryId": 714,
	"Build": [
		{
			"id": 754,
			"base_id": 13396,
			"category_name": "Core",
			"item_id": 13396,
			"requiredForCheckout": false
		},
		{
			"id": 628,
			"base_id": 1000000,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000000,
			"requiredForCheckout": false
		},
		{
			"id": 540,
			"base_id": 1000004,
			"category_name": "Optics",
			"item_id": 1000004,
			"requiredForCheckout": false
		},
		{
			"id": 584,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 672,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};