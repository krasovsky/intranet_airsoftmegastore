var Platform = {
	"name": "11682",
	"upsellCategoryId": 740,
	"Build": [
		{
			"id": 782,
			"base_id": 13402,
			"category_name": "Core",
			"item_id": 13402,
			"requiredForCheckout": false
		},
		{
			"id": 656,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 568,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 612,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 700,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};