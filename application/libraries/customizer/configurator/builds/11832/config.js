var Platform = {
	"name": "11832",
	"upsellCategoryId": 738,
	"Build": [
		{
			"id": 780,
			"base_id": 13554,
			"category_name": "Core",
			"item_id": 13554,
			"requiredForCheckout": false
		},
		{
			"id": 654,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 566,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 610,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};