var Platform = {
	"name": "11881",
	"upsellCategoryId": 726,
	"Build": [
		{
			"id": 768,
			"base_id": 13674,
			"category_name": "Core",
			"item_id": 13674,
			"requiredForCheckout": false
		},
		{
			"id": 642,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 554,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 598,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 686,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};