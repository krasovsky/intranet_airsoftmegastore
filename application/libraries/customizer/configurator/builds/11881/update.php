<?php
/**
 * Created by PhpStorm.
 * User: abstraktron
 * Date: 9/16/2015
 * Time: 3:09 PM
 */


require_once('../../configurator.php');

$gun = new Configurator('11881',511);
$gun->setup();
$gun->updateData();
$gun->removeWorkDirectories();
$gun->resetConfigDefaults();