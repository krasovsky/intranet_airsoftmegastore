var Platform = {
	"name": "1662",
	"upsellCategoryId": 835,
	"Build": [
		{
			"id": 1078,
			"base_id": 1662,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 862,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 860,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 863,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 859,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 861,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};