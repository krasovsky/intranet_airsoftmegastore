var Platform = {
	"name": "1665",
	"upsellCategoryId": 809,
	"Build": [
		{
			"id": 1082,
			"base_id": 1665,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 868,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 865,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 867,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 869,
			"base_id": 2173,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 864,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 866,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};