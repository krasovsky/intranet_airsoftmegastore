var Platform = {
	"name": "1844",
	"upsellCategoryId": 749,
	"Build": [
		{
			"id": 791,
			"base_id": 1844,
			"category_name": "Core",
			"item_id": 1844,
			"requiredForCheckout": false
		},
		{
			"id": 577,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"577": {
					"align": "bottomRight"
				}
			},
			"imgResized": true
		},
		{
			"id": 621,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"621": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"1844": {
			"577": {
				"x1": 460,
				"y1": 4,
				"x2": 588,
				"y2": 86,
				"width": 128,
				"height": 82,
				"item_id": 1844,
				"position": "577"
			},
			"621": {
				"x1": 364,
				"y1": 48,
				"x2": 492,
				"y2": 130,
				"width": 128,
				"height": 82,
				"item_id": 1844,
				"position": "621"
			}
		}
	},
	"Position": {
	}
};