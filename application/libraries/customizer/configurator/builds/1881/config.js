var Platform = {
	"name": "1881",
	"upsellCategoryId": 817,
	"Build": [
		{
			"id": 1094,
			"base_id": 1881,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1148,
			"base_id": 2209,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1149,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};