var Platform = {
	"name": "1906",
	"upsellCategoryId": 711,
	"Build": [
		{
			"id": 751,
			"base_id": 1906,
			"category_name": "Core",
			"item_id": 1906,
			"requiredForCheckout": false
		},
		{
			"id": 537,
			"base_id": 1000003,
			"category_name": "Optics",
			"item_id": 1000003,
			"requiredForCheckout": false,
			"position": {
				"537": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"1906": {
			"537": {
				"x1": 504,
				"y1": 34,
				"x2": 623,
				"y2": 116,
				"width": 119,
				"height": 82,
				"item_id": 1906,
				"position": "537"
			}
		}
	},
	"Position": {
		"1335": {
			"537": {
				"align": "bottomLeft",
				"x": 60
			}
		},
		"1337": {
			"537": {
				"align": "bottomLeft",
				"x": 90
			}
		}
	}
};