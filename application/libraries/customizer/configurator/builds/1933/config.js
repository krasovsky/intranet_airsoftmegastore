var Platform = {
	"name": "1933",
	"upsellCategoryId": 713,
	"Build": [
		{
			"id": 753,
			"base_id": 1933,
			"category_name": "Core",
			"item_id": 1933,
			"requiredForCheckout": false
		},
		{
			"id": 627,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"627": {
					"align": "topLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 539,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"539": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"1933": {
			"539": {
				"x1": 454,
				"y1": 6,
				"x2": 533,
				"y2": 87,
				"width": 79,
				"height": 81,
				"item_id": 1933,
				"position": "539"
			},
			"627": {
				"x1": 256,
				"y1": 160,
				"x2": 384,
				"y2": 242,
				"width": 128,
				"height": 82,
				"item_id": 1933,
				"position": "627"
			}
		}
	},
	"Position": {
		"459": {
			"627": {
				"align": "bottomLeft",
				"y": -110,
				"x": -10
			}
		},
		"72": {
			"627": {
				"align": "bottomLeft",
				"x": 60,
				"y": 20
			}
		},
		"28": {
			"627": {
				"align": "bottomLeft",
				"x": 30,
				"y": -50
			}
		},
		"14636": {
			"627": {
				"align": "topLeft",
				"x": -10,
				"y": -3
			}
		},
		"14656": {
			"627": {
				"align": "topLeft",
				"x": -35,
				"y": -3
			}
		},
		"12798": {
			"627": {
				"align": "topLeft",
				"x": -35,
				"y": -3
			}
		},
		"478": {
			"627": {
				"align": "topLeft",
				"x": -35,
				"y": -3
			}
		}
	}
};