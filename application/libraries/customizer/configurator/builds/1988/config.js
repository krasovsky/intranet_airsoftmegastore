var Platform = {
	"name": "1988",
	"upsellCategoryId": 733,
	"Build": [
		{
			"id": 775,
			"base_id": 1988,
			"category_name": "Core",
			"item_id": 1988,
			"requiredForCheckout": false
		},
		{
			"id": 649,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"649": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 561,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"561": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 605,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"605": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"1988": {
			"561": {
				"x1": 480,
				"y1": 0,
				"x2": 571,
				"y2": 82,
				"width": 91,
				"height": 82,
				"item_id": 1988,
				"position": "561"
			},
			"605": {
				"x1": 302,
				"y1": 90,
				"x2": 384,
				"y2": 154,
				"width": 82,
				"height": 64,
				"item_id": 1988,
				"position": "605"
			},
			"649": {
				"x1": 302,
				"y1": 158,
				"x2": 399,
				"y2": 240,
				"width": 97,
				"height": 82,
				"item_id": 1988,
				"position": "649"
			}
		}
	},
	"Position": {
		"28": {
			"649": {
				"align": "topCenter",
				"y": 0
			}
		}
	}
};