var Platform = {
	"name": "2855",
	"upsellCategoryId": 744,
	"Build": [
		{
			"id": 786,
			"base_id": 2855,
			"category_name": "Core",
			"item_id": 2855,
			"requiredForCheckout": false
		},
		{
			"id": 660,
			"base_id": 1000001,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000001,
			"requiredForCheckout": false,
			"position": {
				"660": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 572,
			"base_id": 1000005,
			"category_name": "Optics",
			"item_id": 1000005,
			"requiredForCheckout": false,
			"position": {
				"572": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 616,
			"base_id": 1000000,
			"category_name": "Flashlights and Lasers",
			"item_id": 1000000,
			"requiredForCheckout": false,
			"position": {
				"616": {
					"align": "topLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 704,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"704": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"2855": {
			"572": {
				"x1": 509,
				"y1": 31,
				"x2": 626,
				"y2": 113,
				"width": 117,
				"height": 82,
				"item_id": 2855,
				"position": "572"
			},
			"616": {
				"x1": 344,
				"y1": 108,
				"x2": 472,
				"y2": 190,
				"width": 128,
				"height": 82,
				"item_id": 2855,
				"position": "616"
			},
			"660": {
				"x1": 344,
				"y1": 151,
				"x2": 472,
				"y2": 233,
				"width": 128,
				"height": 82,
				"item_id": 2855,
				"position": "660"
			},
			"704": {
				"x1": 345,
				"y1": 30,
				"x2": 459,
				"y2": 110,
				"width": 114,
				"height": 79,
				"item_id": 2855,
				"position": "704"
			}
		}
	},
	"Position": {
		"72": {
			"660": {
				"align": "topCenter",
				"x": 20
			}
		},
		"28": {
			"660": {
				"align": "topCenter",
				"x": 30
			}
		}
	}
};