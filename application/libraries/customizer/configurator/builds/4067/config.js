var Platform = {
	"name": "4067",
	"upsellCategoryId": 741,
	"Build": [
		{
			"id": 783,
			"base_id": 4067,
			"category_name": "Core",
			"item_id": 4067,
			"requiredForCheckout": false
		},
		{
			"id": 657,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"657": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 569,
			"base_id": 1000001,
			"category_name": "Optics",
			"item_id": 1000001,
			"requiredForCheckout": false,
			"position": {
				"569": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 613,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"613": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 701,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"701": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"4067": {
			"569": {
				"x1": 468,
				"y1": 28,
				"x2": 596,
				"y2": 110,
				"width": 128,
				"height": 82,
				"item_id": 4067,
				"position": "569"
			},
			"613": {
				"x1": 352,
				"y1": 64,
				"x2": 480,
				"y2": 146,
				"width": 128,
				"height": 82,
				"item_id": 4067,
				"position": "613"
			},
			"657": {
				"x1": 360,
				"y1": 141,
				"x2": 488,
				"y2": 223,
				"width": 128,
				"height": 82,
				"item_id": 4067,
				"position": "657"
			},
			"701": {
				"x1": 352,
				"y1": 27,
				"x2": 480,
				"y2": 109,
				"width": 128,
				"height": 82,
				"item_id": 4067,
				"position": "701"
			}
		}
	},
	"Position": {
		"28": {
			"657": {
				"align": "topCenter",
				"x": 30
			}
		},
		"72": {
			"657": {
				"align": "topCenter",
				"x": 30
			}
		},
		"1000001": {
			"569": {
				"align": "bottomRight",
				"x": -44
			}
		},
		"12804": {
			"613": {
				"align": "bottomRight",
				"y": -5,
				"x": -10
			}
		},
		"12803": {
			"613": {
				"align": "bottomRight",
				"y": -5,
				"x": -10
			}
		}
	}
};