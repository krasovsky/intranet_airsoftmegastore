var Platform = {
	"name": "4071",
	"upsellCategoryId": 822,
	"Build": [
		{
			"id": 1098,
			"base_id": 4071,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 901,
			"base_id": 13652,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1150,
			"base_id": 10395,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 900,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};