var Platform = {
	"name": "4695",
	"upsellCategoryId": 811,
	"Build": [
		{
			"id": 1103,
			"base_id": 4695,
			"category_name": "Core",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 906,
			"base_id": 10717,
			"category_name": "Stocks",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 905,
			"base_id": 9214,
			"category_name": "Handguards",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 1237,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false
		},
		{
			"id": 907,
			"base_id": 4698,
			"category_name": "Magazines",
			"item_id": null,
			"requiredForCheckout": false
		}
	],
	"MasterPosition": {
	},
	"Position": {
	}
};