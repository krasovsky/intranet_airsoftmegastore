var Platform = {
	"name": "5124",
	"upsellCategoryId": 739,
	"Build": [
		{
			"id": 781,
			"base_id": 5125,
			"category_name": "Core",
			"item_id": 5125,
			"requiredForCheckout": false
		},
		{
			"id": 655,
			"base_id": 1000000,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000000,
			"requiredForCheckout": false,
			"position": {
				"655": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 611,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"611": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 699,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"699": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"5125": {
			"611": {
				"x1": 291,
				"y1": 72,
				"x2": 419,
				"y2": 153,
				"width": 128,
				"height": 81,
				"item_id": null,
				"position": "611"
			},
			"655": {
				"x1": 279,
				"y1": 159,
				"x2": 407,
				"y2": 241,
				"width": 128,
				"height": 82,
				"item_id": null,
				"position": "655"
			},
			"699": {
				"x1": 274,
				"y1": 18,
				"x2": 402,
				"y2": 100,
				"width": 128,
				"height": 82,
				"item_id": null,
				"position": "699"
			}
		}
	},
	"Position": {
		"28": {
			"655": {
				"align": "topCenter",
				"x": 40,
				"y": 4
			}
		},
		"72": {
			"655": {
				"align": "topCenter",
				"x": 30,
				"y": 2
			}
		}
	}
};