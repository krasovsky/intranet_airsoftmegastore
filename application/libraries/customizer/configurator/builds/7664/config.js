var Platform = {
	"name": "7664",
	"upsellCategoryId": 721,
	"Build": [
		{
			"id": 761,
			"base_id": 8076,
			"category_name": "Core",
			"item_id": 8076,
			"requiredForCheckout": false
		},
		{
			"id": 635,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"635": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 547,
			"base_id": 1000001,
			"category_name": "Optics",
			"item_id": 1000001,
			"requiredForCheckout": false,
			"position": {
				"547": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 591,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"591": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 679,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"679": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8076": {
			"547": {
				"x1": 428,
				"y1": 28,
				"x2": 556,
				"y2": 110,
				"width": 128,
				"height": 82,
				"item_id": 8076,
				"position": "547"
			},
			"591": {
				"x1": 268,
				"y1": 72,
				"x2": 396,
				"y2": 154,
				"width": 128,
				"height": 82,
				"item_id": 8076,
				"position": "591"
			},
			"635": {
				"x1": 298,
				"y1": 158,
				"x2": 426,
				"y2": 240,
				"width": 128,
				"height": 82,
				"item_id": 8076,
				"position": "635"
			},
			"679": {
				"x1": 283,
				"y1": 28,
				"x2": 411,
				"y2": 110,
				"width": 128,
				"height": 82,
				"item_id": 8076,
				"position": "679"
			}
		}
	},
	"Position": {
		"28": {
			"635": {
				"align": "topCenter",
				"x": 20
			}
		},
		"1000001": {
			"547": {
				"align": "bottomCenter",
				"x": -130,
				"y": 5
			}
		}
	}
};