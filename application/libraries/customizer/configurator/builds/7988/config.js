var Platform = {
	"name": "7988",
	"upsellCategoryId": 723,
	"Build": [
		{
			"id": 764,
			"base_id": 8400,
			"category_name": "Core",
			"item_id": 8400,
			"requiredForCheckout": false
		},
		{
			"id": 638,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"638": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 550,
			"base_id": 1000001,
			"category_name": "Optics",
			"item_id": 1000001,
			"requiredForCheckout": false,
			"position": {
				"550": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 594,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"594": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 682,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"682": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8400": {
			"550": {
				"x1": 437,
				"y1": 25,
				"x2": 565,
				"y2": 107,
				"width": 128,
				"height": 82,
				"item_id": 8400,
				"position": "550"
			},
			"594": {
				"x1": 263,
				"y1": 75,
				"x2": 391,
				"y2": 157,
				"width": 128,
				"height": 82,
				"item_id": 8400,
				"position": "594"
			},
			"638": {
				"x1": 299,
				"y1": 159,
				"x2": 427,
				"y2": 241,
				"width": 128,
				"height": 82,
				"item_id": 8400,
				"position": "638"
			},
			"682": {
				"x1": 271,
				"y1": 25,
				"x2": 399,
				"y2": 107,
				"width": 128,
				"height": 82,
				"item_id": 8400,
				"position": "682"
			},
			"null": {
				"x1": 262,
				"y1": 73,
				"x2": 390,
				"y2": 155,
				"width": 128,
				"height": 82
			}
		}
	},
	"Position": {
		"28": {
			"638": {
				"align": "topCenter",
				"x": 30
			}
		},
		"1000001": {
			"550": {
				"align": "bottomCenter",
				"x": -122
			}
		}
	}
};