var Platform = {
	"name": "8064",
	"upsellCategoryId": 710,
	"Build": [
		{
			"id": 490,
			"base_id": 8476,
			"category_name": "Core",
			"item_id": 8476,
			"requiredForCheckout": false
		},
		{
			"id": 484,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"484": {
					"align": "topLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 481,
			"base_id": 1000002,
			"category_name": "Optics",
			"item_id": 1000002,
			"requiredForCheckout": false,
			"position": {
				"481": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 485,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"485": {
					"align": "centerLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 486,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"486": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8476": {
			"481": {
				"x1": 515,
				"y1": 31,
				"x2": 622,
				"y2": 113,
				"width": 107,
				"height": 82,
				"item_id": 8476,
				"position": "481"
			},
			"484": {
				"x1": 355,
				"y1": 150,
				"x2": 478,
				"y2": 232,
				"width": 123,
				"height": 82,
				"item_id": 8476,
				"position": "484"
			},
			"485": {
				"x1": 345,
				"y1": 122,
				"x2": 473,
				"y2": 146,
				"width": 128,
				"height": 24,
				"item_id": 8476,
				"position": "485"
			},
			"486": {
				"x1": 349,
				"y1": 62,
				"x2": 440,
				"y2": 112,
				"width": 91,
				"height": 50,
				"item_id": 8476,
				"position": "486"
			}
		}
	},
	"Position": {
		"28": {
			"484": {
				"align": "topCenter",
				"x": 60
			}
		},
		"72": {
			"484": {
				"align": "topCenter",
				"x": 60
			}
		},
		"1000002": {
			"481": {
				"align": "bottomCenter",
				"x": -10
			}
		}
	}
};