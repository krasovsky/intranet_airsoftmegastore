var Platform = {
	"name": "8118",
	"upsellCategoryId": 719,
	"Build": [
		{
			"id": 759,
			"base_id": 8530,
			"category_name": "Core",
			"item_id": 8530,
			"requiredForCheckout": false
		},
		{
			"id": 633,
			"base_id": 478,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"633": {
					"align": "topLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 545,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"545": {
					"align": "bottomLeft"
				}
			},
			"imgResized": true
		},
		{
			"id": 589,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"589": {
					"align": "centerLeft"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8530": {
			"545": {
				"x1": 416,
				"y1": 0,
				"x2": 544,
				"y2": 43,
				"width": 128,
				"height": 43,
				"item_id": 8530,
				"position": "545"
			},
			"589": {
				"x1": 267,
				"y1": 74,
				"x2": 373,
				"y2": 96,
				"width": 106,
				"height": 22,
				"item_id": 8530,
				"position": "589"
			},
			"633": {
				"x1": 279,
				"y1": 102,
				"x2": 369,
				"y2": 153,
				"width": 90,
				"height": 51,
				"item_id": 8530,
				"position": "633"
			}
		}
	},
	"Position": {
	}
};