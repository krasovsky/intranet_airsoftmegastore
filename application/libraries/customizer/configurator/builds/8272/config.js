var Platform = {
	"name": "8272",
	"upsellCategoryId": 729,
	"Build": [
		{
			"id": 771,
			"base_id": 8684,
			"category_name": "Core",
			"item_id": 8684,
			"requiredForCheckout": false
		},
		{
			"id": 645,
			"base_id": 1000000,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000000,
			"requiredForCheckout": false,
			"position": {
				"645": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 557,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"557": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 601,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"601": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 689,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"689": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8684": {
			"557": {
				"x1": 473,
				"y1": 24,
				"x2": 601,
				"y2": 106,
				"width": 128,
				"height": 82,
				"item_id": 8684,
				"position": "557"
			},
			"601": {
				"x1": 342,
				"y1": 60,
				"x2": 470,
				"y2": 142,
				"width": 128,
				"height": 82,
				"item_id": 8684,
				"position": "601"
			},
			"645": {
				"x1": 345,
				"y1": 143,
				"x2": 473,
				"y2": 225,
				"width": 128,
				"height": 82,
				"item_id": 8684,
				"position": "645"
			},
			"689": {
				"x1": 323,
				"y1": 21,
				"x2": 451,
				"y2": 103,
				"width": 128,
				"height": 82,
				"item_id": 8684,
				"position": "689"
			}
		}
	},
	"Position": {
	}
};