var Platform = {
	"name": "8273",
	"upsellCategoryId": 745,
	"Build": [
		{
			"id": 787,
			"base_id": 8685,
			"category_name": "Core",
			"item_id": 8685,
			"requiredForCheckout": false
		},
		{
			"id": 661,
			"base_id": 1000000,
			"category_name": "Bipods and Foregrips",
			"item_id": 1000000,
			"requiredForCheckout": false,
			"position": {
				"661": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 573,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"573": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 617,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"617": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 705,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"705": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8685": {
			"573": {
				"x1": 480,
				"y1": 23,
				"x2": 608,
				"y2": 105,
				"width": 128,
				"height": 82,
				"item_id": 8685,
				"position": "573"
			},
			"617": {
				"x1": 230,
				"y1": 69,
				"x2": 358,
				"y2": 151,
				"width": 128,
				"height": 82,
				"item_id": 8685,
				"position": "617"
			},
			"661": {
				"x1": 241,
				"y1": 144,
				"x2": 369,
				"y2": 226,
				"width": 128,
				"height": 82,
				"item_id": 8685,
				"position": "661"
			},
			"705": {
				"x1": 231,
				"y1": 25,
				"x2": 359,
				"y2": 107,
				"width": 128,
				"height": 82,
				"item_id": 8685,
				"position": "705"
			}
		}
	},
	"Position": {
		"1000000": {
			"661": {
				"align": "bottomCenter",
				"y": 5
			}
		}
	}
};