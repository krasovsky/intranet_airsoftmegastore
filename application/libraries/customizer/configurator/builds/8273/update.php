<?php
/**
 * Created by PhpStorm.
 * User: abstraktron
 * Date: 9/16/2015
 * Time: 3:09 PM
 */

include_once("../../config.php");
Config::initIncludes();

require_once('../../configurator.php');

$gun = new Configurator('8273',530);
$gun->setup();
$gun->updateData();
$gun->removeWorkDirectories();
$gun->resetConfigDefaults();