var Platform = {
	"name": "8276",
	"upsellCategoryId": 736,
	"Build": [
		{
			"id": 778,
			"base_id": 8688,
			"category_name": "Core",
			"item_id": 8688,
			"requiredForCheckout": false
		},
		{
			"id": 652,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"652": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 564,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"564": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 608,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"608": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"8688": {
			"564": {
				"x1": 425,
				"y1": 15,
				"x2": 553,
				"y2": 97,
				"width": 128,
				"height": 82,
				"item_id": 8688,
				"position": "564"
			},
			"608": {
				"x1": 272,
				"y1": 73,
				"x2": 400,
				"y2": 155,
				"width": 128,
				"height": 82,
				"item_id": 8688,
				"position": "608"
			},
			"652": {
				"x1": 302,
				"y1": 157,
				"x2": 430,
				"y2": 239,
				"width": 128,
				"height": 82,
				"item_id": 8688,
				"position": "652"
			}
		}
	},
	"Position": {
		"1339": {
			"564": {
				"align": "bottomCenter",
				"x": -30
			}
		}
	}
};