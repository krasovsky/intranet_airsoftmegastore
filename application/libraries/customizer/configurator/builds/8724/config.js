var Platform = {
	"name": "8724",
	"upsellCategoryId": 722,
	"Build": [
		{
			"id": 763,
			"base_id": 9497,
			"category_name": "Core",
			"item_id": 9497,
			"requiredForCheckout": false
		},
		{
			"id": 637,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"637": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 549,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"549": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 593,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"593": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 681,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"681": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"9497": {
			"549": {
				"x1": 485,
				"y1": 30,
				"x2": 613,
				"y2": 112,
				"width": 128,
				"height": 82,
				"item_id": 9497,
				"position": "549"
			},
			"593": {
				"x1": 329,
				"y1": 80,
				"x2": 445,
				"y2": 162,
				"width": 116,
				"height": 82,
				"item_id": 9497,
				"position": "593"
			},
			"637": {
				"x1": 331,
				"y1": 163,
				"x2": 459,
				"y2": 245,
				"width": 128,
				"height": 82,
				"item_id": 9497,
				"position": "637"
			},
			"681": {
				"x1": 326,
				"y1": 31,
				"x2": 454,
				"y2": 113,
				"width": 128,
				"height": 82,
				"item_id": 9497,
				"position": "681"
			}
		}
	},
	"Position": {
		"28": {
			"637": {
				"align": "topCenter",
				"x": 30
			}
		}
	}
};