var Platform = {
	"name": "8737",
	"upsellCategoryId": 724,
	"Build": [
		{
			"id": 765,
			"base_id": 9534,
			"category_name": "Core",
			"item_id": 9534,
			"requiredForCheckout": false
		},
		{
			"id": 639,
			"base_id": 28,
			"category_name": "Bipods and Foregrips",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"639": {
					"align": "topCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 551,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"551": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 595,
			"base_id": 311,
			"category_name": "Flashlights and Lasers",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"595": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		},
		{
			"id": 683,
			"base_id": 7109,
			"category_name": "PEQ Boxes",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"683": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"9534": {
			"551": {
				"x1": 482,
				"y1": 32,
				"x2": 610,
				"y2": 114,
				"width": 128,
				"height": 82,
				"item_id": 9534,
				"position": "551"
			},
			"595": {
				"x1": 329,
				"y1": 80,
				"x2": 457,
				"y2": 162,
				"width": 128,
				"height": 82,
				"item_id": 9534,
				"position": "595"
			},
			"639": {
				"x1": 342,
				"y1": 160,
				"x2": 470,
				"y2": 242,
				"width": 128,
				"height": 82,
				"item_id": 9534,
				"position": "639"
			},
			"683": {
				"x1": 324,
				"y1": 31,
				"x2": 452,
				"y2": 113,
				"width": 128,
				"height": 82,
				"item_id": 9534,
				"position": "683"
			}
		}
	},
	"Position": {
		"72": {
			"639": {
				"align": "topCenter",
				"x": 30,
				"y": 2
			}
		}
	}
};