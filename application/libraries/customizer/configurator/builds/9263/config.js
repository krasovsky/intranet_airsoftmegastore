var Platform = {
	"name": "9263",
	"upsellCategoryId": 728,
	"Build": [
		{
			"id": 770,
			"base_id": 10309,
			"category_name": "Core",
			"item_id": 10309,
			"requiredForCheckout": false
		},
		{
			"id": 556,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"556": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"10309": {
			"556": {
				"x1": 517,
				"y1": 11,
				"x2": 645,
				"y2": 93,
				"width": 128,
				"height": 82,
				"item_id": 10309,
				"position": "556"
			}
		}
	},
	"Position": {
	}
};