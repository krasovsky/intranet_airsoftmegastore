var Platform = {
	"name": "9265",
	"upsellCategoryId": 743,
	"Build": [
		{
			"id": 785,
			"base_id": 10311,
			"category_name": "Core",
			"item_id": 10311,
			"requiredForCheckout": false
		},
		{
			"id": 571,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"571": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"10311": {
			"571": {
				"x1": 516,
				"y1": 11,
				"x2": 644,
				"y2": 93,
				"width": 128,
				"height": 82,
				"item_id": 10311,
				"position": "571"
			}
		}
	},
	"Position": {
		"1339": {
			"571": {
				"align": "bottomCenter",
				"x": -30
			}
		}
	}
};