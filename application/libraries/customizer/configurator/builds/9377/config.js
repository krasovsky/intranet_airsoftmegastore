var Platform = {
	"name": "9377",
	"upsellCategoryId": 732,
	"Build": [
		{
			"id": 774,
			"base_id": 10469,
			"category_name": "Core",
			"item_id": 10469,
			"requiredForCheckout": false
		},
		{
			"id": 560,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"560": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"10469": {
			"560": {
				"x1": 460,
				"y1": 9,
				"x2": 588,
				"y2": 91,
				"width": 128,
				"height": 82,
				"item_id": 10469,
				"position": "560"
			}
		}
	},
	"Position": {
		"1339": {
			"560": {
				"align": "bottomCenter",
				"x": -30
			}
		}
	}
};