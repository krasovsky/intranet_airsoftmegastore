var Platform = {
	"name": "9379",
	"upsellCategoryId": 727,
	"Build": [
		{
			"id": 769,
			"base_id": 10471,
			"category_name": "Core",
			"item_id": 10471,
			"requiredForCheckout": false
		},
		{
			"id": 555,
			"base_id": 483,
			"category_name": "Optics",
			"item_id": null,
			"requiredForCheckout": false,
			"position": {
				"555": {
					"align": "bottomCenter"
				}
			},
			"imgResized": true
		}
	],
	"MasterPosition": {
		"10471": {
			"555": {
				"x1": 462,
				"y1": 9,
				"x2": 590,
				"y2": 91,
				"width": 128,
				"height": 82,
				"item_id": 10471,
				"position": "555"
			}
		}
	},
	"Position": {
		"1339": {
			"555": {
				"align": "bottomCenter",
				"x": -30
			}
		}
	}
};