<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 5/8/15
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */

//select all products from database where in attributes show in platform
//include_once('C:\\xampp\\htdocs\\gunbuilder\\Gunbuilder.php');

class Configurator {

    const OPTION_RELEASE_FOLDER_VALUE = "orlv";

    const OPTION_UPDATE_DATA_ONLY = "oudo";

    const OPTION_IS_GUNBUILDER = "oig";

    const OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE = "oprs";
    const VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH = "vprcfp";

    const VALUE_DEFAULT_OPTICS_ID = 1000001;
    const VALUE_DEFAULT_FOREGRIPS_ID = 1000002;
    const VALUE_DEFAULT_FLASHLIGHTS_ID = 1000003;
    const VALUE_DEFAULT_MAGAZINES_ID = 1000004;
    const VALUE_DEFAULT_STOCK_ID = 1000005;
    const VALUE_DEFAULT_HANGUARDS_ID = 1000006;
    const VALUE_DEFAULT_PECBOX_ID = 1000007;
    const VALUE_DEFAULT_FULL_ID = 1000008;

    public function __construct($name = null, $cat_id = null, $options = array(),&$dbVals = array()){

        if($name == null && $cat_id == null)
            return;

        $this->cat_id = $cat_id;
        $this->name = $name;

        $this->app = realpath(dirname(__FILE__));
        $this->options = $options;
        if(! empty($options[self::OPTION_RELEASE_FOLDER_VALUE])){
            //we dont need builds folder for release!
            $this->base             = $options[self::OPTION_RELEASE_FOLDER_VALUE].$name;
        }
        else{
            $this->base            = $this->app.'\\builds\\'.$name;
        }
        $this->dataPath         = $this->app.'\\assets\\data\\'.$name.'\\'.$name.'.json';
        $this->imgSource        = $this->app.'\\assets\\Dropbox\\customizer';
        $this->imgCustomSource  = $this->app.'\\assets\\Dropbox\customizer\\'.$name;
        $this->imgPath          = $this->base .'\\media';
        $this->svgPath          = $this->base .'\\media\\svg\\';
        $this->bmpPath          = $this->base .'\\media\\bmp\\';
        $this->resizePath       = $this->base .'\\media\\resized\\';

        $this->file = @file_get_contents($this->dataPath)?:'[]';
        $this->json = json_decode($this->file);

        // if(! empty($options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH])){
        //     $cats = preg_replace('/\n*\r*/','',file_get_contents($options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH]));
        // }
        // else{
            $cats = preg_replace('/\n*\r*/','',file_get_contents($this->base.'\\config.js'));
        //}
        $cats = substr($cats, stripos($cats,'var Platform = ')+15,  -1 );
        $this->categories = json_decode($cats);

        if (empty($this->categories)){
            throw new Exception("PLEASE CHECK config.js!!!");
        }

        $this->Config = $this->categories; //backwards compatibility -- must rename values in gunbuilder later

        // Create connection
        $this->db = $dbVals;
        // Check connection
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    public function setup(){
        //if folders do not exist then create them
        if(!is_dir($this->imgPath))mkdir($this->imgPath);
        if(!is_dir($this->svgPath))mkdir($this->svgPath);
        if(!is_dir($this->svgPath.'resized\\'))mkdir($this->svgPath.'resized\\');
        if(!is_dir($this->resizePath))mkdir($this->resizePath);
        if(!is_dir($this->bmpPath))mkdir($this->bmpPath);
        if(!is_dir($this->bmpPath.'resized\\'))mkdir($this->bmpPath.'resized\\');
    }

    protected function setSizeOfProduct($item,$offsets = array()){
        $name = $this->getImageName($item);
        $img = $this->resizePath.$name.'.png';

        if(is_file($img)){
            list($width,$height) = getimagesize($img);
            $item->dimensions = array('width'=>$width,'height'=>$height);
        }

        if(! empty($offsets)){
            $item->dimensions["offsetX"] = $offsets["offsetX"];
            $item->dimensions["offsetY"] = $offsets["offsetY"];
        }
        return $item;
    }

    /**
     * rebuild will update existing config.js file\
     * @param $name Name of a gun (folder name)
     * @param $id category id
     */
    public static function rebuildCategories($name, $id,$path,$options,&$dbVals){
        $json = json_decode('{ "name": "' . $name . '", "upsellCategoryId": 0, "Build": [], "MasterPosition":{}, "Position":{}}');


        $oldJson = preg_replace('/\n*\r*/','',file_get_contents($buildPath .'config.js'));
        $oldJson = substr($oldJson, stripos($oldJson,'var Platform = ')+15,  -1 );
        $oldJson = json_decode($oldJson);

        $gun = new Configurator($name,$id,$options,$dbVals);
        $categories = $gun->getCategoriesFromDB($id);
        foreach ($categories as $c) {

            $c['id'] = (int)$c['id'];
            $c['base_id'] = (int)$c['base_id'] ?: null;
            $c['item_id'] = null;
            $c['requiredForCheckout'] = false;

            if (strtolower($c['category_name']) == 'accessories') {
                $json->upsellCategoryId = $c['id'];
            } else {
                $json->Build[] = $c;
            }
        }

        $json->MasterPosition = $oldJson->MasterPosition;
        $json->Position = $oldJson->Position;

        //connect master position with positions
        foreach($oldJson->Build as $oldBuild){
            //dump($oldBuild);
            foreach($json->Build as &$newBuild){
                //dump($newBuild);
                if($oldBuild->id == $newBuild["id"]){
                    if(! empty($oldBuild->position)) 
                        $newBuild["position"] = $oldBuild->position;
                    if(! empty($oldBuild->imgResized)) 
                        $newBuild["imgResized"] = $oldBuild->imgResized;
                }
            }
        }
        $pretty_json = $gun->prettyPrint(json_encode($json));
        file_put_contents($buildPath . 'config.js', 'var Platform = ' . $pretty_json . ';');
    }

    function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                $this->utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
    }

    public function updateData(){
        $this->json = array();

        $products = $this->getProductsFromDB();
        //$itemId = $product["product_id"];
        $itemId = $this->name;
        foreach($products as $product){
            if($product["product_id"] == $this->name){
                $itemId = $product["item_id"];
            }
        }
        var_dump("itemId ==> ".$itemId);
        if(empty($itemId)){
            throw new Exception("please check script: ERROR:A -> Cannot locate core part id:");
        }

        //gather all images prefixed with the item id and create items from them using duplicate info
        $accessories = $this->getDefaultAccessories($itemId);
        //create new items from accessories cloning item
        foreach($accessories as $a) {
            if(!is_null($a->category)) {
                $core = (object)$products[$itemId];
                $products[$a->item_id] = $this->clonePart($a, $core);
            }
        }

        $positionArr = array();
        $masterPositionArr = array();

        foreach($products as $item){

            $item = (object)$item;

            $item->item_id = (int)$item->item_id;
            $item->product_id = (int)$item->product_id;
            $item->category  = (int)$item->category;
            $item->scaleFactor  = (int)$item->scaleFactor;
            $item->price = (float)$item->price;
            $item->out_of_stock = strtolower($item->out_of_stock) == 'true';
            $item->hidden = $item->hidden == 1;
            //skip hidden items
            if($item->hidden == true) continue;
            $item->new = (strtotime($item->new) > time()) ? true : false;

            if($item->prohibit_products[0] == '' || $item->item_id == $itemId){
                $item->prohibit_products = null;
            }else{
                foreach($item->prohibit_products as $key=>$val){
                    $item->prohibit_products[$key] = (int)$val;
                }
            }

            //resize the image, then get the size of the resized image put within object
            if($item->category != $this->categories->upsellCategoryId){

                $name = $this->getImageName($item);
                $this->moveImage($name);

                $this->resizePNG($name);

                //find offsets
                //$offsets = $this->findOffsets($name);

                //$item = $this->setSizeOfProduct($item,$offsets);
                $item = $this->setSizeOfProduct($item);
                $item = $this->setSVG($item);
                //die();
            }
                unset($item->masterPosition);
           
                unset($item->position);

            $this->json[] = $item;
        }

        //use master positions and posistion from prev config file!
        if(! empty($this->options[self::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE]) && ! empty($this->options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH])){
            $oldJson = preg_replace('/\n*\r*/','',file_get_contents($this->options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH]));
            $oldJson = substr($oldJson, stripos($oldJson,'var Platform = ')+15,  -1 );
            $oldJson = json_decode($oldJson);

            if(empty($oldJson) || $oldJson == false) {
                dump($this->$options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH]);
                throw new Exception("PLEASE CHECK OLD RELEASE CONFIG FILE");
            }

            $this->Config->MasterPosition =  $oldJson->MasterPosition;
            $this->Config->Position = $oldJson->Position;
            if(! empty($oldJson->offset)){
                $this->Config->offset = $oldJson->offset;
            }

            //connect master position with positions
            foreach($oldJson->Build as $oldBuild){
                //dump($oldBuild);
                foreach($this->Config->Build as &$newBuild){
                    //dump($newBuild);
                    if($oldBuild->id == $newBuild->id){
                        if(! empty($oldBuild->position)) 
                            $newBuild->position = $oldBuild->position;
                        if(! empty($oldBuild->imgResized)) 
                            $newBuild->imgResized = $oldBuild->imgResized;
                    }
                }
            }
        }

        $this->setConfigDefaults();
        $this->finishFile('parts3');
    }

    public function fixConfigFile(){
        //STEP 1
        $keysArr = array();
        $valsArr = array();
        foreach($this->Config->MasterPosition as $key => $value){
            $keysArr[] = $key;
            foreach($value as $tKey => $tVal){
                $valsArr[$tKey] = $tVal;
            };
        }

        foreach($this->Config->MasterPosition as $key => $value){
            $temp;
            foreach($valsArr as $tKey => $element){
                $temp = $element;
                $temp->item_id = $key;
            }
            $this->Config->MasterPosition[$key][$tKey] = $temp;
        }
    }

    public function removeWorkDirectories(){
        $this->recursiveRemoveDirectory($this->svgPath);
        $this->recursiveRemoveDirectory($this->bmpPath);
    }

    protected function recursiveRemoveDirectory($directory)
    {
        foreach(glob("{$directory}/*") as $file)
        {
            if(is_dir($file)) {
                $this->recursiveRemoveDirectory($file);
            } else {
                unlink($file);
            }
        }
        rmdir($directory);
    }

    protected static function prettyPrint( $json )
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen( $json );

        for( $i = 0; $i < $json_length; $i++ ) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if( $ends_line_level !== NULL ) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ( $in_escape ) {
                $in_escape = false;
            } else if( $char === '"' ) {
                $in_quotes = !$in_quotes;
            } else if( ! $in_quotes ) {
                switch( $char ) {
                    case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                    case '{': case '[':
                    $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
                }
            } else if ( $char === '\\' ) {
                $in_escape = true;
            }
            if( $new_line_level !== NULL ) {
                $result .= "\n".str_repeat( "\t", $new_line_level );
            }
            $result .= $char.$post;
        }

        return $result;
    }

    public function findOffsets($name){
        $imgFile = $this->imgPath."\\".$name.'.png';
        $image = imagecreatefrompng($imgFile);
        $background = imagecolorat($image, 0, 0);

        $imageHeight = imageSY($image);
        $imageWidth = imageSX($image);

        $minX = $imageWidth;
        $minY = $imageHeight;
        for($x = 0; $x < $imageWidth; $x++){
            for($y = 0; $y < $imageHeight; $y++){
                if(imagecolorat($image, $x, $y) != $background) {
                    if ($x < $minX){
                        $minX = $x;
                    }
                    if ($y < $minY){
                        $minY = $y;
                    }
                }
            }
        }
        return array("offsetX"=> $minX,"offsetY" => $minY);


    }

    protected function finishFile($name){
        //$this->utf8_encode_deep($this->json);
        //dump($this->json);
        $jsonString = json_encode($this->json);
        $error = "";
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                $this->utf8_encode_deep($this->json);
                $jsonString = json_encode($this->json);
            break;
        }
        //if(! empty($error)) throw new Exception("UNABLE TO ENCODE ARRAY : error message {$error}");
        
        file_put_contents($this->base.DS.$name.'.js','var Parts = '.$jsonString);
    }

    public function updateImages(){
        foreach($this->json as $item){

            //resize the image, then get the size of the resized image put within object
            if($item->render_img == 'blank.png')continue;

            $this->resizePNG($item);
            $item = $this->setSizeOfProduct($item);
            $item = $this->setSVG($item);


            //var_dump($item);
        }

        $this->finishFile('parts2');
    }

    public function updateSVG($item){
        //$svg_file = $this->svgPath . substr($item->render_img, strrpos($item->render_img, '/')+1, -4).'.svg' ;
        $item->svg = new stdClass();
        $name = $this->getImageName($item);

        $svg = $this->svgPath.$name.'.svg';
        $resized = $this->svgPath.'resized\\'.$name.'.svg';
        if(is_file($svg)){
            $xml = simplexml_load_file ($svg);
            $item->svg->full = (string)$xml->g->path['d'];
        }
        if(is_file($resized)){
            $xml = simplexml_load_file ($resized);
            $item->svg->resized = (string)$xml->g->path['d'];
        }
        return $item;
    }

    public function clonePart($accessory,$item){
        $clone = clone $item;
        $clone->render_img = $accessory->img;
        $clone->item_id = $accessory->item_id;
        $clone->category = $accessory->category;
        $clone->price = 0;
        //set prohib products only for foregrips
        if($clone->item_id != self::VALUE_DEFAULT_HANGUARDS_ID){
            $clone->prohibit_products = array();
        }
        return $clone;
    }

    public function getProductsFromDB(){
        $products = array();

        $joinStr = (! empty($this->options[self::OPTION_IS_GUNBUILDER]) && $this->options[self::OPTION_IS_GUNBUILDER] == true) ? "LEFT JOIN gunbuilder gb ON gb.id = i.product_id" : "LEFT JOIN customizer gb ON gb.id = i.product_id";

        $sql = "SELECT DISTINCT i.id AS 'item_id',
                        i.product_id,
                        i.sku,
                        xref.`category_id` AS category,
                        TRUNCATE(pp.price - (pp.price *(p.on_sale/100)), 2) AS price,
                        gb.brand,
                        gb.`incompatible` AS 'prohibit_products',
                        CONCAT(i.id,'.png') AS 'render_img',
                        gb.name AS 'product_name',
                        gb.features AS 'short_description',
                        IF(i.inventory > 0, 'false', 'true' ) AS 'out_of_stock',
                        gb.masterPosition,
                        gb.position,
                        gb.scale as scaleFactor,
                        gb.hidden as hidden,
                        gb.new as new
        FROM categories_xref xref
        LEFT JOIN products p ON xref.product_id = p.id
        LEFT JOIN items i ON i.`product_id` = xref.`product_id`
        {$joinStr}
        LEFT JOIN `productpricings` pp ON i.product_id = pp.id
        WHERE category_id IN ( SELECT id FROM categories WHERE parent_id = {$this->cat_id} )
        AND pp.quantity = 1
        AND p.merchandisable = 1
        ";
        var_dump($sql);
        //selects all products from database
        $result = $this->db->query($sql);

        if($this->db->error) echo  $this->db->error;

        while($row = $result->fetch_assoc()) {
            $products[$row["item_id"]] = $row;
            $products[$row["item_id"]]['prohibit_products'] = explode(',',$products[$row["item_id"]]['prohibit_products']);

            //if image has multiple states
        }

        return $products;
    }
    protected function getImageName($item){
        return substr($item->render_img, 0,-4);
        //return $item->item_id;
    }
    protected function getCategoriesFromDB($id){

        $sql = "SELECT DISTINCT c.id, i.id base_id, c.name category_name FROM categories c
                LEFT JOIN categories_xref xref ON c.`id` = xref.`category_id`
                LEFT JOIN items i ON i.product_id = xref.`product_id`
                WHERE parent_id = $id
                GROUP BY c.id
                ORDER BY FIELD(`category_name`,'Bipods and Foregrips','Handguards','Stocks','Core' ) DESC";
        dump($sql);
        $result = $this->db->query($sql);

        $categories = array();

        while($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }

        return $categories;
    }

    public function buildCategories($updateOnly = false){

        $json = json_decode('{ "name": "'.$this->name.'", "upsellCategoryId": 0, "Build": [], "MasterPosition":{}, "Position":{}}');

        //on update we need to have old masterPosition and Position

        $categories= $this->getCategoriesFromDB($this->cat_id);

        foreach($categories as $c){

            $c['id'] = (int)$c['id'];
            $c['base_id'] = (int)$c['base_id'];
            $c['item_id'] = null;
            $c['requiredForCheckout'] = false ;


            if(strtolower($c['category_name'])=='accessories') {
                $json->upsellCategoryId = $c['id'];
            }else{
                $json->Build[] = $c;
            }
        }

        $this->saveConfigFile($json);
    }

    private function setConfigDefaults(){
        $accessories = array_filter($this->json, function($item){return $item->item_id >= 1000000;});

        foreach($accessories as $default){
            foreach($this->Config->Build as &$build_item) {
                if($build_item->id == $default->category){
                    $build_item->base_id = $default->item_id;
                }
            }
        }

        $this->saveConfigFile($this->Config);
    }

    public function saveConfigFile($json){
        $pretty_json = $this->prettyPrint(json_encode($json));
        file_put_contents($this->base.DS.'config.js','var Platform = '.$pretty_json.';');
    }

    public function resetConfigDefaults(){
        foreach($this->Config->Build as $category){
            $this->removeConfiguredProducts($category);
        }
        $this->saveConfigFile($this->Config);
        //var_dump($this->Config->Build);
    }
    private function removeConfiguredProducts($category){
        if($category->category_name == 'Core' || $category->base_id >= 1000000)
            $category->item_id = $category->base_id;
        else
            $category->item_id = null;
    }

    protected function moveImage($name){
        if(is_file($this->imgCustomSource.DS. $name .'.png')){
            var_dump("MOVE IMAGE ". $this->imgCustomSource.DS. $name .'.png');
            copy($this->imgCustomSource.DS. $name .'.png', $this->imgPath.DS. $name .'.png');
        }else{
            var_dump("2");
            copy($this->imgSource.DS. $name .'.png', $this->imgPath.DS. $name .'.png');
        }
    }
    protected function resizePNG($name){
        //$name = $this->getImageName($name);
        $img = $this->imgPath.DS. $name .'.png';
        $img2 = $this->resizePath.$name.'.png';
        //var_dump($img);
        //var_dump($img2);
        if(is_file($img)){
            //change directory;
            chdir('c:\Program Files\ImageMagick-6.9.1-Q16');
            $cmd = 'convert '.$img.' -trim -bordercolor transparent +antialias +repage '.$img2.' 2>&1';
            system($cmd);
        }
    }

    protected function setSVG($item){
        $name = $this->getImageName($item);

        if($name == 'blank')return $item;

        $isResized = true;

        //resized svg
        $this->overExposePNG($name, $isResized);
        $this->createSVGFromBMP($name, $isResized);

        //regular svg
        $this->overExposePNG($name);
        $this->createSVGFromBMP($name);

        $tItem = $this->updateSVG($item);
        return $tItem;
    }

    private function overExposePNG($name, $useResize=false){
        if($useResize){
            $cmd = 'convert '.$this->resizePath . $name. '.png -alpha extract -threshold 1% -negate -transparent white '.$this->bmpPath . 'resized\\'. $name.'.bmp';
        }else{
            $cmd = 'convert '.$this->imgPath .DS. $name. '.png -alpha extract -threshold 1% -negate -transparent white '.$this->bmpPath. $name.'.bmp';
        }
        return system($cmd,$retval);
    }
    private function createSVGFromBMP($name, $useResize=false){
        chdir('C:\\xampp\\_support\\potrace');
        $cmd = 'potrace '. $this->bmpPath . (($useResize)? 'resized\\':''). $name.'.bmp -s -o '. $this->svgPath.(($useResize)? 'resized\\':'') .$name.'.svg';
        //var_dump($cmd);
        system($cmd,$retval);
        return;
    }

    private function getDefaultAccessories($item_id){
        $accessories = new stdClass();
        $images = preg_grep('/^'.$item_id.'-.*\.(png)$/', scandir($this->imgSource));
        $categories = array('sto'=>'stock','han'=>'handguards','mag'=>'magazines','box'=>'pec box','opt'=>'optics', 'for'=>'foregrip', 'full'=>'full', 'fla'=>'flashlight');

        $i = 0;

        foreach($images as $img){
            $img_name = substr($img, 0,-4);
            $split = explode('-',$img_name);
            $name = $categories[$split[1]];

            if($name === null) throw new Exception('Category "'.$split[1].'" not supported'. " IMAGE ==> {$img_name}");

            switch($split[1]){
                case "sto":
                    $presetId = self::VALUE_DEFAULT_STOCK_ID;
                    break;
                case "han":
                    $presetId = self::VALUE_DEFAULT_HANGUARDS_ID;
                    break;
                case "mag":
                    $presetId = self::VALUE_DEFAULT_MAGAZINES_ID;
                    break;
                case "box":
                    $presetId = self::VALUE_DEFAULT_PECBOX_ID;
                    break;
                case "opt":
                    $presetId = self::VALUE_DEFAULT_OPTICS_ID;
                    break;
                case "for":
                    $presetId = self::VALUE_DEFAULT_FOREGRIPS_ID;
                    break;
                case "full":
                    $presetId = self::VALUE_DEFAULT_FULL_ID;
                    break;
                case "fla":
                    $presetId = self::VALUE_DEFAULT_FLASHLIGHTS_ID;
                    break;
                default:
                    throw new Exception("UNABLE TO LOCATE DEFAULT ID FOR ITEM {$split[1]}");
                    break;
            }

            $accessories->{$split[1]} = (object) array('img' => $img, 'item_id' => $presetId, 'category' => $this->getCategoryIdByName($name));

        }
        return $accessories;
    }

    private function getCategoryIdByName($name){
        foreach($this->Config->Build as $category){
            if(stripos(strtolower($category->category_name),$name)!==false){
                return $category->id;
            }
        }
    }
}
