<?php
/**
 * Created by PhpStorm.
 * User: abstraktron
 * Date: 9/25/2015
 * Time: 11:45 AM
 */
//include_once('configurator.php');

class Master
{
    const GENERATION_CONFIG_FILE = "config.conf";

    /**
     * OPTIONS
     */
    const OPTION_BUILD_FULL_RELEASE = "obfr";
    const OPTION_BUILD_PARTS3_RELEASE = "obpr";
    const OPTION_CREATE_BUILDS_FROM_CATEGORIES = "oub";
    const OPTION_PREVIOUS_RELEASE_STRING = "oprs";
    const OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE = "oprs";
    const OPTION_BUILD_ARRAY_ONLY = "oba";
    const OPTION_UPDATE_ATTRIBUTE_COLUMN = "ouac";
    const OPTION_DONT_RESET_CONFIG_FILE ="odrcf"; //dont touch config file

    const OPTION_IS_GUNBUILDER = "oig";


    //generates in process of execution
    const VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH = "vprcfp";
    const VALUE_CATEGORY_PARENT_ID = "vcpi";
    const VALUE_CURRENT_RELEASE_STRING = "vcrs";

    public function __construct($generation = "",$dbVals = array(),$required = true)
    {
        if(empty($dbVals) && $required) return;
        $this->app = realpath(dirname(__FILE__));
        $this->buildPath = $this->app . '\\builds\\';

        if($required){
            $this->db = new mysqli($dbVals->hostname , $dbVals->username , $dbVals->password , $dbVals->database);
       
            // Check connection
            if ($this->db->connect_error) {
                die("Connection failed: " . $this->db->connect_error);
            }
        }

        /**
         * will save and use release info
         */
        if(! empty($generation)){
            $this->generationFolder = $this->app."\\release\\".$generation."\\";

            if(! file_exists($this->generationFolder)){
                mkdir($this->generationFolder);
            }

            $this->generationConfigFile = $this->generationFolder.self::GENERATION_CONFIG_FILE;

            if(file_exists($this->generationConfigFile)){
                $configArr = json_decode(file_get_contents($this->generationConfigFile),true);
                $this->lastReleaseDate = $configArr["lastReleaseDate"];

                if (! empty($configArr["builds"])){
                    $this->generationInfo = array("builds" => $configArr["builds"], "lastReleaseDate" => $configArr["lastReleaseDate"], "releases" => $configArr["releases"]);
                }             
            }

            //use release folder for builds
            $this->buildPath = $this->generationFolder;
        }
    }

    public function listAllBuilds($local = false)
    {

        $sql = "SELECT id, name FROM categories
                    WHERE parent_id = 478
                    ORDER BY CAST(name AS UNSIGNED)";
        $result = $this->db->query($sql);

        $categories = array();

        while ($row = $result->fetch_assoc()) {
            if (!$local) echo '<a href="http://www.airsoft.sandbox.orderdynamics.net/' . $row['name'] . '-admin#">http://www.airsoft.sandbox.orderdynamics.net/' . $row['name'] . '-admin#</a></br>';
            else echo '<a href="http://localhost/configurator/configurator/builds/' . $row['name'] . '/update.php">http://localhost/configurator/configurator/builds/' . $row['name'] . '/update.php</a></br>';
        }

    }


    public function updateAllBuilds()
    {
        if(! empty($this->options[self::OPTION_BUILD_ARRAY_ONLY])){
            $only = $this->options[self::OPTION_BUILD_ARRAY_ONLY];
        }
        else if(! empty($this->generationInfo)){
            $only = $this->generationInfo["builds"];
        }
        // if (count($only)) {
        //     $sql = "SELECT id, name FROM categories
        //             WHERE parent_id = 478 AND name IN (" . implode(',', $only) . ")
        //             ORDER BY CAST(name AS UNSIGNED)";
        // } else if (count($notThese)) {
        //     $sql = "SELECT id, name FROM categories
        //             WHERE parent_id = 478 AND name NOT IN (" . implode(',', $only) . ")
        //             ORDER BY CAST(name AS UNSIGNED)";
        // } else {
        //     $sql = "SELECT id, name FROM categories
        //             WHERE parent_id = 478
        //             ORDER BY CAST(name AS UNSIGNED)";
        // }
        var_dump($only);
        $whereStr = $this->formatArrayForQuery($only);
        $sql = "SELECT id, name FROM categories
                WHERE parent_id = {$this->options[self::VALUE_CATEGORY_PARENT_ID]} AND {$whereStr}
                ORDER BY CAST(name AS UNSIGNED)";

        var_dump($sql);
        $result = $this->db->query($sql);
        $categories = array();
        $resultArr = array("success" => [], "failed" => []);

        while ($row = $result->fetch_assoc()) {
            echo '<h2><a href="http://www.airsoft.sandbox.orderdynamics.net/' . $row['name'] . '-admin#">' . $row['name'] . '</a></h2></br>';

            try {

                //$options = array();
                if (! empty($this->generationFolder)){
                    $this->options[Configurator::OPTION_RELEASE_FOLDER_VALUE] = $this->buildPath;
                }

                if(! empty($this->options[self::OPTION_USE_PREVIOUS_RELEASE_CONFIG_FILE])){
                    //we need to find that file!
                    $this->options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH] = $this->findPrevReleaseFilePath($row["name"],"config.js");
                    //$options[self::VALUE_PREVIOUS_RELEASE_CONFIG_FILE_PATH] = "C:\\xampp\\htdocs\\intranet\\application\\libraries\\customizer\\configurator\\builds\\{$row["name"]}\\config.js";
                }
                $gun = new Configurator($row['name'], $row['id'],$this->options,$this->db);
                $gun->setup();

                $gun->updateData();
                
                $gun->removeWorkDirectories();
                if(empty($options[self::OPTION_DONT_RESET_CONFIG_FILE])){
                    $gun->resetConfigDefaults();
                }

                $resultArr["success"][] = $row['name'];
            } catch (Exception $e) {
                echo <<<HEREDOC
                        <div id="color:red">Caught exception:  {$e->getMessage()}</div> \n
HEREDOC;
                $resultArr["failed"][] = array($row["name"] => $e->getMessage());
            }
        }

        return $resultArr;
    }

    public function showResults($results)
    {

        if (empty($results["failed"])) {
            echo "NO ERRORS FOUND! \n<br/>";
        } else {
            echo "ERRORS!!! \n";
            foreach ($results["failed"] as $result) {
                var_dump($result);
            }
            echo "-------------------------------------------\n<br/>";
        }

        echo "SUCCESS \n<br/>";
        foreacH($results["success"] as $result){
            var_dump($result);
        }
        echo "-------------------------------------------\n<br/>";
        echo "DONE!";
    }

    /**
     * help method to create proper querey 'where' statement
     */
    public function formatArrayForQuery($only){
        $numericArr = array();
        $queryAddStr = "";
        foreach($only as $element){
            if(is_numeric($element)){
                $numericArr[] = $element;
            }
            else{
                if(! empty($queryAddStr)) $queryAddStr .=  " OR "; 
                $queryAddStr .= "name like '{$element}'";
            }
        }

        if(! empty($numericArr)){
            if(! empty($queryAddStr)) $queryAddStr .=  " OR ";
            $queryAddStr .= "name IN (" . implode(',', $numericArr) . ")";
        }
        return $queryAddStr;
       
    }


    public function createBuildsFromCategories($only = array())
    {
        if(empty($only) && ! empty($this->generationInfo)){
            $only = $this->generationInfo["builds"];
        }
        //$sql = "SELECT id, name FROM categories WHERE parent_id = 478 AND name IN (" . implode(',', $only) . ")";
        $whereStr = $this->formatArrayForQuery($only);
        $sql = "SELECT id, name FROM categories WHERE parent_id = {$this->options[self::VALUE_CATEGORY_PARENT_ID]} AND {$whereStr}";

        $result = $this->db->query($sql);

        $categories = array();
        while ($row = $result->fetch_assoc()) {
            var_dump($this->buildPath . '\\' . $row['name']);
            //create new directory in build
            if (!is_dir($this->buildPath . '\\' . $row['name'])) {
                $this->createDirectory($row['name']);
                $this->createUpdateFile($row['name'], $row['id']);
                $this->buildCategories($row['name'], $row['id']);
            }
            else{
                DIE("ASDASD");
                $path = $this->buildPath . '\\' . $row['name']."\\";
                configurator::rebuildCategories($row['name'], $row['id'],$path,$this->options,$this->db);
            }
            echo '<a href="http://localhost/configurator/configurator/builds/' . $row['name'] . '/update.php">' . $row['name'] . '</a> - <a href="http://www.airsoft.sandbox.orderdynamics.net/' . $row['name'] . '-admin#">' . $row['name'] . '</a></br>';
        }

        return $categories;

    }

    protected function getCategoriesFromDB($id)
    {

        //$groupString = (! empty($this->options[self::OPTION_IS_GUNBUILDER]) && $this->options[self::OPTION_IS_GUNBUILDER] == true) ? "" : "ORDER BY FIELD(`category_name`,'Bipods and Foregrips','Handguards','Stocks','Core' ,'Receiver','Forend') DESC";

        $sql = "SELECT DISTINCT c.id, i.id base_id, c.name category_name FROM categories c
                LEFT JOIN categories_xref xref ON c.`id` = xref.`category_id`
                LEFT JOIN items i ON i.product_id = xref.`product_id`
                WHERE parent_id = $id
                GROUP BY c.id
                ORDER BY FIELD(`category_name`,'Bipods and Foregrips','Handguards','Stocks','Core' ,'Receiver','Forend') DESC"; 
        $result = $this->db->query($sql);

        $categories = array();

        while ($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }

        return $categories;
    }

    private function buildCategories($name, $id)
    {
        $json = json_decode('{ "name": "' . $name . '", "upsellCategoryId": 0, "Build": [], "MasterPosition":{}, "Position":{}}');

        $categories = $this->getCategoriesFromDB($id);

        //get all default images for this item id
        //include default images in loop.
        //shrink default images in configurator
        //create svg for defaults in configurator

        foreach ($categories as $c) {

            $c['id'] = (int)$c['id'];
            $c['base_id'] = (int)$c['base_id'] ?: null;
            $c['item_id'] = null;
            $c['requiredForCheckout'] = false;

            if (strtolower($c['category_name']) == 'accessories') {
                $json->upsellCategoryId = $c['id'];
            } else {
                $json->Build[] = $c;
            }
        }

        $pretty_json = $this->prettyPrint(json_encode($json));

        file_put_contents($this->buildPath . $name . '\\config.js', 'var Platform = ' . $pretty_json . ';');
    }

    private function createDirectory($name)
    {
        if ($name)
            mkdir($this->buildPath . $name);
    }

    private function createUpdateFile($name, $id)
    {
        $tmpl = file_get_contents($this->app . '\\update.tmpl');
        $edited = sprintf($tmpl, $name, $id);
        file_put_contents($this->buildPath . $name . '\\update.php', $edited);
    }

    private static function prettyPrint($json)
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if ($ends_line_level !== NULL) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ($in_escape) {
                $in_escape = false;
            } else if ($char === '"') {
                $in_quotes = !$in_quotes;
            } else if (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== NULL) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        return $result;
    }

    /**
     * method will release all parts3.js files into release folder
     */
    public function release($releaseStr,$options = array()){
        //var_dump($this->generationInfo);
        if(empty($this->generationInfo)){
            die("RELEASE CONFIG FILE IS EMPTY! / NO RELEASE INFO");
        }

        if(empty($this->generationInfo["builds"])){
            die("NO BUILDS FOUND!");
        }

        $releaseFolder = $this->generationFolder.$releaseStr."\\";

        $options[self::VALUE_CURRENT_RELEASE_STRING] = $releaseStr;
        $this->buildPath = $releaseFolder;

        //save options in class
        $this->options = $options;

        if(! empty($options[self::OPTION_IS_GUNBUILDER]) && $options[self::OPTION_IS_GUNBUILDER] == true){
            $this->options[self::VALUE_CATEGORY_PARENT_ID] = 353;
        }
        else{
            $this->options[self::VALUE_CATEGORY_PARENT_ID] = 478;
        }

        if(! file_exists($releaseFolder) && empty($options[self::OPTION_BUILD_PARTS3_RELEASE])){
            mkdir($releaseFolder);
        }

        if(! empty($options[self::OPTION_BUILD_ARRAY_ONLY]) && !is_array($options[self::OPTION_BUILD_ARRAY_ONLY])){
            $options[self::OPTION_BUILD_ARRAY_ONLY] = array($options[self::OPTION_BUILD_ARRAY_ONLY]);
        }

        if(! empty($options[self::OPTION_CREATE_BUILDS_FROM_CATEGORIES]) && $options[self::OPTION_CREATE_BUILDS_FROM_CATEGORIES] == true){
            if (! empty($options[self::OPTION_BUILD_ARRAY_ONLY])){

                $this->createBuildsFromCategories($options[self::OPTION_BUILD_ARRAY_ONLY]);
            }
            else{
                $this->createBuildsFromCategories();
            }   
        }
        //will build everything
        if (! empty($options[self::OPTION_BUILD_FULL_RELEASE]) || ! empty($options[self::OPTION_BUILD_ARRAY_ONLY])){
            $result = $this->updateAllBuilds();
            //$this->listAllBuilds(false);

            $this->showResults($result);
        }
        else{
            //only updates
            $changes = array();
            foreach($this->generationInfo["builds"] as $build){
                try{
                    if (! empty($options[self::OPTION_BUILD_PARTS3_RELEASE])){
                        $changes[] = array_values($this->updateParts3file($build));
                    }
                    else if (! empty($options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN])){
                        $changes[] = array_values($this->updateAttributeForParts3File($build));
                    }
                } catch (Exception $e) {
                    echo "ERROR!!!\n";
                    var_dump($e->getMessage());
                    echo "------------------------------\n";
                }
            }
        }

        $changes = array_filter($changes);
            //save last release date and update config file;
        $curretTimeStr = $releaseStr;
        $this->generationInfo["releases"][$releaseStr][]= array("DATE" => $curretTimeStr);
        if(! empty($changes) && $changes != null){
            $this->generationInfo["releases"][$releaseStr]["CHANGES"] =  $changes;
            $this->generationInfo["lastReleaseDate"] = $curretTimeStr;
        }
        else{
            $this->generationInfo["releases"][$releaseStr]["CHANGES"] = "NO CHANGES";
        }

        file_put_contents($this->generationConfigFile, $this->prettyPrint(json_encode($this->generationInfo)));
        var_dump("results");
        dump($this->generationInfo["releases"][$releaseStr]["CHANGES"]);

    }

    /**
     * will loop through the versions to get last changed parts3 file
     */
    public function findPrevPartsFilePath($build){
        $reversedReleasesArr = array_reverse($this->generationInfo["releases"]);
        foreach($reversedReleasesArr as $key => $value){
            if(file_exists($this->generationFolder.$key."\\".$build."\\parts3.js") && $key != $this->options[self::VALUE_CURRENT_RELEASE_STRING])
                return $this->generationFolder.$key."\\".$build."\\parts3.js";
        }
    }

    /**
    * will loop through the versions to get last changed file
    * NOTE: method the same as findPrevPartsFilePath
     */
    public function findPrevReleaseFilePath($build,$fileName){
        $reversedReleasesArr = array_reverse($this->generationInfo["releases"]);
        foreach($reversedReleasesArr as $key => $value){
            if(file_exists($this->generationFolder.$key."\\".$build."\\{$fileName}") && $key != $this->options[self::VALUE_CURRENT_RELEASE_STRING])
                return $this->generationFolder.$key."\\".$build."\\{$fileName}";
        }
    }

    /**
     * method will rebuild parts3 file based on new OD (customizer table)
     * HASNT BEEN TESTED!
     */
    public function updateAttributeForParts3File($build){

        DIE("NOT TESTED!");

        if (empty($this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]))
            throw new Exception("Attribute is UNDEFINED");


        //check if attribue exists!
        $queryStr = <<<HEREDOC
        SHOW columns FROM customizer
HEREDOC;
        
        $columns = array();
        $rez = $this->db->query($queryStr);
        $match = false;
        //dump($options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]);
        while($row = $rez->fetch_assoc()) {
            if($row["Field"] == $this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]) $match = true;
            //dump($row);
        }

        if (!$match) 
            throw new Exception("Attribute IS NOT FOUND");         

        $partsFilePath = $this->findPrevPartsFilePath($build);

        $partsFile = preg_replace('/\n*\r*/','',file_get_contents($partsFilePath));
        $partsJson = substr($partsFile, stripos($partsFile,'var Parts = ')+12);
        $partsArr = json_decode($partsJson,true);

        $productsArr = array();
        foreach($partsArr as $part){
            $productsArr[] = $part["product_id"];
        }

        $inStr = implode(',',$productsArr);
        $queryStr = <<<HEREDOC
            select * from customizer where id in ($inStr)
HEREDOC;

        $result = $this->db->query($queryStr);
        if($this->db->error) throw new Exception($this->db->error);

        $wasUpdated = false;

        if(! empty($this->options["force_release"]) && $this->options["force_release"] === true){
            $wasUpdated = true;
        }

        while($row = $result->fetch_assoc()) {
            foreach ($partsArr as &$part) {                
            if($part["product_id"] == $row["id"]){
                    if($part[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]] != $row[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]]){
                        $changes[] = "Attribute ({$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]}) was changed from {$part[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]]}".
                            "to {$row[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]]} for item id ====> {$this->part["item_id"]} : build => {$build}";

                        $part[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]] = $row[$this->options[self::OPTION_UPDATE_ATTRIBUTE_COLUMN]];
                        $wasUpdated = true;
                    }
                }
            }
        }

        if($wasUpdated){
            if(! file_exists($this->buildPath)){
                mkdir($this->buildPath);
            }
            //save release file
            if(! file_exists($this->buildPath.$build)){
                mkdir($this->buildPath.$build);
            }
            file_put_contents($this->buildPath.$build."\\parts3.js",'var Parts = '.json_encode($partsArr));
        }

        if (empty($changes)){
            $changes = "Everything is up-to-date!";
        }

        return $changes;


    }

    /**
     * method will update only updateParts3 (price and inventory)
     */
    public function updateParts3file($build){
        if(empty($this->options[self::OPTION_PREVIOUS_RELEASE_STRING])){
            DIE("PREVIOUS RELEASE IS UNDEFINED");
        }

        $partsFilePath = $this->findPrevPartsFilePath($build);

        $partsFile = preg_replace('/\n*\r*/','',file_get_contents($partsFilePath));
        $partsJson = substr($partsFile, stripos($partsFile,'var Parts = ')+12);
        $partsArr = json_decode($partsJson,true);

        if(empty($partsArr)){
            throw new Exception("CANNOT FIND PARTS3.js OR ITS INCORRECT FOR {$build}");
        }
        //array of items to check in db
        $itemsArr = array();
        $productsArr = array();

        foreach($partsArr as $part){
            $itemsArr[] = $part["item_id"];
            $productsArr[] = $part["product_id"];
        }
        
        $inStr = implode(',',$itemsArr);
        $prodInStr = implode(',',$productsArr);
        
        $changes = array();
        //if its false -> dont save new file!
        $wasUpdated = false;
        
        //CHECKING PROHIBITPRODUCTS START
        $queryStr = <<<HEREDOC
            SELECT * from customizer where id IN ({$prodInStr})
HEREDOC;
        
        $rez = $this->db->query($queryStr);
        if($this->db->error) throw new Exception($this->db->error);
        //dump($partsArr);
         while($row = $rez->fetch_assoc()) {
            foreach ($partsArr as &$part) {
                if($part["product_id"] == $row["id"]){

                    //compare them is not empty
                    if(! empty($row["incompatible"])){
                        $newProhArr = array_map('intval', explode(',', $row["incompatible"]));
                        //$arrDif = array_diff($part["prohibit_products"], $newProhArr);
                        if($part["prohibit_products"] != $newProhArr){   
                            $changes[] = "changed prohibit products values to ".implode(",",$newProhArr). " for part_id {$part["item_id"]} build {$build}"; 
                            $part["prohibit_products"] = $newProhArr;
                            $wasUpdated = true;
                        }
                    }
                    else{
                        //check if old one is null otherwise overrite it
                        if(! empty($part["prohibit_products"])){
                            $removedStr = implode("," , $part["prohibit_products"]);
                            $changes[] = "removed all ({$removedStr}) from item {$part["item_id"]} and its null now"; 
                            $part["prohibit_products"] = NULL;
                            $wasUpdated = true;
                        }
                    }

                }
            }
        }
        //CHECKING PROHIBITPRODUCTS END

        //CHECK FOR INVENTORY/PRICES/OUT_OF_STOCK/HIDDEN
        $queryStr = <<<HEREDOC
            SELECT 
              i.id AS item_id,
              i.product_id,
              TRUNCATE(pp.price - (pp.price *(p.on_sale/100)), 2) AS price,
              IF(MAX(i.inventory) > 0, 'false', 'true' ) AS 'out_of_stock',
              c.`new`, 
              c.`hidden`
            FROM
              items i 
              INNER JOIN productpricings pp 
                ON pp.id = i.`product_id` 
              LEFT JOIN customizer c 
                ON c.id = i.`product_id`
              INNER JOIN products p ON p.`id` = i.`product_id`
            WHERE i.id IN ({$inStr})
            GROUP BY item_id,product_id
HEREDOC;
        $result = $this->db->query($queryStr);
        if($this->db->error) throw new Exception($this->db->error);

        if(! empty($this->options["force_release"]) && $this->options["force_release"] === true){
            $wasUpdated = true;
        }
        //dump($partsArr);
        
        
        while($row = $result->fetch_assoc()) {
            //dump($row);
            foreach ($partsArr as &$part) {
                $row["hidden"] = ($row["hidden"] == 1);
                $row["out_of_stock"] = ($row["out_of_stock"] == "true");
                
                if($part["item_id"] == $row["item_id"]){
                    //dump($part);
                    //var_dump($row["price"]);
                    if($part["price"] != $row["price"]){
                        $changes[] = "Price was updated from {$part["price"]} to {$row["price"]} for item id ====> {$part["item_id"]} : build => {$build}";
                        $part["price"] = (float)$row["price"];
                        $wasUpdated = true;
                    }
                    if($part["out_of_stock"] != $row["out_of_stock"]){
                        //dump($part["out_of_stock"]);
                        //dump($row["out_of_stock"]);
                        $changes[] = "out_of_stock was changed from {$part["out_of_stock"]} to {$row["out_of_stock"]} for item id => {$part["item_id"]} : build => {$build}";
                        $part["out_of_stock"] = $row["out_of_stock"];
                        $wasUpdated = true;
                    }
                    if($part["hidden"] != $row["hidden"] ){
                        $changes[] = "Attribute ('hidden') was changed from {$part["hidden"]} to {$row["hidden"]} for item id ====> {$part["item_id"]} : build => {$build}";
                        $part["hidden"] = $row["hidden"];
                        $wasUpdated = true;
                    }
                    if(strtotime($row["new"]) > time()){
                        if($part["new"] != 1){
                            $changes[] = "Attribute ('new') was changed from {$part["new"]} to 1 for item id ====> {$part["item_id"]} : build => {$build}";
                            $part["new"] = "1";
                            $wasUpdated = true;
                        }
                    }
                    else{
                        if($part["new"] != 0){
                            $changes[] = "Attribute ('new') was changed from {$part["new"]} to 0 for item id ====> {$part["item_id"]} : build => {$build}";
                            $part["new"] = "0";
                            $wasUpdated = true;
                        }
                    }
                }
            }
        }
        //dump($wasUpdated);

        if($wasUpdated){
            if(! file_exists($this->buildPath)){
                mkdir($this->buildPath);
            }
            //save release file
            if(! file_exists($this->buildPath.$build)){
                mkdir($this->buildPath.$build);
            }
            file_put_contents($this->buildPath.$build."\\parts3.js",'var Parts = '.json_encode($partsArr));
        }

        if (empty($changes)){
            $changes = "Everything is up-to-date!";
        }

        return $changes;
    }

    /**
     * @param array $ids
     * will create a config file
     */
    public function createGenerationConfigFile($ids = array()){
        $fileHandle = fopen($this->generationConfigFile, "w") or die("Unable to open file!");
        $generationInfo = array("builds" => $ids , "lastReleaseDate" => "");
        fwrite($fileHandle,json_encode($generationInfo));
        fclose($fileHandle);

        var_dump("CONFIG FILE IS CREATED! We can use it now!");
        DIE("comment createGenerationConfigFile method");
    }

    public function appplyaGlobalConfig(){

    }

    /**
     * will create config file with all builds that should be live (configRelease.js)
     * @param  array  $generationArr array of generations OR just array of builds
     */
    public function createLiveBuildsFile($generationArr = array()){

        $resultArr = array();

        if(! is_array($generationArr)){
            $tVal = $generationArr;
            unset($generationArr);
            $generationArr = array($tVal);
        }

        foreach($generationArr as $element){
            //if its a version
            if(is_string($element)){
                $tMaster = new Master($element,[],false);
                if (! empty($tMaster->generationInfo["builds"])){
                    $resultArr = array_merge($resultArr,$tMaster->generationInfo["builds"]);
                }
                else{
                    var_dump("WARNING! RELEASE INFO FOR VERSION {$element} IS EMPTY!");
                }
            }
            else if(is_numeric($element)){
                $resultArr[] = $element;
            }
            else if(is_array($element)){
                foreach($element as $value){
                    $resultArr[] = $value;
                }
            }
        }

        $json = json_encode($resultArr);
        file_put_contents(realpath(dirname(__FILE__)).'\\config-release.js','var configRelease = '.$json.';');

        var_dump($resultArr);
        var_dump("DONE!");

    }

    public static function compressAllImages(){
        $assetsFolder = realpath(dirname(__FILE__))."\\assets\\";
        $uncompressedFolder = $assetsFolder."uncompressed\\";
        $sourceFolder = $assetsFolder."source\\";

        //get all subfolders
        $directories = glob($uncompressedFolder . '*' , GLOB_ONLYDIR);
        $foldersNeedToCreateArr = array();
        foreach($directories as $folder){
            $folderNameArr = explode('\\', $folder);
            $folderName = $folderNameArr[count($folderNameArr) - 1];
            $foldersNeedToCreateArr[]  = $folderName;
        }

        //create folders in source
        foreach($foldersNeedToCreateArr as $folder){
            mkdir($sourceFolder.$folder);
        }
        dump($foldersNeedToCreateArr);
        //format uncompressed subdirectories first
        foreach($foldersNeedToCreateArr as $folder){
            $ffs = scandir($uncompressedFolder.$folder);
            foreach($ffs as $filename){
                if(strpos($filename, ".png") !== false){
                    $cmd = <<<HEREDOC
pngquant.exe --force --verbose --output "{$sourceFolder}{$folder}\\{$filename}" --quality=45-85 "{$uncompressedFolder}{$folder}\\{$filename}"
HEREDOC;
                    chdir('C:\\xampp\\_support\\pngquant');
                    system($cmd,$retval);
                    var_dump("compressed {$folder}\\{$filename}");
                }
            }
        }

        //formam uncompressed folder
        $ffs = scandir($uncompressedFolder);
        foreach($ffs as $filename){
            if(strpos($filename, ".png") !== false){
                $cmd = "pngquant.exe --force --verbose --output \"{$sourceFolder}{$filename}\" --quality=45-85 \"{$uncompressedFolder}{$filename}\"";
                system($cmd);
                var_dump("compressed {$filename}");
            }
        }

        var_dump("DONE!");

    }
}
