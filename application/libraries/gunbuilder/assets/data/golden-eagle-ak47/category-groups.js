[
  {
    "id":0,
    "category_group_name":"Platform",
    "data_name" : "TopNav.Platform",
    "data_position": 7
  },
  {
    "id":1,
    "category_group_name":"Receiver",
    "data_name" : "TopNav.Receiver",
    "data_position": 127
  },
  {
    "id":2,
    "category_group_name":"Grips",
    "data_name" : "TopNav.Grips",
    "data_position": 247
  },
  {
    "id":3,
    "category_group_name":"Stocks",
    "data_name" : "TopNav.Stocks",
    "data_position": 367
  },
  {
    "id":4,
    "category_group_name":"Forend",
    "data_name" : "TopNav.Forend",
    "data_position": 487
  },
  {
    "id":5,
    "category_group_name":"Foregrips",
    "data_name" : "TopNav.Foregrips",
    "data_position": 607
  },
  {
    "id":6,
    "category_group_name":"Optics",
    "data_name" : "TopNav.Optics",
    "data_position": 727
  },
  {
    "id":7,
    "category_group_name":"Accessories",
    "data_name" : "TopNav.Accessories",
    "data_position": 847
  }
]