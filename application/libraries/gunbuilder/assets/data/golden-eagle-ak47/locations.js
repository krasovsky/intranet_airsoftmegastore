[
  {
    "id": 1,
    "platform":2,
    "label":"Receiver",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [],
    "enabled":true
  },
  {
    "id": 2.1,
    "platform":2,
    "label":"Barrel Assembly",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 2.2,
    "platform":2,
    "label":"Barrel Assembly",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 3,
    "platform":2,
    "label":"Stock",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 5.1,
    "platform":2,
    "label":"Optic",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.1],
    "enabled":true
  },
  {
    "id": 5.2,
    "platform":2,
    "label":"Optic",
    "offset_y": -1,
    "offset_x": -190,
    "required_locations": [2.2],
    "enabled":true
  },
  {
    "id": 7,
    "platform":2,
    "label":"Magazine",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 8,
    "platform":2,
    "label":"Accessory",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.1],
    "enabled":true
  },
  {
    "id": 9,
    "platform":2,
    "label":"Accessory",
    "offset_y": 4,
    "offset_x": -5,
    "required_locations": [2.2],
    "enabled":true
  },
  {
    "id": 10,
    "platform":2,
    "label":"Grip",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  }
]