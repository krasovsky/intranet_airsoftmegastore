[
  {
    "id":0,
    "category_name":"Platforms",
    "category_group":0
  },
  {
    "id":1,
    "category_name":"Lower Receivers",
    "category_group":1
  },
  {
    "id":2,
    "category_name":"Upper Receivers",
    "category_group":2
  },
  {
    "id":3,
    "category_name":"Stocks",
    "category_group":3
  },
  {
    "id":4,
    "category_name":"Front Iron Sights",
    "category_group":4
  },
  {
    "id":5,
    "category_name":"Rear Iron Sights",
    "category_group":4
  },
  {
    "id":6,
    "category_name":"Foregrips",
    "category_group":5
  },
  {
    "id":8,
    "category_name":"Grip Pods",
    "category_group":5
  },
  {
    "id":9,
    "category_name":"Flashlight Grips",
    "category_group":5
  },
  {
    "id":10,
    "category_name":"Optics",
    "category_group":6
  },
  {
    "id":11,
    "category_name":"Magazines",
    "category_group":7
  },
  {
    "id":12,
    "category_name":"Bipods",
    "category_group":7
  },
  {
    "id":13,
    "category_name":"Rail Covers",
    "category_group":5
  },
  {
    "id":14,
    "category_name":"Essentials",
    "category_group":7
  },
  {
    "id":15,
    "category_name":"Assembly",
    "category_group":null
  }
]