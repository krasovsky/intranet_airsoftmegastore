[
  {
    "id": 1,
    "platform":1,
    "label":"Lower Receiver",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [],
    "enabled":true
  },
  {
    "id": 2.1,
    "platform":1,
    "label":"Upper Receiver",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 2.2,
    "platform":1,
    "label":"Upper Receiver",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 3,
    "platform":1,
    "label":"Stock",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 4,
    "platform":1,
    "label":"Rear Sight",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.2,2.1],
    "enabled":true
  },
  {
    "id": 5,
    "platform":1,
    "label":"Optic",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.2,2.1],
    "enabled":true
  },
  {
    "id": 6.1,
    "platform":1,
    "label":"Front Sight",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.1],
    "enabled":true
  },
  {
    "id": 6.2,
    "platform":1,
    "label":"Front Sight",
    "offset_y": -1,
    "offset_x": -83,
    "required_locations": [2.2],
    "enabled":true
  },
  {
    "id": 7,
    "platform":1,
    "label":"Magazine",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [1],
    "enabled":true
  },
  {
    "id": 8,
    "platform":1,
    "label":"Accessory",
    "offset_y": 0,
    "offset_x": 77,
    "required_locations": [2.2,2.1],
    "enabled":true
  },
  {
    "id": 9,
    "platform":1,
    "label":"Accessory",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.2,2.1],
    "enabled":true
  },
  {
    "id": 10,
    "platform":1,
    "label":"Accessory",
    "offset_y": 0,
    "offset_x": -95,
    "required_locations": [2.2],
    "enabled":true
  },
  {
    "id": 11,
    "platform":1,
    "label":"Upper Receiver",
    "offset_y": 0,
    "offset_x": 0,
    "required_locations": [2.2,2.1],
    "enabled":true
  }
]