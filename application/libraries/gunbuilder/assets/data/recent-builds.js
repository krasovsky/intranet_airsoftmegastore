[
  {
    "item_id":13689,
    "product_id":11896,
    "sku":"CGB-CUSTOM-1",
    "price":227.99,
    "sale_price":227.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Full Metal M4 RIS Carbine AEG",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11896-ams-custom-gun-builder-full-metal-m4-ris-carbine-aeg-cgb-custom-1.aspx"
  },
  {
    "item_id":13692,
    "product_id":11899,
    "sku":"CGB-CUSTOM-2",
    "price":265.99,
    "sale_price":265.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Full Metal M16 RIS AEG Rifle",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11899-ams-custom-gun-builder-full-metal-m16-ris-aeg-rifle-cgb-custom-2.aspx"
  },
  {
    "item_id":13694,
    "product_id":11901,
    "sku":"CGB-CUSTOM-3",
    "price":244.49,
    "sale_price":244.49,
    "product_name": "Airsoft Megastore Custom Gun Builder Full Metal M4 RIS AEG - TAN",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11901-ams-custom-gun-builder-full-metal-m4-ris-aeg-tan-cgb-custom-2.aspx"
  },
  {
    "item_id":13696,
    "product_id":11903,
    "sku":"CGB-CUSTOM-4",
    "price":285.99,
    "sale_price":285.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Full Metal M4 RECCE RIS AEG - TAN",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11903-ams-custom-gun-builder-full-metal-m4-recce-rfle-ris-cgb-custom-4.aspx"
  },
  {
    "item_id":13695,
    "product_id":11902,
    "sku":"CGB-CUSTOM-5",
    "price":199.99,
    "sale_price":199.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Polymer M4 CQB Stubby AEG",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11902-airsoft-megastore-custom-polymer-m4-cqb-stubby-aeg-cgb-custom-5.aspx"
  },
  {
    "item_id":13693,
    "product_id":11900,
    "sku":"CGB-CUSTOM-6",
    "price":229.99,
    "sale_price":229.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Polymer M4 RECCE RIS AEG",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11900-Airsoft-Megastore-Custom-Gun-Builder-Polymer-M4-RECCE-RIS-AEG.aspx"
  },
  {
    "item_id":13691,
    "product_id":11898,
    "sku":"CGB-CUSTOM-7",
    "price":219.99,
    "sale_price":219.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Polymer M4 CQB RIS AEG - Tan",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11898-airsoft-megastore-custom-gun-builder-m4-cqb-aeg-tan-cgb-custom-7.aspx"
  },
  {
    "item_id":13690,
    "product_id":11897,
    "sku":"CGB-CUSTOM-8",
    "price":172.99,
    "sale_price":172.99,
    "product_name": "Airsoft Megastore Custom Gun Builder Polymer M4 RIS AEG - Tan",
    "rating": 0,
    "product_link": "http://www.airsoftmegastore.com/11897-airsoft-megastore-custom-gun-builder-m4-ris-aeg-tan-cgb-custom-8.aspx"
  }
]