var AMS = AMS || {};

AMS.Page = {
    name:'GunBuilder2'
}

AMS.GB2 = {
    Build:[]
    ,cursor:0
    ,ready:function(){
        this.Build = $.extend([],Platform.Build);
        this.History.initialize();
        this.constructBuild();
        this.Menu.initialize();
        this.Gun.initialize();
        this.Price.initialize();
        this.Title.initialize();
        this.Checkout.initialize();
        this.Popup.initialize();
        this.Share.initialize();
        this.Position.initialize();
        //this.Scale.initialize();
        this.initialize();
       // this.setInventory();

        AMS.Analytics.logClick(['user-interaction','arrives']);
    }
    ,initialize:function(){
        $('#main').on('gb2.gunSelect',function(e,evnt){
            var item_id = $(evnt.delegateTarget).attr('data-id');
            this.cursor = this.getCursorPositionFromItemId(item_id);
        }.bind(this));
        this.cursor = this.Build.length; //make sure next goes to zero first
    }
    ,change:function(){
        $('#main').trigger('gb2.change');
    }
    ,getNext:function(){
        ++this.cursor;

        if(this.Build.length<=this.cursor)
            this.cursor = 0;
        return this.Build[this.cursor];
    }
    ,getCursorPositionFromItemId:function(item_id){
        var position = 0;
        $(this.Build).each(function(index,item){
            if(item_id == item.item_id) position = index;
        });
        return position;
    }
    ,setCursorPositionFromCategoryId:function(id){
        $(this.Build).each(function(index,item){
            if(id == item.id) this.cursor = index;
        }.bind(this));
    }
    ,setInventory:function(){
        var that = this;
        $(Parts).each(function(){
            var item_id = this.item_id;
            ASM.Web.Airsoft.RealtimeInventoryService.GetItemRealtimeInventory(this.item_id,'1',function(inventory){
                that.setStock(item_id,inventory);
            })});
    }
    ,setStock:function(item_id,inventory){
        var part = this.getPartByItemId(item_id);
        console.log(part,inventory);
        if(inventory > 0){
            part.out_of_stock = false;
        }else{
            part.out_of_stock = true;
        }
    }
    ,getNextCategoryUnchosen:function(){
        var category =_.find(this.Build, function(item){
            return item.item_id == null;
        });
        return (!!category)?category.id:0;
    }
    ,getCurrentPartCategory:function(){
        return this.Build[this.cursor].id;
    }
    ,getCurrentPartItemId:function(){
        return this.Build[this.cursor].item_id;
    }
    ,getCurrentItem:function(){
        return this.Build[this.cursor];
    }
    ,getPartsFromCategoryId:function(id){
        return _.filter(Parts, function(obj){return obj.category === parseInt(id)})
    }
    ,getSelectedPartByCategoryId:function(id){
        var buildPart = _.find(this.Build,function(obj){return obj.id == parseInt(id)});
        return this.getPartByItemId(buildPart.item_id);
    }
    ,constructBuild:function(){
        //get values from url and add to item_id
        var path = [];
        $(path).each(function(index, item){
            this.assignPartToBuild(item);
        }.bind(this));
    }
    ,assignPartToBuild:function(item_id){
        var part = this.getPartByItemId(item_id);
        var build = this.getCategoryById(part.category);

        build.item_id = parseInt(item_id);
    }
    ,getCategoryById:function(id){
       return _.find(this.Build,function(obj){return obj.id == id});
    }
    ,getPartByItemId:function(id){
       return _.find(Parts,function(obj){return obj.item_id == id});
    }
    ,getBuildPartByItemId:function(item_id){
        return _.find(this.Build,function(obj){return obj.item_id == item_id});
    }
    ,addItem:function(item_id){
        //with item_id get part, find what category it is in, then add the id to the appropriate category in build.
        if(item_id){
            this.assignPartToBuild(item_id);
            this.change();
        }
    }
    ,removeItem:function(item_id){
        //with item_id get part, find what category it is in, then add the id to the appropriate category in build.
        var item = this.getBuildPartByItemId(item_id);
        item.item_id = null;
        this.change();
    }
    ,isCheckoutReady:function(){
        var choiceRequired = _.filter(this.Build,function(obj){return obj.item_id == null && obj.requiredForCheckout});
        if(choiceRequired.length > 0) return false;
        else return true;
    }
    ,isFullyStocked:function(){
        var outOfStock = _.filter(this.Build,function(obj){return obj.out_of_stock});
        if(outOfStock.length > 0) return false;
        else return true;
    }
    ,getIncompatible:function(item_id){
        var result = [];
        var that = this;
        var item = this.getPartByItemId(item_id);

        //check if part has conflict with build
        $(this.Build).each(function(){
            var part = that.getPartByItemId(this.item_id);
            if(!part) return; // if no item_id then return
            if(part.prohibit_products != null && ~part.prohibit_products.indexOf(parseInt(item_id))){
                result.push(that.getPartByItemId(this.item_id));
            }
            if(item.prohibit_products != null && ~item.prohibit_products.indexOf(this.item_id)){
                result.push(that.getPartByItemId(this.item_id));
            }
        });

        return result;
    }
};

AMS.GB2.Menu = {
    initialize:function(){
        AMS.Messenger.initialized('GB2.Menu');

        //set observers
        $('#main').on('gb2.select' ,function(e,evnt){
            var id = $(evnt.delegateTarget).attr('data-id');
            this.select(id);
        }.bind(this));

        $('#main').on('gb2.gunSelect' ,function(e,evnt){
            var id = $(evnt.delegateTarget).attr('data-rel');
            $('#main').trigger('gb2.closeMenu');
            this.select(id);
        }.bind(this));

        $('#main').on('gb2.gunDblClick' ,function(e,evnt){
            var id = $(evnt.delegateTarget).attr('data-rel');
            $('[data-id='+id+']').click();

            AMS.Analytics.logClick(['gun','DblClick']);
        }.bind(this));

        $('#main').on('gb2.itemSelect',function(e,evnt){
            this.close();
            var id = $(evnt.delegateTarget).attr('data-id');
            this.itemSelect(id);
        }.bind(this));

        $('#main').on('gb2.closeMenu',function(){this.close()}.bind(this))
        $('body').on('click',function(){this.close()}.bind(this))

        this.buildNavigation();
    }
    //observers
    ,select:function(id){
        this.current = id;
        $('#menu [data-id]').removeClass('active');
        $('#menu [data-id='+this.current+']').addClass('active');
    }
    ,itemSelect:function(id){
        AMS.GB2.addItem(id);
        this.close();
    }
    ,close:function(){
        $('#main #list').hide();
        $('#details').hide();
    }
    //methods
    ,showList:function(id){
        $('#main #list').remove();
        $('#details').hide();

        var that = this;
        //map list all values
        var data = {};
        data.list  = AMS.GB2.getPartsFromCategoryId(id);

        //get the selected value
        data.selected = AMS.GB2.getSelectedPartByCategoryId(id);

        $(data.list).each(function(){
           var incompatible = AMS.GB2.getIncompatible(this.item_id);
           this.isDisabled = (incompatible.length)? 1 : 0;
        });

        //remove the selected value from the list so there are no duplicates.
        data.list = _.reject(data.list, function(part){return data.selected && part.item_id == data.selected.item_id });

        //sort by available
        data.list.sort(function(a,b){return a.isDisabled - b.isDisabled});
        data.list.sort(function(a,b){return a.out_of_stock - b.out_of_stock});

        //show with handlebars
        var source   = $("#_tmpl_menu_list").html();
        var template = Handlebars.compile(source);

        var html = template(data);
        $('#menu [data-id='+id+']').append(html);

        $('#main #list [data-id]').on('click',function(e){
            e.stopPropagation();
            var id = $(this).attr('data-id')

            if($(this).hasClass('disabled')){
                AMS.Analytics.logClick(['menu-select-disabled',id]);
                that.showDetails(id,'incompatible');
            }else if ($(this).hasClass('oos')){
                AMS.Analytics.logClick(['menu-select-oos',id]);
                that.showDetails(id);
            }else{
                AMS.Analytics.logClick(['menu-select',id]);
                $('#main').trigger('gb2.itemSelect',e);
            }
        });

//        $('#list [data-id]').on('mouseenter',function(e){
//            if($('#details').css('display')!= 'none'){
//                var item_id = $(this).attr('data-id');
//                that.showDetails(item_id, $(this).parent());
//            }
//        });

        $('#main #list .more').on('click',function(e){
            e.stopPropagation();

            var $parent = $(this).parent();
            var id = $parent.attr('data-id');

            if($parent.hasClass('disabled')){
                that.showDetails(id,'incompatible');
                AMS.Analytics.logClick(['menu-details-incompatible',id]);
            }else if($parent.hasClass('outofstock')){
                that.showDetails(id,'oos');
                AMS.Analytics.logClick(['menu-details-oos',id]);
            }else{
                that.showDetails(id);
                AMS.Analytics.logClick(['menu-details',id]);
            }
        });

        $('#main #list .remove').on('click',function(e){
            e.stopPropagation();
            var item_id = $(this).attr('data-rel');
            AMS.GB2.removeItem(item_id);
            that.close();

            AMS.Analytics.logClick(['menu-unselect',item_id]);
        });
    }
    ,buildDetails:function(id,tmpl){
        //with current item id show associated details using handlebars
        var source   = (tmpl)?$("#_tmpl_menu_"+tmpl).html(): $("#_tmpl_menu_details").html();
        var template = Handlebars.compile(source);

        var part = AMS.GB2.getPartByItemId(id);

        if(tmpl=='incompatible'){
            part.incompatible = AMS.GB2.getIncompatible(id);
        }

        var html = template(part);
        $('#details').html(html);

        $('#details').on('click',function(e){e.stopPropagation()});

        $('#details button').on('click',function(e){
            var id = $(e.delegateTarget).attr('data-rel');
            this.itemSelect(id);
        }.bind(this));

        var that = this;
        $('#details .remove').on('click',function(e){
            e.stopPropagation();
            var item_id = $(this).attr('data-rel');
            AMS.GB2.removeItem(item_id);
            that.close();
        });
    }
    ,showDetails:function(id,tmpl){
        $('#details').hide();
        this.buildDetails(id,tmpl);

        $('#details').fadeIn();
    }
    ,buildNavigation:function(){
        var that = this;
        //build categories from data
        var source   = $("#_tmpl_menu").html();
        var template = Handlebars.compile(source);

        var nav = _.filter(AMS.GB2.Build, function(obj){return !obj.isHidden});
        var html = template(nav);
        $('#menu').append(html);

        //set events
        $('#menu [data-id]').on('click',function(e){
            var evnt = e;
            e.stopPropagation();
            var id = $(this).attr('data-id');

            AMS.GB2.setCursorPositionFromCategoryId(id);

            $('#main').trigger('gb2.select', evnt);
            that.showList(id);

            AMS.Analytics.logClick(['menu-category-select',id]);
        });

        $('#menu [data-id]').on('mouseenter',function(e){
            var evnt = e;
            var id = $(this).attr('data-id');
            $('#main').trigger('gb2.select', evnt);
        });
    }
};

AMS.GB2.Position = {
    //global config -> master config -> part config
    normalize:3150,
    initialize:function(){
        this.normalize = $('svg').height()*10;
    }
    ,set:function(part, master){
        for( var p in part.position ){
            var obj = part.position[p];
            if(!!obj.align) this[obj.align](part,master);
        }
    }
    ,topLeft:function(part,master){
        //then we use top/left corner for alignment
        var top = master.y1;
        var left = master.x1;

        if(!! part.position[master.position]){
            var p = part.position[master.position];
            if( !! p[master.item_id]){
                var p = part.position[master.position][master.item_id];
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }else{
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }
        }

        $('img[data-id='+part.item_id+']').css({'top': top, 'left': left });
        $('path[data-id='+part.item_id+']').attr('transform', 'translate('+ ( left * 10 ) +','+ ( this.normalize - ( top  * 10 ) - ( part.dimensions.height * 10 )) +')');

    }
    ,topCenter:function(part,master){
        //then we use bottom/right corner for alignment
        var top = master.y1;
        var left = (master.x1+master.width/2)-(part.dimensions.width/2) ;


        if(!! part.position[master.position]){
            var p = part.position[master.position];
            if( !! p[master.item_id]){
                var p = part.position[master.position][master.item_id];
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }else{
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }
        }

        //set image
        $('img[data-id='+part.item_id+']').css({
            'top': top,
            'left': left
        });
        //set svg
        $('path[data-id='+part.item_id+']').attr('transform', 'translate(' + ( left * 10 ) +','+ ( this.normalize - ( top  * 10 ) - ( part.dimensions.height * 10 )) +')');

    }
    ,topRight:function(part,master){
        //then we use bottom/right corner for alignment
        var top = master.y1;
        var left = master.x2-part.dimensions.width ;


        if(!! part.position[master.position]){
            var p = part.position[master.position];
            if( !! p[master.item_id]){
                var p = part.position[master.position][master.item_id];
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }else{
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }
        }

        //set image
        $('img[data-id='+part.item_id+']').css({
            'top': top,
            'left': left
        });
        //set svg
        $('path[data-id='+part.item_id+']').attr('transform', 'translate(' + ( left * 10 ) +','+ ( this.normalize - ( top  * 10 ) - ( part.dimensions.height * 10 )) +')');
    }
    ,center:function(part,master){}
    ,bottomLeft:function(part,master){
        //then we use bottom/right corner for alignment
        var top = master.y1-part.dimensions.height;
        var left = master.x1 ;


        if(!! part.position[master.position]){
            var p = part.position[master.position];
            if( !! p[master.item_id]){
                var p = part.position[master.position][master.item_id];
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }else{
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }
        }

        //set image
        $('img[data-id='+part.item_id+']').css({
            'top': top,
            'left': left
        });
        //set svg
        $('path[data-id='+part.item_id+']').attr('transform', 'translate(' + ( left * 10 ) +','+ ( this.normalize - ( top  * 10 ) - ( part.dimensions.height * 10 )) +')');

    }
    ,bottomCenter:function(part,master){}
    ,bottomRight:function(part,master){
        //then we use bottom/right corner for alignment
        var top = master.y2-part.dimensions.height;
        var left = master.x2-part.dimensions.width ;


        if(!! part.position[master.position]){
            var p = part.position[master.position];
            if( !! p[master.item_id]){
                var p = part.position[master.position][master.item_id];
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }else{
                if( !! p.y ) top -= p.y;
                if( !! p.x ) left -= p.x;
            }
        }

        //set image
        $('img[data-id='+part.item_id+']').css({
            'top': top,
            'left': left
        });
        //set svg
        $('path[data-id='+part.item_id+']').attr('transform', 'translate(' + ( left * 10 ) +','+ ( this.normalize - ( top  * 10 ) - ( part.dimensions.height * 10 )) +')');
    }
};
AMS.GB2.Scale = {
    initialize:function(){
        $('#main').on('gb2.change',function(){
            this.scaleGun();
        }.bind(this));
        this.scaleGun();
    }
    ,scaleBy:function(percent){
        $('#gun').animate({width:100-percent+'%'});
    }
    ,undoScale:function(){
        $('#gun').animate({width:100+'%'});
    }
    ,scaleGun:function(){
        for(var i in AMS.GB2.Gun.items){
            if(AMS.GB2.Gun.items[i].scaleFactor){
                this.scaleBy(AMS.GB2.Gun.items[i].scaleFactor);
                return;
            }
        }
        this.undoScale();
    }
};
AMS.GB2.Gun = {
    cursor:0,
    items:[],
    $paths:'#gun svg path[data-rel]',
    initialize:function(){
        AMS.Messenger.initialized('GB2.Gun');
        //declarations
        this.$el = $('#gun');

        //observers
        $('#main').on('gb2.change',function(e,evnt){this.change(evnt)}.bind(this));
        $('#main').on('gb2.select',function(e,evnt){this.select(evnt)}.bind(this));
        $('#main').on('gb2.itemSelect',function(e,evnt){this.deselect(evnt)}.bind(this));
        $('#main').on('gb2.next',function(e){this.next(e)}.bind(this));

        $('.path-controls').on('click',function(){
            this.togglePaths();
            AMS.Analytics.logClick(['user-interaction','hide-paths']);
        }.bind(this));

        $('body').on('click',function(){this.removeHighlight()}.bind(this));

        this.build();
    }
    ,change:function(evnt){
        AMS.Messenger.changed('GB2.Gun');
        this.build();
        this.highlight(AMS.GB2.getCurrentPartCategory());
        this.togglePaths();
    }
    ,select:function(evnt){
        AMS.Messenger.selected('GB2.Gun');
        var id = $(evnt.delegateTarget).attr('data-id');
        this.current = id;
        this.highlight(id);
    }
    ,deselect:function(evnt){
        var id = $(evnt.delegateTarget).attr('data-id');
        this.highlight(id);
    }
    ,togglePaths:function(){
        if($('.path-controls input')[0].checked){
            //hide paths
            $('.base').not('.error').css('visibility','hidden');
        }else{
            //show paths
            $('.base').css('visibility','visible');
        }
    }
    ,selectLocation:function(){
        $('#gun').imgAreaSelect({
            handles: true,
            keys: true,
            onSelectEnd: function(arg,selection){console.log(JSON.stringify(selection))}
        });
    }
    ,build:function(){
        this.items.length = 0;
        $(AMS.GB2.Build).each(function(index, build){
            if(build.item_id == null){
                var part = _.find(Parts, function(obj){return obj.item_id == build.base_id});
                var item = $.extend({"isBase":true}, build, part);
            }else{
                var part = _.find(Parts, function(obj){return obj.item_id == build.item_id});
                var item = $.extend({"isBase":false}, build, part);
            }
            this.items.push(item);

        }.bind(this));

        this.clearGun();
        this.setImg();
        this.selectLocation();
        this.setSVG();
        this.positionItems();
    }
    ,positionItems:function(){
        var that = this;
        //find items with master positions and iterate over them
        var hasMasterPositions = _.find(this.items, function(item){return !!item.masterPosition });

        $(hasMasterPositions).each(function(){
            //if any of the items have a master positin then we need to position other elements based upon it
            for(var position in this.masterPosition){
                var part = _.find(that.items, function(item){ return !!item.position && !!item.position[position]; });
                if (part){
                    var master = this.masterPosition[position];
                    master.item_id = this.item_id;
                    master.position = position;
                    AMS.GB2.Position.set(part, master);
                }
            }
        });
    }
    ,clearGun:function(){
        this.$el.html('');
    }
    ,fireClickEvent:function(e){
        $(this.$paths).trigger(e);
    }
    ,setImg:function(){
        var source   = $("#_tmpl_img").html();
        var template = Handlebars.compile(source);
        var $gun = this.$el;

        var imgs = template(this.items);
        $gun.append(imgs);
    }
    ,setSVG:function(){
        var source   = $("#_tmpl_svg").html();
        var template = Handlebars.compile(source);
        var $gun = this.$el;

        var svg = template(this.items);
        $gun.append(svg);

        $(this.$paths).click(function(e){
            e.stopPropagation();
            var id = $(e.delegateTarget).attr('data-rel');

            this.highlight(id);

            AMS.Analytics.logClick(['gun-select',id]);

            $('#main').trigger('gb2.gunSelect',e);
        }.bind(this))

        $(this.$paths).on('dblclick',function(e){
            $('#main').trigger('gb2.gunDblClick',e);
        }.bind(this));

    }
    ,highlight:function(id){
        this.cursor = id;
        //var incompatible = AMS.GB2.getIncompatible();
        var paths = $(this.$paths).each(function(){
            var dataId = $(this).attr('data-rel');
            if(dataId !='noUse'){
                var classes = $(this).attr('class').split(' ');
                var cleaned = _.reject(classes, function(item){return item == 'next'});
                if($(this).attr('data-rel') == id){
                    //then this is the one we highlight
                    cleaned.push('next');
                }

                //if this id is not compatible then highlight in red
//                if(~incompatible.indexOf(dataId)){
//
//                }

                $(this).attr('class',cleaned.join(' '));
            }
        });
    }
    ,removeHighlight:function(id){
        this.highlight(0);
        $('#main').trigger('gb2.gunDeselect');
    }
    ,next:function(){
        var next = AMS.GB2.getNext();
        if(next)
            $('[data-rel='+next.id+']').trigger('dblclick');
//        else
//            $('#cart').trigger('click');
    }
    ,nextPart:function(backwards){
        var category = AMS.GB2.getCurrentPartCategory(this.current);
        var list = AMS.GB2.getPartsFromCategoryId(category);
        var current = this.current;

        //find current part in list set as cursor
        var index = _.indexOf(list,function(parts){parts.item_id == current});

        var next = (!!backwards)? index+1: index-1;
        if(next > list.length) next = 0;

        AMS.GB2.addItem(list[next].item_id);
    }
};

AMS.GB2.History = {
    cursor:0
    ,timeline:[]
    ,initialize:function(){
        //onload check to see if there is a configuration being passed through\
        this.loadHistory();
        //observers
        $('#main').on('gb2.back',function(){this.next(); AMS.Analytics.logClick(['controls','next']);}.bind(this));
        $('#main').on('gb2.save',function(){this.save(); AMS.Analytics.logClick(['controls','save']);}.bind(this));
        $('#main').on('gb2.change',function(){
            this.setHistoryHash()
        }.bind(this));

        //events
        $('#next').click(function(e){e.stopPropagation();$('#main').trigger('gb2.next',e); this.removeNextBlink()}.bind(this));
        $('#save').click(function(e){
            AMS.GB2.Popup.alert({
                message:'Bookmark this page to save your work'
           });
        });
        $('#restart').click(function(e){
            AMS.GB2.Popup.confirm({
                message:'Are you sure you want to start over?'
                ,yes: function(){AMS.GB2.History.restart()}
            });
            AMS.Analytics.logClick(['controls','restart']);
        }.bind(this));

        var that = this;

        $('#main').on('gb2.gunSelect', function(e,evnt){
            if($(evnt.delegateTarget).attr('class').indexOf('base')<0){
                this.removeButtonToggle(true);
                this.selected = $(evnt.delegateTarget).attr('data-id');
            }else{
                $('#main').trigger('gb2.gunDeselect');
            }
        }.bind(this));

        $('#remove').on('click',function(){
            this.removePart();
        }.bind(this))

        $('#main').on('gb2.gunDeselect, gb2.itemSelect', function(e){

            this.removeButtonToggle(false);
        }.bind(this));

        $('body').click(function(e){this.removeNextBlink();this.removeButtonToggle(false);}.bind(this));
    }
    ,removeButtonToggle:function(state){
        if(!!state){
            $('#remove').css({'background':'#D79331'});
        }else{
            this.selected = 0;
            var similarBackground = $('#wizard button').css('background');
            $('#remove').css({'background':similarBackground});
        }
    }
    ,removePart:function(currentPartId){
        var currentPartId = AMS.GB2.getCurrentPartItemId();

        AMS.Analytics.logClick(['gun-unselect',currentPartId]);

        if(this.selected == currentPartId)
            AMS.GB2.removeItem(currentPartId);
    }
    ,removeNextBlink:function(){
        $('#next').removeClass('pulse');
    }
    ,restart:function(){
        window.location = 'http://'+location.hostname + '/airsoft-gun-builder/'+ Platform.name;
    }
    ,getAction:function(step){
        //must have values in the timeline
        if(this.timeline.length > 0){
            //if adding step value creates a negative then give them zero
            if(this.cursor + step > 0){
                this.cursor = 0;
            //if adding a step value gives us a value outside the timeline length then give them max value
            }else if(this.cursor + step < this.timeline.length){
                this.cursor = this.timeline.length;
            }else{
                this.cursor = this.cursor + step;
            }
        }
        return this.timeline[this.cursor];
    }
    ,save:function(){}
    ,setHistoryHash:function(){
        var parts = _.pluck(_.filter(AMS.GB2.Build, function(item){
            return item.item_id != null;
        }),'item_id');

        window.location.hash = '#' + parts.join(',');
    }
    ,resetHistoryHash:function(){
        window.location.hash = '#';
    }
    ,loadHistory:function(){

        //if url comes from a share it is a querystring
        if(window.location.search){
            var url = window.location.search.substring(1);
        }else{
            var url = window.location.hash.substring(1); //trim #
        }
        var path = url.substring(1);
        if(url){
            var parts = url.split(',');

            $(parts).each(function(){
                AMS.GB2.addItem(parseInt(this));
            });
        }
    }
};

AMS.GB2.Share = {
    initialize:function(){
        //tie events to button
        $('#share').on('click',function(e){AMS.GB2.Popup.share()})
    }
};

AMS.GB2.Checkout = {
    initialize:function(){
        upsold:false,
        $('#cart').on('click',function(){
            var missing = this.isMissing();
            if(AMS.GB2.isCheckoutReady()){
                if(AMS.GB2.isFullyStocked()){

                    if(this.upsold || missing.length == 0){
                        AMS.GB2.Essentials.show();
                        AMS.GB2.FinalOrder.initialize();
                    }else{
                        this.upsell(missing);
                    }
                }else{
                    AMS.GB2.Popup.alert({
                        message:'It looks like some of the parts in this build are out of stock. Either remove the items or restart your build for the latest stock levels'
                    })
                }
            }else{
                AMS.GB2.Popup.alert({message:'In order to checkout you\'ll need to add required parts!'})
            }
        }.bind(this));

        $('#main').on('gb2.change',function(){
            this.check();
        }.bind(this));


        $('#main').on('gb2.complete',function(){this.addAllToCart()}.bind(this));

        this.check();
    }
    ,isMissing:function(){
        var missing = [];
        $(AMS.GB2.base).each(function(){
            if(this.item_id==null) missing.push(this.category_name);
        });
        return missing
    }
    ,upsell:function(missing){
        this.upsold = true;
        AMS.GB2.Popup.confirm({
            message:'Get your '+missing.join(' and ')+' now so you don\'t have to worry about compatiblity later!<br/><br/> Do you still want to continue with your checkout?'
            ,yes:function(){
                $('#cart').trigger('click');
            }
        })
    }
    ,check:function(){
        (AMS.GB2.isCheckoutReady())
            ?this.checkoutReady()
            :this.checkoutPrevent();
    }
    ,checkoutReady:function(){
        $('#cart').addClass('checkoutReady');
    }
    ,checkoutPrevent:function(){
        $('#cart').removeClass('checkoutReady');
    }
    ,addAllToCart:function(){
        //add all items to the cart using OD (itemIds,quantity,onSuccess,onFailed,userContext)
        var items = [];
        for(var i in AMS.GB2.Build){
            if(AMS.GB2.Build[i].item_id!=null){
                items.push(parseInt(AMS.GB2.Build[i].item_id));
            }
        }
        items = items.concat(AMS.GB2.Essentials.cart); //get upsell items
        items = items.concat(AMS.GB2.FinalOrder.cart); //get assembly items

        AMS.GB2.FinalOrder.hide();
        AMS.GB2.Popup.checkout({obj:[]},true);

        ASM.Web.Airsoft.ShoppingCartService.AddItems(items, 1, AMS.GB2.Checkout.checkComplete);

        //loop through and log add to cart in analytics
        for(var i in items){
            AMS.Analytics.logClick(['AddToCart',parseInt(items[i])]);
        }

        AMS.Analytics.logClick(['user-interaction','checkedOut']);
    },
    checkComplete:function(){
        location.href = '/ShoppingCart.aspx';
    }
};

AMS.GB2.Stats = {
    current:{}
    ,initialize:function(){
        //update if there are stats in history object
    }
    ,change:function(){
        AMS.Messenger.changed('GB2.Stats');
        this.update();
    }
    ,calculate:function(){
        //combine all assembled objects stats objects into new object
    }
    ,update:function(){
        var calculated = this.calculate();
        //with calculated object animate value increases or decreases
    }
};

AMS.GB2.Title = {
    badwords: ['airsoft megastore', 'megastore', 'gunbuilder', 'gun builder', '2g1c', '2 girls 1 cup', 'acrotomophilia', 'alabama hot pocket', 'alaskan pipeline', 'anal', 'anilingus', 'anus', 'arsehole', 'ass', 'asshole', 'assmunch', 'auto erotic', 'autoerotic', 'babeland', 'baby batter', 'baby juice', 'ball gag', 'ball gravy', 'ball kicking', 'ball licking', 'ball sack', 'ball sucking', 'bangbros', 'bareback', 'barely legal', 'barenaked', 'bastardo', 'bastinado', 'bbw', 'bdsm', 'beaver cleaver', 'beaver lips', 'bestiality', 'big black', 'big breasts', 'big knockers', 'big tits', 'bimbos', 'birdlock', 'bitch', 'black cock', 'blonde action', 'blonde on blonde action', 'blowjob', 'blow job', 'blow your load', 'blue waffle', 'blumpkin', 'bollocks', 'bondage', 'boner', 'boob', 'boobs', 'booty call', 'brown showers', 'brunette action', 'bukkake', 'bulldyke', 'bullet vibe', 'bung hole', 'bunghole', 'busty', 'butt', 'buttcheeks', 'butthole', 'camel toe', 'camgirl', 'camslut', 'camwhore', 'carpet muncher', 'carpetmuncher', 'chocolate rosebuds', 'circlejerk', 'cleveland steamer', 'clit', 'clitoris', 'clover clamps', 'clusterfuck', 'cock', 'cocks', 'coprolagnia', 'coprophilia', 'cornhole', 'creampie', 'cum', 'cumming', 'cunnilingus', 'cunt', 'darkie', 'date rape', 'daterape', 'deep throat', 'deepthroat', 'dendrophilia', 'dick', 'dildo', 'dirty pillows', 'dirty sanchez', 'doggie style', 'doggiestyle', 'doggy style', 'doggystyle', 'dog style', 'dolcett', 'domination', 'dominatrix', 'dommes', 'donkey punch', 'double dong', 'double penetration', 'dp action', 'dry hump', 'dvda', 'eat my ass', 'ecchi', 'ejaculation', 'erotic', 'erotism', 'escort', 'ethical slut', 'eunuch', 'faggot', 'fecal', 'felch', 'fellatio', 'feltch', 'female squirting', 'femdom', 'figging', 'fingerbang', 'fingering', 'fisting', 'foot fetish', 'footjob', 'frotting', 'fuck', 'fuck buttons', 'fudge packer', 'fudgepacker', 'futanari', 'gang bang', 'gay sex', 'genitals', 'giant cock', 'girl on', 'girl on top', 'girls gone wild', 'goatcx', 'goatse', 'gokkun', 'golden shower', 'goodpoop', 'goo girl', 'goregasm', 'grope', 'group sex', 'g-spot', 'guro', 'hand job', 'handjob', 'hard core', 'hardcore', 'hentai', 'homoerotic', 'honkey', 'hooker', 'hot carl', 'hot chick', 'how to kill', 'how to murder', 'huge fat', 'humping', 'incest', 'intercourse', 'jack off', 'jail bait', 'jailbait', 'jelly donut', 'jerk off', 'jigaboo', 'jiggaboo', 'jiggerboo', 'jizz', 'juggs', 'kike', 'kinbaku', 'kinkster', 'kinky', 'knobbing', 'leather restraint', 'leather straight jacket', 'lemon party', 'lolita', 'lovemaking', 'make me come', 'male squirting', 'masturbate', 'menage a trois', 'milf', 'missionary position', 'motherfucker', 'mound of venus', 'mr hands', 'muff diver', 'muffdiving', 'nambla', 'nawashi', 'negro', 'neonazi', 'nigga', 'nigger', 'nig nog', 'nimphomania', 'nipple', 'nipples', 'nsfw images', 'nude', 'nudity', 'nympho', 'nymphomania', 'octopussy', 'omorashi', 'one cup two girls', 'one guy one jar', 'orgasm', 'orgy', 'paedophile', 'panties', 'panty', 'pedobear', 'pedophile', 'pegging', 'penis', 'phone sex', 'piece of shit', 'pissing', 'piss pig', 'pisspig', 'playboy', 'pleasure chest', 'pole smoker', 'ponyplay', 'poof', 'poon', 'poontang', 'punany', 'poop chute', 'poopchute', 'porn', 'porno', 'pornography', 'prince albert piercing', 'pthc', 'pubes', 'pussy', 'queaf', 'raghead', 'raging boner', 'rape', 'raping', 'rapist', 'rectum', 'reverse cowgirl', 'rimjob', 'rimming', 'rosy palm', 'rosy palm and her 5 sisters', 'rusty trombone', 'sadism', 'santorum', 'scat', 'schlong', 'scissoring', 'semen', 'sex', 'sexo', 'sexy', 'shaved beaver', 'shaved pussy', 'shemale', 'shibari', 'shit', 'shota', 'shrimping', 'skeet', 'slanteye', 'slut', 's&m', 'smut', 'snatch', 'snowballing', 'sodomize', 'sodomy', 'spic', 'splooge', 'splooge moose', 'spooge', 'spread legs', 'spunk', 'strap on', 'strapon', 'strappado', 'strip club', 'style doggy', 'suck', 'sucks', 'suicide girls', 'sultry women', 'swastika', 'swinger', 'tainted love', 'taste my', 'tea bagging', 'threesome', 'throating', 'tied up', 'tight white', 'tit', 'tits', 'titties', 'titty', 'tongue in a', 'topless', 'tosser', 'towelhead', 'tranny', 'tribadism', 'tub girl', 'tubgirl', 'tushy', 'twat', 'twink', 'twinkie', 'two girls one cup', 'undressing', 'upskirt', 'urethra play', 'urophilia', 'vagina', 'venus mound', 'vibrator', 'violet blue', 'violet wand', 'vorarephilia', 'voyeur', 'vulva', 'wank', 'wetback', 'wet dream', 'white power', 'women rapping', 'wrapping men', 'wrinkled starfish', 'xx', 'xxx', 'yaoi', 'yellow showers', 'yiffy', 'zoophilia']
    ,initialize:function(){
        this.$header = $('#title h1');

        //observers
        $('#main').on('gb2.titleUpdate',function(){
            var proposed = $('#popup input').val();

            AMS.Analytics.logClick(['title-update',proposed]);

            if(this.isValid(proposed)){
                this.value = this.cleanString(proposed);
                this.updateTitle();
                this.updateURL(this.value);
            }
        }.bind(this));

        this.$header.on('click',function(e){
            AMS.GB2.Popup.prompt({
                message:'Every great weapon needs a name.<br/> Type in the name of your gun.'
                ,yes:function(){
                    $('#main').trigger('gb2.titleUpdate');
                }
            })
        }.bind(this));

        //set initial value
        var proposed = this.getGunTitle() || 'Name Your Gun';
        if(this.isValid(proposed)){
            this.value = this.cleanString(proposed);
            this.updateTitle();
        }else{
            this.updateURL('');
        }
    }
    ,isValid:function(proposed){
        //if badwords reject
        for(var word in this.badwords){
            if(proposed.indexOf(this.badwords[word])!=-1){;
                return false;
            }
        }
        return true;
    }
    ,cleanString:function(proposed){
        //remove all bad characters
        return proposed.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').substring(0,30);
    }
    ,setRealTitle:function(){
        document.title = this.value;
    }
    ,getGunTitle:function(){
        var url = window.location.pathname.split('/');
        return url[url.length-1].replace(/-/g,' '); //decode: replace all dashes with a space
    }
    ,updateURL:function(value){
        var gunName = value.replace(/\s/g,'-'); //encode: replace all spaces with a dash

        var url = window.location.pathname.split('/');
        url[url.length-1] = gunName;

        var updated = 'http://'+ window.location.hostname +url.join('/')+window.location.hash;
        window.location.replace(updated);
    }
    ,updateTitle:function(){
        this.setRealTitle(this.value);
        this.$header.html(this.value);
    }
};

AMS.GB2.Price = {
    initialize:function(){
        $('#main').on('gb2.change',function(){this.updatePrice()}.bind(this));
        this.$el = $('#price');
        this.updatePrice();
    }
    ,updatePrice:function(){
        var price = this.calculatePrice();
        this.$el.html('Total Price: $'+price);
    }
    ,calculatePrice:function(){
        var price = 0.00;
        $(AMS.GB2.Build).each(function(index,item){
            if(item.item_id != null){
                var part = _.find(Parts,function(i){return i.item_id == item.item_id});
                price+= part.price;
            }
        });
        return price.toFixed(2);
    }
};

AMS.GB2.Popup = {
    initialize:function(){
        $('#popup').on('click',function(e){e.stopPropagation()});
    }
    ,show:function(){
        $('.backdrop').on('click',function(){this.hide()}.bind(this));
        $('.backdrop').fadeIn();
        $('#popup').show().animate({'top':'200px'});
    }
    ,hide:function(callback){
        $('#popup').stop().animate({'top':'-500px'})
        $('#popup').hide();
        $('.backdrop').fadeOut(callback);
    }
    ,confirm:function(obj){
        var that = this;
        var $el = $('#popup');
        $el.html($('#_tmpl_message_confirm').html());

        //change out message
        $el.find('p').html(obj.message);

        //tie action to button click
        $el.find('[data-id=true]').on('click', function(){that.hide(obj.yes)});
        $el.find('[data-id=false]').on('click', function(){that.hide()});

        this.show();
    }
    ,prompt:function(obj){
        var that = this;
        var $el = $('#popup');
        $el.html($('#_tmpl_message_prompt').html());

        //change out message
        $el.find('p').html(obj.message);

        //tie action to button click
        $el.find('[data-id=true]').on('click', function(){that.hide(obj.yes)});
        $el.find('[data-id=false]').on('click', function(){that.hide()});

        this.show();
    }
    ,alert:function(obj){
        var that = this;
        var $el = $('#popup');
        $el.html($('#_tmpl_message_alert').html());

        //change out message
        $el.find('p').html(obj.message);

        //tie action to button click
        $el.find('[data-id=true]').on('click', function(){that.hide(obj.yes)});

        this.show();
    }
    ,share:function(){
        var that = this;
        var $el = $('#popup');

        var url = (!location.href.indexOf('?'))
                ?encodeURIComponent(location.href)
                :encodeURIComponent(location.href.replace('#','?'));

        var html = $('#_tmpl_message_share').html();
        html = html.replace(/\{\{url\}\}/g,url);
        $el.html(html);

        //tie action to button click
        $el.find('[data-id=true]').on('click', function(){that.hide(obj.yes)});

        this.show();
    },
    checkout:function(obj,show){
        var that = this;
        var $el = $('#popup');

        var source   = $("#_tmpl_message_checkout").html();
        var template = Handlebars.compile(source);

        var html = template(obj);
        $el.html(html);
        if(show){
            //tie action to button click
            $el.find('[data-id=true]').on('click', function(){that.hide(obj.yes)});
            this.show();
        }
    }
};

AMS.GB2.FinalOrder = {
    initialize:function(){
        var that = this;
        $('#main').on('gb2.finish',function(){this.show()}.bind(this));

        $('#finalOrder').on('click',function(e){e.stopPropagation()});

        $('#finalOrder button').on('click',function(){

            AMS.Analytics.logClick(['finalOrder','checkout']);

            var id = $(this).attr('data-id');
            that.cart = [id] ;
            $('#main').trigger('gb2.complete');
        });

        $('.backdrop').on('click',function(){this.hide()}.bind(this));
        this.reset();

        AMS.Analytics.logClick(['finalOrder','start']);
    }
    ,show:function(){
        AMS.GB2.Essentials.reset();
        this.slide();
    }
    ,reset:function(){
        $('#finalOrder').css({'top':-1000,'display':'none'});
    }
    ,hide:function(){
        $('#finalOrder').hide();
        $('.backdrop').fadeOut(500);

        AMS.Analytics.logClick(['finalOrder','bounce']);
    }
    ,slide:function(){
        $('#finalOrder').css({'display':'block'}).animate({top:0},350);
    }
}

AMS.GB2.Essentials = {
    initialize:function(){
        this.categoryId = Platform.upsellCategoryId;
        this.isInitialized = true;
        this.parts = AMS.GB2.getPartsFromCategoryId(this.categoryId);

        this.buildList();

        $('#upsell').on('click',function(e){
            e.stopPropagation()
            $('.upsell-details').fadeOut(250).hide();
        });
        $('.backdrop').on('click',function(){this.hide()}.bind(this));

        $('#upsell .checkout').on('click',function(){
            $('#main').trigger('gb2.finish');
            this.setSelectedItems();
        }.bind(this));

        this.buildEvaluation();

        //observer refresh list if there was a change in the product config
        $('#main').on('gb2.change',function(){
            this.isInitialized = false;
        }.bind(this));
    }
    ,reset:function(){
        $('#upsell').css({top:-5000}).addClass('hide');
    }
    ,show:function(){
        if(!this.isInitialized){
            this.initialize();
        }else{
            this.buildEvaluation();
        }
        $('#upsell').removeClass('hide').animate({top:0},350, function(){$(this).removeClass('hide')})
        $('.backdrop').fadeIn(500);
    }
    ,hide:function(){
        $('#upsell').animate({top:-5000},350, function(){$(this).addClass('hide')});
        $('.backdrop').fadeOut(500);
    }
    ,buildList:function(){
        var that = this;
        var source   = $("#_tmpl_menu_popuplist").html();
        var template = Handlebars.compile(source);

        $(this.parts).each(function(){
            var incompatible = AMS.GB2.getIncompatible(this.item_id);
            this.isDisabled = (incompatible.length)? 1 : 0;
        });

        var html = template(this.parts);
        $('.upsell-list').html(html);

        $('.upsell-list li .more').on('click',function(e){
            e.stopPropagation();

            var $parent = $(this).parent();
            var id = $parent.attr('data-id');

            AMS.Analytics.logClick(['upsell-moreInfo',id]);
            that.updateDetails(id);
        })

        $('.upsell-list li').on('click',function(e){

            var id = $(this).attr('data-id');

            $(this).hasClass('selected')?
            AMS.Analytics.logClick(['upsell-unselect',id]):
            AMS.Analytics.logClick(['upsell-select',id]);

            $(this).toggleClass('selected');
            that.buildEvaluation();
        })
    }
    ,buildDetails:function(part,tmpl){
        var source   = $("#_tmpl_menu_details").html();
        var template = Handlebars.compile(source);

        var html = template(part);
        $('.upsell-details').html(html);

        $('.upsell-details button').on('click',function(e){
            var id = $(this).attr('data-rel');

            $('.upsell-list [data-id='+id+']').trigger('click');
        });

    }
    ,setSelectedItems:function(){
        var that = this;
        this.cart = [];
        $('.upsell-list .selected').each(function(){
            var item_id = $(this).attr('data-id');
            that.cart.push(item_id);
        })
    }
    ,getSelectedItems:function(){
        var calculation = [];

        $('.upsell-list .selected').each(function(){
            var productName = $(this).find('h3').text();
            var productPrice = $(this).find('.price').text().substring(1);

            calculation.push({name:productName,price:productPrice});
        })

        return calculation;
    }
    ,getSubtotal:function(total,items){
        var subtotal = parseFloat(total);
        $(items).each(function(){
            subtotal+=parseFloat(this.price);
        });
        return subtotal.toFixed(2);
    }
    ,buildEvaluation:function(){
        var source   = $("#_tmpl_menu_evaluation").html();
        var template = Handlebars.compile(source);
        var calculation = {};

        calculation.gunPrice = AMS.GB2.Price.calculatePrice();
        calculation.list = this.getSelectedItems();
        calculation.subTotal = this.getSubtotal(calculation.gunPrice,calculation.list);

        var html = template(calculation);
        $('.upsell-evaluation').html(html);
    }
    ,updateDetails:function(id){
        $('.upsell-details').fadeIn(300);
        var part = AMS.GB2.getPartByItemId(id);
        this.buildDetails(part);
    }
};

AMS.Messenger = {
    stamps:[],
    ready:function(){
        //set start time
        //set GB Loaded event listener, upon trigger showMessages
    }
    ,initialized:function(str){
        //[timestamp] with time stamp show loaded
        this.stamps.push('['+this.getTimestamp()+']'+str+' initialized');
    }
    ,changed:function(str){
        //[timestamp] Obj observed change
        this.stamps.push('['+this.getTimestamp()+']'+str+' observed change');
    }
    ,selected:function(str){
        this.stamps.push('['+this.getTimestamp()+']'+str+' has been selected');
    }
    ,showMessages:function(){
        //loop through all messages and write them to the console if messaging is turned on
        $(this.stamps).each(function(){
            console.log(this);
        });
    }
    ,getTimestamp:function(){
        return Math.floor(Date.now() / 1000);
    }
};
