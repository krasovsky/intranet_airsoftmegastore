[
  {
    "id":1,
    "slug": "m4",
    "platform_name":"Elite M4 Platform",
    "description":"The tried-and-true M4 platform has become the most ubiquitous rifle of western militaries, and is the standard issue carbine for the U.S. Armed Forces. Popular for its modularity, lightweight design, and modern functionality, the M4 carbine is one of the most adaptable platforms on the market. Our airsoft M4 platform AEG rifle components are quality manufactured and designed for airsoft players looking to create the perfect customized airsoft rifle to suit their tastes and gameplay needs. Start building your customized M4 platform airsoft rifle now!",
    "img":"/customizer/images/guns/m4/platform-m4.png",
    "build_btn_text":"Build Your M4",
    "data_dir" : "/customizer/data/m4",
    "build_btn_enabled":true,
    "show":true,
    "beta":false
  },
  {
    "id":2,
    "slug": "ak47",
    "platform_name":"Kalashnikov - AK47",
    "description":"Known for their reliability and rugged design, the AK platform is the most widely used assault rifle design in the world! Based on the iconic AK-47 assault rifle, designed by the Soviet Union at the beginning of the Cold War, AK style rifles have since proliferated across the globe! Build your very own custom AK airsoft rifle with the AMS Gun Builder, using our array of AK parts and compatible accessories! Whether you want a compact AK for indoor CQB work, or a full size AK for long range outdoor takedowns, Airsoft Megastore’s Gun Builder has the parts and accessories you need to make your dream AK come to life! Build you custom AK now!",
    "img":"/customizer/images/guns/ak47/platform-ak.png",
    "build_btn_text":"Build Your AK",
    "data_dir" : "/customizer/data/ak47",
    "build_btn_enabled":true,
    "show":true,
    "beta":true
  }
]