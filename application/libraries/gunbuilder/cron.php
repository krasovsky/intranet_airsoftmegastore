<?php
//This is a master build file that will generate all builds upon a schedule

$path = realpath(dirname(__FILE__));

$dir = new DirectoryIterator($path);
foreach ($dir as $fileinfo) {
    if ($fileinfo->isDir() && !$fileinfo->isDot()) { //if is directory
        $name = $fileinfo->getFilename();
        if($name[0] != '_' && $name[0] != '.' && $name != 'assets'){
            $update_script = $path.'\\'.$name.'\\update.php'; //update script for the build

            if(is_file($update_script))
                include_once($update_script); //update runs

        $release = 'test';
        $js = $path.'\\'. $name.'\\parts3.js';
        $release_folder = $path.'\\_releases\\'.$release.'\\'. $name;

        if (!file_exists($release_folder)) {
            mkdir($release_folder, 0777, true);

        }

        copy( $js, $release_folder.'\\parts3.js');

        }
    }
}