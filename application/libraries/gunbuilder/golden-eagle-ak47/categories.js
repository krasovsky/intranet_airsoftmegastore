var Platform = {"name": "golden-eagle-ak47", "upsellCategoryId": 380, "Build": [
    {"id": 375, "base_id": 13329, "category_name": "Receiver", "item_id": null, "requiredForCheckout": true},
    {"id": 389, "base_id": 13326, "category_name": "Forend", "item_id": null, "requiredForCheckout": true},
    {"id": 377, "base_id": 13339, "category_name": "Stocks", "item_id": null, "requiredForCheckout": true},
    {"id": 376, "base_id": 13340, "category_name": "Motor Grips", "item_id": null, "requiredForCheckout": true},
    {"id": 378, "base_id": 13334, "category_name": "Foregrips", "item_id": null, "requiredForCheckout": false,"imgResized": true,
        "position":{
            "barrel-bottom":{
                "align":"topCenter"
            }
        }},
    {"id": 379, "base_id": 1333, "category_name": "Optics", "item_id": null, "requiredForCheckout": false,"imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomRight"
            }
        }},
    {"id": 392, "base_id": 13336, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false}
]};