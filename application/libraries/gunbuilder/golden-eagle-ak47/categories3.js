var Platform = {
    "name":"ak47",
    "Build": [
        {
            "id": 1,
            "category_name": "Receivers",
            "category_group": 1,
            "base_id": 13329,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 2,
            "category_name": "Barrel Assembly",
            "category_group": 4,
            "base_id": 13325,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 3,
            "category_name": "Stocks",
            "category_group": 3,
            "base_id": 13339,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 7,
            "category_name": "Grips",
            "category_group": 2,
            "base_id": 13340,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 6,
            "category_name": "Foregrips",
            "category_group": 5,
            "base_id": 13334,
            "item_id": null,
            "imgResized": true,
            "requiredForCheckout": false,
            "position":{
                "barrel-bottom":{
                    "align":"topCenter"
                }
            }
        },
        {
            "id": 10,
            "category_name": "Optics",
            "category_group": 6,
            "base_id": 1333,
            "item_id": null,
            "imgResized": true,
            "requiredForCheckout": false,
            "position":{
                "barrel-rear":{
                    "align":"bottomRight"
                }
            }
        },
        {
            "id": 11,
            "category_name": "Magazines",
            "category_group": 7,
            "base_id": 13336,
            "item_id": null,
            "requiredForCheckout": false
        }
    ]
};