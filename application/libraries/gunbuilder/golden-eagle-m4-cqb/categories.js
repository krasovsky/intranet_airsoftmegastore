var Platform = {"name": "golden-eagle-m4-cqb", "upsellCategoryId": 407, "Build": [
    {"id": 399, "base_id": 12805, "category_name": "Lower", "item_id": null, "requiredForCheckout": true},
    {"id": 400, "base_id": 12784, "category_name": "Upper", "item_id": null, "requiredForCheckout": true},
    {"id": 401, "base_id": 12811, "category_name": "Stocks", "item_id": null, "requiredForCheckout": true},
    {"id": 402, "base_id": null, "category_name": "Front Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-front":{
                "align":"bottomLeft"
            }
        }},
    {"id": 403, "base_id": 12797, "category_name": "Foregrips", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-bottom":{
                "align":"topCenter"
            }
        }},
    {"id": 405, "base_id": 1338, "category_name": "Optics and Rear Sights", "item_id": null, "requiredForCheckout": false,"imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomRight"
            }
        }},
    {"id": 406, "base_id": 12800, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false},
    {"id": 448, "base_id": 12803, "category_name": "Rail Panels", "item_id": null, "requiredForCheckout": false}
]};