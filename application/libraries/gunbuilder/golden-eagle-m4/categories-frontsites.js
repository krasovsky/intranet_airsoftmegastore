var Platform = {"name": "golden-eagle-m4", "upsellCategoryId": 374, "Build": [
    {"id": 368, "base_id": 12805, "category_name": "Lower", "item_id": null, "requiredForCheckout": false},
    {"id": 369, "base_id": 12785, "category_name": "Upper", "item_id": null, "requiredForCheckout": false},
    {"id": 370, "base_id": 12783, "category_name": "Stocks", "item_id": null, "requiredForCheckout": false},
    {"id": 371, "base_id": null, "category_name": "Front Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "site-front":{
                "align":"bottomRight"
            }
        }},
    {"id": 372, "base_id": 12797, "category_name": "Bipods and Foregrips", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-bottom":{
                "align":"topLeft",
                "x":-30
            }
        }},
    {"id": 373, "base_id": 1333, "category_name": "Optics and Rear Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomRight"
            }
        }},
    {"id": 391, "base_id": 12800, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false},
    {"id": 447, "base_id": null, "category_name": "Rail Panels", "item_id": null, "requiredForCheckout": false}
]};