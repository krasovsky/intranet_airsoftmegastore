var Platform = {
    "name": "m4",
        "upsellCategoryId": 374,
        "Build": [
        {
            "id": 368,
            "category_name": "Lower",
            "base_id": 12807,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 369,
            "category_name": "Upper",
            "base_id": 12786,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 370,
            "category_name": "Stocks",
            "base_id": 12810,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 371,
            "category_name": "Iron Sights",
            "base_id": 12794,
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position":{
                "barrel-rear2":{
                    "align":"bottomRight"
                }
            }
        },
        {
            "id": 372,
            "category_name": "Foregrips",
            "base_id": 11071,
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position":{
                "barrel-bottom":{
                    "align":"topCenter"
                }
            }
        },
        {
            "id": 373,
            "category_name": "Optics",
            "base_id": 1337,
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position":{
                "barrel-rear":{
                    "align":"bottomRight"
                }
            }
        },
        {
            "id": 391,
            "category_name": "Magazines",
            "base_id": 12801,
            "item_id": null,
            "requiredForCheckout": false
        }
    ]
};