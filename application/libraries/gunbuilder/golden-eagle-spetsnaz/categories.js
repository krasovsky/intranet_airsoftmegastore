var Platform = {"name": "golden-eagle-spetsnaz", "upsellCategoryId": 415, "Build": [
    {"id": 408, "base_id": 13329, "category_name": "Receiver", "item_id": null, "requiredForCheckout": true},
    {"id": 411, "base_id": 13328, "category_name": "Forend", "item_id": null, "requiredForCheckout": true},
    {"id": 410, "base_id": 13339, "category_name": "Stocks", "item_id": null, "requiredForCheckout": true},
    {"id": 409, "base_id": 13522, "category_name": "Motor Grips", "item_id": null, "requiredForCheckout": true},
    {"id": 412, "base_id": 11083, "category_name": "Foregrips", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-bottom":{
                "align":"topLeft"
            }
        }},
    {"id": 413, "base_id": 1349, "category_name": "Optics", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomRight"
            }
        }},
    {"id": 414, "base_id": 13338, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false}
]};
