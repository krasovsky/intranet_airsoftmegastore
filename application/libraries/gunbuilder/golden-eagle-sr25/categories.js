var Platform = {
    "name": "golden-eagle-sr25",
    "upsellCategoryId": 469,
    "Build": [{
        "id": 459,
        "base_id": 13929,
        "category_name": "Lower",
        "item_id": null,
        "requiredForCheckout": true
    }, {
        "id": 458,
        "base_id": 13914,
        "category_name": "Upper",
        "item_id": null,
        "requiredForCheckout": true
    }, {
        "id": 460,
        "base_id": 12791,
        "category_name": "Stocks",
        "item_id": null,
        "requiredForCheckout": true
    }, {
        "id": 464,
        "base_id": 49,
        "category_name": "Bipods and Foregrips",
        "item_id": null,
        "requiredForCheckout": false,
        "imgResized": true,
        "position": {
            "barrel-bottom": {
                "align": "topLeft",
                "x": -30
            }
        }
    }, {
        "id": 465,
        "base_id": null,
        "category_name": "Front Sights",
        "item_id": null,
        "requiredForCheckout": false,
        "imgResized": true,
        "position": {
            "barrel-front": {
                "align": "bottomLeft"
                , "x": -5
            }
        }
    }, {
        "id": 466,
        "base_id": 1335,
        "category_name": "Optics and Rear Sights",
        "item_id": null,
        "requiredForCheckout": false,
        "imgResized": true,
        "position": {
            "barrel-rear": {
                "align": "bottomRight"
            }
        }
    }, {
        "id": 468,
        "base_id": 2055,
        "category_name": "Magazines",
        "item_id": null,
        "requiredForCheckout": false
    }],
    "Position": {
        "13297": {
            "barrel-rear": {
                "align": "bottomLeft"
            }
        }, "7073": {
            "barrel-rear": {
                "align": "bottomLeft"
            }
        }, "1349": {
            "barrel-rear": {
                "align": "bottomLeft"
            }
        }, "1333": {
            "barrel-rear": {
                "align": "bottomLeft"
            }
        }, "483": {
            "barrel-rear": {
                "align": "bottomLeft"
            }
        }, "1337": {
            "barrel-rear": {
                "align": "bottomCenter"
            }
        }, "4545": {
            "barrel-rear": {
                "align": "bottomCenter"
            }
        }, "72": {
            "barrel-bottom": {
                "align": "topLeft",
                "x": 80
            }
        }, "1951": {
            "barrel-bottom": {
                "align": "topLeft",
                "x": 60
            }
        }, "1952": {
            "barrel-bottom": {
                "align": "topLeft",
                "x": 60
            }
        }
    }
};