<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 5/8/15
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */

//select all products from database where in attributes show in platform
define('DS' , '\\');

class GunBuilder{

    public function __construct($name = null ,$cat_id = null){
        
        if($name == null && $cat_id == null)
            return;

        $this->cat_id = $cat_id;
        $this->name = $name;
        $this->app = realpath(dirname(__FILE__));
        $this->dataPath =   $this->app.'airsoft-gun-builder\\assets\\data\\'.$name.'\\'.$name.'.json';
        $this->imgSource =   $this->app.'airsoft-gun-builder\\assets\\source';
        $this->imgCustomSource =   $this->app.'airsoft-gun-builder\\assets\\source\\'.$name;
        $this->base =        $this->app.'airsoft-gun-builder\\'.$name;
        $this->imgPath =    $this->base .'\\media';
        $this->svgPath =    $this->base .'\\media\\svg\\';
        $this->bmpPath =    $this->base .'\\media\\bmp\\';
        $this->resizePath = $this->base .'\\media\\resized\\';

        $this->file = @file_get_contents($this->dataPath)?:'[]';
        $this->json = json_decode($this->file);

        $cats = preg_replace('/\n*\r*/','',file_get_contents($this->base.'\\categories.js'));
        $cats = substr($cats, stripos($cats,'var Platform = ')+15,  -1 );
        $this->categories = json_decode($cats);

// Create connection
        $user = 'root';
        $pass = '';
        $this->db = new mysqli("192.168.1.9", "vladk", "M3ga$0ft", "ams");
// Check connection
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }


    public function setup(){
        //if folders do not exist then create them
        if(!is_dir($this->imgPath))mkdir($this->imgPath);
        if(!is_dir($this->svgPath))mkdir($this->svgPath);
        if(!is_dir($this->resizePath))mkdir($this->resizePath);
        if(!is_dir($this->bmpPath))mkdir($this->bmpPath);
    }

    public function removeWorkDirectories(){
        $this->recursiveRemoveDirectory($this->svgPath);
        $this->recursiveRemoveDirectory($this->bmpPath);
    }

    protected function recursiveRemoveDirectory($directory)
    {
        foreach(glob("{$directory}/*") as $file)
        {
            if(is_dir($file)) {
                $this->recursiveRemoveDirectory($file);
            } else {
                unlink($file);
            }
        }
        rmdir($directory);
    }

    public function renameImages(){
        foreach($this->json as $item){
            if($item->render_img == 'blank.png') continue;
            if(is_file($this->imgCustomSource.DS.$item->render_img)){
                copy($this->imgCustomSource.DS.$item->render_img, $this->imgPath.DS.$item->item_id.'.png');
            }else{
                copy($this->imgSource.DS.$item->render_img, $this->imgPath.DS.$item->item_id.'.png');
            }
        }
    }

    public function updateImages(){
        foreach($this->json as $item){

            //resize the image, then get the size of the resized image put within object
            if($item->render_img == 'blank.png')continue;

            $this->resizePNG($item);
            $item = $this->setSizeOfProduct($item);
            $item = $this->setSVG($item);


            var_dump($item);
        }

        $this->finishFile('parts2');
    }

    public function fixData(){
        foreach($this->json as $item){
            $this->updateProductsInDB($item);
        }
    }

    public function updateData(){
        $this->json = array();

        $products = $this->getProductsFromDB();

        foreach($products as $item){

            $item = (object)$item;

            $item->item_id = (int)$item->item_id;
            $item->product_id = (int)$item->product_id;
            $item->category  = (int)$item->category;
            $item->scaleFactor  = (int)$item->scaleFactor;
            $item->price = (float)$item->price;
            $item->out_of_stock = strtolower($item->out_of_stock) == 'true';

            if($item->prohibit_products[0] == ''){
                $item->prohibit_products = null;
            }else{
                foreach($item->prohibit_products as $key=>$val){
                    $item->prohibit_products[$key] = (int)$val;
                }
            }

            //gather all images prefixed with the item id and create items from them using duplicate info
            $accessories = $this->getDefaultAccessories($item->item_id);

            //resize the image, then get the size of the resized image put within object
            if($item->category != $this->categories->upsellCategoryId){
                $this->moveImage($item);
                $this->resizePNG($item);
                $item = $this->setSizeOfProduct($item);
                $item = $this->setSVG($item);
            }

            if($item->masterPosition){
                $item->masterPosition = json_decode($item->masterPosition);
            }else{
                unset($item->masterPosition);
            }

            if($item->position){
                $item->position = json_decode($item->position);
            }else{
                unset($item->position);
            }

            $this->json[] = $item;
            var_dump($item);
        }

        $this->finishFile('parts3');
    }

    protected function moveImage($item){
        $name = $this->getImageName($item);

        if(is_file($this->imgCustomSource.DS. $name .'.png')){
            copy($this->imgCustomSource.DS. $name .'.png', $this->imgPath.DS. $name .'.png');
        }else{
            copy($this->imgSource.DS. $name .'.png', $this->imgPath.DS. $name .'.png');
        }
    }

    protected function getImageName($item){
        // return substr($item->render_img, 0,-4);
        return $item->item_id;
    }

//    private function setPosition($item){
//        if($item->category == 10){
//            $item->position = array('barrel-bottom'=>'{}');
//        }elseif($item->category == 6 ){
//            $item->position = array('barrel-rear'=>'{}');
//        }
//    }
    public function updateSVG($item){
        //$svg_file = $this->svgPath . substr($item->render_img, strrpos($item->render_img, '/')+1, -4).'.svg' ;

        $name = $this->getImageName($item);

        $svg = $this->svgPath.$name.'.svg';

        if(is_file($svg)){
            $xml = simplexml_load_file ($svg);
            $item->svg = (string)$xml->g->path['d'];
        }

        return $item;
    }
    protected function setSizeOfProduct($item,$offsets = array()){
        $name = $this->getImageName($item);
        $img = $this->resizePath.$name.'.png';

        if(is_file($img)){
            list($width,$height) = getimagesize($img);
            $item->dimensions = array('width'=>$width,'height'=>$height);
        }

        if(! empty($offsets)){
            $item->dimensions["offsetX"] = $offsets["offsetX"];
            $item->dimensions["offsetY"] = $offsets["offsetY"];
        }
        return $item;
    }

    private function renameImageToItemId(){

    }

    private function setStockLevel($item){
        if(!isset($item->out_of_stock)){
            $item->out_of_stock = 'false';
        }
        //check the data base for this item id
    }

    private function removeLongDescription($item){
        unset($item->long_description);
    }

    private function removePNGPath($item){
        //Roberts old data needs to be updated
        $item->render_img = substr($item->render_img, strrpos($item->render_img,'/')+1);
    }

    protected function resizePNG($item){
        $name = $this->getImageName($item);
        $img = $this->imgPath.DS. $name .'.png';
        $img2 = $this->resizePath.$name.'.png';

        if(is_file($img)){
            $cmd = 'C:\"Program Files"\ImageMagick-6.9.1-Q16\convert '.$img.' -trim -bordercolor transparent +antialias +repage '.$img2.' 2>&1';
            system($cmd);
        }
    }

    private function checkIsResized($item){

        foreach($this->categories->Build  as $c){
            if($c->id == $item->category){
                if(isset($c->imgResized) && $c->imgResized == true)
                    return true;
            }
        }
        return false;
    }

    protected function setSVG($item){
        $name = $this->getImageName($item);

        if($name == 'blank')return $item;

        $isResized = $this->checkIsResized($item);

        $this->overExposePNG($name, $isResized);
        $this->createSVGFromBMP($name);

        return $this->updateSVG($item);
    }

    private function overExposePNG($name, $useResize=false){
        if($useResize){
            $cmd = 'convert '.$this->resizePath . $name. '.png -alpha extract -threshold 1% -negate -transparent white '.$this->bmpPath. $name.'.bmp';
        }else{
            $cmd = 'convert '.$this->imgPath .DS. $name. '.png -alpha extract -threshold 1% -negate -transparent white '.$this->bmpPath. $name.'.bmp';
        }
        return system($cmd);
    }
    private function createSVGFromBMP($name){
        $cmd = 'potrace '. $this->bmpPath . $name.'.bmp -s -o '. $this->svgPath .$name.'.svg';
        return system($cmd);
    }

    function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                $this->utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
    }

    protected function finishFile($name){
        //$this->utf8_encode_deep($this->json);
        //dump($this->json);
        $jsonString = json_encode($this->json);
        $error = "";
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                $this->utf8_encode_deep($this->json);
                $jsonString = json_encode($this->json);
            break;
        }
        //if(! empty($error)) throw new Exception("UNABLE TO ENCODE ARRAY : error message {$error}");
        
        //dump($error);
        //dump($jsonString);
        //die("asdasd");
        file_put_contents($this->base.DS.$name.'.js','var Parts = '.$jsonString);
    }

    public function getProductsFromDB(){
        $products = array();

        $sql = "SELECT DISTINCT i.id AS 'item_id',
                        i.product_id,
                        i.sku,
                        xref.`category_id` AS category,
                        TRUNCATE(pp.price - (pp.price *(p.on_sale/100)), 2) AS price,
                        gb.brand,
                        gb.`incompatible` AS 'prohibit_products',
                        CONCAT(i.id,'.png') AS 'render_img',
                        gb.name AS 'product_name',
                        gb.features AS 'short_description',
                        IF(i.inventory > 0, 'false', 'true' ) AS 'out_of_stock',
                        gb.masterPosition,
                        gb.position,
                        gb.scale as scaleFactor
        FROM categories_xref xref
        LEFT JOIN products p ON xref.product_id = p.id
        LEFT JOIN items i ON i.`product_id` = xref.`product_id`
        LEFT JOIN gunbuilder gb ON gb.id = i.product_id
        LEFT JOIN `productpricings` pp ON i.product_id = pp.id
        WHERE category_id IN ( SELECT id FROM categories WHERE parent_id = {$this->cat_id} )
        AND pp.quantity = 1
        AND p.merchandisable = 1
        ";

        $result = $this->db->query($sql);

        if($this->db->error) echo  $this->db->error;

        while($row = $result->fetch_assoc()) {
            $products[$row["item_id"]] = $row;
            $products[$row["item_id"]]['prohibit_products'] = explode(',',$products[$row["item_id"]]['prohibit_products']);
        }

        return $products;
    }

    public function updateProductsInDB($item){
        $servername = "localhost";
        $username = "root";
        $password = "2abstrakt";
        $dbname = "ams";

// Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $incompatible = (count($item->prohibit_products)) ? implode(',', $item->prohibit_products): null ;
        $position = (isset($item->position)) ? json_encode($item->position) : '' ;
        $masterPosition = (isset($item->masterPosition)) ? json_encode($item->masterPosition) : '' ;

        $sql = "UPDATE gunbuilder
                SET
                incompatible = '$incompatible',
                masterPosition = '$masterPosition',
                position = '$position'
                WHERE id = {$item->product_id}";

        echo $sql . '</br>';
        if ($conn->query($sql) === TRUE) {
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . $conn->error;
        }

        $conn->close();
    }

    protected function getCategoriesFromDB($id){

        $sql = "SELECT DISTINCT c.id, i.id base_id, c.name category_name FROM categories c
                LEFT JOIN categories_xref xref ON c.`id` = xref.`category_id`
                LEFT JOIN items i ON i.product_id = xref.`product_id`
                WHERE parent_id = $id
                GROUP BY c.id";

        $result = $this->db->query($sql);

        $categories = array();

        while($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }

        return $categories;
    }


    private function getItemIds(){
        $item_ids = array();
        foreach($this->json as $obj){
            $item_ids[] = $obj->item_id;
        }
        return $item_ids;
    }

    public function buildCategories(){
        $json = json_decode('{ "name": "'.$this->name.'", "upsellCategoryId": 0, "Build": []}');

        $categories= $this->getCategoriesFromDB($this->cat_id);

        foreach($categories as $c){

            $c['id'] = (int)$c['id'];
            $c['base_id'] = (int)$c['base_id'];
            $c['item_id'] = null;
            $c['requiredForCheckout'] = false ;

            $json->Build[] = $c;

            if(strpos(strtolower($c['category_name']),'accessories')!=-1) $json->upsellCategoryId = $c['id'];
        }

        file_put_contents($this->base.DS.'categories.js','var Platform = '.json_encode($json).';');
    }

    protected static function prettyPrint( $json )
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen( $json );

        for( $i = 0; $i < $json_length; $i++ ) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if( $ends_line_level !== NULL ) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ( $in_escape ) {
                $in_escape = false;
            } else if( $char === '"' ) {
                $in_quotes = !$in_quotes;
            } else if( ! $in_quotes ) {
                switch( $char ) {
                    case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                    case '{': case '[':
                    $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
                }
            } else if ( $char === '\\' ) {
                $in_escape = true;
            }
            if( $new_line_level !== NULL ) {
                $result .= "\n".str_repeat( "\t", $new_line_level );
            }
            $result .= $char.$post;
        }

        return $result;
    }
}



//$dir = new DirectoryIterator($svg);
//foreach ($dir as $fileinfo) {
//    if (!$fileinfo->isDot()) {
//        $xml = simplexml_load_file ($fileinfo->getPath().'\\'.$fileinfo->getFilename());
//        var_dump($xml->g->path['d']);
//    }
//}