<?php

foreach($argv as $key => $arg){
	switch($arg){
		case "-config":
			$gunCategorisJsFilePath = $argv[$key + 1];
			break;
		case "-part":
			$gunPartsJsFilePath = $argv[$key +1];
			break;
		case "help":
			echo "use\n -config for config file path \n -part = for part file location";
			return;
			break;
	}
}
// $gunCategorisJsFilePath = "_ak47\\categories.js";
// $gunPartsJsFilePath = "_ak47\\parts2.js";
//$outputFile = "_ak47\\permutations.js";

if(empty($gunCategorisJsFilePath) || empty($gunPartsJsFilePath)){
	echo "Wrong parametrs \n use\n -config for config file path \n -part = for part file location";
	return;
}

function permutations(array $array, $inb=false)
{
    switch (count($array)) {
        case 1:
            // Return the array as-is; returning the first item
            // of the array was confusing and unnecessary
            return $array[0];
            break;
        case 0:
            throw new InvalidArgumentException('Requires at least one array');
            break;
    }
 
    // We 'll need these, as array_shift destroys them
    $keys = array_keys($array);
     
    $a = array_shift($array);
    $k = array_shift($keys); // Get the key that $a had
    $b = permutations($array, 'recursing');
     
    $return = array();
    foreach ($a as $v) {
        if($v)
        {
            foreach ($b as $v2) {
                // array($k => $v) re-associates $v (each item in $a)
                // with the key that $a originally had
                // array_combine re-associates each item in $v2 with
                // the corresponding key it had in the original array
                // Also, using operator+ instead of array_merge
                // allows us to not lose the keys once more
                if($inb == 'recursing')
                    $return[] = array_merge(array($v), (array) $v2);
                else
                    $return[] = array($k => $v) + array_combine($keys, $v2);
            }
        }
    }
 
    return $return;
}

$partsForCheck = array();

//get get config file for required categories
$configJson = preg_replace('/\n*\r*/','',file_get_contents($gunCategorisJsFilePath));
$configJson = substr($configJson, stripos($configJson,'var Platform = ')+15,  -1 );
$configJson = json_decode($configJson,true);

if(empty($configJson) || $configJson == false) {
	echo ("CONFIG FILE IS BROKEN / NOT FOUND");
	return;
}

foreach($configJson["Build"] as $element){
	if($element["requiredForCheckout"] == true){
		$partsForCheck[$element["id"]] = array();
	}
}

//read parts fron parts.js
$partsFile = preg_replace('/\n*\r*/','',file_get_contents($gunPartsJsFilePath));
$partsJson = substr($partsFile, stripos($partsFile,'var Parts = ')+12);
$partsArr = json_decode($partsJson,true);

if(empty($partsArr) || $partsArr == false) {
	echo "PARTS FILE IS BROKEN / NOT FOUND";
	return;
}

foreach($partsArr as $part){
	foreach($partsForCheck as $key => $element){
		if($key == $part["category"]){
			$partsForCheck[$key][] = $part["item_id"];
		}
	}
}

//var_dump($partsForCheck);
// var_dump("INITIAL ARRAY");
// var_dump($partsForCheck);
// var_dump("______________________");
$x = permutations($partsForCheck);
//delete prohibited combinations
//var_dump($x);
foreach($x as $key => $combination){
	$prohibitedArr = array();

	//need to get all prohibited elements
	foreach($combination as $element){
		foreach($partsArr as $part){
			if($part["item_id"] == $element && ! empty($part["prohibit_products"])){
				foreach($part["prohibit_products"] as $el){
					$prohibitedArr[] = $el;
				}
			}
		}
	}

	//check if there is suck combination
	//var_dump($prohibitedArr);

	foreach($prohibitedArr as $prohElement){
		foreach($combination as $part){
			if($part == $prohElement){
				unset($x[$key]);
				continue 3;
			}
		}
	}
}

$x = array_values($x);
//var_dump("RESULT!");
echo "TOTAL: ".count($x)."\n";
foreach($x as $element){
	echo implode(",",$element)."\n";
}

// var_dump("LINKS");
// foreacH($x as $element){
// 	var_dump("http://www.airsoftmegastore.com/airsoft-gun-builder/golden-eagle-ak47/#".implode(",",$element));
// }


