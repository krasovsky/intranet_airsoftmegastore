var Platform = {"name": "we-tech-m16", "upsellCategoryId": 435, "Build": [
    {"id": 427, "base_id": 14046, "category_name": "Lower", "item_id": null, "requiredForCheckout": true},
    {"id": 426, "base_id": 14040, "category_name": "Upper", "item_id": null, "requiredForCheckout": true},
    {"id": 428, "base_id": 14059, "category_name": "Stocks", "item_id": null, "requiredForCheckout": false},
    {"id": 429, "base_id": 14062, "category_name": "Handguards", "item_id": null, "requiredForCheckout": true},
    {"id": 430, "base_id": 14051, "category_name": "Pistol Grips", "item_id": null, "requiredForCheckout": false},
    {"id": 431, "base_id": null, "category_name": "Bipods and Foregrips", "item_id": null, "requiredForCheckout": false},
    {"id": 434, "base_id": 14066, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false},
    {"id": 440, "base_id": 1337, "category_name": "Optics and Rear Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomCenter"
            }
        }}
]};
