var Platform = {
    "name": "we-tech-m16",
    "upsellCategoryId": 435,
    "Build": [{
        "id": 426,
        "base_id": 0,
        "category_name": "397 - Upper",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 427,
        "base_id": 0,
        "category_name": "397 - Lower",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 428,
        "base_id": 0,
        "category_name": "397 - Stocks",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 429,
        "base_id": 0,
        "category_name": "397 - Handguards",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 430,
        "base_id": 0,
        "category_name": "397 - Pistol Grips",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 431,
        "base_id": 0,
        "category_name": "397 - Foregrips",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 432,
        "base_id": 0,
        "category_name": "397 - Iron Sights",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 433,
        "base_id": 0,
        "category_name": "397 - Cheek Risers",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 434,
        "base_id": 0,
        "category_name": "397 - Magazines",
        "item_id": null,
        "requiredForCheckout": false
    }, {"id": 435, "base_id": 0, "category_name": "397 - Accessories", "item_id": null, "requiredForCheckout": false}]
}