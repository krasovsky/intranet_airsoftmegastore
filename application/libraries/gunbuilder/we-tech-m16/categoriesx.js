var Platform = {"name": "we-tech-m16", "upsellCategoryId": 435, "Build": [
    {"id": 426, "base_id": 12222, "category_name": "397 - Upper", "item_id": null, "requiredForCheckout": false},
    {"id": 427, "base_id": 12229, "category_name": "397 - Lower", "item_id": null, "requiredForCheckout": false},
    {"id": 429, "base_id": 12244, "category_name": "397 - Handguards", "item_id": null, "requiredForCheckout": false},
    {"id": 430, "base_id": 12232, "category_name": "397 - Pistol Grips", "item_id": null, "requiredForCheckout": false},
    {"id": 431, "base_id": 49, "category_name": "397 - Foregrips", "item_id": null, "requiredForCheckout": false},
    {"id": 432, "base_id": 0, "category_name": "397 - Front Sights", "item_id": null, "requiredForCheckout": false},
    {"id": 434, "base_id": 12248, "category_name": "397 - Magazines", "item_id": null, "requiredForCheckout": false},
    {"id": 440, "base_id": 1334, "category_name": "Optics and Rear Sights", "item_id": null, "requiredForCheckout": false},
    {"id": 441, "base_id": 7202, "category_name": "Flashlights and Lasers", "item_id": null, "requiredForCheckout": false},
    {"id": 455, "base_id": 12743, "category_name": "Rail Segments", "item_id": null, "requiredForCheckout": false}
]};