var Platform = {"name": "we-tech-m4", "upsellCategoryId": 453, "Build": [
    {"id": 381, "base_id": 12218, "category_name": "Upper", "item_id": null, "requiredForCheckout": false},
    {"id": 382, "base_id": 12227, "category_name": "Lower", "item_id": null, "requiredForCheckout": false},
    {"id": 383, "base_id": 11947, "category_name": "Stocks", "item_id": null, "requiredForCheckout": false},
    {"id": 384, "base_id": 12246, "category_name": "Handguards", "item_id": null, "requiredForCheckout": false},
    {"id": 385, "base_id": 12232, "category_name": "Pistol Grips", "item_id": null, "requiredForCheckout": false},
    {"id": 386, "base_id": 28, "category_name": "Bipods and Foregrips", "item_id": null, "requiredForCheckout": false},
    {"id": 387, "base_id": 4250, "category_name": "Front Sights", "item_id": null, "requiredForCheckout": false},
    {"id": 388, "base_id": 194, "category_name": "Accessories", "item_id": null, "requiredForCheckout": false},
    {"id": 393, "base_id": 12248, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false},
    {"id": 436, "base_id": 483, "category_name": "Optics\/Rear Sights", "item_id": null, "requiredForCheckout": false},
    {"id": 437, "base_id": 7202, "category_name": "Flashlights and Lasers", "item_id": null, "requiredForCheckout": false},
    {"id": 453, "base_id": 12743, "category_name": "Rail Segments", "item_id": null, "requiredForCheckout": false}
]};