var Platform = {"name": "we-tech-m4", "upsellCategoryId": 388, "Build": [
    {"id": 382, "base_id": 14046, "category_name": "Lower", "item_id": null, "requiredForCheckout": true},
    {"id": 381, "base_id": 14044, "category_name": "Upper", "item_id": null, "requiredForCheckout": true},
    {"id": 383, "base_id": 14622, "category_name": "Stocks", "item_id": null, "requiredForCheckout": true},
    {"id": 385, "base_id": 14052, "category_name": "Pistol Grips", "item_id": null, "requiredForCheckout": true},
    {"id": 384, "base_id": null, "category_name": "Handguards", "item_id": null, "requiredForCheckout": false},
    {"id": 386, "base_id": 12798, "category_name": "Foregrips", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-bottom":{
                "align":"topCenter"
            }
        }},
    {"id": 387, "base_id": null, "category_name": "Front Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "site-front":{
                "align":"bottomLeft",
                "x":-10
            }
        }},
    {"id": 436, "base_id": 1342, "category_name": "Optics and Rear Sights", "item_id": null, "requiredForCheckout": false, "imgResized": true,
        "position":{
            "barrel-rear":{
                "align":"bottomRight",
                "x":-10
            }
        }},
    {"id": 393, "base_id": 14071, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false}
]};