var Platform = {
    "name": "we-tech-m4",
    "upsellCategoryId": 388,
    "Build": [
        {
            "id": 382,
            "category_name": "Lower",
            "base_id": 12807,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 381,
            "category_name": "Upper",
            "base_id": 12786,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 383,
            "category_name": "Stocks",
            "base_id": 12810,
            "item_id": null,
            "requiredForCheckout": true
        },
        {
            "id": 387,
            "category_name": "Iron Sights",
            "base_id": null,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 384,
            "category_name": "Handguards",
            "base_id": null,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 385,
            "category_name": "Pistol Grips",
            "base_id": null,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 386,
            "category_name": "Foregrips",
            "base_id": 11071,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 393,
            "category_name": "Magazines",
            "base_id": 12801,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 394,
            "category_name": "Cheek Risers",
            "base_id": null,
            "item_id": null,
            "requiredForCheckout": false
        },
        {
            "id": 374,
            "category_name": "Accessories",
            "base_id": null,
            "item_id": null,
            "requiredForCheckout": false,
            "isHidden":true
        }
    ]
};