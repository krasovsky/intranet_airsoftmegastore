var Platform = {
    "name": "we-tech-pdw", "upsellCategoryId": 425, "Build": [
        {"id": 417, "base_id": 14049, "category_name": "Lower", "item_id": null, "requiredForCheckout": true},
        {"id": 416, "base_id": 14043, "category_name": "Upper", "item_id": null, "requiredForCheckout": true},
        {"id": 420, "base_id": 14052, "category_name": "Pistol Grips", "item_id": null, "requiredForCheckout": true},
        {"id": 419, "base_id": null, "category_name": "Handguards", "item_id": null, "requiredForCheckout": false},
        {
            "id": 421,
            "base_id": 14657,
            "category_name": "Foregrips",
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position": {
                "barrel-bottom": {
                    "align": "topCenter"
                }
            }
        },
        {
            "id": 422,
            "base_id": null,
            "category_name": "Front Sights",
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position": {
                "site-front": {
                    "align": "bottomLeft",
                    "x": -10
                }
            }
        },
        {
            "id": 438,
            "base_id": 1334,
            "category_name": "Optics and Rear Sights",
            "item_id": null,
            "requiredForCheckout": false,
            "imgResized": true,
            "position": {
                "barrel-rear": {
                    "align": "bottomRight",
                    "x": -10
                }
            }
        },
        {"id": 424, "base_id": 14069, "category_name": "Magazines", "item_id": null, "requiredForCheckout": false}
    ]
};
