var Platform = {
    "name": "we-tech-pdw",
    "upsellCategoryId": 425,
    "Build": [{
        "id": 416,
        "base_id": 0,
        "category_name": "396 - Upper",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 417,
        "base_id": 0,
        "category_name": "396 - Lower",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 418,
        "base_id": 0,
        "category_name": "396 - Stocks",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 419,
        "base_id": 0,
        "category_name": "396 - Handguards",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 420,
        "base_id": 0,
        "category_name": "396 - Pistol Grips",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 421,
        "base_id": 0,
        "category_name": "396 - Foregrips",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 422,
        "base_id": 0,
        "category_name": "396 - Iron Sights",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 423,
        "base_id": 0,
        "category_name": "396 - Cheek Risers",
        "item_id": null,
        "requiredForCheckout": false
    }, {
        "id": 424,
        "base_id": 0,
        "category_name": "396 - Magazines",
        "item_id": null,
        "requiredForCheckout": false
    }, {"id": 425, "base_id": 0, "category_name": "396 - Accessories", "item_id": null, "requiredForCheckout": false}]
}