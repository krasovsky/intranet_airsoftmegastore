<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Dataminer extends CI_Model {

    public function setAlsoOrdered($pid){

        $sql = "SELECT COUNT(*) as count FROM items WHERE id = $pid";

        $itemCount = $this->db->query($sql);
        $exists = $itemCount->row('count');

        if(!$exists)return;

        $sql = "INSERT IGNORE INTO ams_people_also_ordered (
                SELECT *, COUNT(item_record_id) AS `count`, $pid AS item_id FROM order_items
                WHERE order_record_id IN ( SELECT order_record_id FROM order_items WHERE item_record_id = $pid )
                AND price > 0
                AND item_record_id <> $pid
                GROUP BY item_record_id
                ORDER BY COUNT DESC
                LIMIT 20)";

        $this->db->query($sql);
    }



}