<?php

class FishbowlModel extends CI_Model {

    const ITEMS_INVENTORY_SKU_COLUMN_NAME = "sku";

    const CYCLE_COUNT_DATA_FILE_NAME = "cycle_count_data.csv";

    public function getInventoryChangedData($optionsArr = array()){
        $queryStr = <<<HEREDOC
            SELECT
              i.sku AS PartNumber,
              CONCAT("Pick Location-",REPLACE(pr.`pick_location`,"-"," ")) AS Location,
              i.`inventory` AS Qty
            FROM
              items i
              INNER JOIN productlocations pr
                ON pr.id = i.`product_id`
              INNER JOIN products p
                ON p.id = i.`product_id`
              WHERE i.`inventory` > 0 AND pr.pick_location <> '' AND pr.`pick_location` NOT LIKE "Discontinued%" and pr.`pick_location` not like "%BONEYARD%"
              AND i.`inventory` <> i.`fishbowl_inventory`
HEREDOC;


        $query = $this->db->query($queryStr);
        $rez = $query->result_array();

        return $rez;
    }

    public function updateInventoryValues($data,$optionsArr = array()){
        $updateArr = array();

        $updateKey = (! empty($optionsArr["updateKey"])) ? $optionsArr["updateKey"] : "PartNumber";
        $updateColumnKey = (! empty($optionsArr["updateColumnKey"])) ? $optionsArr["updateColumnKey"] : "sku";

        foreach($data as $element){
            $updateArr[] = $element["PartNumber"];
            $updateInStr = implode('","',$updateArr);
        }

        $queryStr = <<<HEREDOC
            update items set fishbowl_inventory = inventory
            where sku in ("{$updateInStr}")
HEREDOC;
        //var_dump($queryStr);
        $this->db->query($queryStr);
    }

    public function createCsvFileFromData($data,$optionsArr = array()){

        $fileName = (! empty($optionsArr["fileName"])) ? $optionsArr["fileName"] : self::CYCLE_COUNT_DATA_FILE_NAME;

        $firstRow = true;
        $headerData = array();
        if (($handle = fopen(APPPATH."assets/data/fishbowlData/Dropbox/".$fileName , "w")) !== FALSE) {
            foreach($data as $element){
                if ($firstRow) {
                    foreach ($element as $key => $value) {
                        $headerData[] = $key;
                    }
                    $firstRow = false;

                    fputcsv($handle, $headerData);
                    fwrite($handle,"\n");
                }

                fputcsv($handle, $element);
                fwrite($handle,"\n");
            }
        }

        fclose($handle);
    }
}