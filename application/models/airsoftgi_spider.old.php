<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Airsoftgi_spider extends CI_Model {

    private function getCartQuantity($body){
        $this->page = htmlqp($body);
        //if($quantity = $this->page->find('.cartlistqty input[type=text]')->attr('value')){
        if($quantity = $this->page->find('input[name^=cart_quantity]')->attr('value')){

            $outofstock = $this->page->is('.markProductOutOfStock');

            if(!$outofstock){
                //instock
                return false;
            }else{
                //out of stock message
                return true;
            }
        }
    }

    private function getItemId($body){
        $page = htmlqp($body);
        return $id = $page->find('input[name^=products_id]')->attr('value');
    }


    public function deleteCookie(){
        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsoftgi.txt" ;
        return unlink($file);
    }

    public function getInventoryMulti($ids){
        $obj = new stdClass();
        $obj->cookie = $this->getCookie($obj);

        $cart = array();

        foreach($ids as $id){
            $cart[$id['product_id']]['max']=100;
            $cart[$id['product_id']]['min']=0;
            $cart[$id['product_id']]['quantity']=100;
            $cart[$id['product_id']]['loop']=true;
        }

        $body = $this->updateCartMulti($obj,$cart); //initial add at max amount

        for($i=1; $i<12; ++$i){
            //limit 10 guesses
            $page = htmlqp($body);
            $items = $page->find('table.padding2:first tr:odd');

            foreach($items as $item){//with each of the items from the returned dom

                $id = $this->getItemId($item); //get the id so we can relate the item to the cart array

                if(is_null($id))continue; //if row doesn't have a product in it.

                if($cart[$id]['loop']){

                    $check = $this->getCartQuantity($item); //with the item, check to see if it is out of stock or not

                    if($check){
                        //out of stock too high of a number
                        $cart[$id]['max'] = $cart[$id]['quantity'];
                    }else{
                        //in stock too low of a number
                        $cart[$id]['min'] = $cart[$id]['quantity'];
                    }
                    //create next quantity guess
                    $cart[$id]['quantity'] = ceil(($cart[$id]['max']-$cart[$id]['min'])/2)+$cart[$id]['min'];

                    //shall we loop this item anymore
                    if($cart[$id]['min'] + 1 == $cart[$id]['quantity'] || $cart[$id]['max'] == $cart[$id]['min']){
                        $cart[$id]['loop'] = false; //we found the value, don't reduce anymore
                    }
                }
            }

            $loop = 0;
            foreach($cart as $c){
                if($c['loop'])
                    ++$loop;
            }

            if($loop)
                $body = $this->updateCartMulti($obj,$cart);
            else
                break;

        }

        $this->removeItemFromCartMulti($obj, $cart);

        var_dump($cart);

        return $cart;
    }

    public function removeItemFromCartMulti($obj,$cart){

        $postDataString = '';
        foreach($cart as $key=>$value){
            $postDataString .= "cart_delete[]=$key&products_id[]=$key&";
        }

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function updateCartMulti($obj, $cart){

        $postDataString = '';
        foreach($cart as $key=>$value){
            $postDataString .= "cart_quantity[]={$value['quantity']}&products_id[]=$key&";
        }

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function getInventory($id){
        $obj = new stdClass();
        $obj->competitor = 'airsoftgi';
        $obj->cookie = $this->getCookie($obj);
        $obj->product_id = $id;
        $this->addtocart($obj);

        $min = 0;
        $max = 1000;

        $guess = $max; //start by guessing max, should show failure and begin the matching process

        for($i=1; $i<20; ++$i){ //do ten iterations

            if($this->getCartQuantity($this->updateCart($obj,$guess))){ //outofstock too high of a number
                $max = $guess;
            }else{ //instock too low of a number
                $min = $guess;
            }
            $guess = ceil(($max-$min)/2)+$min;
            echo "Guess $i max = $max min = $min: ". $guess . "</br>";

            if($min + 1 == $guess || $max == $min){
                break;
            }
        }

        echo "Product: $id - In Stock: " . $guess . "</br>";

        $this->removeItemFromCart($obj);

        return $guess;
    }


    public function removeItemFromCart($obj){

        $postDataString = "cart_delete[]={$obj->product_id}&products_id[]={$obj->product_id}";

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function updateCart($obj, $quantity){

        $postDataString = "cart_quantity[]=$quantity&products_id[]={$obj->product_id}";

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function addtocart($obj){

        $postDataString = "products_id={$obj->product_id}";

        $ch = curl_init("http://www.airsoftgi.com/product_info.php?products_id={$obj->product_id}&action=add_product");
        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/products/{$obj->product_id}");

        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function getCookie($obj){

        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsoftgi.txt" ;

        if(file_exists($file)){
            return $file;
        }else{

            file_put_contents($file,'');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"http://www.airsoftgi.com/");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

            ob_start();      // prevent any output
            curl_exec ($ch); // execute the curl command
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            return $file;
        }
    }

    public function getNextIdToSpider($count){
        $sql = "SELECT id FROM `competitors_airsoftgi_brands` LIMIT $count, 1";
        $result = $this->db->query($sql);

        return $result->row()->id;
    }


    public function getNextProductId($count){
        $sql = "SELECT p.product_id FROM competitors_airsoftgi p
                    LEFT JOIN competitors_airsoftgi_inventory i ON i.product_id = p.product_id
                    WHERE i.inventory IS NULL LIMIT $count, 1";

        $result = $this->db->query($sql);

        return $result->row()->product_id;
    }

    public function getNextProductIdMulti($count,$quantity=1){
        $sql = "SELECT p.product_id, i.inventory, MAX(i.timestamp)
                    FROM `competitors_airsoftgi` p
                    LEFT JOIN competitors_airsoftgi_inventory i ON i.product_id = p.`product_id`
                    WHERE
                    i.inventory > 0
                    OR i.inventory IS NULL
                    GROUP BY i.product_id
                    HAVING DATE(MAX(i.`timestamp`)) < CURDATE()
                    LIMIT $count, $quantity";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function getAllProductsInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('.productListing-odd')->text() == 'There is no product available from this manufacturer.') return false;

        $page->top();
        $txt = $page->find('.gridx12')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPage($body, $brand);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function getAllProductsPricesInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('.productListing-odd')->text() == 'There is no product available from this manufacturer.') return false;

        $page->top();
        $txt = $page->find('.gridx12')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPrices($body);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function scrapeBrandPage($body, $brand){
        $this->page = htmlqp($body);
        //if($quantity = $this->page->find('.cartlistqty input[type=text]')->attr('value')){
        $items = $this->page->find('.productListing-data');

        foreach($items as $i){

            $id = substr($i->find('a')->attr('href'),strlen('http://www.airsoftgi.com/product_info.php?products_id='));
            $name = $i->find('b')->text();

            $this->saveCrawlResult($id, $name, $brand);

            echo $name . "<br/>";
        }

    }

    public function scrapeBrandPrices($body){
        $this->page = htmlqp($body);
        $items = $this->page->find('.productListing-data');

        foreach($items as $i){

            $id = substr($i->find('a')->attr('href'),strlen('http://www.airsoftgi.com/product_info.php?products_id='));
            $price = $i->find('span')->text();

            if($price){
                //clean it up for entry
                preg_match_all('/([0-9,]+(\.[0-9]{2})?)/', $price, $matches);

                if(count($matches[0])){
                    //if product is on sale then the second price is the sale price
                    $price = (count($matches[0])>1)?$matches[0][1]:$matches[0][0];
                    $this->savePrice($id, $price);
                    echo $id.' = '.$price . "<br/>";
                }

                if($i->is('img[alt^=Out]')){
                    //product is out of stock
                    $this->setInventory($id, 0);
                }
            }else{
                //product is discontinued
                $this->setInventory($id, -1);
            }
        }
    }

    public function getBrand($url){
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

       return $body;
    }

    public function getAllProductsOnHomepage(){

        $body  = $this->getHomepage();

        $this->page = htmlqp($body);
        $items = $this->page->find('[href*=products_id]');

        foreach($items as $i){
            $id = substr($i->attr('href'),strlen('http://www.airsoftgi.com/product_info.php?products_id='));
            $this->savePromotedProduct($id);
            echo $id . '<br/>';
        }

    }

    private function getHomepage(){

        $ch = curl_init('http://www.airsoftgi.com/');

        curl_setopt($ch, CURLOPT_REFERER, "http://www.airsoftgi.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        return $body;
    }

    public function saveProduct($product){
        //insert product into database
        $sql = "INSERT IGNORE INTO item_compatiblity SET product_id = $id, item_id = 0";
        $this->db->query($sql);
    }

    public function saveCrawlResult($id, $name, $brand){
        $name = addslashes($name);
        //insert product into database
        $sql = "INSERT IGNORE INTO competitors_airsoftgi SET product_id = '$id', name = '$name', brand_id = '$brand'";
        return $this->db->query($sql);
    }

    public function savePrice($id, $price){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO competitors_airsoftgi_prices SET product_id = '$id', price = '$price', timestamp = '$date'";
        return $this->db->query($sql);
    }

    public function setInventory($id, $count){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO competitors_airsoftgi_inventory SET product_id = '$id', inventory = '$count', timestamp = '$date'";
        return $this->db->query($sql);
    }

    public function setDailyInventory($id, $count){
        $date = date('Y-m-d H:i:s');
        //get the current record for computation
        $sql = "SELECT * FROM competitors_airsoftgi_daily WHERE product_id = $id";
        $previous = $this->db->query($sql);

        //get the current price
        $sql = "SELECT price FROM competitors_airsoftgi_prices WHERE product_id = $id GROUP BY product_id ORDER BY timestamp DESC limit 1";
        $result = $this->db->query($sql);
        $price = $result->row()->price;

        if($previous->num_rows() > 0){
            $sql = "
            UPDATE competitors_airsoftgi_daily
            SET
               previous = {$previous->current},
               current = $count,
               profit = $price * $count,
               difference = {$previous->current}-$count
               timestamp = '$date'
            WHERE
               product_id = $id,
            ";
            return $this->db->query($sql);
        }else{
            $sql = "
            INSERT INTO competitors_airsoftgi_daily
            SET
               product_id = $id,
               previous = $count,
               current = $count,
               profit = $price * $count,
               difference = 0,
               timestamp = '$date'
            ";
            return $this->db->query($sql);
        }
    }

    public function savePromotedProduct($id){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO competitors_airsoftgi_promotions SET product_id = '$id', timestamp = '$date'";
        return $this->db->query($sql);
    }

    public function getYesterdaysInventory(){

        $sql = "SELECT i.*, n.name
                FROM competitors_airsoftgi_inventory i
                LEFT JOIN competitors_airsoftgi n ON i.`product_id` = n.`product_id`
                WHERE TIMESTAMP = (SELECT TIMESTAMP FROM competitors_airsoftgi_inventory WHERE product_id = i.`product_id` ORDER BY TIMESTAMP DESC LIMIT 1,1)
                GROUP BY i.product_id
                ";
        $previous =  $this->db->query($sql);

        $sql = "SELECT i.*, n.name
                FROM competitors_airsoftgi_inventory i
                LEFT JOIN competitors_airsoftgi n ON i.`product_id` = n.`product_id`
                WHERE TIMESTAMP = (SELECT TIMESTAMP FROM competitors_airsoftgi_inventory WHERE product_id = i.`product_id` ORDER BY TIMESTAMP DESC LIMIT 0,1)
                GROUP BY i.product_id
                ";
        $last =  $this->db->query($sql);

        $combine = array();
        //array combine using product_id keys
        foreach($previous->result() as $p){
            $combine[$p->product_id]['id'] = $p->product_id;
            $combine[$p->product_id]['name'] = $p->name;
            $combine[$p->product_id]['previous'] = $p->inventory;
        }

        foreach($last->result() as $l){
            $combine[$l->product_id]['id'] = $l->product_id;
            $combine[$l->product_id]['name'] = $l->name;
            $combine[$l->product_id]['last'] = $l->inventory;
        }

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('product_id', 'name', 'previous', 'last', 'difference'));

        foreach($combine as $id=>$row){
            $diff = $row['last'] - $row['previous'];
            if($diff){
                $combine[$id]['diff'] = $diff;
                fputcsv($output, $combine[$id]);
            }else{
               unset($combine[$id]);
            }
        }
        exit(0);

    }

}