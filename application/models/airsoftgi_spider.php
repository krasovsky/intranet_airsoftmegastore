<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Airsoftgi_spider extends CI_Model {

    function __construct(){
        $this->xref = 'competitors_airsoftgi_xref';
        $this->prices = 'competitors_airsoftgi_prices';
        $this->inventory = 'competitors_airsoftgi_inventory';
        $this->brands = 'competitors_airsoftgi_brands';
        $this->products = 'competitors_airsoftgi';
        $this->promotions = 'competitors_airsoftgi_promotions';
    }

    private function getCartQuantity($body){
        $page = htmlqp($body);

        $quantity = $page->find('input[name^=cart_quantity]')->attr('value');

        if($quantity){
            $outofstock = $page->is('.markProductOutOfStock');

            if(!$outofstock){
                //instock
                return false;
            }else{
                //out of stock message
                return true;
            }
        }
    }

    private function getItemId($item){
        $page = htmlqp($item);
        return $id = $page->find('input[name^=products_id]')->attr('value');
    }

    public function deleteCookie(){
        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsoftgi.txt" ;
        return unlink($file);
    }

    public function getInventory($id){
        $obj = new stdClass();
        $obj->competitor = 'airsoftgi';
        $obj->cookie = $this->getCookie($obj);
        $obj->product_id = $id;
        $this->addtocart($obj);

        $min = 0;
        $max = 100;

        $guess = $max; //start by guessing max, should show failure and begin the matching process

        for($i=1; $i<20; ++$i){ //do ten iterations

            if($this->getCartQuantity($this->updateCart($obj,$guess))){ //outofstock too high of a number
                $max = $guess;
            }else{ //instock too low of a number
                $min = $guess;
            }
            $guess = ceil(($max-$min)/2)+$min;
            echo "Guess $i max = $max min = $min: ". $guess . "</br>";

            if($min + 1 == $guess || $max == $min){
                break;
            }
        }

        echo "Product: $id - In Stock: " . $guess . "</br>";

        $this->removeItemFromCart($obj);

        return $guess;
    }

    public function getInventoryMulti($ids){
        $obj = new stdClass();
        $obj->competitor = 'airsoftgi';
        $obj->cookie = $this->getCookie($obj);

        $cart = array();

        foreach($ids as $id){
//            $obj->product_id = $id['product_id'];
//            $this->addtocart($obj);
            $cart[$id['product_id']]['max']=5000;
            $cart[$id['product_id']]['min']=0;
            $cart[$id['product_id']]['quantity']=5000;
            $cart[$id['product_id']]['loop']=true;

        }

        $body = $this->updateCartMulti($obj,$cart); //initial add at max amount

        for($i=1; $i<14; ++$i){
            //limit 10 guesses
            $page = htmlqp($body);
            $items = $page->find('table.padding2:first tr:odd');

            foreach($items as $item){//with each of the items from the returned dom

                $id = $this->getItemId($item); //get the id so we can relate the item to the cart array
                //echo $item->writeHTML('C:\Users\abstraktron\Desktop\airsofgi.html');
                if(is_null($id))continue; //if row doesn't have a product in it.
                
                if($cart[$id]['loop']){

                    $outofstock = $this->getCartQuantity($item); //with the item, check to see if it is out of stock or not

                    if($outofstock){
                        //out of stock too high of a number
                        $cart[$id]['max'] = $cart[$id]['quantity'];
                    }else{
                        //in stock too low of a number
                        $cart[$id]['min'] = $cart[$id]['quantity'];
                    }
                    //create next quantity guess
                    $cart[$id]['quantity'] = ceil(($cart[$id]['max']-$cart[$id]['min'])/2)+$cart[$id]['min'];

                    //shall we loop this item anymore
                    if($cart[$id]['min'] + 1 == $cart[$id]['quantity'] || $cart[$id]['max'] == $cart[$id]['min']){
                        $cart[$id]['loop'] = false; //we found the value, don't reduce anymore
                    }
                }
            }

            $loop = 0;
            foreach($cart as $c){
                if($c['loop'])
                    ++$loop;
            }

            if($loop)
                $body = $this->updateCartMulti($obj,$cart);
            else
                break;

        }

        $this->removeItemFromCartMulti($obj, $cart);

        var_dump($cart);

        return $cart;
    }


    public function removeItemFromCartMulti($obj,$cart){

        $postDataString = '';
        foreach($cart as $key=>$value){
            $postDataString .= "cart_delete[]=$key&products_id[]=$key&";
        }

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function updateCartMulti($obj, $cart){

        $postDataString = '';
        foreach($cart as $key=>$value){
            $postDataString .= "cart_quantity[]={$value['quantity']}&products_id[]=$key&";
        }

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        //file_put_contents('C:\\Users\\abstraktron\\Desktop\\debug\\'.time(),$body);

        return $body;
    }

    public function updateCart($obj, $cart){

        $postDataString = '';
        foreach($cart as $key=>$value){
            $postDataString .= "cart_quantity[]={$value['quantity']}&products_id[]=$key&";
        }

        $ch = curl_init("https://www.airsoftgi.com/shopping_cart.php?action=update_product");

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/shopping_cart.php");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        return $body;
    }

    public function addtocart($obj){

        $postDataString = "products_id={$obj->product_id}";

        $ch = curl_init("http://www.airsoftgi.com/product_info.php?products_id={$obj->product_id}&action=add_product");
        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/products/{$obj->product_id}");

        curl_setopt($ch, CURLOPT_COOKIEJAR, $obj->cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $obj->cookie);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function getCookie($obj){

        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsoftgi.txt" ;

        if(file_exists($file)){
            return $file;
        }else{

            file_put_contents($file,'');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"http://www.airsoftgi.com/");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

            ob_start();      // prevent any output
            curl_exec ($ch); // execute the curl command
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            return $file;
        }
    }

    public function getNextIdToSpider($count){
        $sql = "SELECT id FROM `{$this->brands}` LIMIT $count, 1";
        $result = $this->db->query($sql);

        return $result->row()->id;
    }

    public function getNextProductId($count){
        $sql = "SELECT p.product_id FROM {$this->products} p
                    LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                    WHERE i.inventory IS NULL LIMIT $count, 1";

        $result = $this->db->query($sql);

        return $result->row()->product_id;
    }

    public function getNextProductIdMulti($quantity=1){
//        $sql = "SELECT p.product_id, i.inventory, MAX(i.timestamp)
//                    FROM {$this->products} p
//                    LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
//                    WHERE
//                    i.inventory > 0
//                    OR i.inventory IS NULL
//                    GROUP BY i.product_id
//                    HAVING DATE(MAX(i.`timestamp`)) < CURDATE()
//                    LIMIT $count, $quantity";

        $sql = "SELECT product_id FROM {$this->products}
                WHERE inventory_update IS NULL
                OR inventory_update < DATE_SUB( NOW(), INTERVAL 24 HOUR )
                OR inventory <> -1
                LIMIT $quantity
                ";

        $result = $this->db->query($sql);

        return $result->result_array();
    }

    public function getEverythingInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('.productListing-odd')->text() == 'There is no product available from this manufacturer.') return false;

        $page->top();
        $txt = $page->find('.gridx12')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/20); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPage($body, $brand);
            $this->scrapeBrandPrices($body);

            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function getAllProductsInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('#mcol h4')->text() == 'No products found.') return false;

        $page->top();
        $txt = $page->find('#mcol .col1')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPage($body, $brand);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function getAllProductsPricesInBrand($url){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('#mcol h4')->text() == 'No products found.') return false;

        $page->top();
        $txt = $page->find('#mcol .col1')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPrices($body);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function scrapeBrandPrices($body){
        $this->page = htmlqp($body);
        $items = $this->page->find('.productListing-data');

        foreach($items as $i){

            $id = substr($i->find('a')->attr('href'),strlen('http://www.airsoftgi.com/product_info.php?products_id='));
            $price = $i->find('span')->text();

            if($price){
                //clean it up for entry
                preg_match_all('/([0-9,]+(\.[0-9]{2})?)/', $price, $matches);

                if(count($matches[0])){
                    //if product is on sale then the second price is the sale price
                    $price = (count($matches[0])>1)?$matches[0][1]:$matches[0][0];
                    $this->logPrice($id,$price);
                    $lastPrice = $this->getLastSavedPrice($id);
                    if ($lastPrice != $price){
                        $this->savePrice($id, $price);
                    }
                    echo $id.' = '.$price . "<br/>\n";
                }

                if($i->is('img[alt^=Out]')){
                    //product is out of stock
                    $this->setInventory($id, 0);
                }
            }else{
                //product is discontinued
                $this->setInventory($id, -1);
            }
        }
    }

    public function scrapeBrandPage($body, $brand){
        $this->page = htmlqp($body);
        //if($quantity = $this->page->find('.cartlistqty input[type=text]')->attr('value')){
        $items = $this->page->find('.productListing-data');

        foreach($items as $i){

            $id = substr($i->find('a')->attr('href'),strlen('http://www.airsoftgi.com/product_info.php?products_id='));
            $name = $i->find('b')->text();

            $this->saveCrawlResult($id, $name, $brand);

            echo $name . "<br/>\r\n";
        }

    }

    public function getBrand($url){
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsoftgi.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

       return $body;
    }

    public function getAllProductsOnHomepage(){

        $body  = $this->getHomepage();

        $this->page = htmlqp($body);
        $items = $this->page->find('[href*=products]');

        foreach($items as $i){
            $id = substr($i->attr('href'),strlen('http://www.evike.com/products/'),-1);
            $this->savePromotedProduct($id);
            echo $id . '<br/>';
        }

    }

    private function getHomepage(){

        $ch = curl_init('http://www.airsoftgi.com/');

        curl_setopt($ch, CURLOPT_REFERER, "http://www.airsoftgi.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        return $body;
    }

    public function saveCrawlResult($id, $name, $brand){
        $name = addslashes($name);
        $date = date('Y-m-d H:i:s');

        //insert product into database
        $sql = "INSERT IGNORE INTO {$this->products} SET product_id = '$id', name = '$name', brand_id = '$brand', created = '$date'";
        $this->db->query($sql);
    }

    public function getLastSavedPrice($id){
        $sql = <<<HEREDOC
            SELECT price from {$this->prices} where product_id = '$id' order by timestamp desc limit 1
HEREDOC;
        $query = $this->db->query($sql);
        if (! empty($query->row()->price)){
            return $query->row()->price;
        }
        else return false;

    }

    public function logPrice($id, $price){
        $configArr = $this->config->item("log4php");

        //we need to change log file
        $configArr["appenders"]["default"]["params"]["file"] = APPPATH . "\\assets\\data\\logs\\airsoftgi\\{$id}.log";

        Logger::configure($configArr);

        $log = Logger::getLogger($id);
        $log->info($price);
    }

    public function savePrice($id, $price){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO {$this->prices} SET product_id = '$id', price = '$price', timestamp = '$date'";
        $this->db->query($sql);

        $sql = "UPDATE {$this->products} SET price = '$price', pricing_update = '$date' WHERE product_id = '$id'";
        $this->db->query($sql);
    }

    public function setInventory($id, $count){
        $date = date('Y-m-d H:i:s');

        //get the previous entry for math below
        $sql = "SELECT * FROM {$this->products} WHERE product_id = '$id'";
        $query = $this->db->query($sql);

        //figure out how much was bought and sold
        $sold = $this->soldHowMany($query->row(),$count);
        $sold_total = $sold + $query->row()->sold;

        $restocked = $this->restockedHowMany($query->row(),$count);
        $restocked_total = $query->row()->restocked + $restocked;

        $change = $sold > $restocked ? $sold*-1 : $restocked;

        if($sold){
            $sold = $sold;
        }
        if($restocked)
            $restocked = $restocked;

        $sql = "INSERT IGNORE INTO {$this->inventory} SET product_id = '$id', inventory = $count, `change` = $change, timestamp = '$date'";
        $this->db->query($sql);

        $sql = "UPDATE {$this->products} SET
                inventory = $count,
                inventory_update = '$date',
                sold = $sold_total,
                restocked = $restocked_total
                WHERE product_id = '$id'";
        $this->db->query($sql);
    }

    private function soldHowMany($row,$count){
        if($row->inventory > $count && $row->inventory > 0){
            return $row->inventory - $count;
        }else{
            return 0;
        }

    }
    private function restockedHowMany($row,$count){
        if($row->inventory < $count && $row->inventory > 0){
            return $count - $row->inventory;
        }else{
            return 0;
        }
    }

    public function savePromotedProduct($id){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO {$this->promotions} SET product_id = '$id', timestamp = '$date'";
        return $this->db->query($sql);
    }

    public function getProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id,  xref.product_id ams_id, p.brand_id, p.name, b.name brand, i.inventory FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) OR inventory IS NULL
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProductsFromBrandId($brand_id){

        $sql = "SELECT p.product_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE p.brand_id = $brand_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) OR inventory IS NULL
                ORDER BY i.inventory DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getBrandTotals($brand_id){

        $sql = "SELECT COUNT(t.product_id) total_products, SUM(t.inventory) inventory, t.price*SUM(t.inventory) potential
                FROM (
                SELECT p.product_id, p.name, i.inventory, i.`timestamp`, c.`price` FROM {$this->products} p
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->prices} c ON p.product_id = c.product_id
                WHERE p.brand_id = $brand_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) AND MAX(c.`timestamp`)
                ORDER BY i.inventory DESC ) t
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }


    public function getAllBrandTotals(){

        $sql = "SELECT id, name, COUNT(id) total, SUM(inventory) stock, price*SUM(inventory) invested FROM (
                SELECT b.id, b.name, i.inventory, c.`price` FROM {$this->products} p
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                JOIN {$this->brands} b ON b.`id` = p.`brand_id`
                LEFT JOIN {$this->prices} c ON p.product_id = c.product_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) AND MAX(c.`timestamp`)) q
                GROUP BY name
                ORDER BY name
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTopBrands(){

        $sql = "SELECT b.id, b.name, COUNT(p.name) product_line, SUM(p.inventory) total_inventory, SUM(sold) units_moved, SUM(restocked) restocked, SUM(sold) * price revenue FROM `{$this->products}` p
                JOIN `{$this->brands}` b ON p.`brand_id` = b.`id`
                GROUP BY p.brand_id
                ORDER BY revenue DESC, units_moved DESC
                LIMIT 100
                ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduct($id){
        $sql = "SELECT p.product_id, p.name, b.name brand, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->xref} xref ON xref.product_id = p.`product_id`
                WHERE p.product_id = $id";

        $query = $this->db->query($sql);
        return $query->row();
    }


    public function getOutOfStock($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE i.inventory = 0
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.timestamp DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function getDiscontinued($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE i.inventory = -1
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.timestamp DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getExclusiveCompetitorProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp`, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                WHERE  xref.`ams_id` IS NULL
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getIdenticalCompetitorProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp`, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                WHERE  xref.`ams_id` IS NOT NULL
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getExclusiveCompetitorBrands(){
        $sql = "SELECT e.id, e.name, b.name ams, b.id ams_id
                FROM {$this->brands} e
                LEFT JOIN brands b ON e.`ams_id` = b.`id`
                ORDER BY e.name";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function updateBrandXref($id, $ams){
        $sql = "UPDATE {$this->brands} SET ams_id = '$ams' WHERE id = '$id'";
        return $this->db->query($sql);
    }

    public function getPagination($itemsPerPage=200){
        $sql = "SELECT count(*) as total FROM {$this->products}";
        $query = $this->db->query($sql);
        return $query->row()->total/$itemsPerPage;
    }

    public function dailyRollup($id){
        $sql = "SELECT count(*) as total FROM {$this->products}";
        $query = $this->db->query($sql);
    }

    public function getCommonBrands(){
        $sql = "SELECT GROUP_CONCAT(id) ids FROM `{$this->brands}`
                WHERE ams_id IS NOT NULL";
        $query = $this->db->query($sql);
        return $query->row()->ids;
    }

    public function getAMSBrand($id){
        $sql = "SELECT ams_id FROM `{$this->brands}`
                WHERE id  = $id";
        $query = $this->db->query($sql);
        return $query->row()->ams_id;
    }

    public function getYearOfProductInventory($id){
        $sql = "SELECT inventory, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days   FROM {$this->inventory}
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getYearOfProductPrices($id){
        $sql = "SELECT price, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days  FROM {$this->prices}
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function updateXref(){
        $sql = "TRUNCATE TABLE competitors_airsoftgi_xref";
        $query = $this->db->query($sql);

        $sql = "
            INSERT IGNORE INTO `competitors_airsoftgi_xref`
            SELECT 	SUBSTRING_INDEX(airsoftgi, 'products_id=',-1) airsoftgi,
                p.id AS ams_id
            FROM `ams_competitors` c
            JOIN products p ON p.sku = c.sku
            WHERE airsoftgi <> ''
                AND airsoftgi IS NOT NULL
                AND LOCATE('airsoftgi',airsoftgi)
        ";

        $this->db->query($sql);
    }

    public function getLatestInventoryAndPrice($ams_id=0){
        $sql = "SELECT i.product_id, ams_id, inventory, price, inventory_update, pricing_update
                FROM {$this->products} i
                LEFT JOIN {$this->xref} xref ON i.`product_id` = xref.product_id
                WHERE xref.`ams_id` = $ams_id";

        $query = $this->db->query($sql);
        return $query->row();
    }


}