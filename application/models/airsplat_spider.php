<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Airsplat_spider extends CI_Model {

    function __construct(){
        $this->xref = 'competitors_airsplat_xref';
        $this->prices = 'competitors_airsplat_prices';
        $this->inventory = 'competitors_airsplat_inventory';
        $this->brands = 'competitors_airsplat_brands';
        $this->products = 'competitors_airsplat';
        $this->promotions = 'competitors_airsplat_promotions';
    }


    private function getItemId($body){
        $this->page = htmlqp($body);
        return $id = $this->page->find('input[name^=products_id]')->attr('value');
    }

    public function deleteCookie(){
        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsplat.txt" ;
        return unlink($file);
    }

    public function getCookie($obj){

        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\airsplat.txt" ;

        if(file_exists($file)){
            return $file;
        }else{

            file_put_contents($file,'');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"http://www.evike.com/");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

            ob_start();      // prevent any output
            curl_exec ($ch); // execute the curl command
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            return $file;
        }
    }

    public function getEverythingInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('#mcol h4')->text() == 'No products found.') return false;

        $page->top();
        $i=0;
        do{
            //scrape the page and add to the database

            $body = $this->getBrand( $url .'&p='. ++$i);
            $page = htmlqp($body);

            $this->scrapeBrandPage($body, $brand);
            $this->scrapeBrandPrices($body);

        } while( $page->find('.i-next')->length > 0 ); //while there is a next arrow for pagination on the page loop

        return true;
    }

    public function getAllProductsInBrand($url, $brand){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('#mcol h4')->text() == 'No products found.') return false;

        $page->top();
        $txt = $page->find('#mcol .col1')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPage($body, $brand);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function getAllProductsPricesInBrand($url){

        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->find('#mcol h4')->text() == 'No products found.') return false;

        $page->top();
        $txt = $page->find('#mcol .col1')->text();
        $itemCount = (int) substr($txt, stripos($txt,'(of ')+4 ,stripos($txt,' products'));

        $pages = ceil($itemCount/48); //deal with pagination

        for($i = 1; $i<=$pages;){
            //scrape the page and add to the database
            $this->scrapeBrandPrices($body);
            $i++;
            if( $i>1 && $i<=$pages ){//if there are more pages then requery (no extra queries on last page)
                $body = $this->getBrand($url.'&page='.$i);
            }
        }

        return true;
    }

    public function scrapeBrandPrices($body){
        $this->page = htmlqp($body);
        $items = $this->page->find('.item');

        foreach($items as $i){

            $id = substr($i->find('.regular-price')->attr('id'),strlen('product-price-'));

            $matches = array();
            if($id == ''){ //if the id isn't listed in a regular price then find it in the add to cart button
                $str = $i->find('button')->attr('onclick');

                if(stripos($str, 'showOptions')===0){
                    $matches  = explode("'", $str );
                    $id = $matches[1];
                }else{
                    preg_match('/product\/([0-9]*)/', $str, $matches);
                    $id = $matches[1];
                }

            }

            $price = ltrim($i->find('.price')->text(), '$'); //remove dollar sign

            //set if instock or out of stock
            if($i->find('.out-of-stock')->length > 0){
                //product is out of stock
                $this->setInventory($id, 0);
            }else{
                $this->setInventory($id, 1);

            }

            $this->savePrice($id, $price);
            echo $id.' = '.$price . "<br/>\r\n";
        }
    }

    public function scrapeBrandPage($body, $brand){
        $this->page = htmlqp($body);
        //if($quantity = $this->page->find('.cartlistqty input[type=text]')->attr('value')){
        $items = $this->page->find('.item');

        foreach($items as $i){

            $img = $i->find('img')->attr('src');

            $sku = substr($img, strrpos($img,'/')+1,-4);
            $name = $i->find('.product-name')->text();


            $id = substr($i->find('.regular-price')->attr('id'),strlen('product-price-'));

            $matches = array();
            if($id == ''){ //if the id isn't listed in a regular price then find it in the add to cart button
                $str = $i->find('button')->attr('onclick');

                if(stripos($str, 'showOptions')===0){
                    $matches  = explode("'", $str );
                    $id = $matches[1];
                }else{
                    preg_match('/product\/([0-9]*)/', $str, $matches);
                    $id = $matches[1];
                }

            }

            $url = $i->find('a')->attr('href');

            $this->saveCrawlResult($id, $sku, $name, $url, $brand);

            echo $name . "<br/>\r\n";
        }

    }

    public function getBrand($url){
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_REFERER, "https://www.airsplat.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

       return $body;
    }

    public function getAllProductsOnHomepage(){

        $body  = $this->getHomepage();

        $this->page = htmlqp($body);
        $items = $this->page->find('[href*=products]');

        foreach($items as $i){
            $id = substr($i->attr('href'),strlen('http://www.evike.com/products/'),-1);
            $this->savePromotedProduct($id);
            echo $id . '<br/>';
        }

    }

    private function getHomepage(){

        $ch = curl_init('http://www.evike.com/');

        curl_setopt($ch, CURLOPT_REFERER, "http://www.evike.com/");

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        return $body;
    }

    public function getNextIdToSpider($count){
        $sql = "SELECT id FROM `{$this->brands}` LIMIT $count, 1";
        $result = $this->db->query($sql);

        return $result->row()->id;
    }

    public function getNextProductId($count){
        $sql = "SELECT p.product_id FROM {$this->products} p
                    LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                    WHERE i.inventory IS NULL LIMIT $count, 1";

        $result = $this->db->query($sql);

        return $result->row()->product_id;
    }

    public function getNextProductIdMulti($quantity=1){

        $sql = "SELECT product_id FROM {$this->products}
                WHERE inventory_update IS NULL
                OR inventory_update < DATE_SUB( NOW(), INTERVAL 24 HOUR )
                LIMIT $quantity
                ";

        $result = $this->db->query($sql);

        return $result->result_array();
    }


    public function saveCrawlResult($id, $sku, $name, $url, $brand){
        $name = addslashes($name);
        $date = date('Y-m-d H:i:s');

        //insert product into database
        $sql = "INSERT IGNORE INTO {$this->products} SET product_id = '$id', name = '$name', brand_id = '$brand', url = '$url', sku = '$sku', created = '$date'";
        $this->db->query($sql);
    }

    public function savePrice($id, $price){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO {$this->prices} SET product_id = '$id', price = '$price', timestamp = '$date'";
        $this->db->query($sql);

        $sql = "UPDATE {$this->products} SET price = '$price', pricing_update = '$date' WHERE product_id = '$id'";
        $this->db->query($sql);
    }

    public function setInventory($id, $count){
        $date = date('Y-m-d H:i:s');

        $sql = "INSERT IGNORE INTO {$this->inventory} SET product_id = '$id', inventory = '$count', timestamp = '$date'";
        $this->db->query($sql);

        $sql = "UPDATE {$this->products} SET
                inventory = '$count',
                inventory_update = '$date'
                WHERE product_id = '$id'";
        $this->db->query($sql);
    }

    public function savePromotedProduct($id){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT IGNORE INTO {$this->promotions} SET product_id = '$id', timestamp = '$date'";
        return $this->db->query($sql);
    }

    public function getProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id,  xref.product_id ams_id, p.brand_id, p.name, b.name brand, i.inventory FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) OR inventory IS NULL
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProductsFromBrandId($brand_id){

        $sql = "SELECT p.product_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE p.brand_id = $brand_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) OR inventory IS NULL
                ORDER BY i.inventory DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getBrandTotals($brand_id){

        $sql = "SELECT COUNT(t.product_id) total_products, SUM(t.inventory) inventory, t.price*SUM(t.inventory) potential
                FROM (
                SELECT p.product_id, p.name, i.inventory, i.`timestamp`, c.`price` FROM {$this->products} p
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->prices} c ON p.product_id = c.product_id
                WHERE p.brand_id = $brand_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) AND MAX(c.`timestamp`)
                ORDER BY i.inventory DESC ) t
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }


    public function getAllBrandTotals(){

        $sql = "SELECT id, name, COUNT(id) total, SUM(inventory) stock, price*SUM(inventory) invested FROM (
                SELECT b.id, b.name, i.inventory, c.`price` FROM {$this->products} p
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                JOIN {$this->brands} b ON b.`id` = p.`brand_id`
                LEFT JOIN {$this->prices} c ON p.product_id = c.product_id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`) AND MAX(c.`timestamp`)) q
                GROUP BY name
                ORDER BY name
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTopBrands(){

        $sql = "SELECT b.id, b.name, COUNT(p.name) product_line, SUM(p.inventory) total_inventory, SUM(sold) units_moved, SUM(restocked) restocked, SUM(sold) * price revenue FROM `{$this->products}` p
                JOIN `{$this->brands}` b ON p.`brand_id` = b.`id`
                GROUP BY p.brand_id
                ORDER BY revenue DESC, units_moved DESC
                LIMIT 100
                ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduct($id){
        $sql = "SELECT p.product_id, p.name, b.name brand, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                LEFT JOIN {$this->xref} xref ON xref.product_id = p.`product_id`
                WHERE p.product_id = $id";

        $query = $this->db->query($sql);
        return $query->row();
    }


    public function getOutOfStock($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE i.inventory = 0
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.timestamp DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function getDiscontinued($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                WHERE i.inventory = -1
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.timestamp DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getExclusiveCompetitorProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp`, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                WHERE  xref.`ams_id` IS NULL
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getIdenticalCompetitorProducts($page=1, $itemsPerPage=200){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT p.product_id, p.brand_id, p.name, b.name brand, i.inventory, i.`timestamp`, xref.`ams_id` FROM {$this->products} p
                JOIN {$this->brands} b ON b.id = p.brand_id
                JOIN {$this->inventory} i ON i.product_id = p.product_id
                LEFT JOIN {$this->xref} xref ON p.`product_id` = xref.product_id
                WHERE  xref.`ams_id` IS NOT NULL
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.inventory DESC
                LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getExclusiveCompetitorBrands(){
        $sql = "SELECT e.id, e.name, b.name ams, b.id ams_id
                FROM {$this->brands} e
                LEFT JOIN brands b ON e.`ams_id` = b.`id`
                ORDER BY e.name";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function updateBrandXref($id, $ams){
        $sql = "UPDATE {$this->brands} SET ams_id = '$ams' WHERE id = '$id'";
        return $this->db->query($sql);
    }

    public function getPagination($itemsPerPage=200){
        $sql = "SELECT count(*) as total FROM {$this->products}";
        $query = $this->db->query($sql);
        return $query->row()->total/$itemsPerPage;
    }

    public function dailyRollup($id){
        $sql = "SELECT count(*) as total FROM {$this->products}";
        $query = $this->db->query($sql);
    }

    public function getCommonBrands(){
        $sql = "SELECT GROUP_CONCAT(id) ids FROM `{$this->brands}`
                WHERE ams_id IS NOT NULL";
        $query = $this->db->query($sql);
        return $query->row()->ids;
    }

    public function getAMSBrand($id){
        $sql = "SELECT ams_id FROM `{$this->brands}`
                WHERE id  = $id";
        $query = $this->db->query($sql);
        return $query->row()->ams_id;
    }

    public function getYearOfProductInventory($id){
        $sql = "SELECT inventory, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days   FROM {$this->inventory}
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getYearOfProductPrices($id){
        $sql = "SELECT price, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days  FROM {$this->prices}
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function updateProductXref(){

        //go through ams_competitors get airplat values
        //if value is null then


        $sql = "
            INSERT IGNORE INTO `competitors_airsplat_xref` SELECT  a.product_id, p.id
            FROM ams_competitors c
            LEFT JOIN competitors_airsplat a ON a.`sku` = IF(LOCATE('Itemdesc.asp?ic=',airsplat),
                SUBSTRING_INDEX(airsplat, '/Itemdesc.asp?ic=',-1),
                SUBSTRING_INDEX( SUBSTRING_INDEX(airsplat, '/',-1) , '.htm', 1) )
            JOIN products p ON p.sku = c.`sku`
            WHERE LOCATE('airsplat',airsplat) AND product_id IS NOT NULL
        ";

        $this->db->query($sql);

    }

    public function updateDuplicateSkus(){
        $skus = $this->getDuplicateSkus();
        foreach($skus as $row){
            //crawl the url of this sku get the real sku and update the index
            $true_sku = $this->crawlProductPage($row->url);
            if($true_sku){
                $this->updateProductSku($row->product_id,$true_sku);
            }else{
                $this->setProductAsDiscontinued($row->product_id);
            }
        }
        $this->updateXref();
    }
    public function getDuplicateSkus(){

        $sql = "
            SELECT p.*
            FROM competitors_airsplat p
            LEFT JOIN
            (SELECT sku FROM competitors_airsplat
            GROUP BY sku
            HAVING COUNT(sku) > 1) AS s ON s.sku = p.sku
            WHERE s.sku IS NOT NULL AND p.`inventory` <> -1
        ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function crawlProductPage($url){
        $body = $this->getBrand($url);
        $page = htmlqp($body);

        if($page->is('.error-area')) return false;

        $page->top();
        $txt = $page->find('.product-code span')->text();
        return substr($txt, strlen('Product Code:'));
    }

    public function updateProductSku($id, $sku){
        $sql = "UPDATE competitors_airsplat SET sku = '$sku' WHERE product_id = $id";
        $this->db->query($sql);
    }

    public function setProductAsDiscontinued($id){
        $sql = "UPDATE competitors_airsplat SET inventory = '-1' WHERE product_id = $id";
        $this->db->query($sql);
    }


    public function getLatestInventoryAndPrice($ams_id=0){
        $sql = "SELECT i.product_id, ams_id, inventory, price, i.`change`, i.`timestamp` inventory_update, p.timestamp pricing_update
                FROM competitors_airsplat_inventory i
                LEFT JOIN competitors_airsplat_prices p ON i.`product_id` = p.`product_id`
                LEFT JOIN competitors_airsplat_xref xref ON i.`product_id` = xref.product_id
                WHERE xref.`ams_id` = $ams_id
                HAVING MAX(i.timestamp) AND MAX(p.`timestamp`)";

        $query = $this->db->query($sql);
        return $query->row();
    }

}