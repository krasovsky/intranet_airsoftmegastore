<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Ams extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
//        $this->mongo_config = $ci->config->item('mongo_db');

//        $this->categories = $this->getCategories();
    }

//    public function getProducts($page=1){
//        $itemsPerPage = 20;
//        $skip = (int)($itemsPerPage * ($page - 1));
//
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_products;
//
//        $result =  $collection->find()->limit(20)->skip($skip);
//        return iterator_to_array($result,false);
//    }

    public function getAllProducts($page){
        $page *= 50;
        $sql = "SELECT id from products";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getAllProductsForLogging(){
        $sql = "SELECT id, re FROM products";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProductIds(){
        $sql = "SELECT p.id product_id FROM products p
                LEFT JOIN item_compatiblity i ON p.id = i.`product_id`
                WHERE i.`product_id` IS NOT NULL
                AND p.`merchandisable` = 1
                AND i.`item_id` <> 0
                GROUP BY p.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSkuFromId($id){
        $sql = "SELECT i.sku FROM products p
                LEFT JOIN items i ON i.product_id = p.id
                WHERE p.id =  $id";
        $query = $this->db->query($sql);
        return $query->row()->sku;
    }

    public function getIdFromSku($sku){
        $sql = "SELECT product_id id FROM items i
                WHERE sku = '$sku'";
        $query = $this->db->query($sql);
        return $query->row()->id;
    }

    public function getProducts($page=1, $itemsPerPage=20, $options = array()){
        $skip = $itemsPerPage * $page;
        $whereStr = "";

        if(! empty($options["inStock"]) && $options["inStock"] == true){
            $whereStr  = "WHERE IF((i.inventory - i.allocated_inventory) > 0 AND (i.inventory - i.allocated_inventory) > i.`outofstock_threshold`, i.inventory - i.allocated_inventory , 0 )  > 0";
        }
        else if(! empty($options["onSale"]) && $options["onSale"] == true){
            $whereStr  = "WHERE p.on_sale  > 0";
        }

        $sql = "SELECT p.* FROM products p
        inner join items i on i.product_id = p.id
        {$whereStr}
        LIMIT $skip, $itemsPerPage";
        //dump($sql);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPagination($itemsPerPage=20){
        $sql = "SELECT count(*) as total FROM products";
        $query = $this->db->query($sql);
        return $query->row()->total/$itemsPerPage;
    }

//    public function getItem($_id){
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_products;
//
//        $result =  $collection->find(array('_id'=>new MongoId($_id)));
//        return iterator_to_array($result,false);
//    }


    public function setDescription($id,$description){
        $sql = "INSERT INTO product_descriptions SET id = $id, description = '".addslashes($description)."'".
        "ON DUPLICATE KEY UPDATE description = '".addslashes($description)."'";
        return $query = $this->db->query($sql);

    }
    public function getDescriptions(){
        $sql = "SELECT * FROM product_descriptions d
                JOIN products p ON d.id = p.id
                WHERE p.`long_description` <> ''
                AND p.merchandisable = 1
                ";
        $query = $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduct($id){
        $sql = "SELECT p.id, p.name, p.long_description, ap.features, ap.specs FROM products p
                LEFT JOIN ams_products as ap ON p.id = ap.id WHERE p.id = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getProductCompare($id){
        $sql = "SELECT p.id, p.name, b.name brand FROM products p
                JOIN brands b ON b.name = p.brand_backend_name
                WHERE p.id = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }



    public function getProductById($id){
        $sql = "SELECT id, name,long_description
                FROM products
                WHERE id = '$id'";

        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getItem($item_id){
        $sql = "SELECT i.id, p.name, p.long_description, opr.create_date, opr.30_days_sales,opr.90_days_sales,opr.365_days_sales 
                FROM items i
                LEFT JOIN products p ON i.product_id = p.id
                left join od_product_ratings opr on opr.item_id = i.id and opr.30_days_sales is not null
                WHERE i.id = '$item_id'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function getSimilarProducts($item_id){
        $sql = "SELECT p.name, CONCAT(x.product_id,'-',p.vanity_url,'.aspx') url, p.id product_id
                FROM categories_xref X
                JOIN categories c ON x.`category_id` = c.`id`
                JOIN products p ON  x.product_id = p.id
                JOIN items i ON p.`id` = i.`product_id`
                WHERE p.merchandisable = 1
                AND x.category_id IN ( SELECT category_id FROM categories_xref WHERE product_id = $item_id )
                AND p.`merchandisable` = 1
                AND i.`inventory` > 3
                AND p.id <> $item_id
                ORDER BY x.category_id DESC
                LIMIT 5";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getProductCategories($item_id){
        $sql = "SELECT c.`name` cat_name, CONCAT('/Categories/', c.id,'-',c.`vanity_url`,'.aspx') cat_url, c.id cat_id
                FROM categories_xref X
                JOIN categories c ON x.`category_id` = c.`id`
                WHERE x.product_id = $item_id
                ORDER BY x.category_id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCompatibleProducts($item_id){
        $sql = "SELECT c.item_id, i.sku, p.name, p.brand_backend_name brand, CONCAT(c.item_id, '-', p.vanity_url, '.aspx') link, SUBSTRING_INDEX(p.img,';',1) img, pp.price, top.profit FROM item_compatiblity c
                LEFT JOIN items i ON item_id = i.id
                LEFT JOIN products p ON i.product_id = p.id
                LEFT JOIN topsellers2013 top ON i.sku = top.sku
                LEFT JOIN productpricings pp ON i.id = pp.id
                WHERE c.product_id = $item_id
                        AND p.merchandisable = 1
                        AND pp.quantity = 1
                        AND CONCAT(',' , p.categories , ',') NOT LIKE '%,22,%'
                        AND i.inventory > 0
                ORDER BY top.profit DESC, RAND()
                LIMIT 15";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getRandomCompatibleProducts($item_id){
        $sql = "SELECT c.item_id id, p.name, CONCAT(c.item_id, '-', p.vanity_url, '.aspx') link, top.profit FROM item_compatiblity c
                LEFT JOIN items i ON item_id = i.id
                LEFT JOIN products p ON i.product_id = p.id
                LEFT JOIN topsellers2013 top ON i.sku = top.sku
                WHERE c.product_id = $item_id
                AND p.merchandisable = 1
                AND CONCAT(',' , p.categories , ',') NOT LIKE '%,22,%'
                AND i.inventory > 0
                ORDER BY RAND(), top.profit DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getRandomReverseCompatibleProducts($item_id){
        $sql = "SELECT DISTINCT c.product_id id, p.name, CONCAT(c.product_id, '-', p.vanity_url, '.aspx') link, top.profit
                FROM item_compatiblity c
                LEFT JOIN items i ON c.product_id = i.id
                LEFT JOIN products p ON i.product_id = p.id
                LEFT JOIN topsellers2013 top ON i.sku = top.sku
                WHERE c.item_id = $item_id
                AND p.merchandisable = 1
                AND CONCAT(',' , p.categories , ',') NOT LIKE '%,22,%'
                AND i.inventory > 0
                ORDER BY RAND(), top.profit DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function parse($id){
        $sql = "SELECT p.id, p.name, p.long_description, ap.features, ap.specs FROM products p
                LEFT JOIN ams_products as ap ON p.id = ap.id";
        $query = $this->db->query($sql);

        foreach($query->result() as $row){
            $this->getFeatures($row);
        }
    }
    public function setFeatures($id, $text){
        //insert feature into feature options
        $sql = "INSERT IGNORE INTO ams_features SET title = '$text'";
        $this->db->query($sql);

        //get the feature id
//        $sql = "SELECT id FROM ams_features WHERE features = '$text'";
//        $query = $this->db->query($sql);
//        $insert_id = $query->row()->id;

        //use the insert id for adding to cross reference table
        $sql = "INSERT IGNORE INTO ams_features_xref SET pid = '$id', fid = (SELECT id FROM ams_features WHERE title = '$text')";
        $this->db->query($sql);
    }
    public function getFeatures($row){
        //find features in desc
        $start = stripos($row->long_description, '<strong>Features:</strong>');

        if($start !== false){

            $end = stripos($row->long_description, '<strong>Specifications:</strong>')-$start-32;
            $featureTextList = substr($row->long_description, $start+26, $end);
            //clean up syntax
            $featureTextList = strip_tags($featureTextList);
            $featureTextList = preg_replace('/^-\s/','',$featureTextList);
            $featureTextList = str_ireplace('&nbsp;',' ',$featureTextList);
            $featureTextList = str_ireplace('&amp;','and ',$featureTextList);
            $featureTextList = str_ireplace('\w','with',$featureTextList);
            $featureList = explode("- ",$featureTextList);

            foreach($featureList as $feature){
                $this->setFeatures($row->id, mysql_real_escape_string($feature));
            }
        }
    }


    public function getCategories(){
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_categories;
//
//        $result =  $collection->find();

//        $indexed = array();
//        foreach($result as $r){
//            $indexed[$r['_id']] = $r;
//        }

       // return iterator_to_array($result,true);
    }

    public function getCategory($id){
        return $this->categories[$id]?:'';
    }

    public function comparePrices($id){
        $sql = "SELECT price from productpricings WHERE id = $id AND quantity = 1)";
        $result = $this->db->query($sql);
    }

    private function isPriceSame($id){
        $sql = "SELECT price from productpricings WHERE id = $id AND quantity = 1)";
        $result = $this->db->query($sql);

        $new = $result->row()->price;

        $sql = "SELECT price from productpricings_control WHERE id = $id AND quantity = 1)";
        $result = $this->db->query($sql);

        $old = $result->row()->price;

        return $new == $old;
    }

    public function getAllBrands(){
        $sql = "SELECT id, name FROM brands ORDER BY name";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getAllProductsInBrandId($brand_id){
        $sql = "SELECT p.id, p.name FROM products p
                LEFT JOIN brands b ON p.`brand_backend_name` = b.`name`
                WHERE b.id = $brand_id
                ORDER BY NAME";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getYearOfProductInventory($id){
        $sql = "SELECT inventory, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days   FROM competitors_ams_inventory
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getYearOfProductPrices($id){
        $sql = "SELECT price, DATE_FORMAT(`timestamp`,'%m/%d/%Y') 'timestamp', DATEDIFF(NOW(), TIMESTAMP) days  FROM competitors_ams_prices
                WHERE product_id = $id
                AND TIMESTAMP > DATE_SUB(NOW(), INTERVAL 1 YEAR)
                GROUP BY DAY(`timestamp`)
                ORDER BY `timestamp`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function setAllProductPrices(){
        $sql = "INSERT IGNORE INTO competitors_ams_prices SELECT p.id, pp.price, NOW() FROM products p
                LEFT JOIN productpricings pp ON p.id = pp.id";
        $query = $this->db->query($sql);
    }
    public function setAllProductInventory(){

        $sql = "SELECT product_id,inventory FROM competitors_ams_inventory
                GROUP BY product_id
                HAVING MAX(TIMESTAMP)";
        $query = $this->db->query($sql);
        $previous = $query->result();

        foreach($previous as $row){
            $sql = "SELECT inventory FROM items WHERE product_id = {$row->product_id}";
            $this->db->query($sql);
            $current_inventory = $query->row()->inventory;

            $change = $current_inventory - $row->inventory;

            $sql = "INSERT IGNORE INTO competitors_ams_inventory
                    SET product_id = {$row->product_id}, inventory = $current_inventory, `change` = $change, `timestamp` = NOW()";
            $this->db->query($sql);
        }
    }

    public function findSilimarProductsByBrand($productId){}

}