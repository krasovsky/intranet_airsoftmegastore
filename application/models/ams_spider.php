<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class ams_spider extends CI_Model {

    public function getNextIdToSpider($id){
        if(!$id){
            $sql = "SELECT id FROM products WHERE
                    merchandisable = 1 AND
                    id NOT IN (SELECT DISTINCT product_id FROM item_compatiblity)
                    LIMIT 1";
            $result = $this->db->query($sql);
            $id = $result->row()->id;
        }

        return $id;
    }
    public function getCompatibleItems($id){
        $url = "http://www.airsoftmegastore.com/$id-spider.aspx";
        $this->page = htmlqp($url);
        $related = $this->page->find('#relatedAccordion option');

        $items = array();
        foreach($related as $i){
            $key = (int)$i->attr('value');
            if($key > 0)
                $items[$key] = 0;
        }

        return array_keys($items);
    }

    public function saveItems($id,$items){
        foreach($items as $item){
            $sql = "INSERT IGNORE INTO item_compatiblity SET product_id = $id, item_id = $item";
            $this->db->query($sql);
        }

        if(count($items)==0){
            $sql = "INSERT IGNORE INTO item_compatiblity SET product_id = $id, item_id = 0";
            $this->db->query($sql);
        }
    }

}