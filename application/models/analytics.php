<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

include(APPPATH.'/libraries/ga-api/GoogleAnalyticsAPI.class.php');

class Analytics extends CI_Model {

    function __construct()
    {
        parent::__construct();

    }

    public function getTopTrends($start,$end){
        $sql = "SELECT i.id, `events`, p.sku, p.name,  DATE_FORMAT(`timestamp` ,'%m/%d/%y') AS `timestamp` FROM ga_addtocart ga
                LEFT JOIN items i ON ga.product_id = i.id
                LEFT JOIN products p ON i.product_id = p.id
                WHERE `timestamp` <= '$start'
                    AND `timestamp` >= '$end'
                ORDER BY ga.events DESC
                LIMIT 100";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduct($id){

        $sql = "SELECT p.*, i.id itemid
                FROM products p
                LEFT JOIN items i ON p.id = i.product_id
                WHERE i.id = '$id'
                ";

        $query = $this->db->query($sql);
        return $query->row();
    }
    public function getOrders($id, $start, $end){

    $sql = "SELECT count(o.id) quantity, SUM(quantity) total, DATE_FORMAT(ordered_at_datetime ,'%m/%d/%y') AS `timestamp` FROM orders o
                JOIN order_items i ON i.order_record_id = o.id
                WHERE item_record_id = '$id'
                    AND `ordered_at_datetime` <= '$start'
                    AND `ordered_at_datetime` >= '$end'
                GROUP BY DATE(ordered_at_datetime)
                ORDER BY `ordered_at_datetime` ASC
                ";

    $query = $this->db->query($sql);
    return $query->result_array('stamp');
    }
    public function getProductTrends($id, $start, $end){

        $sql = "SELECT product_id, `events`, DATE_FORMAT(timestamp ,'%m/%d/%y') as `timestamp` FROM ga_addtocart
                WHERE product_id = '$id'
                    AND`timestamp` <= '$start'
                    AND `timestamp` >= '$end'
                GROUP BY `timestamp`, product_id
                ORDER BY `timestamp` ASC
                ";



        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function setup($startDate='', $endDate=''){

        $ci =& get_instance();
        $oath_session = $ci->session->userdata('oauth_access_token');

        // From the APIs console
        $client_id = '1078608048317-ck9vv5l0is29buqljh9c00jqb1b880b6.apps.googleusercontent.com';
        // From the APIs console
        $client_secret = 'Pmito4CTfpqgc_-8SD2P4Qfr';
        // Url to your this page, must match the one in the APIs console
        $redirect_uri = 'http://catalog.airsoftmegastore.net/index.php/seo';
        // Analytics account id like, 'ga:xxxxxxx'
        $account_id = 'ga:13033046';

        //session_start();
        $this->ga = new GoogleAnalyticsAPI();
        $this->ga->auth->setClientId($client_id);
        $this->ga->auth->setClientSecret($client_secret);
        $this->ga->auth->setRedirectUri($redirect_uri);


        if (isset($_GET['force_oauth'])) {
            $ci->session->unset_userdata('oauth_access_token');
        }

        /*
         *  Step 1: Check if we have an oAuth access token in our session
         *          If we've got $_GET['code'], move to the next step
         */
        if (!$oath_session && !isset($_GET['code'])) {
            // Go get the url of the authentication page, redirect the client and go get that token!
            $url = $this->ga->auth->buildAuthUrl();
            header("Location: ".$url);
        }

        /*
         *  Step 2: Returning from the Google oAuth page, the access token should be in $_GET['code']
         */
        if (!$oath_session && isset($_GET['code'])) {
            $auth = $this->ga->auth->getAccessToken($_GET['code']);
            if ($auth['http_code'] == 200) {
                $accessToken    = $auth['access_token'];
                $refreshToken   = $auth['refresh_token'];
                $tokenExpires   = $auth['expires_in'];
                $tokenCreated   = time();

                // For simplicity of the example we only store the accessToken
                // If it expires use the refreshToken to get a fresh one
                $ci->session->set_userdata('oauth_access_token', $accessToken );
            } else {
                die("Sorry, something wend wrong retrieving the oAuth tokens");
            }
        }

        /*
         *  Step 3: Do real stuff!
         *          If we're here, we sure we've got an access token
         */
        $this->ga->setAccessToken($ci->session->userdata('oauth_access_token'));
        $this->ga->setAccountId($account_id);

        // Set the default params. For example the start/end dates and max-results
        $defaults = array(
            'start-date' => ( ($startDate)? $startDate: date('Y-m-d', strtotime('-1 month')) ),
            'end-date'   => ( ($endDate)? $endDate : date('Y-m-d') )
        );
        $this->ga->setDefaultQueryParams($defaults);
    }
    public function getTopOrganicKeywords(){
        //top keywords
        $params = array(
            'dimensions' => 'ga:keyword',
            'metrics'    => 'ga:sessions,ga:revenuePerTransaction,ga:transactionsPerSession',
            'segment' => 'gaid::-5',
            'sort' => '-ga:sessions'
        );

        return  $this->ga->query($params);
    }
    public function getTopPPCKeywords(){
        $params = array(
            'dimensions' => 'ga:keyword',
            'metrics'    => 'ga:sessions,ga:revenuePerTransaction,ga:transactionsPerSession',
            'segment' => 'gaid::-4',
            'sort' => '-ga:sessions'
        );
        return  $this->ga->query($params);
    }
    public function getTrendingMenuItems(){
        $params = array(
            'dimensions' => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
            'metrics'    => 'ga:totalEvents',
            'filters' => 'ga:eventAction==MegaMenu',
            'sort' => '-ga:totalEvents'
        );
        return  $this->ga->query($params);
    }
    public function getTrendingProducts(){
        $params = array(
            'dimensions' => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
            'metrics'    => 'ga:totalEvents',
            'start-date'=> '2014-07-25',
            'end-date'=> '2014-08-22',
            'filters' => 'ga:eventAction==AddToCart,ga:eventAction==AddToWishlist',
            'sort' => '-ga:totalEvents'
        );
        return  $this->ga->query($params);
    }
    public function getTrendingSliders(){
        $params = array(
            'dimensions' => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
            'metrics'    => 'ga:totalEvents',
            'filters' => 'ga:eventAction==Hero',
            'sort' => '-ga:totalEvents'
        );
        return  $this->ga->query($params);
    }

    public function setOrganicKeywords($data){
        $dtime = new DateTime($data['query']['end-date']);
        $timestamp = $dtime->format("Y-m-d H:i:s");

        foreach($data['rows'] as $row){
            $sql = "INSERT IGNORE INTO ga_keywords_organic
                SET `keyword` = '".addslashes($row[0])."',
                    `sessions` = '{$row[1]}',
                    `revenue` = '{$row[2]}',
                    `conversions` = '{$row[3]}',
                    `timestamp` = '$timestamp'
                ";
            $this->db->query($sql);
        }
    }
    public function setTopPPCKeywords($data){
    $dtime = new DateTime($data['query']['end-date']);
    $timestamp = $dtime->format("Y-m-d H:i:s");

        foreach($data['rows'] as $row){
            $sql = "INSERT IGNORE INTO ga_keywords_ppc
                    SET `keyword` = '".addslashes($row[0])."',
                        `sessions` = '{$row[1]}',
                        `revenue` = '{$row[2]}',
                        `conversions` = '{$row[3]}',
                        `timestamp` = '$timestamp'
                    ";
            $this->db->query($sql);
        }
    }
    public function setTrendingProducts($data){
        $dtime = new DateTime($data['query']['end-date']);
        $timestamp = $dtime->format("Y-m-d H:i:s");

        foreach($data['rows'] as $row){
            $sql = "INSERT IGNORE INTO ga_addtocart
                SET `location` = '{$row[0]}',
                    `product_id` = '{$row[2]}',
                    `events` = '{$row[3]}',
                    `timestamp` = '$timestamp'
                ";
            $this->db->query($sql);
        }
    }

    public function ordersToTrendsRemap($orders, $trends){
        $empty = array();
        $empty['quantity'] = 0;

        $count = count($trends);
        $remap = array();
        for($i = 0; $i < $count; $i++){
            foreach($orders as $o){
                if($o['timestamp']==$trends[$i]['timestamp']){
                    $remap[$i] = $o;
                    break;
                }else{
                    $remap[$i] = $empty;
                }
            }
            if(count($orders)==0){
                $empty['timestamp'] = $trends[$i]['timestamp'];
                $remap[$i] = $empty;
                continue;
            }
        }
        return $remap;
    }

    public function getProductLinks(){
        $sql = "SELECT CONCAT('/',id,'-',vanity_url,'.aspx') url,
            IF ( t.total_price IS NOT NULL ,'0.8', '0.6') score,
            img, p.name
            FROM products p
            LEFT JOIN topsellers2013 t ON t.`product_id` = p.`id`
            WHERE p.merchandisable = 1
            ORDER BY total_price DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getCategoryLinks(){
        $sql = "SELECT CONCAT('/Categories/',id,'-',vanity_url,'.aspx') url, '0.5' score
                FROM categories p
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
}