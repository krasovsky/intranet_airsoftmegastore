<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Categorymanager extends CI_Model {

    public function addProductsToCategory($cid=0,$pid=array()){

        foreach($pid as $p){
            $sql = "INSERT INTO categories_xref
                    SET category_id = $cid, product_id = $p
                    ON DUPLICATE KEY UPDATE product_id = $p";

            $this->db->query($sql);
        }
    }
    public function removeProductsFromCategory($cid=0,$pid=array()){

        foreach($pid as $p){
            $sql = "DELETE FROM categories_xref
                    WHERE category_id = $cid AND product_id = $p
                    ";

            $this->db->query($sql);
        }
    }

    public function downloadCategories($ids=''){
        $sql = "SELECT id,
                GROUP_CONCAT(c.category_id ORDER BY c.category_id) AS categories
                FROM products p
                JOIN categories_xref c ON c.`product_id` = p.`id`
                WHERE c.product_id IN ( $ids )
                GROUP BY c.product_id
                ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function downloadAllCategories(){
        $sql = "SELECT id,
                GROUP_CONCAT(c.category_id ORDER BY c.category_id) AS categories
                FROM products p
                JOIN categories_xref c ON c.`product_id` = p.`id`
                GROUP BY c.product_id
                ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}