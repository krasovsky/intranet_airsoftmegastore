<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Competitormanager extends CI_Model {

    public function getNextSku(){
        $sql = "SELECT sku FROM `ams_competitors_latest`
                ORDER BY last_update ASC
                LIMIT 1";

        $links = $this->db->query($sql);
        return $links->row()->sku;
    }
//
//    public function removeSku($sku){
//        //remove from competitors
//        $sql = "DELETE FROM `ams_competitors`
//                WHERE sku = '$sku'";
//
//        $this->db->query($sql);
//        //remove from competitors latest
//        $sql = "DELETE FROM `ams_competitors_latest`
//                WHERE sku = '$sku'";
//
//        $this->db->query($sql);
//
//        //remove from competitors_prices
//        $sql = "DELETE FROM `ams_competitor_prices`
//                WHERE sku = '$sku'";
//
//        $this->db->query($sql);
//    }

    public function getItemIdFromSku($sku){
        $sql = "SELECT product_id id FROM items
                WHERE sku = '$sku'";

        $links = $this->db->query($sql);
        return @$links->row()->id;
    }

    public function getLinks($sku){

        $sql = "SELECT * FROM ams_competitors
                WHERE sku = '$sku'";

        $links = $this->db->query($sql);
        return $links->row();
    }


    public function getWholesalePrice($sku){

        $sql = "SELECT wholesale_price FROM `products`
                WHERE sku = '$sku'";

        $links = $this->db->query($sql);
        return @$links->row()->wholesale_price;
    }

    public function getWholesalePriceById($id){

        $sql = "SELECT wholesale_price FROM `products`
                WHERE id = $id";

        $links = $this->db->query($sql);
        return @$links->row()->wholesale_price;
    }

    public function getInventory($sku){

        $sql = "SELECT inventory FROM `items`
                WHERE sku = '$sku'";

        $links = $this->db->query($sql);
        return @$links->row()->inventory;
    }

    public function getGrossMarginPercentage($wholesale,$retail){
        return round(( $retail - $wholesale ) / $retail * 100, 2);
    }

    public function getFourtyPercent($wholesale){
        return round($wholesale / 0.6,2);
    }

    public function getLowestPossible($wholesale){
        return round($wholesale / 0.8,2);
    }

//    public function insertPrice($sku,$name,$price){
//        var_dump($sku,$name,$price);
//
//        if(!$price) return false;
//
//        $sql = "INSERT INTO ams_competitor_prices
//                SET `sku` = '$sku', `name` = '$name', `price` = $price";
//
//        return $this->db->query($sql);
//
//    }

    public function getAllSkus(){
        $sql = "SELECT c.sku, l.ams, l.evike, l.airsoftgi, l.airsplat, l.hobbytron, l.hitguns, l.airrattle, l.last_update FROM ams_competitors c
                LEFT JOIN  `ams_competitors_latest` l ON c.sku = l.sku";

        $skus = $this->db->query($sql);
        return $skus->result();
    }

    public function getUrls($sku){
        $sql = "SELECT * FROM ams_competitors
                WHERE sku = '$sku'";

        $links = $this->db->query($sql);
        return $links->row();
    }

    public function getCompetitorPrices($sku, $competitor, $count=1){
        $sql = "SELECT price FROM ams_competitor_prices
                WHERE `name` = '$competitor'
                AND `sku` = '$sku'
                ORDER BY `timestamp` DESC
                LIMIT $count";

        $prices = $this->db->query($sql);
        return ($prices->row()) ?$prices->row()->price:'';
    }

//    public function updatePrices($sku){
//
//        $links = $this->competitormanager->getLinks($sku);
//        $prices = array();
//        var_dump($links);
//
//        $prices['ams'] = $this->getAMSPrice($sku);
//        $this->insertPrice($sku, 'ams', $prices['ams']);
//
//        if($links->evike){
//            $prices['evike'] = $this->getEvikePrice($links->evike);
//            $this->insertPrice($sku, 'evike', $prices['evike']);
//        }
//
//        if($links->airsoftgi){
//            $prices['airsoftgi'] = $this->getAirsoftGIPrice($links->airsoftgi);
//            $this->insertPrice($sku, 'airsoftgi',$prices['airsoftgi']);
//        }
//
//        if($links->airsplat){
//            $prices['airsplat'] = $this->getAirsplatPrice($links->airsplat);
//            $this->insertPrice($sku, 'airsplat',$prices['airsplat']);
//        }
//
//        if($links->hobbytron){
//            $prices['hobbytron'] = $this->getHobbytronPrice($links->hobbytron);
//            $this->insertPrice($sku, 'hobbytron',$prices['hobbytron']);
//        }
//
//        if($links->hitguns){
//            $prices['hitguns'] = $this->getHitgunsPrice($links->hitguns);
//            $this->insertPrice($sku, 'hitguns',$prices['hitguns']);
//        }
//
//        if($links->airrattle){
//            $prices['airrattle'] = $this->getAirrattlePrice($links->airrattle);
//            $this->insertPrice($sku, 'airrattle',$prices['airrattle']);
//        }
//
//        $this->setLatestPrices($sku, $prices);
//
//    }

    public function setLatestPrices($sku, $prices){

        $set = '';
        foreach($prices as $key=>$val){
           $set .=  "`$key` = '$val'," ; //with or without commma
        }

        $sql = "INSERT IGNORE INTO `ams_competitors_latest` SET `sku` = '$sku', $set  `last_update` = TIMESTAMP (NOW())
                ON DUPLICATE KEY UPDATE $set `last_update` = TIMESTAMP (NOW())";

        return $this->db->query($sql);

    }

    public function saveUrls($urls){

        $set = "`sku`='{$urls['sku']}',`evike`='{$urls['evike']}',`airsoftgi`='{$urls['airsoftgi']}',`airsplat`='{$urls['airsplat']}',`airrattle`='{$urls['airrattle']}'";
        $sql = "INSERT INTO `ams_competitors` SET $set
                ON DUPLICATE KEY UPDATE $set";

        $this->db->query($sql);

       // $this->updatePrices($urls['sku']);
    }

//    public function getAMSPrice($sku){
//        $sql = "SELECT price FROM productpricings WHERE product_skus = '$sku,'";
//        $price = $this->db->query($sql);
//        return $price->row()->price;
//    }
//
//    public function getEvikePrice($url){
//        $this->page = htmlqp($url);
//
//
//        if($price = str_replace('$','',$this->page->find('.productSpecialPrice')->first()->text())){
//            return $price;
//        }else{
//            $this->page->top();
//            $price = $this->page->find('#productpageheader h2')->text();
//            $price = str_replace('$','', $price);
//            return $price;
//        }
//    }
//
//    public function getAirsoftGIPrice($url){
//        $this->page = htmlqp($url);
//
//        if($price = str_replace('Now: $','', $this->page->find('.productSpecialPrice[style^=font]')->text())){
//            return $price;
//        }else{
//            $this->page->top();
//            $price = $this->page->find('b[style^=font]')->text();
//            $price = str_replace('Price: $','', $price);
//            return $price;
//        }
//    }
//
//    public function getAirsplatPrice($url){
//        $this->page = htmlqp($url);
//        if($price = $this->page->find('span[itemprop=price]')->text()){
//            return $price;
//        }
//    }
//
//    public function getHobbytronPrice($url){
//
//        $this->page = htmlqp($url);
//        return str_replace('$','',$this->page->find('span[itemprop=price] b')->text());
//    }
//
//    public function getHitgunsPrice($url){
//
//        $this->page = htmlqp($url);
//        return str_replace('$','',$this->page->find('span[itemprop=price]')->text());
//    }
//
//    public function getAirrattlePrice($url){
//
//        $this->page = htmlqp($url);
//        return str_replace('$','',$this->page->find('span[itemprop=price]')->text());
//    }
//
       public function getAllProductsSummary($page=1,$count=400){
       $page -= 1;
       $page *= $count;
        $sql = "SELECT * FROM competitors_summary ORDER BY product_id
            LIMIT $page, $count
        ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function generateDataList($option = array()){
        $rez = $this->db->query("SELECT distinct sku,id from products");
        $resultArr = $rez->result_array();
        $returnArr = array();
        foreacH($resultArr as $element){
            $returnArr[$element["sku"]] = $element;
        }
        return $returnArr;
    }

    public function getAllProducts($page=1,$count=400,$optionsArr = array()){
        $page -= 1;
        $page *= $count;

        //$this->load->library('simple_cache');
        $whereStr = "";
        if (! empty($optionsArr["search"])){
            $whereStr = " AND ams.sku like '%{$optionsArr["search"]}%'";
        }

        $limitStr = "LIMIT {$page} , {$count}";

        $sql = "SELECT ams.`product_id`, ams.sku, ams.inventory, p.price,
                e.`inventory` evike_inventory, e.price evike,
                gi.`inventory` airsoftgi_inventory, gi.price airsoftgi
                FROM items ams
                LEFT JOIN productpricings p ON p.id = ams.`product_id`
                LEFT JOIN competitors_evike_xref xref ON xref.ams_id = ams.`product_id`
                LEFT JOIN competitors_evike e ON e.product_id = xref.product_id

                LEFT JOIN competitors_airsoftgi_xref xref2 ON xref2.ams_id = ams.`product_id`
                LEFT JOIN competitors_airsoftgi gi ON gi.product_id = xref2.product_id
                WHERE p.`quantity` = 1 {$whereStr} {$limitStr}";

        //if (! $this->simple_cache->is_cached(md5($sql))){

            $queryRez = $this->db->query($sql);

            $data = $queryRez->result();

            //$this->simple_cache->cache_item(md5($sql),$data);
        //}
        //else{
            //$data = $this->simple_cache->get_item(md5($sql));
        //}

        return $data;
    }

    public function updateProductSummary($id){
        $sql = "DELETE FROM competitors_summary WHERE product_id = $id";
        $query = $this->db->query($sql);

        $date = date('Y-m-d H:i:s');

        $sql = "
            INSERT IGNORE INTO competitors_summary
            SELECT ams.`product_id`, p.name, i.sku, ams.price ams, i.inventory,
                 e.`price` airrattle, e.`inventory` airrattle_inventory,
                 f.`price` airsoftgi, f.`inventory` airsoftgi_inventory,
                 g.`price` evike, g.`inventory` evike_inventory,
                 h.`price` airsplat, h.`inventory` airsplat_inventory,
                 '$date' updated
            FROM `competitors_ams_prices` ams
            LEFT JOIN products p ON ams.`product_id` = p.id
            LEFT JOIN items i ON i.product_id = p.id

            LEFT JOIN `competitors_airrattle_xref` a ON a.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airrattle` e ON e.`product_id` = a.`product_id`

            LEFT JOIN `competitors_airsoftgi_xref` b ON b.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airsoftgi` f ON f.`product_id` = b.`product_id`

            LEFT JOIN `competitors_evike_xref` c ON c.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_evike` g ON g.`product_id` = c.`product_id`

            LEFT JOIN `competitors_airsplat_xref` d ON d.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airsplat` h ON h.`product_id` = d.`product_id`
            WHERE ams.product_id = $id
        ";

        $query = $this->db->query($sql);
    }

    public function updateSummary(){

        $sql = "TRUNCATE TABLE competitors_summary";
        $query = $this->db->query($sql);

        $date = date('Y-m-d H:i:s');

        $sql = "
            INSERT IGNORE INTO competitors_summary
            SELECT ams.`product_id`, p.name, i.sku, ams.price ams, i.inventory,
                 e.`price` airrattle, e.`inventory` airrattle_inventory,
                 f.`price` airsoftgi, f.`inventory` airsoftgi_inventory,
                 g.`price` evike, g.`inventory` evike_inventory,
                 h.`price` airsplat, h.`inventory` airsplat_inventory,
                 '$date' updated
            FROM `competitors_ams_prices` ams
            LEFT JOIN products p ON ams.`product_id` = p.id
            LEFT JOIN items i ON i.product_id = p.id

            LEFT JOIN `competitors_airrattle_xref` a ON a.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airrattle` e ON e.`product_id` = a.`product_id`

            LEFT JOIN `competitors_airsoftgi_xref` b ON b.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airsoftgi` f ON f.`product_id` = b.`product_id`

            LEFT JOIN `competitors_evike_xref` c ON c.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_evike` g ON g.`product_id` = c.`product_id`

            LEFT JOIN `competitors_airsplat_xref` d ON d.`ams_id` = ams.`product_id`
            LEFT JOIN `competitors_airsplat` h ON h.`product_id` = d.`product_id`
            ORDER BY p.id
        ";

        $query = $this->db->query($sql);
    }

    public function getCompetitorInfoForProduct($id){

        $sql = "SELECT ams.*,`airrattle_inventory`, `airsoftgi_inventory`, `airsplat_inventory`, `evike_inventory`,
                z.`timestamp`, e.`pricing_update` airrattle_pricing_update, f.`pricing_update` airsoftgi_pricing_update, g.`pricing_update` evike_pricing_update, h.`pricing_update` airsplat_pricing_update
                FROM competitors_summary ams
                LEFT JOIN competitors_ams_prices z ON z.`product_id` = ams.`product_id`

                LEFT JOIN `competitors_airrattle_xref` a ON a.`ams_id` = ams.`product_id`
                LEFT JOIN `competitors_airrattle` e ON e.`product_id` = a.`product_id`

                LEFT JOIN `competitors_airsoftgi_xref` b ON b.`ams_id` = ams.`product_id`
                LEFT JOIN `competitors_airsoftgi` f ON f.`product_id` = b.`product_id`

                LEFT JOIN `competitors_evike_xref` c ON c.`ams_id` = ams.`product_id`
                LEFT JOIN `competitors_evike` g ON g.`product_id` = c.`product_id`

                LEFT JOIN `competitors_airsplat_xref` d ON d.`ams_id` = ams.`product_id`
                LEFT JOIN `competitors_airsplat` h ON h.`product_id` = d.`product_id`
                WHERE ams.product_id = '$id'
        ";

        $query = $this->db->query($sql);
        return $query->row_array();
    }


}