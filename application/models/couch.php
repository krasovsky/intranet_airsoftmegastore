<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Couch extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function getAllProducts($page=0){
        $page *= 50;
        $sql = "SELECT id as product_id, sku, name, brand_backend_name as brand, manufacturer_backend_name as manufacture, categories, wholesale_price as wholesale, retail_price as retail, img, long_description, title, seo_link_title, seo_meta_keywords, seo_meta_description, CONCAT(id,'-',vanity_url,'.aspx') as URL, height, width, weight, height_unit, width_unit, weight_unit, merchandisable, site_channelAdv, site_AMS, site_AMS_mobile, site_AMS_call, on_sale
         from products LIMIT 10";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getItemsByProductId(){
        $sql = "SELECT id as product_id, sku, name, brand_backend_name as brand, manufacturer_backend_name as manufacture, categories, wholesale_price as wholesale, retail_price as retail, img, long_description, title, seo_link_title, seo_meta_keywords, seo_meta_description, CONCAT(id,'-',vanity_url,'.aspx') as URL, height, width, weight, height_unit, width_unit, weight_unit, merchandisable, site_channelAdv, site_AMS, site_AMS_mobile, site_AMS_call, on_sale
         from products LIMIT 10";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAllProducts2($page=0){
        $page *= 50;
        $sql = "SELECT id as product_id, name, retail_price as retail
         from products";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function insertDocument($product){
        $ch = curl_init();

        $payload = json_encode($product);

        curl_setopt($ch, CURLOPT_URL, 'http://127.0.0.1:5984/ams/'.$product['product_id']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); /* or PUT */
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Accept: */*'
        ));

        //curl_setopt($ch, CURLOPT_USERPWD, 'myDBusername:myDBpass');

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}