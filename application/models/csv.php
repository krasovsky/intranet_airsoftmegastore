<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Csv extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    public function getItems(){

        $sql = "SELECT i.id, i.name, i.price, i.item_url, i.manufacturer, i.quantity,i.image_list,
                    (SELECT GROUP_CONCAT(c.name) FROM categories c WHERE FIND_IN_SET(c.record_id, i.categories)) AS cats
                FROM items i
                LIMIT 20
        ";

        $query = $this->db->query($sql);

        var_dump($query->result());
    }

    public function processAll(){
        $unprocessed = $this->getAllUnprocessedCSVs();
        foreach($unprocessed->result() as $import){
          //  if zip is in the name then it came from od export otherwise it came from retention science
             if(stripos($import->filename, '.zip')=== false){
                //retention science csv is tab delimited
                 $success = $this->processTabCSV($import->filename, $import->name);
				  echo '<h4>Retention Science feed '.$import->name.'</h4>';
             }else{
                 $csv = str_replace('.zip', '.csv', $import->filename);
                 $success = $this->processCSV($csv, $import->name);
				  echo '<h4>Standard Export '.$import->name.'</h4>';
             }
             if($success){
                 $this->markCSVProcessed($import->id);
                 echo '<h4>Processed '.$import->name.'</h4>';
             }
         }
    }

    public function cleanAllProcessed(){
        $processed = $this->getAllprocessedCSVs();
        foreach($processed->result() as $row){
            $this->removeFiles($row->filename);
            $this->markImportRemoved($row->id);
        }
    }

    private function removeFiles($filename){

        echo "<h4>Removing $filename</h4>";

        $zip = APPPATH.'assets/data/zip/'.$filename;
        $csv = APPPATH.'assets/data/csv/'.str_replace('.zip', '.csv', $filename);

        @unlink($zip);
        @unlink($csv);
    }

    private function markImportRemoved($id){
        $update = "UPDATE imports SET removed = 1 WHERE id = $id";
        return $this->db->query($update);
    }
    private function markCSVProcessed($id){
        $update = "UPDATE imports SET processed = 1 WHERE id = $id";
        return $this->db->query($update);
    }

    private function getAllUnprocessedCSVs(){
        $select = "SELECT *
                    FROM imports
                    WHERE unzipped = 1 AND processed = 0";

        return $this->db->query($select);
    }
    private function getAllprocessedCSVs(){
        $select = "SELECT *
                    FROM imports
                    WHERE processed = 1 AND removed = 0";

        return $this->db->query($select);
    }

    private function processTabCSV($file, $tableName){
        $success = false;
        if (($handle = fopen(APPPATH."assets/data/csv/$file" , "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
                if($tableName == 'users'){
                    $success = $this->saveUsers($data);
                }elseif($tableName == 'orders'){
                    $success = $this->saveOrders($data);
                }elseif($tableName == 'order_items'){
                    $success = $this->saveOrderItems($data);
                }
            }

            fclose($handle);
            return $success;
        }
    }

    public function processCSV($file, $tableName){
        $success = false;
        $removeBOM = false;

        if($tableName == 'categories'){
            $this->db->query("DELETE from categories_changes");
        }
        if (($handle = fopen(APPPATH."assets/data/csv/$file" , "r")) !== FALSE) {

            echo "<h4>Saving $tableName</h4>";

            $count = 0;
            while (($data = fgetcsv($handle, 0, ",", "~")) !== FALSE) {
                $count++;
                if(!$removeBOM){
                    $data[0] = preg_replace("/[^0-9]/","",$data[0]);
                    $removeBOM=true;
                }
				
				if($data[0] == '') continue; //if the row is blank then skip

                if($tableName == 'categories'){
                    $success = $this->saveCategories($data);
                }elseif($tableName == 'productreviews'){
                    $success = $this->saveProductReviews($data);
                }elseif($tableName == 'addCategories'){
                    $success = $this->addCategoriesXref($data);
                }elseif($tableName == 'productcategories'){
                    $success = $this->saveProductCategories($data);
                }elseif($tableName == 'productspecs'){
                    $success = $this->saveProductSpecs($data);
                }elseif($tableName == 'productpricings'){
                    $success = $this->saveProductPricings($data);
                }elseif($tableName == 'productlocations'){
                    $success = $this->saveProductLocations($data);
                }elseif($tableName == 'items'){
                    $success = $this->saveItems($data);
                }elseif($tableName == 'relatedproducts'){
                    $success = $this->saveRelatedProducts($data);
                }elseif($tableName == 'productbundles'){
                    $success = $this->saveProductBundles($data);
                }elseif($tableName == 'itemcategories'){
                    $success = $this->saveItemCategories($data);
                }elseif($tableName == 'itemcategoryitems'){
                    $success = $this->saveItemCategoryItems($data);
                }elseif($tableName == 'brands'){
                    $success = $this->saveBrands($data);
                }elseif($tableName == 'users'){
                    $success = $this->saveUsers($data);
                }elseif($tableName == 'orders'){
                    $success = $this->saveOrders($data);
                }elseif($tableName == 'orderItems'){
                    $success = $this->saveOrderItems($data);
                }elseif($tableName == 'products'){
                    $success = $this->saveProducts($data);
                }elseif($tableName == 'gunbuilder'){
                    $success = $this->saveGunbuilder($data);
                }elseif($tableName == 'customizer'){
                    $success = $this->saveCustomizer($data);
                }

                
            }
            $this->setAllCategoriesXref();
            echo "<p>saved $count records</p>";

            fclose($handle);
            return $success;
        }
    }

    public function isDifferent($source , $target){
        if($source["parent_id"] != $target["parent_id"]) return true;

        if($source["name"] != $target["name"]) return true;

        if($source["backend_name"] != $target["backend_name"]) return true;

        if($source["title"] != $target["title"]) return true;

        if($source["long_desc"] != $target["long_desc"]) return true;

        if($source["short_desc"] != $target["short_desc"]) return true;

        if($source["meta_keywords"] != $target["meta_keywords"]) return true;

        if($source["meta_description"] != $target["meta_description"]) return true;

        if($source["vanity_url"] != $target["vanity_url"]) return true;

        return false;
    }

    private function saveCategories($row){

        $table = 'categories';
        $keys = array('id','parent_id','name','backend_name','title','img','long_desc','short_desc','meta_keywords','meta_description','vanity_url','active','custom_text_1','custom_text_2','custom_text_3');
        $hash = array_combine($keys, $row);
        //dump($hash);
        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        //$update = (count($update->result()))? true: false;

        $rez = $update->result_array()[0];
        $update = (empty($rez)) ? false : true;

        $changeSql = "INSERT INTO categories_changes (id,status,change_date) values (?,?,?)";
        if($update){
            $sql = "UPDATE $table SET ";

            if($this->isDifferent($hash,$rez)) {
                //dump($hash);
                //dump($rez);
                $this->db->query($changeSql, array($hash["id"], 1 , date("Y-m-d")));
            }
        }else{
            $sql = "INSERT INTO $table SET ";
            
            $this->db->query($changeSql, array($hash["id"], 2 , date("Y-m-d")));
        }

        foreach($keys as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $val . '` = ' . (int)$row[$key];
            }elseif($key == 'active'){
                $sql .= ', `' . $val . '` = ' . ((strtolower($row[$key]) == 'true')? '1' : '0');
            }else{
                $sql .= ", `" . $val . "` = '" . addslashes($row[$key]) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }
        return $this->db->query($sql);
    }

    public function setAllCategoriesXref(){

		$sql = "TRUNCATE categories_xref";
        $query = $this->db->query($sql);

		//select all products from products
        $sql = "SELECT id, categories FROM products";
        $query = $this->db->query($sql);

        foreach($query->result() as $row){
            $this->saveCategoriesXref($row->id, explode(',', $row->categories));
            echo $row->id . ', ';
        }
    }
    private function addCategoriesXref($row){

        $product_id = $row[0];
        $category = $row[1];

        $sql = "SELECT category_id FROM categories_xref WHERE product_id = $product_id";
        $query = $this->db->query($sql);

        $add = array();

        foreach($query->result() as $row){
            $add[] = $row->category_id;
        }

        $add[] = $category;

        return $this->saveCategoriesXref($product_id, $add);

    }
    private function saveCategoriesXref($product_id, $categories){

        if(!count($categories))return false;

        $table = 'categories_xref';
        //remove keys for product
       // $sql = "DELETE FROM $table WHERE product_id = $product_id";
        //$this->db->query($sql);

        //re-insert keys for product
		$row = array();
        foreach($categories as $category_id){
            if($category_id){
                $row[] = "($category_id,$product_id)";
            }
        }

		$values = implode(',',$row);
		if($values){
			$sql = "INSERT INTO $table (`category_id`,`product_id`) VALUES $values";
			echo $sql . '<br/>';
			$this->db->query($sql);
		}
    }
    private function saveProductCategories($row){
        $table = 'productcategories';
        $keys = array('id','category_id');

        $splitCategories = explode(',', $row[1]);

        foreach($splitCategories as $cat){

            $hash = array_combine($keys, $row);

            $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
            $update = (count($update->result()))? true: false;

            if($update){
                $sql = "UPDATE $table SET ";
            }else{
                $sql = "INSERT INTO $table SET ";
            }

            foreach($keys as $index=>$key){
                if($index === 0){
                    $sql .= '`' . $key . '` = ' . (int)$row[$index];
                }elseif($index === 1){
                    $sql .= ', `' . $key . '` = ' . (int)$row[$index];
                }elseif($index === 11){
                    $sql .= ', `' . $key . '` = ' . ((strtolower($row[$index]) == 'true')? '1' : '0');
                }else{
                    $sql .= ", `" . $key . "` = '" . addslashes($row[$index]) ."'";
                }
            }

            if($update){
                $sql .= ' WHERE id = '. $hash['id'];
            }

            $final = $this->db->query($sql);
        }

        return $final;
    }

    private function saveProducts($row){
		var_dump($row);
	
        $table = 'products';
        $keys = array('id','sku','warehouse_id','name','backend_name','brand_backend_name','manufacturer_backend_name','categories','wholesale_price','retail_price','dropship','img','long_description','title','seo_link_title','seo_meta_keywords','seo_meta_description','vanity_url','tax_code','height','width','weight','height_unit','width_unit','weight_unit','merchandisable','site_channelAdv','site_AMS','site_AMS_mobile','site_AMS_call','on_sale','depth');
        $isBoolean = array('merchandisable','site_channelAdv','site_AMS','site_AMS_mobile','site_AMS_call');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'categories') $categories = explode(',',$val);
            if($key == 'id'){
                $product_id = $val;
                $sql .= '`' . $key . '` = ' . (int)$val;
            }elseif(in_array($key, $isBoolean)){
                $sql .= ',`' . $key . '` = ' . ((strtolower($val) == 'true')? '1' : '0');
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        //$this->saveCategoriesXref($product_id, $categories);
        return $this->db->query($sql);
    }

	 private function saveGunbuilder($row){
		var_dump($row);

        $table = 'gunbuilder';
        $keys = array('id','name','brand','features','length','capacity','velocity','weight','incompatible','masterPosition','position', 'requires', 'hidden','scale','new');
        $isBoolean = array('hidden');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $product_id = $val;
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        //$this->saveCategoriesXref($product_id, $categories);
        return $this->db->query($sql);
    }

	private function saveCustomizer($row){
		var_dump($row);

        $table = 'customizer';
        $keys = array('id','name','brand','features','length','capacity','velocity','weight','incompatible','masterPosition','position', 'requires', 'hidden','scale','new');
        $isBoolean = array('hidden');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $product_id = $val;
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        //$this->saveCategoriesXref($product_id, $categories);
        return $this->db->query($sql);
    }

    private function saveProductBundles($row){
        $table = 'productbundles';
        $keys = array('product_id','id','quantity_break','quantity_break_product');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE product_id = ". $hash['product_id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'product_id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }elseif($key == 'id'){
                $sql .= ',`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE product_id = '. $hash['product_id'];
        }

        return $this->db->query($sql);
    }

    private function saveProductLocations($row){
        $table = 'productlocations';
        $keys = array('id','aisle_overstock','pick_location','warehouse_bin','warehouse_custom');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        return $this->db->query($sql);
    }

    private function saveProductPricings($row){
        $table = 'productpricings';
        $keys = array('id','price','product_skus','quantity','auto_ship_price','free_product_ids','free_product_skus','free_product_count','recursive');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id'] .' AND quantity = ' . $hash['quantity']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }elseif($key == 'recursive'){
                $sql .= ', `' . $key . '` = ' . ((strtolower($hash['recursive']) == 'true')? '1' : '0');
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'] .' AND quantity = ' . $hash['quantity'];
        }

        return $this->db->query($sql);
    }
    private function saveProductReviews($row){
        $table = 'productreviews';
        $keys = array('id','product_id','customer_id','customer_email','display_name','title','rating','review','post_date','helpful_count','not_helpful_count','culture','site_id','status');
        $hash = array_combine($keys, $row);

        if($hash['id']){

            $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
            $update = (count($update->result()))? true: false;

            if($update){
                $sql = "UPDATE $table SET ";
            }else{
                $sql = "INSERT INTO $table SET ";
            }

            foreach($hash as $key=>$val){
                if($key == 'id'){
                    $sql .= '`' . $key . '` = ' . (int)$val;
                }elseif($key=='post_date'){
                    $dtime = new DateTime($val);
                    $hash[$key] = $dtime->format("Y-m-d H:i:s");
                }else{
                    $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
                }
            }

            if($update){
                $sql .= ' WHERE id = '. $hash['id'];
            }

            return $this->db->query($sql);
        }else{
            echo 'problem';
        }
    }

    private function saveProductSpecs($row){
        $table = 'productspecs';
        $keys = array('product_id','battery_type','build_material','color','gearbox','inner_barrel','magazine_capacity','material','muzzle_velocity','length','plate_size','propellant','weight','shell_sku',"model");
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE product_id = ". $hash['product_id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'product_id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE product_id = '. $hash['product_id'];
        }

        return $this->db->query($sql);
    }

    private function saveBrands($row){
        $table = 'brands';
        $keys = array('id','img','name','long_description','short_description','title','link_title','seo_keywords','seo_description','vanity_url','active');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        return $this->db->query($sql);
    }

    private function saveRelatedProducts($row){
        $table = 'relatedproducts';
        $keys = array('id','bound_site_ids','bount_site_name','name','description','product_ids','product_skus','related_product_list_ids','related_product_list_skus');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        return $this->db->query($sql);
    }

    private function saveItems($row){
        $table = 'items';

        $keys = array('product_id','id','addto_price','addto_weight','addto_shipping','allocated_inventory','asin','backorder_limit','backorderable','inventory','disable_inventory','isbn','item_category','item_name','min_purchase','max_purchase','MPN','outofstock_behavior','outofstock_threshold','preorder_date','preorder_limit','preorderable','reorder_threshold','send_inventory_reorder','sku','target_quantity','unit_of_measure','UPC','warehouse_location');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $curDate = date("Y-m-d");
            $sql = "UPDATE $table SET last_update_date = '{$curDate}', processed = 0,";
        }else{
            $sql = "INSERT INTO $table SET processed = 0,";
        }

        foreach($hash as $key=>$val){
            if($key == 'product_id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }elseif($key=='preorder_date'){
                $dtime = new DateTime($val);
                $hash[$key] = $dtime->format("Y-m-d H:i:s");
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        return $this->db->query($sql);
    }

    private function saveItemCategories($row){
        $table = 'itemcategories';
        $keys = array('id','product_id','category_backend_name','category_name');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE `id` = '. $hash['id'];
        }

        return $this->db->query($sql);
    }

    private function saveItemCategoryItems($row){
        $table = 'itemcategoryitems';
        $keys = array('id','item','name');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }

        return $this->db->query($sql);
    }
    private function saveUsers($row){
        $table = 'users';
        $keys = array('email','full_name','address1','address2','city','state','zip','country','phone','birth_year','gender','ip_address','number_logons','account_created_on','last_logon_at','registration_source','EmailOpt');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE email = '". addslashes($hash['email'])."'");
        $update = (count($update->result()))? true: false;

        if($hash['account_created_on']){
            $hash['account_created_on_date'] = $this->zulu2mysql($hash['account_created_on']);
        }
        if($hash['last_logon_at']){
            $hash['last_logon_at_datetime'] = $this->zulu2mysql($hash['last_logon_at']);
        }

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT IGNORE INTO $table SET ";
        }

        foreach($hash as $key=>$val){

            if($key == 'email'){
                $sql .= '`' . $key . "` = '" . addslashes($val) ."'";
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE email = \''. addslashes($hash['email'])."'";
        }
		
		var_dump($sql);
        return $this->db->query($sql);
    }

    private function zulu2mysql($str){
        if($str == '\N') return null;

        $date = str_ireplace('Z', '', $str);
        $dtime = new DateTime($date);
        return $dtime->format("Y-m-d H:i:s");
    }

    private function saveOrders($row){
        $table = 'orders';
        $keys = array('id','email','total_price','ordered_at','discount_amount','shipping_amount','tax_amount','payment_method');
        $hash = array_combine($keys, $row);

        if($hash['ordered_at']){
            $hash['ordered_at_datetime'] = $this->zulu2mysql($hash['ordered_at']);
        }

        $update = $this->db->query("SELECT * FROM $table WHERE id = ". $hash['id']);
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){

            if($key == 'id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE id = '. $hash['id'];
        }
		
        return $this->db->query($sql);
    }

    private function saveOrderItems($row){
        $table = 'order_items';
        $keys = array('order_record_id','item_record_id','name','quantity','price');
        $hash = array_combine($keys, $row);

        $update = $this->db->query("SELECT * FROM $table WHERE order_record_id = ". $hash['order_record_id'].' AND `item_record_id` = '. $hash['item_record_id'] );
        $update = (count($update->result()))? true: false;

        if($update){
            $sql = "UPDATE $table SET ";
        }else{
            $sql = "INSERT INTO $table SET ";
        }

        foreach($hash as $key=>$val){
            if($key == 'order_record_id'){
                $sql .= '`' . $key . '` = ' . (int)$val;
            }else{
                $sql .= ", `" . $key . "` = '" . addslashes($val) ."'";
            }
        }

        if($update){
            $sql .= ' WHERE `order_record_id` = '. $hash['order_record_id'].' AND `item_record_id` = '. $hash['item_record_id'];
        }
		
        return $this->db->query($sql);
    }
}