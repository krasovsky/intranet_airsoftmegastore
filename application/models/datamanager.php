    <?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Datamanager extends CI_Model {
    var $orders = '';
    function __construct()
    {
        parent::__construct();
        $this->load->helper("ups");

    }

    private $shipping_rates = array(
        1 => 7.31 ,
        3 => 10.17 ,
        6 => 14.46 ,
        9 => 18.75 ,
        13 => 24.47 ,
        17 => 28.19 ,
        21 => 33.91 ,
        32 => 42.20 ,
        47 => 59.49 ,
        72 => 70.78 ,
        95 => 86.21 ,
        112 => 108.50 ,
        200 => 167.79 ,
        300 => 212.22 ,
        400 => 256.65 ,
        600 => 401.08 ,
        1000 => 645.51
    );

    private function groupOrders($order_items){
        $orders = array();
        foreach($order_items as $i){
            $orders[$i->order_id][] = $i;
        }
        return $orders;
    }

    private function getShippingRate($weight){
        $final_price = 0.00;
        $last_weight = 0;
        foreach($this->shipping_rates as $pounds => $price){
            if($weight<=$pounds && $weight>$last_weight){
                $last_weight = $weight;
                $final_price = $price;
            }
        }
        return $final_price;
    }

    private function calculateShippingCostOnItems($item){
        //if they paid for premium shipping then shipping price was zero
        foreach($item as &$o){
            if( $o->shipping_amount > 0 ){
                $o->shipping = 0.00;
            }else{
                $o->shipping = $this->getShippingRate( $o->weight*$o->quantity );
            }
        }
        return $item;
    }

    private function calculateShippingCost($orders, $upsInfo){
        /**
         * if we have ups_billing info - use it
         * otherwise check db for api results
         * otherwise create api request (slow)
         */

        $apiRequestArr = array();
        $usersArr = array();
        foreach($orders as $key => &$o){
            if(! empty($upsInfo[$key])){
                $o["shipping_price"] = $upsInfo[$key]["amount"];
                $o["ship"] = "actual";
            }
            else{
                //dump($o);
                $apiRequestArr[$key] = $o;
            }

            $o["total_weight"] = 0;
            foreach($o as $key => $element){
                if(is_numeric($key)){
                    $o["total_weight"] += $element->weight * $element->quantity;
                    @$o["total_returned"] += $element->returned;
                    @$o["total_items"] += $element->quantity;
                }
            }
        }

        //check what we have saved in db
        if(! empty($apiRequestArr)){
            $ordersArr = array();
            foreach($apiRequestArr as $key => &$element){
                $ordersArr[]  = $key;
            }
            $orderStr = implode(',',$ordersArr);
            $queryStr = "SELECT * from ups_api_results where order_id in ({$orderStr})";
            //dump($queryStr);
            $rez = $this->db->query($queryStr);
            $dateBaseSavedArr = regroup_array($rez->result_array(),"order_id");

            //merge with result array
            foreach($orders as $key => &$o){
                //dump($o);
                if(empty($o["shipping_price"])){
                    if(! empty($dateBaseSavedArr[$key])){
                        $o["shipping_price"] = $dateBaseSavedArr[$key]["price"];
                        $o["ship"] = "ups_api";
                        $o["total_weight"] = $dateBaseSavedArr[$key]["total_weight"];
                        $o["zip"] = $dateBaseSavedArr[$key]["zip_where"];
                    }
                    else{
                        //use od data
                        $o["shipping_price"] = $o[0]->ship_charged;
                        $o["ship"] = "od";
                        $o["zip"] = 9999999;
                        //dump($o);
                        // $orderWeight = 0;
                        // foreach($o as $key => $product){
                        //     if(is_numeric($key)){
                        //         $orderWeight += $product->weight * $product->quantity;
                        //     }                          
                        // }
                        // $options = array(
                        //     UpsConfig::OPTION_RECIEVER_ADDRESS_VALUE => $o[0]->ship_to_address,
                        //     UpsConfig::OPTION_PACKAGE_WEIGHT_VALUE => $orderWeight,
                        //     );
                        // $rate = calculate_ups_rate($options);
                        //dump($rate);
                    }
                }

                // if($o["total_items"] == $o["total_returned"]){
                //     $o["shipping_price"] = 0;
                // }
            }
            //die();
            
        }
        //api call
        //dump($usersArr);
//         if(! empty($usersArr)){
//             $queryStr = "SELECT email, zip from users where email in ?";
//             $rez = $this->db->query($queryStr,array(array_values($usersArr)));
//             $usersInfoArr = regroup_array($rez->result_array(),"email");

//             foreach($orders as $key => &$o){
//                 if(is_numeric($key) && empty($o["shipping_price"])){

//                     $orderWeight = 0;
//                         foreach($o as $key => $product){
//                             if(is_numeric($key)){
//                                 $orderWeight += $product->weight * $product->quantity;
//                             }                          
//                         }

//                     if(! empty($usersInfoArr[$o[0]->email])){

//                         $options = array(
//                             UpsConfig::OPTION_RECIEVER_ZIPCODE_VALUE => $usersInfoArr[$o[0]->email]["zip"],
//                             UpsConfig::OPTION_PACKAGE_WEIGHT_VALUE => $orderWeight,
//                             );
//                         $rate = calculate_ups_rate($options);
//                         //dump($rate);

//                         if($rate !== false){
//                             $o["shipping_price"] = $rate;
//                             $o["ship"] = "API Result";
//                             $o["zip"] = $usersInfoArr[$o[0]->email]["zip"];
                            
//                         }
//                         else{
//                             $o["shipping_price"] = $o[0]->ship_charged;
//                             $o["ship"] = "OD Data";
//                             $o["zip"] = 9999999;
//                         }
//                     }
//                     else{
//                         //var_dump("user_email_not_found");
//                         $o["shipping_price"] = $o[0]->ship_charged;
//                         $o["ship"] = "OD Data";
//                         $o["zip"] = 9999999;
//                         //$doSave = false;
//                     }
//                     $o["total_weight"] = $orderWeight;
//                     $queryStr = <<<HEREDOC
//                                 INSERT INTO ups_api_results (order_id,total_weight,zip_where,create_date,update_date,price)
//                                 values (?,?,?,?,?,?)
//                                 on DUPLICATE key update update_date = ? 
// HEREDOC;
//                     $currentTime = date('Y-m-d');
//                         //if(empty($doSave) && $doSave !== false){
//                     $this->db->query($queryStr,array($o[0]->order_id,$o["total_weight"],$o["zip"],$currentTime,$currentTime,$o["shipping_price"],$currentTime));
//                         //}                  
//                 }
//             }
//         }
        return $orders;
    }

    private function calculateShippingWeight($orders){
        $calculated_orders = array();

        foreach($orders as $id => $order){
            $calculated_orders[$id]['total_weight'] = $order["total_weight"];
            $calculated_orders[$id]['total_revenue']  = 0.0;

            foreach($order as $key => $item){
                if(is_numeric($key)){
                    $calculated_orders[$id]['total_revenue']  += $item->revenue;
                }
                
            }
            //$calculated_orders[$id]['total_revenue'] += $order[0]->ship_charged;
            $calculated_orders[$id]['items'] = $order;
        }
        return $calculated_orders;
    }

    private function calculateProfit($orders){
        foreach($orders as &$o){
            $o['profit'] = $o['total_revenue']-$o["items"]['shipping_price'] + $o["items"][0]->ship_charged;
        }
        return $orders;
    }

    private function calculateFreebies($orders){
        foreach($orders as &$o){
            //dump($o);
            foreach($o['items'] as $key => $i){
                if(is_numeric($key)){
                    if($i->total == 0){
                        //if no price then it is a freebie
                        if($i->ship_charged == 0 ){
                            //if shipping was not paid then we paid it
                            if($o['total_weight']==0) $o['total_weight'] = 1;
                            $o['freebie_shipping'] = $o['items']["shipping_price"]/$o['total_weight']*$i->weight;
                        }else{
                            //if they paid for it it shouldn't be in the equation
                            $o['freebie_shipping'] = 0;
                        }
                        $o['freebie_cost'] = $i->revenue - $o['freebie_shipping'] ;
                    }
                }
            }
        }
        return $orders;
    }



    private function calculateDiscountedProfit($orders){
        foreach($orders as &$o){
            $discounts = $o['items'][0]->discount_amount;
            $o['profit'] = ( $o['total_revenue']- $discounts - $o['shipping_price'] ) * 1.25;
        }
        return $orders;
    }


    private function calculateSkus($orders,$sku){
        $skus = array();
        foreach($orders as $key => &$o){
            //loop through the items
            foreach($o['items'] as $key1 => $i){
            //find the sku that matches sku in question
                if(is_numeric($key1) && $i->sku == $sku){
                    $skus[$key] = $i;
                    $skus[$key]->shipping = $o["items"]['shipping_price'];
                    $skus[$key]->profit = $i->revenue;
                }
            }
        }

        return $skus;
    }


    public function getOrdersWithProfit($start,$end,$options = array()){
        $no_profit = array();
        $this->orders = $this->orders ?: $this->getOrders($start,$end,$options);

        foreach($this->orders as $o){
            if( $o['profit'] >= 0 ) array_push( $no_profit, $o );
        }
        return $no_profit;
    }

    public function getOrdersWithoutProfit($start,$end,$options = array()){
        $no_profit = array();
        $this->orders = $this->orders ?: $this->getOrders($start,$end,$options);

        foreach($this->orders as $o){
            if( $o['profit'] < 0 ) array_push( $no_profit, $o );
        }
        return $no_profit;
    }

    public function getDiscountedOrdersWithProfit($start,$end){
        $no_profit = array();
        $this->orders = $this->orders ?: $this->getDiscountedOrders($start,$end);

        foreach($this->orders  as $o){
            if( $o['profit'] >= 0 ) array_push( $no_profit, $o );
        }
        return $no_profit;
    }

    public function getDiscountedOrdersWithoutProfit($start,$end){
        $no_profit = array();
        $this->orders = $this->orders ?: $this->getDiscountedOrders($start,$end);

        foreach($this->orders  as $o){
            if( $o['profit'] < 0 ) array_push( $no_profit, $o );
        }
        return $no_profit;
    }

    public function getUpsInfo($orderArr){
        $inArr = array();
        foreach($orderArr as $key => $element){
            $inArr[] = $key;
        }
        $inStr = implode(",",$inArr);
        if(empty($inStr)) return array();
        $upsQuery = <<<HEREDOC
            SELECT SUM( b.net_amount ) AS amount, b.order_id
                    FROM (SELECT DISTINCT a.invoice_number, a.`invoice_date`, a.`net_amount`, a.`order_id`,a.`published_charge`,a.`incentives` FROM ups_billing_report a 
                        WHERE a.order_id in ($inStr)) b
                    GROUP BY b.order_id
HEREDOC;
        $rez = $this->db->query($upsQuery);
        $upsInfo = $rez->result_array();
        $returnArr = array();
        foreach($upsInfo as $element){
            $returnArr[$element["order_id"]] = $element;
        }

        return $returnArr;
    }

    public function getOrders($start,$end,$options = array()){
        //dump($options);
        if (! empty($options)){
            if(!empty($options["id"])){
                $whereStr = "o.id = {$options["id"]}";
            }
            else if(!empty($options["sku"])){
                $whereStr = "AND oi.sku = '{$options["sku"]}'";
            }
            else if(!empty($options["email"])){
                $whereStr = "o.email = '{$options["email"]}' AND o.ordered_at_datetime > '{$start}'  AND o.ordered_at_datetime < '{$end}'";
            }
            else if(! empty($options["filter"])){
                $whereStr = "AND o.discount like '{$options["filter"]}'"; 
            }
        }
        else{
            $whereStr = "";
        }


        $sql = "SELECT 
                  o.order_number order_id,
                  o.last_update_date as ordered_at_datetime,
                  oi.name,
                  oi.sku,
                  oi.ordered as quantity,
                  oi.returned,
                  oi.unit_price,
                  oi.unit_disc,
                  oi.tax,
                  oi.total,
                  o.customer_id,
                  o.shipping AS ship_charged,
                  o.discount AS coupon,
                  (oi.total - p.`wholesale_price` * oi.ordered - oi.unit_price * oi.returned + p.`wholesale_price` * oi.returned + if(oi.returned > 0, oi.unit_disc *oi.returned, 0))  AS revenue,
                  p.wholesale_price,
                  p.`weight`,
                  p.height,
                  p.width,
                  o.ship_via,
                  o.ship_to_address,
                  oor.site
                FROM
                od_orders o
                INNER JOIN od_order_items oi ON oi.order_number = o.order_number and oi.ordered <> 0
                INNER JOIN items i on i.id = oi.item_id
                INNER JOIN products p ON p.id = i.product_id and p.merchandisable = 1
                INNER JOIN od_order_reports oor ON oor.`order_number` = o.`order_number`
                    WHERE o.last_update_date > '{$start}' 
                  AND o.last_update_date < '{$end}' and o.is_shipped = 1 {$whereStr}
                ORDER BY o.last_update_date DESC ";

        $query = $this->db->query($sql);

        $grouped = $this->groupOrders($query->result());
        //$weight_calculated  = $this->calculateShippingWeight($grouped);
      
        $upsInfo = $this->getUpsInfo($grouped);
        $shipping_calculated  = $this->calculateShippingCost($grouped,$upsInfo);
        $weight_calculated  = $this->calculateShippingWeight($shipping_calculated);
        $revenue_calculated  = $this->calculateProfit($weight_calculated);
        $freebies_calculated = $this->calculateFreebies($revenue_calculated);
        return $freebies_calculated;
    }

    public function getDiscountedOrders($start,$end, $id = null){
        if ($id !== null){
            $whereStr = "o.id = {$id}";
        }
        else{
            $whereStr = "o.ordered_at_datetime > '{$start}'  AND o.ordered_at_datetime < '{$end}'";
        }
        $sql = "SELECT o.id order_id, o.`ordered_at_datetime`, oi.name, i.sku, oi.quantity, oi.`price`, (oi.price-p.`wholesale_price`)*oi.quantity AS revenue, p.`weight`, 
        o.discount_amount, o.shipping_amount
                FROM order_items oi
                JOIN orders o ON oi.order_record_id = o.`id`
                JOIN items i ON i.id = oi.`item_record_id`
                JOIN products p ON i.product_id = p.id
                WHERE
                    o.ordered_at_datetime > '$start'  AND
                    o.ordered_at_datetime < '$end'
                ORDER BY ordered_at_datetime DESC";

        $query = $this->db->query($sql);

        $grouped = $this->groupOrders($query->result());
        $weight_calculated  = $this->calculateShippingWeight($grouped);
        $shipping_calculated  = $this->calculateShippingCost($weight_calculated);
        $revenue_calculated  = $this->calculateDiscountedProfit($shipping_calculated);
        return $revenue_calculated;
    }

    public function getOrdersBySku($sku,$start,$end){

//         $sql = "SELECT o.id order_id, o.email, o.`ordered_at_datetime`, oi.name, i.sku, oi.quantity, oi.`price`, p.wholesale_price,(oi.price-p.`wholesale_price`)*oi.quantity AS revenue, 
//         p.`weight`, o.discount_amount, o.shipping_amount
//                 FROM order_items oi
//                 JOIN orders o ON oi.order_record_id = o.`id`
//                 JOIN items i ON i.id = oi.`item_record_id`
//                 JOIN products p ON i.product_id = p.id
//                 WHERE
//                     o.ordered_at_datetime > '$start'  AND
//                     o.ordered_at_datetime < '$end'
//                     AND o.id IN (
//                         SELECT o.id
//                         FROM order_items oi
//                         JOIN orders o ON oi.order_record_id = o.`id`
//                         JOIN items i ON i.id = oi.`item_record_id`
//                         WHERE i.sku = '$sku'
//                     )
//                 ORDER BY ordered_at_datetime";


// //
//         $query = $this->db->query($sql);
// //        $items = $query->result();
// //        $items = $this->calculateShippingCostOnItems($items);

//         $grouped = $this->groupOrders($query->result());
//         $weight_calculated  = $this->calculateShippingWeight($grouped);
//         $shipping_calculated  = $this->calculateShippingCost($weight_calculated);
//         $revenue_calculated  = $this->calculateProfit($shipping_calculated);
        $revenue_calculated = $this->getOrders($start,$end,["sku" => $sku]);
        $skus = $this->calculateSkus($revenue_calculated,$sku);
//        foreach($items as &$i){
//            $i->profit = $i->revenue - $i->shipping - $i->discount_amount;
//        }

        return $skus;
    }

    public function getOrdersByOrderId($id){
        $revenue_calculated = $this->getOrders(1,1,["id" => $id]);
        return $revenue_calculated;
    }

    public function getTop20($start,$end){
        $queryStr = <<<HEREDOC
            SELECT  oi.sku, oi.item_id, SUM(oi.total - p.`wholesale_price` * oi.ordered - oi.unit_price * oi.returned + p.`wholesale_price` * oi.returned + IF(oi.returned > 0, oi.unit_disc *oi.returned, 0)) AS money_amount ,SUM(oi.ordered - oi.returned) AS units_sold 
            FROM od_orders o 
            INNER JOIN od_order_items= oi ON oi.order_number = o.order_number
            INNER JOIN items i ON i.id = oi.item_id
            INNER JOIN products p ON p.id = i.product_id AND p.merchandisable = 1
            WHERE o.last_update_date BETWEEN ? AND ? and o.is_shipped = 1
            GROUP BY oi.sku
            ORDER BY money_amount DESC
            LIMIT 100
HEREDOC;

        $rez = $this->db->query($queryStr,array($start,$end));

        $result = $rez->result_array();
        return $result;
    }


    public function getCustomerOrders($email, $start, $end){
        // $sql = "SELECT o.id order_id, o.`ordered_at_datetime`, oi.name, i.sku, oi.quantity, oi.`price`, (oi.price-p.`wholesale_price`)*oi.quantity AS revenue, p.`weight`, o.discount_amount, o.shipping_amount
        //         FROM order_items oi
        //         JOIN orders o ON o.id = oi.`order_record_id`
        //         JOIN items i ON oi.item_record_id = i.id
        //         JOIN products p ON p.id = i.product_id
        //         WHERE o.email = '$email' AND
        //             o.ordered_at_datetime > '$start'  AND
        //             o.ordered_at_datetime < '$end'";
        // $query = $this->db->query($sql);
        // $grouped = $this->groupOrders($query->result());
        // $weight_calculated  = $this->calculateShippingWeight($grouped);
        // $shipping_calculated  = $this->calculateShippingCost($weight_calculated);
        // $revenue_calculated  = $this->calculateProfit($shipping_calculated);
        $revenue_calculated = $this->getOrders($start,$end,["email" => $email]);
        return $revenue_calculated;
    }


    public function getAllSkusOrdered($start, $end){
        $sql = "SELECT i.sku FROM order_items oi
                JOIN orders o ON o.id = oi.`order_record_id`
                JOIN items i ON oi.item_record_id = i.id
                LEFT JOIN shipping_by_sku s ON s.sku = i.sku
                WHERE
                    s.sku IS NULL OR s.count < 10 AND
                    o.ordered_at_datetime > '$start'  AND
                    o.ordered_at_datetime < '$end'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function setSkuShipping($sku,$price=0.0,$quantity=0){
        $sql = "INSERT INTO shipping_by_sku VALUES( '$sku', $price, $quantity ) ON DUPLICATE KEY UPDATE shipping = $price AND count = $quantity";
        return $this->db->query($sql);
    }

    public function getCouponInfo($couponId){
        $rez = $this->db->query("SELECT * from od_coupons where id = ?", array($couponId));
        $result = $rez->result_array();
        return $result[0];
    }

    public function calculateHightSeasonRunRate($year){
        $queryStr = "SELECT oi.sku,SUM(oi.ordered - oi.returned) AS high_season_sale
            FROM od_orders o
            INNER JOIN od_order_items oi ON oi.order_number = o.order_number
            INNER JOIN items i ON i.id = oi.item_id
            WHERE o.last_update_date BETWEEN ? AND ? AND is_shipped = 1 AND oi.sku IS NOT NULL
            GROUP BY oi.sku,i.id,i.product_id";

        $startDate = "{$year}-10-01 00:00:00";
        $year++;
        $endDate = "{$year}-01-01 00:00:00";

        $rez = $this->db->query($queryStr,array($startDate,$endDate));
        $result = $rez->result_array();
        return regroup_array($result,"sku");
    }


    public function generateMasterReport(){

        $queryStr = "SELECT 
          opr.*,(i.inventory - i.allocated_inventory) AS inventory, i.mpn
        FROM
          od_product_ratings  opr
          INNER JOIN 
            (SELECT 
              MAX(op.id) AS id 
            FROM
              od_product_ratings op
              WHERE 365_days_sales IS NOT NULL
            GROUP BY op.sku) a ON a.id = opr.id
          INNER JOIN items i ON i.id = opr.`item_id`
        ";

        $rez = $this->db->query($queryStr);
        $result = $rez->result_array();
        $skuArr = regroup_array($result,"sku");
        $highSeasonArr = $this->calculateHightSeasonRunRate("2015");
        $resultArr = array_merge_recursive($skuArr,$highSeasonArr);
        return $resultArr;
    }

    public function saveCupon($dataArr){
        $update = true;
        if(empty($dataArr["id"])) {
            $queryStr = "INSERT INTO od_coupons (promo_name,promdo_product_text,promo_card_text,promo_code,discount_value,description,product_list,start_date,end_date,active)
                values (?,?,?,?,?,?,?,?)";

            $rez = $this->db->query("SELECT LAST_INSERT_ID() as last_id from od_coupons ");
            $result = $rez->row();

            $value = $result->last_id;
            if(empty($value)) $value = 0;

            $dataArr["id"] = $value + 1;

        }
        else {
            $queryStr = "UPDATE od_coupons set promo_name = ?, promo_product_text = ? , promo_card_text = ? , promo_code = ? , discount_value = ?,description = ?, product_list =?, start_date = ? , end_date = ?, active = ? where id = {$dataArr["id"]}";

            //delete xref
            $this->db->query("DELETE from od_coupons_xref where coupon_id = ?", array($dataArr["id"]));
        }

        $this->db->query($queryStr,array(
                $dataArr["promo_name"],
                $dataArr["promo_product_text"],
                $dataArr["promo_card_text"],
                strtoupper($dataArr["promo_code"]),
                $dataArr["discount_value"],
                $dataArr["description"],
                implode(',',$dataArr["product_list"]),
                date("Y-m-d H:i:s", strtotime($dataArr["start_date"])),
                date("Y-m-d H:i:s", strtotime($dataArr["end_date"])),
                $dataArr["active"]
            ));


        //create od_coupons_xref
        foreach($dataArr["product_list"] as $product){
            $this->db->query("INSERT IGNORE INTO od_coupons_xref values (?,?)" , array($dataArr["id"], $product));
        }
        
    }

    public function getAllCoupons(){
        $rez = $this->db->query("SELECT * from od_coupons order by start_date desc");
        $result = $rez->result_array();
        return $result;
    }
}