<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class DownloadManager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();

        $config = array();
        $config['hostname'] = 'ftp.airsoftmegastore.net';
        $config['username'] = 'vladk';
        $config['password'] = 'eb49zvgtpc';
        $config['port']     = 21;
        $config['passive']  = TRUE;
        $config['debug']    = FALSE;

        $this->config = $config;
    }

    public function download(){
        $sql = "SELECT *
                FROM imports
                WHERE `downloaded` = 0
                LIMIT 10";

        $query = $this->db->query($sql);

        foreach($query->result() as $row){
            if($this->downloadZip($row->url, $row->filename)){
                echo "<h4>Downloaded " . $row->filename . '</h4>';
                $this->setDownloaded($row->id);
            }
        }

    }

    private function setDownloaded($id){
        $update = "UPDATE imports SET `downloaded` = 1 WHERE `id` = $id";
        return $this->db->query($update);
    }

    private function setFtpDownloaded($name, $file){
        $update = "INSERT INTO imports
                    SET `name` = '$name',
                    `filename` = '$file',
                    `downloaded` = 1,
                    `unzipped` = 1";

        return $this->db->query($update);
    }

    private function downloadZip($file, $newfilename)
    {
        if(!$file)return false;

        $out = fopen( APPPATH . 'assets/data/zip/' . $newfilename, 'wb');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_FILE, $out);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $file);

        $success =curl_exec($ch);
        curl_close($ch);

        return $success;
    }

    private function deleteZip($file){
        echo "<h4>Deleting $file</h4>";
        return unlink( APPPATH . 'assets/data/zip/' . $file);
    }

    private function getAllUnzippedZips(){
        $select = "SELECT *
                    FROM imports
                    WHERE downloaded = 1 AND unzipped = 0";

        return $this->db->query($select);
    }

    private function flagProcessed($id){
        $update = "UPDATE imports SET `unzipped` = 1 WHERE `id` = $id";
        return $this->db->query($update);
    }

    public function unzip(){
        $unzipped = $this->getAllUnzippedZips();

        foreach($unzipped->result() as $row){
            $zipped = $this->unzipFile($row->filename, $row->password);
			echo $zipped;
            //check to see if command line mentions an error
            if($zipped != '' && stripos($zipped,'error')===false){ //no error continue
               echo "<h4>unzipped " . $row->filename . '</h4>';
               $this->flagProcessed($row->id);
            }
        }

    }
    private function unzipFile($filename, $password){

        $file = escapeshellarg( FCPATH. APPPATH . 'assets/data/zip/'.$filename);
        $output = escapeshellarg( FCPATH. APPPATH . 'assets/data/csv');


		$password = $password;
        $zipExec = "7z e -p$password -o$output -y $file";
        return exec($zipExec);
    }

    public function downloadOrders(){

        $randomName = uniqid('orders') .'.csv';

        $this->load->library('ftp');
        $this->ftp->connect($this->config);

        if($this->ftp->download('/orders.csv', APPPATH . 'assets/data/csv/'.$randomName)){
            $this->ftp->delete_file('/orders.csv');
            $this->setFtpDownloaded('orders', $randomName);

            echo "<h4>Downloaded orders</h4>";
        }

        $this->ftp->close();
    }

    public function downloadOrderItems(){

        $randomName = uniqid('orderItems') .'.csv';

        $this->load->library('ftp');
        $this->ftp->connect($this->config);

        if($this->ftp->download('/order_items.csv', APPPATH . 'assets/data/csv/'.$randomName)){
            $this->ftp->delete_file('/order_items.csv');
            $this->setFtpDownloaded('order_items', $randomName);
            echo "<h4>Downloaded order_items</h4>";
        }
        $this->ftp->close();
    }

    public function downloadUsers(){
    //get files from our server

    $randomName = uniqid('users') .'.csv';

    $this->load->library('ftp');
    $this->ftp->connect($this->config);

    if($this->ftp->download('/users.csv', APPPATH . 'assets/data/csv/'.$randomName)){
        $this->ftp->delete_file('/users.csv');
        $this->setFtpDownloaded('users', $randomName);
        echo "<h4>Downloaded Users</h4>";
    }

    $this->ftp->close();
}
}