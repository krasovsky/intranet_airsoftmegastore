<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Evikemanager extends CI_Model {

    public function getAllProducts(){
        $sql = "SELECT p.`product_id`, p.`brand_id`, p.name, b.`name` brand,  i.`inventory`, ams.id ams_id, ams.name ams_name
                FROM competitors_evike p
                LEFT JOIN competitors_evike_brands b ON p.`brand_id` = b.`id`
                LEFT JOIN competitors_evike_inventory i ON p.`product_id` = i.`product_id`
                LEFT JOIN competitors_evike_xref xref ON p.`product_id` = xref.`product_id`
                LEFT JOIN products ams ON xref.`ams_id` = ams.id
                GROUP BY p.`product_id`
                HAVING MAX(i.`timestamp`)
                ORDER BY i.inventory DESC
                ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTopProducts(){
        $sql = "SELECT * FROM competitors_airsoftgi_daily
                WHERE difference IS NOT NULL
                GROUP BY TIMESTAMP
                ORDER BY TIMESTAMP, difference DESC";

        $query = $this->db->query($sql);
        return $query->result();
    }
}