<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Image_manager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    public function saveDirectory(){
        $count = 1313;
        //get directory and shift out directories
        $listings =  scandir('C:\SKU Edits\Watermark edits');
        array_shift($listings);
        array_shift($listings);

        $listings = array_slice($listings,$count,-1);


        foreach($listings as $item){
            echo $count++;
            $sku = str_ireplace('.jpg','',$item);
            $unique = md5($sku);

            $sql = "SELECT id FROM products WHERE sku = '$sku'";
            $result = $this->db->query($sql);
            $id = $result->row()->id;

            $sql = "INSERT IGNORE INTO images SET product_id = '$id', sku = '$sku', img = '$item', alias = '$unique'";
            $this->db->query($sql);
        }
    }

    public function processCSV(){
        $success = false;
        if (($handle = fopen(APPPATH."assets/data/csv/images.csv" , "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 0, "\t")) !== FALSE) {
                $success = $this->updateImages($data);
            }

            fclose($handle);
            return $success;
        }
    }

    private function updateImages($arr){
        $img = $arr[0];
        $sku = $arr[1];

        $sql = "SELECT id FROM products WHERE sku = '$sku'";
        $result = $this->db->query($sql);
        $id = $result->row()->id;

        $file = APPPATH.'assets/data/csv/images_missed.csv';

        if($id){
            $sql = "UPDATE images SET product_id = '$id', sku = '$sku' WHERE img = '$img'";
            $this->db->query($sql);
        }else{
            file_put_contents($file,"$sku\t$img\n",FILE_APPEND) ;
        }
    }

  }