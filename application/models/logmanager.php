<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Logmanager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
      }

//    public function getProducts($page=1){
//        $itemsPerPage = 20;
//        $skip = (int)($itemsPerPage * ($page - 1));
//
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_products;
//
//        $result =  $collection->find()->limit(20)->skip($skip);
//        return iterator_to_array($result,false);
//    }

      public function addLogEntry($entry){

          date_default_timezone_set('America/Los_Angeles');
          $timestamp = date('Y-m-d H:i:s');

          //Something to write to txt log
          $log  = $_SERVER['REMOTE_ADDR'].' - '.$timestamp. ' - ' . $entry . PHP_EOL;
//Save string to log, use FILE_APPEND to append.
          file_put_contents( APPPATH . 'assets/data/logs/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND );
      }

      public function readLog(){
        return file_get_contents( APPPATH . 'assets/data/logs/log_'.date("j.n.Y").'.txt');
      }

//    public function addtocart($obj){
//        $sql = "INSERT into ams_addtocart SET item_id = {$obj->id}, browser = '{$obj->browser}', user_id = {$obj->user}";
//        $this->db->query($sql);
//    }


}