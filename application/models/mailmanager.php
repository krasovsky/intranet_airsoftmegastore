<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class MailManager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    public function getMessage(){
        $inbox = imap_open("{mail.airsoftmegastore.net:143/novalidate-cert}INBOX", "odautoams@airsoftmegastore.net", "3YkcDQLcyN3Xgoc6x9zJ");
        $emails = imap_search($inbox,'ALL');

        if($emails){
            foreach($emails as $email_number) {

                $body = imap_base64(imap_fetchbody($inbox,$email_number,1));
                $tableName = $this->getTableName($body);
                $filename = $this->getFilename($body);
                $zipPassword = $this->getPassword($body);
                $zipUrl = $this->getZipUrl($body);

                //save data to database
                if($this->saveResults($filename, $tableName, $zipUrl, $zipPassword)){
                    //delete email
                    echo "<h4>Message $filename downloaded</h4>";

                    $deleted = imap_delete($inbox, $email_number);
                }
            }
        }
        imap_expunge($inbox);
        imap_close($inbox);
        return true;
    }

    private function HTMLtoArray($body){
        $prefix = preg_quote('<td>');
        $suffix = preg_quote('</td>');

        if(preg_match_all("!$prefix(.*?)$suffix!", $body, $matches)) {
            return $matches[1];
        }
        return array();
    }

    private function getZipUrl($body){
        $answers = $this->HTMLtoArray($body);
        return $answers[9];
    }

    private function getPassword($body){

        $answers = $this->HTMLtoArray($body);
        return $answers[10];
    }

    private function getTableName($body){

        $answers = $this->HTMLtoArray($body);
        return trim(strtolower($answers[3]), '_');
    }

    private function getFilename($body){

        $answers = $this->HTMLtoArray($body);

        $path = $answers[9];
        $filename = substr(strrchr($path, "/"), 1);

        return $filename;
    }

    private function saveResults($filename, $name, $url, $pass){
        $sql = "
        INSERT INTO imports
            SET
            `filename` = '$filename',
            `name` = '$name',
            `url` = '$url',
            `password` = '$pass'
        ";
        return $this->db->query($sql);
    }

    public function mailOutput($subject,$content){
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: Aaron <aaron@airsoftmegastore.com>' . "\r\n";
        $headers .= 'From: AMS Robot <odautoams@airsoftmegastore.com>' . "\r\n";

        mail('aaron@airsoftmegastore.com',$subject,$content, $headers);
    }

//    private function unzip($file,$password){}
//    private function saveCSV($file,$name){}
}