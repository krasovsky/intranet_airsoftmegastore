<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Merchantmanager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    private function getProducts(){
        $select = "SELECT
                    DISTINCT p.id,
                    p.name AS 'title',
                    SUBSTRING(fnStripTags(p.long_description),1,5000) AS 'description',
                    'Sporting Goods > Outdoor Recreation > Shooting Games > Airsoft' AS 'google_product_category',
                    'Airsoft Sporting Goods' AS 'product_type',
                    CONCAT('http://www.airsoftmegastore.com/',p.id,'-',p.vanity_url,'.aspx?source=Google+Base_Airsoft+Megastore') AS 'link',
                    CONCAT('http://images.airsoftmegastore.net/',a.img) AS 'image_link',
                    'new' AS 'condition',
                    'in stock' AS 'availability',
                    IF( pp.price IS NOT NULL, pp.price, p.retail_price) AS 'price',
                    pp.price AS 'sale_price',
                    p.brand_backend_name AS 'brand',
                    i.UPC AS 'gtin',
                    i.MPN AS 'mpn',
                    '' AS 'gender',
                    '' AS 'age_group',
                    s.color AS 'color',
                    '' AS 'size',
                    '' AS 'item_group_id',
                    s.build_material AS 'material',
                    '' AS 'pattern',
                    p.weight AS 'shipping_weight',
                    '' AS 'adwords_grouping',
                    '' AS 'adwords_labels'
                    FROM images a
                    JOIN products p ON a.product_id = p.id
                    JOIN items i ON p.id = i.product_id
                    JOIN productspecs s ON p.id = s.product_id
                    JOIN productpricings pp ON p.id = pp.id
                    WHERE p.merchandisable = 1
                    AND (
			CONCAT(',',p.categories,',') LIKE '%,63,%' OR
			CONCAT(',',p.categories,',') LIKE '%,174,%' )
                    AND i.inventory > 0
                    AND i.UPC <> ''
                    AND p.on_sale = 0
                    GROUP BY p.id
                    ORDER BY p.id
                    LIMIT 10000";

        $query =  $this->db->query($select);
        return $query->result_array();
    }

    public function makeCSV(){


        $filename = realpath( APPPATH . "\\assets\\data" ) ."\\googleMerchant.csv" ;

       // create a file pointer connected to the output stream
        $file = fopen($filename , 'w');

        $D = '"';
        $Sp = ',';
        // fetch the data
        $products = $this->getProducts();
        fwrite($file,'"id","title","description","google_product_category","product_type","link","image_link","condition","availability","price","sale_price","brand","gtin","mpn","gender","age_group","color","size","item_group_id","material","pattern","shipping_weight","adwords_grouping","adwords_labels"'."\n");

        function doubleQuote($str){return str_replace('"', '""', $str);}

        foreach($products as $product){

            $line = preg_replace('/\s+/', ' ', $product);

            $line = array_map('doubleQuote', $line);

            $line['description'] = $this->removeDescriptionLinks($line['description']);

            fwrite($file, $D.implode($D.$Sp.$D, $line).$D."\r\n");

        }

        fclose($file);

    }

    public function removeDescriptionLinks($text){
        //find end of the product description
        $flag = '<!--end product description-->';
        $end = stripos($text, $flag);
        return ($end)? substr($text, 0, $end + strlen($flag)) : $text;
    }

}