<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 2/19/14
 * Time: 2:44 PM
 * To change this template use File | Settings | File Templates.
 */

class Odmanager extends CI_Model {

    const VALUE_REPORT_URL = "https://manager.orderdynamics.com/viewreport.aspx?filename=reports%2fordersByStatusDetail.xml&StartDate={{START}}+00%3a00%3a00&EndDate={{END}}+23%3a59%3a59&Status=Shipped";

    const ORDER_STATUS_QUEUED = 0;
    const ORDER_STATUS_IN_PROCESS = 1;
    const ORDER_STATUS_COMPLETE = 2;

    public function __construct(){
        parent::__construct();


        $obj = new stdClass();
        $this->cookie = $this->getCookie($obj);
        $form = $this->getLoginFormValues($this->cookie);
        $this->login($form,$this->cookie);
        set_time_limit(0);
;        $this->load->helper('simple_html_dom');
    }


    public function deleteCookie(){
        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\od.txt" ;
        return unlink($file);
    }

    public function getCookie(){

        $file = realpath( APPPATH . "\\assets\\data\\cookies" ) ."\\od.txt" ;

        if(file_exists($file) && false){
            return $file;
        }else{

            file_put_contents($file,'');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://manager.orderdynamics.com/signin.aspx");
            curl_setopt($ch, CURLOPT_COOKIEJAR, $file);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

            ob_start();      // prevent any output
            curl_exec ($ch); // execute the curl command
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            return $file;
        }
    }

    public function login($form,$cookie){

        $postDataString = http_build_query($form);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_REFERER, "https://manager.orderdynamics.com/signin.aspx");

        curl_setopt($ch, CURLOPT_URL,"https://manager.orderdynamics.com/signin.aspx");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataString);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
    }

    public function getLoginFormValues($cookie){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_REFERER, "https://manager.orderdynamics.com/signin.aspx");

        curl_setopt($ch, CURLOPT_URL,"https://manager.orderdynamics.com/signin.aspx");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);

        $page = htmlqp($body);
        $inputs = $page->find('input');

        $form = array();
        foreach($inputs as $i){
           $form[$i->attr('name')] = $i->attr('value');
        }

        $form['__EVENTTARGET'] = 'btnSignIn';
        $form['__EVENTARGUMENT'] = '';
        $form['txtEmail'] = 'scraper@airsoftmegastore.com';
        $form['txtPassword'] = 'Eb49zvgtpc!';

        return $form;
    }
    public function addProduct(){
        // $obj = new stdClass();
        // $cookie = $this->getCookie($obj);
        // $form = $this->getLoginFormValues($cookie);
        // $this->login($form,$cookie);

        $this->viewReport($this->cookie);
    }

    public function products($cookie){


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_REFERER, "https://manager.orderdynamics.com/default.aspx");

        curl_setopt($ch, CURLOPT_URL,"https://manager.orderdynamics.com/products.aspx");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        echo $body;

    }

    public function getHtml($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_REFERER, "https://manager.orderdynamics.com/default.aspx");

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie);
        curl_setopt($ch, CURLOPT_TIMEOUT , 100);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 0);

        $response = curl_exec($ch);
        //echo $response;
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        curl_close($ch);
        return $body;
    }

    public function parseReportTr($tr){
        $num = 0;
        $tArr = array();
        foreach($tr->find("td") as $td){
            switch ($num) {
                case 0:
                    $tArr["orderId"] = $td->plaintext;
                    break;
                case 1:
                    $tArr["userId"] = $td->plaintext;
                    break;
                case 2:
                    $tArr["eShop"] = $td->plaintext;
                    break;
                case 3:
                    $tArr["Units"] = $td->plaintext;
                    break;
                case 4:
                    $tArr["subtotal"] = $td->plaintext;
                    break;
                case 5:
                    $tArr["freight"] = $td->plaintext;
                    break;
                case 6:
                    $tArr["tax"] = $td->plaintext;
                    break;
                case 7:
                    $tArr["total"] = $td->plaintext;
                    break;
                default:
                    break;
            }
            
            $num++;
        }
        //$tArr["status"] = 0;
        return $tArr;
    }

    public function saveDataToDb($data){
    }

    public function getOrdersReport($start,$end){
        $url = str_replace(["{{START}}","{{END}}"], [$start,$end], self::VALUE_REPORT_URL);
        dump($url);
        $myhtml = $this->getHtml($url);
        $html = str_get_html($myhtml);
        if(! empty($html)){
            var_dump("Parsing Report");
        }
        else {
            echo $myhtml;
            DIE("REPORT SCRAPE FAILED");
        }
        $resultArr = array();
        foreach($html->find("tr[class=reportgriditem]") as $tr){
            $resultArr[] = $this->parseReportTr($tr);
        }

        foreach($html->find("tr[class=reportgridaltitem]") as $tr){
            $resultArr[] = $this->parseReportTr($tr);
        }
        
        var_dump("inserting to db");
        $queryStr = "INSERT IGNORE INTO od_order_reports (order_number,customer_id,site,units,subtotal,freight,tax,total,status) VALUES (?,?,?,?,?,?,?,?,?)";
        foreach($resultArr as $arr){
            try{
                $this->db->query($queryStr, array($arr["orderId"],$arr["userId"],$arr["eShop"],$arr["Units"],$arr["subtotal"],$arr["freight"],$arr["tax"],$arr["total"],self::ORDER_STATUS_QUEUED));
            }
            catch (Exception $e) {
                echo $e->getMessage();   
            }
        }
        var_dump("complete!");
    }

    public function getOrdersToScrape($limit){
        $rez = $this->db->query("SELECT * from od_order_reports where status = ? order by order_number desc limit ?", array(self::ORDER_STATUS_QUEUED, $limit));
        //$rez = $this->db->query("SELECT * from od_order_reports where status = ? order by order_number desc limit ?", array(self::ORDER_STATUS_IN_PROCESS, $limit));
        return $rez->result_array();
    }

    public function scrapeOrder($order){
        //update status
        $this->db->query("UPDATE od_order_reports set status = ? where order_number = ?", array(self::ORDER_STATUS_IN_PROCESS, $order["order_number"]));

        //get data
        $url = "https://manager.orderdynamics.com/viewOrder.aspx?orderId=".$order["order_number"];

        //$url = "https://manager.orderdynamics.com/viewOrder.aspx?orderId=1252562";
        $html = str_get_html($this->getHtml($url));
        $resultArr = array("order_number" => $order["order_number"]);
        foreach($html->find("#content") as $content){
            //get customerlink
            foreach($content->find("#ctl00_mainContentPlaceHolder_lnkCustomer") as $customer){
                $resultArr["customer_id"] = substr($customer->href, strpos($customer->href,'=') + 1);
            }

            foreach($content->find("table[class=fourcol]") as $topTable){
                $tdNum = 0;
                foreach($topTable->find("td") as $td){
                    if($tdNum == 2){
                        //its ship TO
                        foreach($td->find("span[class=Normal]") as $span){
                            $resultArr["ship_to"] = trim($span->plaintext);
                            $shipArr = explode(' ', $resultArr["ship_to"]);
                            $resultArr["ship_to_zipcode"] = $shipArr[count($shipArr) - 1];
                            $shipArr = explode(PHP_EOL, $resultArr["ship_to"]);
                            $resultArr["ship_address"] = $shipArr[1].' '.$shipArr[2].' '.$shipArr[3];
                        }
                    }
                    else if($tdNum == 3){
                        //its Ship Via
                        $pNum = true;
                        foreach($td->find("p") as $p){
                            if($pNum){
                                @$resultArr["ship_via"] .= trim($p->plaintext);
                            }
                            $pNum = false;
                        }                        
                    }
                    $tdNum++;
                }
            }

            $resultArr["products"] = array();
            foreach($content->find("#ctl00_mainContentPlaceHolder_updatePanelRefresh") as $productsGrid){
                foreach($productsGrid->find("tr[class=GridItem]") as $row){
                    $tArr = array();
                    
                    //get name
                    foreach($row->find("td[class=prodcol]") as $prodName){
                        $tArr["prod_name"] = trim($prodName->plaintext);
                    }

                    if(empty($tArr["prod_name"])) continue;

                    //get sku
                    foreach($row->find("td[class=skucol]") as $sku){
                        $tArr["sku"] = trim($sku->plaintext);
                    }

                    //get Ordered
                    $tdNum = 0;
                    foreach($row->find("td[class=qtycol]") as $td){
                        if($tdNum == 0){
                            $tArr["ordered"] = trim($td->plaintext);
                        }
                        else if($tdNum == 1){
                            $tArr["invoiced"] = trim($td->plaintext);
                        }
                        else if($tdNum == 2){
                            $tArr["shipped"] = trim($td->plaintext);
                        }
                        else if($tdNum == 3){
                            $tArr["returned"] = trim($td->plaintext);
                        }
                        else if($tdNum == 4){
                            $tArr["on_hand"] = trim($td->plaintext);
                        }
                        $tdNum++;
                    }

                    //getPrice
                    $tdNum = 0;
                    foreach($row->find("td[class=pricecol]") as $td){
                        if($tdNum == 0){
                            $tArr["unit_price"] = trim(trim($td->plaintext),'$');
                        }
                        else if($tdNum == 1){
                            $tArr["unit_disc"] = trim(trim($td->plaintext),'$');
                        }
                        else if($tdNum == 2){
                            $tArr["sub_total"] = trim(trim($td->plaintext),'$');
                        }
                        else if($tdNum == 3){
                            $tArr["tax"] = floatval(trim(trim($td->plaintext),'$'));
                        }
                        else if($tdNum == 4){
                            $tArr["product_total"] = trim(trim($td->plaintext),'$');
                        }
                        $tdNum++;
                    }

                    $resultArr["products"][] = $tArr;
                }
                
                //getTotals
                foreach($productsGrid->find("#postgridcontainer") as $totalDiv){
                    $resultArr["totals"] = array();
                    foreach($totalDiv->find("table[id=totaltable]") as $table){
                        foreach($table->find("tr") as $tr){
                            foreach($tr->find("td[class=field]") as $field){
                                $fieldVal = preg_replace('/(\s\s+|\t|\n)/', '', trim($field->plaintext));
                            }
                            foreach($tr->find("td[class=value]") as $value){
                                $value = trim($value->plaintext);
                            }
                            if(! empty($fieldVal)){
                                if($fieldVal == "Sub-Total:"){
                                     $resultArr["totals"]["sub_total"] = trim($value,"$");
                                }
                                else if($fieldVal == "Shipping:"){
                                     $resultArr["totals"]["shipping"] = trim($value,"$");
                                }
                                else if(strpos($fieldVal,"Discounts")  !== false){
                                    $discArr = explode(':', $fieldVal);
                                    $resultArr["totals"]["discount"] = trim(substr($discArr[1], 0, strpos($discArr[1], '(')));
                                    $resultArr["totals"]["discount_amount"] = trim(trim($value,'-'),"$");
                                }
                                else if($fieldVal == "Total:"){
                                     $resultArr["totals"]["total"] = trim($value,"$");
                                }
                            }  
                        }
                    }

                    //figure out if it was shipped
                    foreach($totalDiv->find("table[id=kvptable]") as $table){
                        foreach($table->find("tr") as $tr){
                            $found = false;
                            foreach($tr->find("td[class=field]") as $td){
                                //dump($td);
                                if($td->plaintext == "Tracking Number:") {
                                    $resultArr["totals"]["tracking_number"] = "";
                                    foreach($tr->find("td[class=value]") as $td){
                                        foreach($td->find("input[id=ctl00_mainContentPlaceHolder_txtTrackNumber]") as $input){
                                            if($input->value !== false){
                                                $resultArr["totals"]["tracking_number"] = $input->value;
                                                $resultArr["totals"]["is_shipped"] = 1;
                                            }
                                            else{
                                                $resultArr["totals"]["is_shipped"] = 0;
                                            }
                                        }
                                        
                                    }
                                }
                                else if($td->plaintext == "Last Updated:"){
                                    foreach($tr->find("td[class=value]") as $td){
                                        $resultArr["totals"]["last_update_date"] = date("Y-m-d H:i:s", strtotime(substr($td->plaintext,0,strpos($td->plaintext,'('))));
                                    }
                                }
                            }
                        }

                    }
                }
                

            }
        }

        //SAVE DATA
        var_dump($resultArr);
        //SAVE OD_ORDERS
        $queryStr = "INSERT IGNORE INTO od_orders values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($queryStr,array(
                $resultArr["order_number"],
                $resultArr["customer_id"],
                $resultArr["ship_to_zipcode"],
                $resultArr["ship_address"],
                $resultArr["ship_to"],
                $resultArr["ship_via"],
                $resultArr["totals"]["sub_total"],
                $resultArr["totals"]["shipping"],
                $resultArr["totals"]["discount"],
                $resultArr["totals"]["discount_amount"],
                $resultArr["totals"]["total"],
                $resultArr["totals"]["last_update_date"],
                $resultArr["totals"]["tracking_number"],
                $resultArr["totals"]["is_shipped"],
            ));
        //SAVE OD_ORDER_ITEMS
        $queryStr = "INSERT IGNORE INTO od_order_items  (order_number,item_id,sku,name,ordered,invoiced,shipped,returned,on_hand,unit_price,unit_disc,sub_total,tax,total,order_item_md5) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $skuArr = array();
        foreach($resultArr["products"] as $product){
            $rez = $this->db->query("SELECT id from items where sku = ? limit 1", array($product["sku"]));
            $result = $rez->row();
            //dump($result);
            //die();
            $this->db->query($queryStr, array(
                $resultArr["order_number"],
                $result->id,
                $product["sku"],
                $product["prod_name"],
                $product["ordered"],
                $product["invoiced"],
                $product["shipped"],
                $product["returned"],
                $product["on_hand"],
                $product["unit_price"],
                $product["unit_disc"],
                $product["sub_total"],
                $product["tax"],
                $product["product_total"],
                md5( $product["sku"] . $product["prod_name"] . $resultArr["order_number"]),
            ));

            $skuArr[] = "'".$product["sku"]."'";
        }
        if(empty($skuArr)) throw new Exception("Order is broken: ". $resultArr["order_number"]);
        
        //calculate ups shipment
        $rez = $this->db->query("SELECT SUM(weight) as order_weight FROM products WHERE sku IN ? ", array($skuArr));
        $orderWeight = $rez->row();
        var_dump($orderWeight);
        $this->load->helper("ups");
        $options = array(
            UpsConfig::OPTION_RECIEVER_ZIPCODE_VALUE => $resultArr["ship_to_zipcode"],
            UpsConfig::OPTION_PACKAGE_WEIGHT_VALUE => floatval($orderWeight->order_weight),
            );
        $rate = calculate_ups_rate($options);
        var_dump($rate);
        if($rate !== false){
            $queryStr = <<<HEREDOC
                INSERT INTO ups_api_results (order_id,total_weight,zip_where,create_date,update_date,price)
                values (?,?,?,?,?,?)
                on DUPLICATE key update update_date = ? , price = ?
HEREDOC;

            $currentTime = date('Y-m-d');
            $this->db->query($queryStr,array($resultArr["order_number"],floatval($orderWeight->order_weight),$resultArr["ship_to_zipcode"],$currentTime,$currentTime,$rate,$currentTime,$rate)); 
        }
       

        //set order as complete
        $this->db->query("UPDATE od_order_reports set status = ? where order_number = ?", array(self::ORDER_STATUS_COMPLETE, $order["order_number"]));

        var_dump($order["order_number"]);
    }

    public function generateRatings($currentTime){
        dump($currentTime);
        $queryStr = "SELECT oi.sku,SUM(oi.ordered - oi.returned) AS ordered,i.id AS item_id,i.product_id
            FROM od_orders o
            INNER JOIN od_order_items oi ON oi.order_number = o.order_number
            INNER JOIN items i ON i.id = oi.item_id
            WHERE o.last_update_date BETWEEN ? AND ? AND is_shipped = 1 and oi.sku is not null
            GROUP BY oi.sku,i.id,i.product_id";

        $back30 = date("Y-m-d H:i:s",strtotime("30 days ago",$currentTime));
        $back90 = date("Y-m-d H:i:s",strtotime("90 days ago",$currentTime));
        $back365 = date("Y-m-d H:i:s",strtotime("365 days ago",$currentTime));
        $now = date("Y-m-d H:i:s",$currentTime);
        $rez30 = $this->db->query($queryStr,array($back30,$now));
        $rez90 = $this->db->query($queryStr,array($back90,$now));
        $rez365 = $this->db->query($queryStr,array($back365,$now));

        $result30 = $rez30->result_array();

        $result90 = $rez90->result_array();

        $result365 = $rez365->result_array();
        // foreach($result90 as &$result){
        //     foreach($result30 as &$res30){
        //         if($res30["item_id"] == $result["item_id"]){
        //             $result["sales_30"] = $res30["ordered"];
        //         }
        //     }
        //  }

        // $result365 = $rez365->result_array();
        // foreach($result365 as &$result){
        //     $result["rate_365"] = $result["ordered"] / 365;
        // }
        

        foreach($result365 as &$res365){
            foreach($result90 as $res90){
                if($res90["item_id"] == $res365["item_id"]){
                    $res365["sales_90"] = $res90["ordered"];
                }
            }
            foreach($result30 as $res30){
                if($res30["item_id"] == $res365["item_id"]){
                    $res365["sales_30"] = $res30["ordered"];
                }
            }
        }

        //dump($result365);
        //
        //
        foreach($result365 as $result){
            dump($result);
            $queryStr = "INSERT into od_product_ratings (sku,product_id,item_id,create_date,30_days_sales,90_days_sales,365_days_sales) 
            VALUES (?,?,?,?,?,?,?)";

            $this->db->query($queryStr,array($result["sku"], $result["product_id"], $result["item_id"],$now,$result["sales_30"],$result["sales_90"],$result["ordered"]));
        }

    }
}