<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Products extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
      }

//    public function getProducts($page=1){
//        $itemsPerPage = 20;
//        $skip = (int)($itemsPerPage * ($page - 1));
//
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_products;
//
//        $result =  $collection->find()->limit(20)->skip($skip);
//        return iterator_to_array($result,false);
//    }

    public function getProducts($page=1, $itemsPerPage=20){
        $skip = $itemsPerPage * $page;

        $sql = "SELECT * FROM products LIMIT $skip, $itemsPerPage";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduct($id){
        $sql = "SELECT p.id, p.name FROM products p
                 WHERE p.id = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getLatestInventoryAndPrice($ams_id=0){
        $sql = "SELECT i.product_id, inventory, pp.price, NOW() inventory_update, NOW() pricing_update
                FROM items i
                LEFT JOIN productpricings pp ON pp.id = i.`product_id`
                WHERE i.product_id = $ams_id
                AND pp.`quantity` = 1";

        $query = $this->db->query($sql);
        return $query->row();
    }


}