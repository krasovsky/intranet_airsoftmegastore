<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/28/13
 * Time: 7:52 PM
 * To change this template use File | Settings | File Templates.
 */

class ScrapeRobot extends CI_Model {

    public function saveLink($link, $table){

        $sql = "INSERT IGNORE INTO $table SET url = '".mysql_real_escape_string($link)."'";

        return $this->db->query($sql);
    }

    public function getLink($table){

        $sql = "SELECT * FROM $table
                WHERE url LIKE '%/items/%'
                AND item_name IS NULL
                LIMIT 1";

        $links = $this->db->query($sql);
        return $links->row()->url;
    }

    public function saveItem($data, $link, $table){
        $values = array();

        foreach($data as $key=>$val){
            $value = ($val) ?  mysql_real_escape_string($val) : null;
            $values[] = '`'.$key.'`'." = '".$value."'";
        }

        $set = implode(', ', $values);

        $sql = "UPDATE $table SET $set WHERE url = '$link'";

        return $this->db->query($sql);
    }

    public function getPage($domain, $page)
    {
        $linkArray = array();

        // $html = $this->get_data('http://www.airsplat.com');
        $this->page = htmlqp($domain.$page);


    }

    public function getLinks($domain){
        $this->page->top();
        foreach($this->page->find('a') as $a){
            $link = $a->attr('href');
            if(stripos($link, 'javascript:') !== 0){

                if($link[0] === '/') $link = $domain.$link;
                if(stripos($link, 'items') === 0 ) $link = $domain.'/'.$link;
                if(stripos($link, $domain) === 0){
                    $linkArray[] = $link;
                }
            }
        }
        return array_values(array_flip(array_flip($linkArray)));
    }

    public function getItemData($page){
        $this->page = htmlqp($page);
        $item = array();

        $item['item_name'] = $this->getItemName();
        $item['item_code'] = $this->getItemCode();
        $item['manufacturer'] = $this->getManufacturer();
        $item['price'] = $this->getPrice();
        $item['category'] = $this->getCategory();
        $item['model'] = $this->getModel();
        $item['stock'] = $this->getStock();
        $item['manufacturer_part_no'] = $this->getManufacturerPartNo();


        return $item;
    }

    private function getItemName(){
        $this->page->top();
        return trim($this->page->find('[itemprop=name]')->text());
    }
    private function getCategory(){
        $this->page->top();
        return trim($this->page->find('#breadcrumbs a:last-child')->text());
    }
    private function getItemCode(){
        $this->page->top();
        return trim($this->page->find('[itemprop=identifier]')->text());
    }
    private function getManufacturer(){
        $this->page->top();
        return trim($this->page->find('[itemprop=brand]')->text());
    }
    private function getPrice(){
        $this->page->top();
        return trim($this->page->find('[itemprop=price]')->text());
    }
    private function getStock(){
        $this->page->top();
        return trim($this->page->find('.prodinfo strong strong span')->text());
    }

    private function getModel(){
        $this->page->top();
        return trim($this->page->find('.prodinfo strong:last-child')->textAfter());
    }
    private function getManufacturerPartNo(){
        $this->page->top();
        return trim($this->page->find('.prodinfo strong:nth-child(4)')->textAfter());
    }


    function get_data($url) {

        // $out = fopen( APPPATH . 'assets/data/html/airsplat.html', 'wb');

        $ch = curl_init();
        $timeout = 5;

        curl_setopt($ch, CURLOPT_URL, $url);
        //  curl_setopt($ch, CURLOPT_FILE, $out);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

}