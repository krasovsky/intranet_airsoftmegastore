<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Seo extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    function importXML($xml){
        $xml = simplexml_load_file($xml);

        foreach($xml->children() as $add){
            $this->insertRedirect((string)$add->attributes()->key,(string)$add->attributes()->value);
        }
    }

    function insertRedirect($old,$new){

        $old = str_ireplace('http://www.airsoftmegastore.com','',$old);

        $sql = "INSERT IGNORE INTO redirects SET old = '$old', new = '$new'
                ON DUPLICATE KEY UPDATE new = '$new'";
        $this->db->query($sql);
    }

    function getRedirects(){
        $sql = "SELECT * FROM redirects WHERE publish = 1";
        $response = $this->db->query($sql);
        return $response->result();
    }

    function createXML($file){
        $xml = new SimpleXMLElement('<SiteRedirects/>');

        $redirects = $this->getRedirects();
        foreach( $redirects as $row ){
            $add = $xml->addChild('add');
            $add->addAttribute('key',$row->old);
            $add->addAttribute('value',$row->new);
        }

        $xml->asXML($file);
    }

    function find404(){
       // $sql = "SELECT * FROM `names` WHERE `404` = '1' LIMIT 1";
        $sql = "SELECT * FROM `404` WHERE `crawl` = '1' LIMIT 1";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 0) exit(0);

        $id = $query->row()->id;

        $url = 'http://airsoftmegastore.com/'. $id .'-'. $query->row()->url .'.aspx';
        $header = get_headers($url);

        if(stripos($header[1], 'Error404.aspx')===false){
            $sql = "UPDATE `names` SET `404` = 0 WHERE id = '$id'";
        }else{
            $sql = "UPDATE `names` SET `404` = 1 WHERE id = '$id'";
        }

       $query = $this->db->query($sql);

        return $header;
    }

}