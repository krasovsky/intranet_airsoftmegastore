<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class SimilarityManager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    public function get404(){
        $sql = "SELECT id, `url` FROM `404` LIMIT 64,1";
        $urls = $this->db->query($sql);
        $url = preg_replace('/http:\/\/www.airsoftmegastore.com\//','',$urls->row()->url);
        return preg_replace('/.htm/', '', $url);
    }

    public function getProduct($next=1){
        $sql = "SELECT id, url, `item_name` FROM airsplat
            WHERE `item_name` IS NOT NULL
            AND `item_name` <> ''
            LIMIT $next,1";

        $product = $this->db->query($sql);
        return $product->row()->item_name;
    }

    public function levenshteinProducts($name){
        //get all products for levenshtein calculation
        $sql = "SELECT id, `name` FROM products";
        $products = $this->db->query($sql);

        $products = $this->reindex_by_key($products->result_array());

        //levenshtein
        foreach($products as &$product){
            $score = levenshtein($product['name'], $name);
            $product['score'] = $score;
        }

        function sortScores($a,$b){
            $result = $a['score'] - $b['score'];
            return $result;
        }

        usort($products,"sortScores");
        return array_slice($products, 0, 10);
    }

    public function similarTextProducts($name){
        //get all products for levenshtein calculation
        $sql = "SELECT id, `name` FROM products";
        $products = $this->db->query($sql);

        $products = $this->reindex_by_key($products->result_array());

        foreach($products as &$product){
            $score = similar_text($product['name'], $name);
            $product['score'] = $score;
        }

        function sortScores($a,$b){
            $result = $b['score'] - $a['score'];
            return $result;
        }


        usort($products,"sortScores");
        return array_slice($products, 0, 10);
    }

    public function alikeProducts($name){
        //get all products for levenshtein calculation
        $sql = "SELECT id, `name` FROM products";
        $products = $this->db->query($sql);

        $products = $this->reindex_by_key($products->result_array());

        //levenshtein
        foreach($products as &$product){
            $score = levenshtein($product['name'], $name);
            $product['score'] = $score;
        }

        function sortScores($a,$b){
            $result = $a['score'] - $b['score'];
            return $result;
        }

        usort($products,"sortScores");
        return array_slice($products, 0, 10);
    }

    public function compare404sToProducts($name){
        //get all products for levenshtein calculation
        $sql = "SELECT id, `name` FROM products";
        $products = $this->db->query($sql);

        $products = $this->reindex_by_key($products->result_array());

        //levenshtein
        foreach($products as &$product){
            $score = levenshtein($product['name'], $name);
            $product['score'] = $score;
        }

        function sortScores($a,$b){
            $result = $a['score'] - $b['score'];
            return $result;
        }

        usort($products,"sortScores");
        return array_slice($products, 0, 10);
    }


    function reindex_by_key($input, $key = 'id')
    {
        if ( ! is_array($input))
        {
            return $input;
        }

        // FROM:  http://ellislab.com/forums/viewthread/193209/
        $result = array();
        foreach($input AS $i)
        {
            $result[$i[$key]] = $i;
        }
        return $result;
    }
}