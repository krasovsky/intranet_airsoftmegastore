<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Slimanager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
        $this->excludeArr = array("478","470","1222","1226","1227","1229","1412","1428","1433","1438","1483","1485","1490","478","456","171","123","96","127","307","470","695","696"); //Proshop 353 and gunbuilder 478
    }

    private function getProducts($excludeArr = array()){
        $whereStr = "";

        if(! empty($excludeArr)){
           $excludeString = implode(",",$excludeArr); 
           $whereStr = "AND c.id not in ({$excludeString}) and c.parent_id not in ({$excludeString})";
        }

        $select = "SELECT DISTINCT 
                      p.id,
                      i.id AS cart_id,
                      p.sku,
                      i.max_inventory AS inventory,
                      IF(
                        pp.price IS NOT NULL,
                        pp.price,
                        p.retail_price
                      ) AS price,
                      p.`name`,
                      p.vanity_url,
                      SUBSTRING(p.long_description, 1, 2000) AS long_description,
                      p.img,
                      brand_backend_name AS brand,
                      categories,
                      (SELECT 
                        GROUP_CONCAT(c.name SEPARATOR ';') 
                      FROM
                        categories c 
                      WHERE FIND_IN_SET(c.id, p.categories)) AS category_labels,
                      p.on_sale AS sale,
                      b.id + ps.shell_sku AS bundle,
                      ps.* 
                    FROM
                      products p 
                      JOIN productspecs ps 
                        ON p.id = ps.product_id 
                      JOIN productpricings pp 
                        ON p.id = pp.id 
                      JOIN 
                        (SELECT 
                          MAX(inventory) AS max_inventory,
                          i.* 
                        FROM
                          items i 
                        GROUP BY i.product_id) i 
                        ON p.id = i.product_id 
                      JOIN productbundles b 
                        ON p.id = b.product_id 
                    WHERE p.merchandisable = 1 
                      AND pp.quantity = 1 
                      AND price > 0 
                    GROUP BY id ";
        return $this->db->query($select);
    }

    private function generateExcludeList(){
        $allCat = $this->getCategories();

        $excludeArr = array();
        foreach($allCat as $key => $element){
            foreach($this->excludeArr as $excludeId){
                if($excludeId == $key){
                    $excludeArr+= array_values($this->findChildCategories($key,$allCat));
                }
            }
        }

        $excludeArr = array_unique($excludeArr);
        return $excludeArr;

    }

    private function getCategories($excludeArr = array()){
        $whereStr = "";

        if(! empty($excludeArr)){
           $excludeString = implode(",",$excludeArr); 
           $whereStr = "where id not in ({$excludeString}) and parent_id not in ({$excludeString})";
        }
        
        $queryStr = "SELECT id,parent_id,name from categories {$whereStr}";

        $rez = $this->db->query($queryStr);
        
        $returnArr = array();
        $temp = $rez->result_array();

        foreach($temp as $element){
            $returnArr[$element["id"]] = array("parent_id" => $element["parent_id"],"name" => $element["name"]);
        }


        return $returnArr;
    }

    private function findChildCategories($search,$arr){
        $result = array();

        foreach($arr as $key => $element){
            if($element["parent_id"] == $search){
                $result[] = $key;
            }
        }

        return $result;
    }

    public function showChildred(&$xml,$parentId,$allCategories,$deep = 0){
        foreach($allCategories as $key => $element){
            if($element["parent_id"] == $parentId){

                //printing
                // $i = 0;
                // $arrow = "";
                // while($i <= $deep){
                //     $arrow .= " = ";
                //     $i++;
                // }
                // $arrow .= ">>  ";

                // var_dump($arrow.$key."({$element["name"]})");
                $child = $xml->addChild("category");
                $child->addAttribute("id",$key);
                $child->addChild("name",htmlspecialchars($element["name"]));
                
                $grandChildren = $child->addChild("categories");
                //printing end
                
                $this->showChildred($grandChildren,$key,$allCategories,$deep+1);
            }
        }
    }

    public function test(){
        $allCategories = $this->getCategories($this->excludeArr);
        $xml = new SimpleXMLElement('<catalog/>');
        $this->showChildred($xml,0,$allCategories,0);
        //$this->generateExcludeList();
        $xml->asXML(APPPATH."assets/data/categories.xml");
    }

    public function makeXML(){
        $excludeArr = $this->generateExcludeList();

        $products = $this->getProducts($this->excludeArr);
        $allCategories = $this->getCategories($this->excludeArr);

        $noShow = array('id', 'category_labels', 'product_id', 'sale');

        $xml = new SimpleXMLElement('<catalog/>');
        $arr = array();

        $this->showChildred($xml,0,$allCategories,0);

        $count = 0;
        foreach($products->result() as $item){
            $count++;

            $child = $xml->addChild('product');
            $child ->addAttribute('id', $item->id);

            foreach($item as $key=>$val){

                if(in_array($key,$noShow)){
                    continue;
                }else{
                    if($key == "categories"){

                        $cat_ids = explode(',', $item->categories);
                        $cat_labels = explode(';', $item->category_labels);
                        if(count($cat_ids) !== count($cat_labels)){
                            var_dump($item);
                        }
                        
                        $cat_ids = array_values($cat_ids);

                        $cat_assoc = array_combine($cat_ids, $cat_labels);

                        $categories = $child->addChild('categories');

                        foreach($cat_assoc as $key=>$val){
                            //check if category is in all showing categories
                            // dump($allCategories);
                            // dump($key);
                            // die();
                            if(! isset($allCategories[$key])) {
                                continue;
                            }
                            $cat = $categories->addChild('category',htmlspecialchars($val));

                            //pull parent_id
                            $cat->addAttribute('id', $key);
                            if(! empty($allCategories[$key]["parent_id"])){
                                $cat->addAttribute('parent_id', $allCategories[$key]["parent_id"]);
                            }
                            else{
                                $cat->addAttribute('parent_id', 0);
                            }
                        }
                    }elseif($key == "price"){
                            $p = $child->addChild('price');
                            $p->addChild('retail', $val);
                            if(intval($item->sale)){
                                $p->addChild('sale', $this->adjustSalePricing($item));
                            }
                    }elseif($key == "name"){
                        $c = $child->addChild($key, htmlspecialchars($val));
                    }elseif($key == "img"){
						$val = str_replace('http:///','http://www.airsoftmegastore.com/',$val);
                        $val_array = explode('|', $val);
                        $images = $child->addChild('images');

                        foreach($val_array as $img){
							
                            $images->addChild('image',$img);
                        }
                    }elseif($key == "vanity_url"){
                        $child->addChild('url', 'http://www.airsoftmegastore.com/'.$item->id.'-'.$val.'.aspx');
                    }elseif($key == "long_description"){
                        $desc = $child->addChild('long_description');
                        $this->sxml_cdata($desc, $val);
                    }else{
                        $c = $child->addChild($key,htmlspecialchars($val));
                    }
                }
            }
        }

        $xml->asXML(APPPATH."assets/data/products.xml");

        echo "Added $count items to index";
    }

    private function adjustSalePricing($row){
        $price = floatval($row->price);
        $percent = $row->sale / 100;
        return number_format(($price - ( $price * $percent )), 2);
    }

    private function sxml_cdata($path, $string){
        $dom = dom_import_simplexml($path);
        $cdata = $dom->ownerDocument->createCDATASection($string);
        $dom->appendChild($cdata);
    }
}