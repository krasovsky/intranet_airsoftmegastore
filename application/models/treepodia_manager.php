<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Treepodia_manager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
    }

    private function getProducts(){
        $select = "SELECT p.id,
                    p.sku,
                    IF( pp.price IS NOT NULL, pp.price, p.retail_price) AS price,
                    p.`name`,
                    p.vanity_url,
                    p.img,
                    p.seo_meta_description AS description,
                    brand_backend_name AS brand,
                    b.img AS brand_img,
                    p.categories,
                    (SELECT GROUP_CONCAT(c.name SEPARATOR ';') FROM categories c WHERE FIND_IN_SET(c.id, p.categories)) AS category_labels,
                    ps.*
                    FROM products p
                    LEFT JOIN productspecs ps ON p.id = ps.product_id
                    LEFT JOIN productpricings pp ON p.id = pp.id
                    LEFT JOIN brands b ON b.name = brand_backend_name
                    WHERE p.merchandisable = 1
        ";

        return $this->db->query($select);
    }

    public function makeXML(){

        $products = $this->getProducts();
        $noShow = array('id', 'category_labels', 'product_id');

        $xml = new SimpleXMLElement('<catalog/>');

        foreach($products->result() as $item){

            $child = $xml->addChild('product');
            $child ->addAttribute('id', $item->id);

            foreach($item as $key=>$val){

                if(in_array($key,$noShow)){
                    continue;
                }else{
                    if($key == "categories"){

                        $cat_ids = explode(',', $item->categories);
                        $cat_labels = explode(';', $item->category_labels);
                        if(count($cat_ids) !== count($cat_labels)){
                            echo 'bad';
                        }
                        $cat_assoc = array_combine($cat_ids, $cat_labels);

                        $categories = $child->addChild('categories');

                        foreach($cat_assoc as $key=>$val){
                            $cat = $categories->addChild('category',htmlspecialchars($val));
                            $cat->addAttribute('id', $key);
                        }
                    }elseif($key == "price"){
                        $p = $child->addChild('price');
                        $p->addChild('retail', $val);
                        if($sale = $this->adjustSalePricing($item)){
                            $p->addChild('sale', $sale);
                        }
                    }elseif($key == "name"){
                        $c = $child->addChild($key, htmlspecialchars($val));
                    }elseif($key == "img"){

                        $val_array = explode(';', $val);
                        $images = $child->addChild('images');

                        foreach($val_array as $img){
                            $images->addChild('image',$img);
                        }
                    }elseif($key == "vanity_url"){
                        $child->addChild('url', 'http://www.airsoftmegastore.com/'.$item->id.'-'.$val.'.aspx');
                    }elseif($key == "long_description"){
                        $desc = $child->addChild('long_description');
                        $this->sxml_cdata($desc, $val);
                    }else{
                        $c = $child->addChild($key,htmlspecialchars($val));
                    }
                }
            }
        }

        echo $xml->asXML(APPPATH."assets/data/treepodia.xml");
    }

    private function adjustSalePricing($row){
        $price = floatval($row->price);
        if(stripos($row->name,'SUMMER BLOWOUT DEAL')===0){
            return number_format(($price - ( $price * 0.25)), 2);
        }
        return false;
    }

    private function sxml_cdata($path, $string){
        $dom = dom_import_simplexml($path);
        $cdata = $dom->ownerDocument->createCDATASection($string);
        $dom->appendChild($cdata);
    }
}