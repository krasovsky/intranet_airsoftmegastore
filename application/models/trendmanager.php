<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Trendmanager extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $ci =& get_instance();
      }

//    public function getProducts($page=1){
//        $itemsPerPage = 20;
//        $skip = (int)($itemsPerPage * ($page - 1));
//
//        $m = new Mongo($this->mongo_config);
//        $collection = $m->AMS->od_products;
//
//        $result =  $collection->find()->limit(20)->skip($skip);
//        return iterator_to_array($result,false);
//    }

    public function addtocart($obj){
        $sql = "INSERT into ams_addtocart SET item_id = {$obj->id}, browser = '{$obj->browser}', user_id = {$obj->user}";
        $this->db->query($sql);

    }
    public function getCategories(){
        $sql = "SELECT id, name FROM categories";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function topTenProducts(){
        $sql = "SELECT p.name FROM products p
                LEFT JOIN items i ON i.product_id = p.id
                WHERE i.id IN (
                    SELECT item_id FROM ams_addtocart
                    GROUP BY item_id
                    HAVING COUNT(*) > 2
                    ORDER BY COUNT(*) DESC
                )";
        $result = $this->db->query($sql);

        var_dump($result->result());
    }


}