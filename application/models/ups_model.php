<?php

class Ups_model extends CI_Model {

    const TYPE_EXPORT_ORDER = 1;

    const UPS_FOLDER_PATH = "assets/data/ups/";
    const UPS_EXPORT_DAYS_BACK_VALUE = 10;

    const OPTION_DAYS_BACK_VALUE = "odbv";
    const OPTION_START_DATE = "osd";
    const OPTION_END_DATE = "oed";
    const OPTION_SCRAPE_BACK = "osb";

    public function downloadExport($options = array()){

        $startDate = date ("Y-m-d H:i:s");
        var_dump("started at :" . $startDate);

        if(! empty($options[self::OPTION_SCRAPE_BACK]) && $options[self::OPTION_SCRAPE_BACK] == true){
            $rez = $this->db->query("SELECT * FROM ups_exports WHERE export_date_value IS NOT NULL ORDER BY export_date_value ASC LIMIT 1");
            $minValue = $rez->row();
            $options[self::OPTION_END_DATE] = date("m/d/Y", strtotime($minValue->export_date_value));
            $options[self::OPTION_START_DATE] = date("m/d/Y", strtotime($minValue->export_date_value ." -2 days"));

            if(strtotime($options[self::OPTION_START_DATE]) < strtotime("1/1/2013")) DIE("STOPPED...1/1/2013");
        }

        if(! empty($options[self::OPTION_DAYS_BACK_VALUE])){
            $daysBack = $options[self::OPTION_DAYS_BACK_VALUE];
        }
        else{
            $daysBack = self::UPS_EXPORT_DAYS_BACK_VALUE;
        }
            

        $dateRangeStart = date("m/d/Y", strtotime($daysBack ." days ago"));
        if(! empty($options[self::OPTION_START_DATE])){
            $dateRangeStart = date("m/d/Y", strtotime($options[self::OPTION_START_DATE]));
        }
       

        $dateRangeEnd = date("m/d/Y", strtotime(($daysBack - 1)." days ago"));
        if(! empty($options[self::OPTION_END_DATE])){
            $dateRangeEnd = date("m/d/Y", strtotime($options[self::OPTION_END_DATE]));
        }


        $queryStr = "INSERT INTO ups_exports (ups_scrape_type,start,export_date_value) VALUES(?,?,?)";
        $this->db->query($queryStr,array(self::TYPE_EXPORT_ORDER, $startDate,date("Y-m-d H:i:s", strtotime($dateRangeStart))));


        dump($options);

        /**
         * IMACROS RUN
         */
        $iim1 = new COM("imacros");
        $s = $iim1->iimOpen("-fx");
     
        


        
        $fileName = date("m-d-y-H-i" ,strtotime($dateRangeStart));
        $s = $iim1->iimSet("FILENAME", $fileName);

        $s = $iim1->iimSet("DATESTART", $dateRangeStart);
        $s = $iim1->iimSet("DATEEND", $dateRangeEnd);


        $s = $iim1->iimPlay("IMPORT.iim");     
        $s = $iim1->iimClose();

        $endDate = date ("Y-m-d H:i:s");
        var_dump("imacros finished at :".$endDate);
        if(file_exists(APPPATH.self::UPS_FOLDER_PATH.$fileName.".csv")){
            $queryStr = "UPDATE ups_exports set end = ? , export_file_name = ? , export_date_value = ? where start = ?";
            $this->db->query($queryStr,array($endDate,$fileName,date("Y-m-d H:i:s", strtotime($dateRangeStart)),$startDate));
        }
        else{
            var_dump("UNABLE TO FILE FILE" . APPPATH.self::UPS_FOLDER_PATH.$fileName.".csv");
        }
    }

    public function processExport(){
        $queryStr = "SELECT * from ups_exports where processed = 0 and export_file_name is not null order by export_date_value desc ";
        $rez = $this->db->query($queryStr);
        $results = $rez->result_array();
        
        if(empty($results)) var_dump("nothing to process!");

        foreach($results as $result){  
            $data = $this->parceCSV($result["export_file_name"].".csv");
            var_dump("start inserting data");
            $this->insertDataToDB($data);
            var_dump("finished file ".$result["export_file_name"].".csv");
            $this->db->query("update ups_exports set processed = 1 where id = ?", array($result["id"]));
        }
    }

    public function parseValues($data){
        if(empty($data[0])) return false;
        $returnArr = array();
        $returnArr[0] = $data[0];
        $returnArr[1] = date("Y-m-d",strtotime($data[1]));
        $returnArr[2] = $data[2];
        $returnArr[3] = $data[3];
        $returnArr[4] = $data[4];
        $value = trim($data[5]);
        if(is_numeric($value)){
             $returnArr[5] = $value;
        }
        else {
            $value = strtolower($value);
            if(strpos($value,"order") !== false){
                $temp = explode(' ', $value);
                $returnArr[5] = $temp[1];
            }
            else{
                $returnArr[5] = $value;
            }
        }

        return $returnArr;
    }

    public function insertDataToDB($data){

        foreach($data as $element){
            $queryStr = "INSERT INTO ups_billing_report (invoice_number,invoice_date,published_charge,incentives,net_amount,order_id) values(?,?,?,?,?,?)";
            $vals = $this->parseValues(array_values($element));
            if($vals !== false){
                $this->db->query($queryStr,$vals);
            }
        }
    }

    public function parceCSV($fileName){
        if(! file_exists(APPPATH.self::UPS_FOLDER_PATH.$fileName)){
            var_dump("FILE DOESNT EXIST: ".$fileName);
            return;
        }

        $returnArr = array();

        if (($handle = fopen(APPPATH.self::UPS_FOLDER_PATH.$fileName , "r")) !== FALSE) {
            $count = 0;
            while (($data = fgetcsv($handle, 0, ",", "~")) !== FALSE) {
                //read from 7nth line only
                if($count > 6){
                    unset($data[0]);
                    unset($data[7]);
                    $returnArr[] = $data;
                }
                $count++;
            }
        }
        return $returnArr;
    }
}