<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Usermanager extends CI_Model {
    var $orders = '';
    function __construct()
    {
        parent::__construct();

    }

    public function getCustomerOrders($email, $start, $end){
        $sql = "SELECT * FROM order_items oi
                JOIN orders o ON o.id = oi.`order_record_id`
                JOIN items i ON oi.item_record_id = i.id
                JOIN products p ON p.id = i.product_id
                WHERE o.email = '$email' AND
                    o.ordered_at_datetime > '$start'  AND
                    o.ordered_at_datetime < '$end'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCustomerInfo($email){
        $sql = "SELECT * FROM users WHERE email = '$email'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getCustomers($start,$end,$limit=100){
        $sql = "
            SELECT o.id, o.email, SUM(o.`total_price`) spent, COUNT(o.id) transactions, MAX(o.ordered_at_datetime) last_purchase FROM orders o
            WHERE
                    o.ordered_at_datetime > '$start'  AND
                    o.ordered_at_datetime < '$end'
            GROUP BY email
            ORDER BY spent DESC, transactions DESC
            LIMIT $limit
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCustomersByState($state,$start,$end,$limit=100){
        $sql = "
            SELECT o.id, o.email, SUM(o.`total_price`) spent, COUNT(o.id) transactions, MAX(o.ordered_at_datetime) last_purchase FROM orders o
            JOIN users u on o.email = u.email
            WHERE   u.state = '$state' AND
                    o.ordered_at_datetime > '$start'  AND
                    o.ordered_at_datetime < '$end'
            GROUP BY email
            ORDER BY spent DESC, transactions DESC
            LIMIT $limit
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }

}