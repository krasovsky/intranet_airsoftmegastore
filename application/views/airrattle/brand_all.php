<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <h1><?=$h1?></h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="col-md-1">id</th>
            <th>name</th>
            <th class="col-md-1">total product lines</th>
            <th class="col-md-1">total stock</th>
            <th class="col-md-2">total invested (40% margin)</th>
        </tr>
        </thead>
        <?php foreach($result as $row):?>
        <tr>
            <td><a href="/index.php/airsoftgi/brand/<?=$row->id?>"><?=$row->id?></a></td>
            <td><a href="/index.php/airsoftgi/brand/<?=$row->id?>"><?=$row->name?></a></td>
            <td><?=$row->total?></td>
            <td><?=$row->stock==100?'100+':$row->stock?></td>
            <td>$<?= number_format( $row->invested * .60 ) ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

<?php $this->load->view('common/footer.php'); ?>
