<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <h1><?=$h1?></h1>
    <table class="table table-striped">
     <thead>
         <tr>
             <th class="col-md-1">id</th>
             <th class="col-md-1">ams_id</th>
             <th>name</th>
             <?php if(isset($result[0]->timestamp)):
                 echo '<th class="col-md-2">Timestamp</th>';
             endif; ?>
             <th class="col-md-2">Brand</th>
             <th class="col-md-1">Inventory</th>
         </tr>
     </thead>
     <?php foreach($result as $row):?>
     <tr>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->product_id?></a></td>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->ams_id?></a></td>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->name?></a></td>
         <?php if(isset($row->timestamp)): ?>
            <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->timestamp?></a></td>
         <?php endif; ?>
         <td><a href="/index.php/airsoftgi/brand/<?=$row->brand_id?>"><?=$row->brand?></a></td>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->inventory==100?'100+':$row->inventory?></a></td>
     </tr>
     <?php endforeach; ?>
    </table>
    <div class="pagination text-center">
<?php /*        <ul>
            <?php if($current <= 1):?>
                <li class="disabled"><a href="#">Prev</a></li>
            <?php else:?>
                <li><a href="/index.php/products/page/<?=$current-1?>">Prev</a></li>
            <?php endif;?>

            <?php for($i = 1; $i < $pages; $i++ ): ?>
            <?php if($i < 20): ?>
                <li <?php if($i==$current):?>class="active"<?php endif;?>>
                    <a href="/index.php/products/page/<?=$i;?>"><?=$i;?></a>
                </li>
            <?php elseif($i==20):?>
                <li <?php if($i==$current):?>class="active"<?php endif;?>>
                    <a href="/index.php/products/page/<?=$i;?>"><?=$i;?></a>
                </li>
            <?php endif;?>
            <?php endfor; ?>
            <li <?php if($current == $pages):?>class="disabled"<?php endif;?>>
                <a href="/index.php/products/page/<?=$current+1?>">Next</a>
            </li>
        </ul>
 */ ?>
        <?php for($i = 1; $i < $pages; $i++ ): ?>
            <?php if($i==1):?><ul><?php endif;?>
            <li <?php if($i==$current):?>class="active"<?php endif;?>>
                <a href="/index.php/airsoftgi/products/<?=$i;?>/<?=$itemsPerPage?>"><?=$i;?></a>
            </li>
            <?php if($i%18==0 and $i!=$pages):?>
                </ul><ul>
            <?php endif;?>
            <?php if($i==$pages):?></ul><?php endif;?>
        <?php endfor; ?>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>
