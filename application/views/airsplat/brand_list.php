<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <h1><?=$h1?></h1>
    <?php if(isset($totals)): ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Total Products</th>
            <th>Total Inventory</th>
            <th>Total Potential</th>
            <th>Total Invested (40% margin)</th>
        </tr>
        </thead>
            <tr>
                <td><?=$totals->total_products?></td>
                <td><?=$totals->inventory?></td>
                <td>$<?=$totals->potential?></td>
                <td>$<?=$totals->potential * .60 ?></td>
            </tr>
    </table>
    <?php endif;?>
    <table class="table table-striped">
     <thead>
         <tr>
             <th class="col-md-1">Product Id</th>
             <th>name</th>
             <th class="col-md-2">Brand</th>
             <th class="col-md-1">Inventory</th>
         </tr>
     </thead>
     <?php foreach($result as $row):?>
     <tr>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->product_id?></a></td>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->name?></a></td>
         <td><?=$row->brand?></td>
         <td><a href="/index.php/airsoftgi/product/<?=$row->product_id?>"><?=$row->inventory==100?'100+':$row->inventory?></a></td>
     </tr>
     <?php endforeach; ?>
    </table>
</div>
<?php $this->load->view('common/footer.php'); ?>
