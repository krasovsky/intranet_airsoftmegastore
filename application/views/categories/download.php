<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>

<div class="container">

    <?php if(isset($message)): ?>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?= $message ?>
    </div>
    <?php endif; ?>

    <h1>OD Category CSV download</h1>
    <p>Please enter 1 or more product ids (comma delimited) for select products or leave blank to select all.</p>

   <form action="/categories/getDownload" method="post" class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label>Product Ids</label>
                <textarea name="product_ids" placeholder="1234, 4567, 2345, 8747, 701" class="form-control" rows="7"><?= isset($product_ids)? implode(',',$product_ids):''; ?></textarea>
                <input type="hidden" name="action" value="add" id="actionInput" />
            </div>

            <div class="form-group">
                <input type="submit" name="add" class="btn" value="download" />
            </div>
        </div>
   </form>

</div>
<?php $this->load->view('common/footer.php'); ?>
