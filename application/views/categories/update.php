<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>
<script>
    var AMS = AMS || {};
    AMS.Categories = {
        ready:function(){
            $('#action .btn').on('click',function(){
                $('#actionInput').val($(this).text());
                $(this).addClass("active").siblings().removeClass("active");
            });
        }
    };
</script>
<div class="container">

    <?php if(isset($message)): ?>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?= $message ?>
    </div>
    <?php endif; ?>

    <h1>Mass Category Update</h1>
    <p>Please enter 1 or more category ids (comma delimited), 1 or more product ids (comma delimited), and choose an option below.</p>

   <form action="/categories/update" method="post" class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="">Category Ids </label>
                <input name="category_ids" placeholder="21, 22, 34" value="<?= isset($category_ids)? implode(',',$category_ids):''; ?>" class="form-control" />
            </div>
            <div class="form-group">
                <label>Product Ids</label>
                <textarea name="product_ids" placeholder="1234, 4567, 2345, 8747, 701" class="form-control" rows="7"><?= isset($product_ids)? implode(',',$product_ids):''; ?></textarea>
                <input type="hidden" name="action" value="add" id="actionInput" />
            </div>
            <div class="form-group">
               <div class="btn-group" role="group" aria-label="group" id="action">
                   <button type="button" class="btn btn-default active">add</button>
                   <button type="button" class="btn btn-default">remove</button>
               </div>
            </div>
            <div class="form-group">
                <input type="submit" name="add" class="btn" value="update" />
            </div>
        </div>
   </form>

</div>
<?php $this->load->view('common/footer.php'); ?>
