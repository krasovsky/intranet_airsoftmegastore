<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-editable.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-formhelpers.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/fonts/bebasneue_regular_macroman/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/common.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/js/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

    <!--ASSET LOADER-->
    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/head.load.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript" src="/assets/js/bower_components/moment/min/moment.min.js"></script>
    <script type="text/javascript" src="/assets/js/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="/assets/js/bower_components/remarkable-bootstrap-notify/bootstrap-notify.min.js"></script>
    <script>
        var AMS = {};
        head.js(
            '/assets/js/bootstrap.js',
            '/assets/js/bootstrap-editable.js',
            "/assets/js/bootstrap-formhelpers.js",
            '/assets/js/chart.js',
            '/assets/js/ams.js',
            '/assets/js/chartBuilder.js'
            ,function(){
            });
    </script>

    <!--    TABLE SORTER-->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" serc="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" />

    


    <!--    CHARTS-->
    <script type="text/javascript" src="/assets/js/highcharts.js"></script>
    <script type="text/javascript" src="/assets/js/exporting.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

</head>
<body>
<div class="container">
<div class="row">&nbsp;</div>
<div class="row">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="//<?=$_SERVER['SERVER_NAME']?>"><img src="/assets/img/logo_95x95.png" style="width:60px;height:60px;margin-top:-20px" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
<!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
<!--                    <li><a href="#">Link</a></li>-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Product <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="dropdown-submenu"><a href="/products">Product</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/products">All Products</a></li>
                                    <li><a href="/products/inStock">In Stock</a></li>
                                    <li><a href="/products/onSale">On Sale</a></li>
                                    <li><a href="/products">Discontinued</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a href="/#">Categories</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/categories/process">Manually Refresh Categories</a></li>
                                    <li><a href="/categories/update">Mass Update</a></li>
                                    <li><a href="/categories/download">Download OD Import</a></li>
                                    <!--                            <li class="divider"></li>-->
                                </ul>
                            </li>
                            <!-- <li class="dropdown-submenu"><a href="/trends">Trends</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/trends/topTen">Top 10 in Category</a></li>
                                    <!--                            <li class="divider"></li>
                                </ul>
                            </li> -->
<!--                            <li class="divider"></li>-->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Orders <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/data">Order List</a></li>
                        </ul>
                    </li>
                    <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Users <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/data">User List</a></li>
                            <li><a href="/data">Returning Customers</a></li>
                            <li><a href="/data">One Time Customer</a></li>
                        </ul>
                    </li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Competitors <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/competitors">Competitor Pricing</a></li>
                            <li><a href="/competitors/getDownload">Competitor Pricing CSV</a></li>
                            <li class="dropdown-submenu"><a href="#">Competitor Inventory</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu" ><a href="#">Evike</a>
                                        <ul class="dropdown-menu" role="menu">
                                            <!-- <li><a href="#">Opportunity</a></li>
                                            <li><a href="/evike/topBrands">Top Brands</a></li>
                                            <li><a href="#">Top Products</a></li> -->
                                            <li><a href="/evike/exclusiveBrands">Brands</a></li>
                                            <li><a href="/evike/exclusive">Exclusive Products</a></li>
                                            <li><a href="/evike/identical">Identical Products</a></li>
                                            <li class="divider"></li>
                                            <!-- <li><a href="/evike/brands">Brands Rollup</a></li> -->
                                            <li><a href="/evike/products">All Products</a></li>
                                            <!-- <li><a href="#">New Products</a></li> -->
                                            <li><a href="/evike/discontinued">Discontinued Products</a></li>
                                            <li><a href="/evike/outofstock">Out of Stock Products</a></li>
                                            <!-- <li><a href="#">Restocked Products</a></li> -->
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a href="#">Airsoft Gi</a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="/airsoftgi/exclusiveBrands">Brands</a></li>
                                            <li><a href="/airsoftgi/exclusive">Exclusive Products</a></li>
                                            <li><a href="/airsoftgi/identical">Identical Products</a></li>
                                            <li class="divider"></li>
                                            <li><a href="/airsoftgi/products">All Products</a></li>
                                            <li><a href="/airsoftgi/discontinued">Discontinued Products</a></li>
                                            <li><a href="/airsoftgi/outofstock">Out of Stock Products</a></li>
                                        </ul>
                                    </li>
<!--                                    <li class="dropdown-submenu"><a href="#">Airsplat</a>-->
<!--                                        <ul class="dropdown-menu" role="menu">-->
<!--                                            <li><a href="/airsplat/exclusiveBrands">Brands</a></li>-->
<!--                                            <li><a href="/airsplat/exclusive">Exclusive Products</a></li>-->
<!--                                            <li><a href="/airsplat/identical">Identical Products</a></li>-->
<!--                                            <li class="divider"></li>-->
<!--                                            <li><a href="/airsplat/products">All Products</a></li>-->
<!--                                            <li><a href="/airsplat/discontinued">Discontinued Products</a></li>-->
<!--                                            <li><a href="/airsplat/outofstock">Out of Stock Products</a></li>-->
<!--                                        </ul>-->
<!--                                    </li>-->
<!--                                    <li class="dropdown-submenu"><a href="#">Airrattle</a>-->
<!--                                        <ul class="dropdown-menu" role="menu">-->
<!--                                            <li><a href="/airrattle/exclusiveBrands">Brands</a></li>-->
<!--                                            <li><a href="/airrattle/exclusive">Exclusive Products</a></li>-->
<!--                                            <li><a href="/airrattle/identical">Identical Products</a></li>-->
<!--                                            <li class="divider"></li>-->
<!--                                            <li><a href="/airrattle/products">All Products</a></li>-->
<!--                                            <li><a href="/airrattle/discontinued">Discontinued Products</a></li>-->
<!--                                            <li><a href="/airrattle/outofstock">Out of Stock Products</a></li>-->
<!--                                        </ul>-->
<!--                                    </li>-->
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Robots <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/">Order Dynamics</a></li>
                            <li><a href="">SLI</a></li>
                            <li><a href="">Google Merchant</a></li>
                            <li><a href="/spider/evike/1">Evike Crawler</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Promotions <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/promotions/">Coupons</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">FishBowl <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/fishbowl/generateInventoryChangeCsv">Generate Cycle Data File.</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reports<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/reports">TOP 20</a></li>
                            <li><a href="/reports/masterReport">Master report</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Email-templates<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/notify">Auto-notify</a></li>
                        </ul>
                    </li>
                </ul>
<!--                <form class="navbar-form navbar-right" role="search">-->
<!--                    <div class="form-group">-->
<!--                        <input type="text" class="form-control" placeholder="sku">-->
<!--                    </div>-->
<!--                    <button type="submit" class="btn btn-default">Search</button>-->
<!--                </form>-->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>
</div>