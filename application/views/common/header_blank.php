<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-editable.css">
    <link rel="stylesheet" type="text/css" href="/assets/fonts/bebasneue_regular_macroman/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

    <!--ASSET LOADER-->
    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/head.load.min.js"></script>
    <script>
        var AMS = {};
        head.js(
            '/assets/js/bootstrap.js',
            '/assets/js/bootstrap-editable.js',
            '/assets/js/chart.js',
            '/assets/js/ams.js'
            ,function(){
            });
    </script>
</head>
<body>
<div class="container">
<div class="row">&nbsp;</div>
<div class="row">
    <div class="navbar">

    <div class="navbar-inner">
        <a class="brand" href="//"><img src="http://www.airsoftmegastore.com/images/logo_95x95.png" style="width:60px;height:60px" />&nbsp;&nbsp;&nbsp;&nbsp;</a>
        <ul class="nav">
            <li class="active"></li>
        </ul>
    </div>
</div>
</div>
</div>