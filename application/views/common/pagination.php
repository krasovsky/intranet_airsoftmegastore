<?php for($i = 1; $i < $pages; $i++ ): ?>
    <?php if($i==1):?>
        <ul class="pagination">
    <?php endif;?>

    <li <?php if($i==$current):?>class="active"<?php endif;?>>
        <a href="<?=$path?><?=$i;?>/<?=$itemsPerPage?>"><?=$i;?></a>
    </li>

    <?php if($i==$pages):?>
        </ul>
    <?php endif;?>
<?php endfor; ?>

