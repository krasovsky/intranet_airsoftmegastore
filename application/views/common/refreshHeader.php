<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/fonts/bebasneue_regular_macroman/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

    <!--ASSET LOADER-->
    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/head.load.min.js"></script>
    <script>
        var AMS = {};
        head.js(
            '/assets/js/bootstrap.js',
            '/assets/js/ams.js'
            ,function(){
                    setTimeout(function(){
                        location.href = location.href;
                    },3000)
            });
    </script>
</head>
<body>