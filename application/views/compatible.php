<style>
    .compatible .product-image{
        overflow:hidden;
        margin:0px auto;
        text-align:center;
    }
    .compatible li{
        height: 219px;
    }
</style>
<div class="container">
    <table class="table table-striped">
        <tr>
           <th>name</th>
           <td><a href="#" id="name" data-type="text" data-pk="1" data-original-title="Enter Product Name" class="editable editable-click"><?= $item->name; ?></a></td>
        </tr>
        <tr>
            <th>Description</th>
            <td>

            </td>
        </tr>
    </table>

    <p>
        <?= $item->long_description; ?>
    </p>
    <div class="slider row margin-top">
        <h2>Most Popular Compatible Products</h2>
        <ul class="compatible unstyled">
            <?php foreach($compatible as $c):?>
            <li>
                <div class="product-image">
                    <a href="<?=$c->link;?>" title="<?=$c->name;?>">
                        <img src="http://airsoftmegastore.resultspage.com/thumb.php?&s=140&aspect=true&f=<?=$c->img;?>" style="width:140px" alt="<?=$c->name;?>"/>
                    </a>
                </div>
                <div class="product-container">
                    <h3><a href="<?=$c->link;?>" title="<?=$c->name;?>"><?=$c->name;?></a></h3>
                    <p>Brand:<?=$c->brand?></p>
                    <div class="price-box">
                        <strong class="price"><?=$c->price;?></strong>
                    </div>
                </div>
                <a href="/AddItem.aspx?itemId=<?=$c->item_id;?>" class="btn-add" data-name="AddToCart.<?=$c->item_id;?>"><span>ADD TO CART</span> </a>
            </li>

        <?php endforeach; ?>
        </ul>
    </div>
</div>


