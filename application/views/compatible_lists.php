<!--product description-->
<?= $item->long_description; ?>
</br>
<!--end product description-->
<!--compatible products module-->
<?php if(count($compatible)): ?>
    <!--SEO Compatible Links-->
    </br>
    <div class="row margin">
        <strong>Products Most Often Purchased with our <?=$item->name?>:</strong>
        <ul class="pull-left span-3">
            <?php foreach($compatible as $c): ?>
                    <li>
                        <a href="http://www.airsoftmegastore.com/<?=$c->link;?>" data-name="compatible.<?=$item->id?>.<?=$c->id?>"><?=$c->name;?></a>
                    </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <!--END SEO Compatible Links-->
<?php endif;?>
<!--end compatible products module-->
<!--SIMILAR Products-->
<?php if(count($similar)): ?>
    <!--SEO Compatible Links-->
    </br>
    <div class="row margin">
        <strong>Other Products Similar to our <?=$item->name?>:</strong>
        <ul class="pull-left span-3">
            <?php foreach($similar as $c): ?>
                <li>
                    <a href="http://www.airsoftmegastore.com/<?=$c->url;?>" data-name="similar.<?=$item->id?>.<?=$c->product_id?>"><?=$c->name;?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif;?>
<?php if(count($categories)): ?>
    </br>
    <div class="row margin">
        <strong><?=$item->name?> Categories:</strong>
        <ul class="pull-left inline span-3">
            <?php foreach($categories as $c): ?>
                <li>
                    <a href="http://www.airsoftmegastore.com/<?=$c->cat_url;?>" data-name="categories.<?=$item->id?>.<?=$c->cat_id?>"><?=$c->cat_name;?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <!--END SEO Compatible Links-->
<?php endif;?>
<!--END SIMILAR Products-->

