<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header_blank.php'); ?>
<div class="container" style="min-height:700px">
<a href="/index.php/data/orders/<?=$start?>/<?=$end?>">View Orders</a>
<table class="table table-bordered">
    <tr>
        <th>Sku</th>
        <th>Name</th>
        <th>Retail</th>
        <th>Cost</th>
        <th>Quantity</th>
        <th>Revenue</th>
        <th>Weight</th>
        <th>Shipping Charged</th>
        <th>Shipping Cost</th>
        <th>Discount</th>
        <th>Profit</th>
    </tr>
    <?php $total_retail = 0.00; $total_whole_sale = 0;
    foreach($order['items'] as $key => $row): ?>
        <?php IF(is_numeric($key)): ?>
            <?php  $total_retail += $row->price * $row->quantity; ?>
            <?php  $total_whole_sale += $row->wholesale_price * $row->quantity; ?>
            <tr>
                <td>
                    <a href="/index.php/data/sku/<?=$start?>/<?=$end?>/<?=$row->sku?>"><?=$row->sku?></a>
                </td>
                <td>
                    <?=$row->name?>
                </td>
                <td>
                    $<?=$row->price?>
                </td>
                <td>
                    $<?=$row->wholesale_price ?>
                </td>
                <td>
                    <?=$row->quantity?>
                </td>
                <td>
                    $<?=$row->revenue?>
                </td>
                <td>
                    <?=$row->weight?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
        </tr>
        <?php ENDIF; ?>
    <?php endforeach; ?>
    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th>$<?=$total_retail?></th>
        <th>$<?=$total_whole_sale?></th>
        <th></th>
        <th>$<?=$order['total_revenue']?></th>
        <th><?=$order['total_weight']?></th>
        <th><?=$order["items"][0]->ship_charged;?></th>
        <th>-<?=$order["items"]['shipping_price']?></th>
        <th>-<?=$order["items"][0]->discount_amount;?> (<?php echo ceil($order["items"][0]->discount_amount / $total_retail * 10000) / 100; ?>%)</th>
        <th><?=number_format($order['profit'], 2, '.', ',')?></th>
    </tr>
    </tfoot>
</table>
</div>
<?php $this->load->view('common/footer.php'); ?>
