<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>
    <div class="container">
        <div class="row">
            <h1>Orders</h1>
            <div class="form_block">
                <h4>Choose date range</h4>
                <form action="/data/orders" method="post">
                    <input type="hidden" name="start"/>
                    <input type="hidden" name="end"/>
                    <div class="bfh-datepicker col-md-3" data-format="y-m-d" data-date="today" data-name="start"></div>
                    <div class="bfh-datepicker col-md-3" data-format="y-m-d" data-date="today" data-name="end"></div>
                    <input type="submit" name="submit" class="btn" value="get orders" />
                </form>
            </div>
            <div class="clear"></div>
            
            <div class="form_block" style="padding-top:20px">
                <h4>Manual Order Search</h4>
                <form action="/data/autopsy" method="post" role="form" class="form_inline">
                    
                    <?php IF(! empty($errors["orderId"])): ?>
                        <div class="alert alert-danger">
                            <?=$errors["orderId"];?>
                        </div>
                        <div class="clear"></div>
                    <?php ENDIF; ?>
                    <div class="form-group" style="">
                        <input type="text" class="form-control" name="orderId" value="" style="max-width:25%; float:left" />
                        <input type="submit" name="submit" class="btn" value="Search" style="margin-left:25px" />
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $this->load->view('common/footer.php'); ?>