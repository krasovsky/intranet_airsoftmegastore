<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>
<div class="container">
<h3>Orders Between <?php echo $start . ' and ' , $end ?></h3>
<div class="row">
    <div class="span6">
        <table class="table table-bordered">
            <tr><th>Orders with No Profit</th><td><?=count($without)?></td></tr>
            <tr><th>Orders with Profit</th><td><?=count($with)?></td></tr>
            <tr><th>Total Orders</th><td><?=count($with)+count($without)?></td></tr>
            <tr><th>Shipping Costs</th><td>-<?=number_format($total_shipping, 2, '.', ',')?></td></tr>
            <tr><th>Shipping Revenue</th><td><?=number_format($total_shipping_revenue,2,'.', ',')?></td></tr>
        </table>
    </div>
    <div class="span6">
        <table class="table table-bordered">
            <tr><th>Freebies</th><td><?=$total_freebies; ?></td></tr>
            <tr><th>Freebie Cost</th><td><?=number_format($freebie_cost, 2, '.', ',')?></td></tr>
            <tr><th>Freebie Shipping</th><td>-<?=number_format($freebie_shipping, 2, '.', ',')?></td></tr>
            <tr><th>Total Freebie Loss</th><td>-<?=number_format($freebie_shipping-$freebie_cost, 2, '.', ',')?></td></tr>
        </table>
    </div>
    <div class="span12">
        <table class="table table-bordered">
            <tr><th>Orders with Profit</th><td><?=number_format($with_total, 2, '.', ',')?></td></tr>
            <tr><th>Orders with Loss</th><td><?=number_format($without_total, 2, '.', ',')?></td></tr>
            <tr><th>Bottom Line</th><td><?=number_format($with_total+$without_total, 2, '.', ',')?></td></tr>
        </table>
    </div>
</div>
<div class="button_group">
    <?php if($show_with): ?>
        <?php if(! empty($filters["filter"])): ?>  
        <a href="/data/orders/<?=$start?>/<?=$end?>/withProfit"><button type="button" class="btn btn-warning" style="float:left">Filter "<?=$filters["filter"]?>" Applied <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></a>
        <?php endif; ?>
        <a href="/data/orders/<?=$start?>/<?=$end?>"><button type="button" class="btn btn-info" style="float:right">Showing Orders without Profit</button></a>
    <?php else: ?>
        <?php if(! empty($filters["filter"])): ?>  
        <a href="/data/orders/<?=$start?>/<?=$end?>/"><button type="button" class="btn btn-warning" style="float:left">Filter "<?=$filters["filter"]?>" Applied <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></a>
        <?php endif; ?>
        <a href="/data/orders/<?=$start?>/<?=$end?>/withProfit"><button type="button" class="btn btn-info" style="float:right">Showing Orders with Profit</button></a>
    <?php endif;?>
</div>
<div class="clear"></div>
    <table class="table table-bordered ">
    <tr>
        <th>Order Id</th>
        <th>Items</th>
        <th>Shipping</th>
        <th>Shipping</br>Revenue</th>
        <th>Coupon Applied</th>
        <!-- <th>Has UPS Data</th> -->
        <!-- <th>Discount</th> -->
        <th>Site</th>
        <th>Profit</th>
    </tr>
    <?php
    $set = $show_with ? $with : $without ;
    foreach($set as $row): ?>
    <?php //dump($row); ?>
    <tr>
        <td>
            <a href="https://manager.orderdynamics.com/viewOrder.aspx?orderId=<?=$row['items'][0]->order_id?>" target="_BLANK"><?=$row['items'][0]->order_id?></a>
        </td>
        <td>
            <div class="span4 pull-left"><strong>DATE :</strong> <?=$row['items'][0]->ordered_at_datetime?></div>
            <?php if($row['items']["shipping_price"] == 0):?>
                <div class="span2 pull-right" style="color:#3CC44C;"><strong>Free Shipping</strong></div>
            <?php endif;?>
            <table class="table table-bordered">
                <tr>
                    <th>Name</th>
                    <th>Sku</th>
                    <th>Quantity</th>
                    <th>Returned Quantity</th>
                    <th>Unit Price</th>
                    <th>Unit Wholesale Price</th>
                    <th>Unit Discount</th>
                    <th>Revenue</th>
                </tr>
            <?php foreach($row['items'] as $key => $item):?>

                <?php IF(is_numeric($key)): ?>
                <?php //dump($item); ?>
                    <tr>
                        <td <?php if($item->total == 0){ echo 'style="color:red"'; };?>><?=$item->name?><?php if($item->total == 0){ echo ' [freebie]'; };?></td>
                        <td><a href="/index.php/data/sku/<?=$start?>/<?=$end?>/<?=$item->sku?>"><?=$item->sku?></a></td>
                        <td><?=$item->quantity?></td>
                        <td><?=$item->returned?></td>
                        <td>$<?=$item->unit_price?></td>
                        <td>$<?=$item->wholesale_price?></td>
                        <td>$<?=$item->unit_disc?></td>                   
                        <td>$<?=$item->revenue?></td>
                    <tr>
                <?php ENDIF; ?>
            <?php endforeach; ?>
            </table>

        </td>
        <td>
            <?php if($row["items"]["ship"] != "od"): ?>
                <strike><?=$row["items"][0]->ship_charged?></strike>
                </br> <?=$row["items"]['shipping_price']?> </br>(<?=$row["items"]["ship"]?>)
            <?php else: ?>
                <?=$row["items"][0]->ship_charged?>
            <?php endif;?>
        </td>
        <td><?=$row["items"][0]->ship_charged - $row["items"]['shipping_price']?></td>
        <td><a href="/data/orders/<?=$start?>/<?=$end?>/?filter=<?=$row["items"][0]->coupon?>"><?=$row["items"][0]->coupon?></a></td>
        <td><?=$row["items"][0]->site?></td>
        <!-- <td style="<?php if ($row["items"][0]->discount_amount > 0) echo "color:red"; ?>"><?php echo number_format(($row["items"][0]->discount_amount * (-1)), 2,'.', ',') ;?></td> -->
        <!-- td><?php //if($row["items"][0]->has_ups_data == 1) echo "<img style='padding-left: 34%' src='/assets/img/icons/check73.png'/>";?></td> -->
        <td><?=number_format($row['profit'], 2, '.', ',')?></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
<?php $this->load->view('common/footer.php'); ?>
