<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>
<div class="container">
<a href="/index.php/data/orders/<?=$start?>/<?=$end?>">View Orders</a>
<h1><?=$sku;?></h1>
<h3><?=$items[key($items)]->name;?></h3>
<table class="table table-bordered">
    <tr><td>Quantity</td><td><?=$total_quantity?></td></tr>
    <tr><td>Revenue</td><td><?=$total_revenue?></td></tr>
    <tr><td>Average Shipping Cost</td><td>-<?=number_format($total_shipping/count($items), 2, '.', ',')?></td></tr>
    <tr><td>Total Shipping Cost</td><td>-<?=number_format($total_shipping, 2, '.', ',')?></td></tr>
    <tr><td>Total Profit / Loss</td><td><?=number_format($total_profit, 2, '.', ',')?></td></tr>
</table>

<table class="table table-bordered">
    <tr>
        <th>Order Id</th>
        <th>User</th>
        <th>Date</th>
        <th>Retail</th>
        <th>Cost</th>
        <th>Discount</th>
        <th>Quantity</th>
        <th>Revenue</th>
        <th>Shipping Cost</th>
        <th>Profit / Loss</th>
    </tr>
    <?php foreach($items as $row): ?>
    <tr <?php if($row->profit < 0) echo "class='disabled'"?>>
        <td>
            <a href="https://manager.orderdynamics.com/viewOrder.aspx?orderId=<?=$row->order_id?>" target="_BLANK"><?=$row->order_id?></a>
        </td>
        <td>
            <div style="width:300px;overflow:hidden;">
                <a href="https://manager.orderdynamics.com/viewCustomer.aspx?customerId=<?=$row->customer_id?>"target="_BLANK"><?=$row->customer_id?></a>
            </div>
        </td>
        <td>
            <?=$row->ordered_at_datetime?>
        </td>
        <td>
            $<?=$row->unit_price?>
        </td>
        <td>
            $<?=$row->wholesale_price?>
        </td>
        <td>
            $<?=$row->unit_disc; ?>
        </td>
        <td>
            <?=$row->quantity?>
        </td>
        <td>
            $<?=$row->revenue?>
        </td>
        <td><?=number_format($row->shipping, 2, '.', ',')?></td>
        <td><?=number_format($row->profit, 2, '.', ',')?></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
<?php $this->load->view('common/footer.php'); ?>
