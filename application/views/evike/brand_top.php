<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <h1><?=$h1?></h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="col-md-1">id</th>
            <th>name</th>
            <th class="col-md-1">total product lines</th>
            <th class="col-md-1">total stock</th>
            <th class="col-md-1">units moved</th>
            <th class="col-md-1">restocked</th>
            <th class="col-md-1">revenue</th>
        </tr>
        </thead>
        <?php foreach($result as $row):?>
        <tr>
            <td><a href="/index.php/evike/brand/<?=$row->id?>"><?=$row->id?></a></td>
            <td><a href="/index.php/evike/brand/<?=$row->id?>"><?=$row->name?></a></td>
            <td><?=$row->product_line?></td>
            <td><?=$row->total_inventory?></td>
            <td><?=$row->units_moved?></td>
            <td><?=$row->restocked?></td>
            <td>$<?= number_format( $row->revenue * .60 ) ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

<?php $this->load->view('common/footer.php'); ?>
