<?php $this->load->view('common/header.php'); ?>
<script type="text/javascript">
    var AMS = AMS || {};
    AMS.Brand = {
        ready:function(){
            var that = this;
            $('.bfh-selectbox').removeClass('hide');

            $(document.body).on('change.bfhselectbox',function(e){
                var input = $(e.target).find('input[type=hidden]');
                var data = {
                    id: input.attr('name'),
                    ams_id: input.val()
                };

                $.post('/evike/updateBrand', data, function(response){
                    console.log(response);
                });
            });

        }
    }
</script>
<div class="container">
    <h1><?=$h1?></h1>
    <table class="table table-striped">
     <thead>
         <tr>
             <th>brand Id</th>
             <th>Evike Brand</th>
             <th>AMS Brand</th>
         </tr>
     </thead>

     <?php foreach($result as $row):?>
     <tr>
         <td><a href="/index.php/evike/brand/<?=$row->id?>"><?=$row->id?></a></td>
         <td><a href="/index.php/evike/brand/<?=$row->id?>"><?=$row->name?></a></td>
         <td><div class="bfh-selectbox hide" data-name="<?=$row->id?>" data-value="<?=$row->ams_id?>" data-filter="true">
                 <div data-value=""> - </div>
                 <?php foreach($brands as $b): ?>
                     <div data-value="<?=$b->id?>"><?=$b->name?></div>
                 <?php endforeach;?>
             </div></td>
     </tr>
     <?php endforeach; ?>
    </table>
</div>
<script type="text/tmpl" id="brands">
    <select name="ams_id" id="selectbox">
        <?php foreach($brands as $b): ?>
            <options value="<?=$b->id?>"><?=$b->name?></options>
        <?php endforeach;?>
    </select>
</script>
<?php $this->load->view('common/footer.php'); ?>
