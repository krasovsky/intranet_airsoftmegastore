<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <div class="row">
        <h1 class="col-md-8">Product Management</h1>
        <div class="col-md-4">
            <button class="pull-right btn btn-primary">Connect AMS Product</button>
        </div>
    </div>
    <table class="table table-bordered table-striped">
        <tr>
            <th class="col-md-1"></th>
            <th class="col-md-1">id</th>
            <th class="col-md-2">brand</th>
            <th class="col-md-6">name</th>
            <th class="col-md-1">price</th>
            <th class="col-md-1">inventory</th>
        </tr>
        <tr>
            <th>evike</th>
            <td><?=$product->product_id?></td>
            <td><?=$product->brand?></td>
            <td><?=$product->name?></td>
            <td><?=@$prices[count($prices)-1]['price']?:'-'?></td>
            <td><?=@$inventory[count($inventory)-1]['inventory']?:'-'?></td>
        </tr>
        <tr>
            <th>ams</th>
            <td><?=@$ams_product->id?:'-'?></td>
            <td><?=@$ams_product->brand?:'-'?></td>
            <td><?=@$ams_product->name?:'-'?></td>
            <td><?=@$prices[count($prices)-1]['price']?:'-'?></td>
            <td><?=@$inventory[count($inventory)-1]['inventory']?:'-'?></td>
        </tr>
    </table>

<!--    <div class="row">-->
<!--        <div class="col-md-6">-->
<!--            <table class="table table-bordered table-striped">-->
<!--                <tr>-->
<!--                    <th class="col-md-2">Evike</th>-->
<!--                    <th class="col-md-1">sold</th>-->
<!--                    <th class="col-md-1">revenue</th>-->
<!--                    <th class="col-md-1">restocked</th>-->
<!--                    <th class="col-md-1">cost</th>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Year</th>-->
<!--                    <td>--><?//=$rollup_inventory['year']['sold'];?><!--</td>-->
<!--                    <td>3,800.00</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Month</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Week</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Yesterday</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--            </table>-->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            <table class="col-md-6 table table-bordered table-striped">-->
<!--                <tr>-->
<!--                    <th class="col-md-2">AMS</th>-->
<!--                    <th class="col-md-1">inventory</th>-->
<!--                    <th class="col-md-1">sold</th>-->
<!--                    <th class="col-md-1">restocked</th>-->
<!--                    <th class="col-md-1">net profit</th>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Year</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Month</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Week</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Yesterday</th>-->
<!--                    <td>134</td>-->
<!--                    <td>24</td>-->
<!--                    <td>24</td>-->
<!--                    <td>3,800.00</td>-->
<!--                </tr>-->
<!--            </table>-->
<!--        </div>-->
<!--    </div>-->

    <div class="row">
        <h4 class="col-md-6">price over time</h4>
        <h4 class="col-md-6">inventory over time</h4>
    </div>
    <div class="row">
        <canvas id="prices" class="col-md-6"></canvas>
        <canvas id="inventory" class="col-md-6"></canvas>
    </div>
    <div class="row">
<!--        <div class="span-2 btn-group pull-right">-->
<!--            <a href="/index.php/evike/product/--><?//=$product->product_id?><!--/30" class="btn">30</a>-->
<!--            <a href="/index.php/evike/product/--><?//=$product->product_id?><!--/60" class="btn">60</a>-->
<!--            <a href="/index.php/evike/product/--><?//=$product->product_id?><!--/90" class="btn">90</a>-->
<!--            <a href="/index.php/evike/product/--><?//=$product->product_id?><!--/120" class="btn">120</a>-->
<!--            <a href="/index.php/evike/product/--><?//=$product->product_id?><!--/150" class="btn">150</a>-->
<!--        </div>-->
    </div>
</div>
<div class="row">
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</div>

<script type="text/javascript">
    var prices = {
        labels: [
            <?php
            $events = array();
            foreach($prices as $row){
                $events[] =  $row['timestamp'];
            }
            echo '"'.implode('","',$events).'"';
            ?>
        ],
        datasets: [
            {
                label: "Add To Cart Events",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [
                    <?php
                    $events = array();
                    foreach($prices as $row){
                        $events[] =  $row['price'];
                    }
                    echo implode(',',$events);
                    ?>
                ]
            }
        ]
    };

    var inventory = {
        labels: [
            <?php
            $events = array();
            foreach($inventory as $row){
                $events[] =  $row['timestamp'];
            }
            echo '"'.implode('","',$events).'"';
            ?>

        ],
        datasets: [
            {
                label: "Add To Cart Events",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [
                    <?php
                    $events = array();
                    foreach($inventory as $row){
                        $events[] =  $row['inventory'];
                    }
                    echo implode(',',$events);
                    ?>
                ]
            }
        ]
    };

    var AMS = AMS || {};
    AMS.Page = {
        ready:function(){
            var pricesChart = $('#prices').get(0).getContext("2d");
            var inventoryChart = $('#inventory').get(0).getContext("2d");

            var myPrices = new Chart(pricesChart
            ).Line(prices, {
                    scaleOverride: true,
                    scaleSteps:10,
                    scaleStepWidth:10,
                    scaleBeginAtZero:true
                });
            var myInventory = new Chart(inventoryChart
            ).Line(inventory, {
                    scaleOverride: true,
                    scaleSteps:10,
                    scaleStepWidth:10,
                    scaleBeginAtZero:true
                });
        }
    }
</script>

</div>
<?php $this->load->view('common/footer.php'); ?>
