<?php $this->load->view('common/header.php'); ?>
<script type="text/javascript">
    var AMS = AMS || {};
    AMS.Brands = [
        <?= $brands ?>
    ];
    AMS.modal = {
        ready:function(){
            $('[data-modal]').on('click',function(){
                var brandId = parseInt($(this).attr('data-modal'));

                if(~AMS.Brands.indexOf(brandId)){
                    $.get('/evike/brandProducts/'+brandId, function(data){
                        var dropdown = $(data);
                        $(dropdown).bfhselectbox({filter:true});
                        $('#brandsModal [data-target]').html(dropdown);
                       $('#brandsModal').modal();
                    });
                }else{
                    $('#noBrandsModal').modal();
                }
            });
        },
    };
</script>
<div class="container">
    <h1><?=$h1?></h1>
    <table class="table table-striped">
     <thead>
         <tr>
             <th class="col-md-1">id</th>
             <th>name</th>
             <?php if(isset($result[0]->timestamp)):
                 echo '<th class="col-md-2">Timestamp</th>';
             endif; ?>
             <th class="col-md-2">Brand</th>
             <th class="col-md-1">Inventory</th>
             <th class="col-md-1"></th>
         </tr>
     </thead>
     <?php foreach($result as $row):?>
     <tr>
         <td><a href="/index.php/evike/product/<?=$row->product_id?>"><?=$row->product_id?></a></td>
         <td><a href="/index.php/evike/product/<?=$row->product_id?>"><?=$row->name?></a></td>
         <?php if(isset($row->timestamp)): ?>
            <td><a href="/index.php/evike/product/<?=$row->product_id?>"><?=$row->timestamp?></a></td>
         <?php endif; ?>
         <td><a href="/index.php/evike/brand/<?=$row->brand_id?>"><?=$row->brand?></a></td>
         <td><a href="/index.php/evike/product/<?=$row->product_id?>"><?=$row->inventory==100?'100+':$row->inventory?></a></td>
         <td><button class="btn" data-modal="<?=$row->brand_id?>">Update</button></td>
     </tr>
     <?php endforeach; ?>
    </table>
    <div class="pagination text-center">
        <?php $this->load->view('common/pagination',array('path'=>'/evike/exclusive/')); ?>
    </div>
</div>
<div class="modal fade" id="brandsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Product Connection</h4>
            </div>
            <div class="modal-body">
                <p>Select a product from our list below. Products are populated from our Brand's product line.</p>
                <div data-target="true"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="noBrandsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">No Brand Connection</h4>
            </div>
            <div class="modal-body">
                <div data-id="nobrand">
                    We don't have this Brand connected. Please <a href="/evike/exclusiveBrands">connect Brand</a> to access all similar products.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->load->view('common/footer.php'); ?>
