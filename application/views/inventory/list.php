<?php $this->load->view('common/header'); ?>
<div class="container">
    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Brand</th>
                    <th>Inventory</th>
                    <th>AMS id</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($products as $p): ?>
                <tr>
                    <td><a href="/evike/product/<?= $p->product_id ?>"><?= $p->name ?></a></td>
                    <td><a href="/evike/brand/<?= $p->brand_id ?>"><?= $p->brand ?></a></td>
                    <td><?php
                        if((int)$p->inventory > 0 ) echo $p->inventory;
                        elseif((int)$p->inventory == -1) echo '<a href="/evike/discontinued">discontinued</a>';
                        elseif((int)$p->inventory == -2) echo '100 or more';
                        else echo '<a href="/evike/outofstock">out of stock</a>';
                        ?></td>
                    <td><a href="http://www.airsoftmegastore.com/<?= $p->ams_id ?>-" target="_blank"><?= $p->ams_id ?></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
<?php $this->load->view('common/footer'); ?>