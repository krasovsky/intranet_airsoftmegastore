<?php $this->load->view('common/header.php'); ?>
<script type="text/javascript">
    var AMS = AMS || {}

    AMS.DeleteButton = {
        ready:function(){
            $('.remove').click(function(e){
                e.preventDefault();
                if(confirm("This can not be undone, do you wish to remove this entry?")){
                    location.href = this.href;
                }
            });
        }
    };
</script>
<?php //dump($dataList); ?>
<div class="container">
<div class="row">
    <div class="col-md-10">
        <h1>All SKUS</h1>
    </div>
    <div class="col-md-2">
<!--        <a href="/competitors/updateIndex" class="pull-right btn btn-default">reindex page</a>-->
    </div>
</div>
<div class="container">
    <div class="row" style="">
        <div class="col-md-4 col-md-offset-8">
            <form onsubmit="form.submitForm(); return false;" class="search-form">
                <div class="form-group has-feedback">
                    <label for="search" class="sr-only">Search</label>
                    <input type="text" class="form-control" name="search" id="search" placeholder="search" data-provide="typeahead" list="skus-datalist">
                    <datalist id="skus-datalist">
                        <?php FOREACH($dataList as $element): ?>
                            <option value="<?=$element["sku"]?>" key="<?=$element["id"]?>">
                        <?php ENDFOREACH; ?>
                    </datalist>
                    <div id="skus-datalist-json" style="display:none">
                        <?php echo json_encode($dataList); ?>
                    </div>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>
        </div>
    </div>
</div>
    <table class="table table-striped table-bordered">
        <tr>
            <th>AMS SKU</th>
            <th>AMS</th>
            <th>Evike</th>
            <th>Airsoft GI</th>
<!--            <th>Airsplat</th>-->
<!--            <th>Airrattle</th>-->
<!--            <th>Last Summary Update</th>-->
        </tr>
        <?php foreach($skus as $sku): ?>
        <tr>
            <td><a href="/competitors/viewProduct/<?=$sku->product_id;?>"><?=$sku->sku?$sku->sku:$sku->name;?></a></td>
            <td class="<?= $sku->lowest == 'price' ? 'lowest' : '' ?> <?= $sku->inventory <= 0 ? 'strikethrough' : '' ?>"><?=($sku->price)?$sku->price:'';?></td>
            <td class="<?= $sku->lowest == 'evike' ? 'lowest' : '' ?> <?= $sku->evike_inventory <= 0 ? 'strikethrough' : '' ?>"><?=($sku->evike)?$sku->evike:'';?></td>
            <td class="<?= $sku->lowest == 'airsoftgi' ? 'lowest' : '' ?> <?= $sku->airsoftgi_inventory <= 0 ? 'strikethrough' : '' ?>"><?=($sku->airsoftgi)?$sku->airsoftgi:'';?></td>
       <?php /*     <td class="<?= $sku->lowest == 'airsplat' ? 'lowest' : '' ?> <?= $sku->airsplat_inventory <= 0 ? 'strikethrough' : '' ?>"><?=($sku->airsplat)?$sku->airsplat:'';?></td>
            <td class="<?= $sku->lowest == 'airrattle' ? 'lowest' : '' ?> <?= $sku->airrattle_inventory <= 0 ? 'strikethrough' : '' ?>"><?=($sku->airrattle)?$sku->airrattle:'';?></td>
            <td><?=$sku->updated;?></td> */?>
        </tr>
        <?php endforeach; ?>
    </table>
    <div class="text-center">
        <?php $this->load->view('common/pagination.php', array('path'=>'/competitors/viewAll/')); ?>
    </div>
    <script>
        var form = Object();
        form.submitForm = function(){
            var inputVal = $("input[id=search]").val();

            var dataList = JSON.parse($("#skus-datalist-json").html());
            if (dataList[inputVal]){
                key =  dataList[inputVal]["id"];
                window.location.replace("/competitors/viewProduct/" + key);
            }
            else {
                alert(inputVal + " not FOUND! Try again!");
            }
        }
    </script>
<?php $this->load->view('common/footer.php'); ?>
