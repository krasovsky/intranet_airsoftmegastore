<?php $this->load->view('common/header.php'); ?>
<script type="text/javascript">
    var AMS = AMS || {}
    AMS.LatestPrice = {
        ready:function(){
            $('#latestPrices').click(function(e){
                $(e.target).button('loading')
            });
        }
    }
</script>
<div class="container">

<?php if(!isset($urls->sku)): ?>
<!--WE DON'T HAVE DATA-->
<a href="/competitors/" class="btn">Back All SKUs</a>
<h1>We don't have any data for: <?= $sku ?></h1>
   <form action="/competitors/saveurls" method="post">
    <input type="hidden" name="sku" value="<?= $sku; ?>" />
    <p>
        Add the product urls for any of our competitors with same or similar product to compare our prices<br/>
        ( http://www.competitor.com/product_page.html )
    </p>
    <table class="table table-striped">
        <tr><td><label>Evike</label></td><td><input type="text" name="evike" value="" class="input-xxlarge"/></td></tr>
        <tr><td><label>Airsoft GI</label></td><td><input type="text" name="airsoftgi" value="" class="input-xxlarge"/></td></tr>
<!--        <tr><td><label>Airsplat</label></td><td><input type="text" name="airsplat" value="" class="input-xxlarge"/></td></tr>-->
<!--        <tr><td><label>Airrattle</label></td><td><input type="text" name="airrattle" value="" class="input-xxlarge"/></td></tr>-->
        <tr><td colspan="2"><input type="submit" name="update" value="Add Competitor Product Pages" class="btn btn-block btn-warning"/></td></tr>
    </table>
   </form>
<? else: ?>
<div class="row">


<a href="/competitors/" class="btn">Back All SKUs</a>
<!--WE DO HAVE DATA-->
<!--    <a href="/index.php/competitors/updatePrices/--><?//=$urls->sku;?><!--" class="pull-right btn btn-primary" style="margin:20px;" id="latestPrices" data-loading-text="Updating...">Get Latest Prices</a>-->
    <a href="https://manager.orderdynamics.com/editProductPricing.aspx?pId=<?=$itemId;?>&sId=0" class="pull-right btn" style="margin:20px;" target="_blank">Update AMS Price</a>
    <h1><?=$urls->sku?></h1>

    <div class="col-md-4">

            <table class="table table-bordered">
                <tr><th class="col-md-3">Wholesale</th><td class="col-md-1">$<?=$wholesale?></td></tr>
                <tr><th>Retail</th><td>$<?=($ams->price)?$ams->price:'';?></td></tr>
                <tr><th>Gross Margin</th><td><?=$gross?>%</td></tr>
            </table>

            <table class="table table-bordered">
                <tr <?=($ams->inventory==0)? 'style="background-color:red;color:#fff; font-weight:bold"' : ''?>>
                    <th class="col-md-3">Inventory</th><td class="col-md-1"><?=$ams->inventory?></td>
                </tr>
                <tr><th>40% Margin Price</th><td>$<?=$fourtyPercent?></td></tr>
                <tr><th>Break Even (20%)</th><td>$<?=$lowestPossible?></td></tr>
            </table>

    </div>
    <div class="col-md-8">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th class="col-md-2">price</th>
                    <th class="col-md-2">instock</th>
                    <th class="col-md-3">last update</th>
                </tr>
            </thead>
            <tr <?= $lowest == 'price' ? 'class="lowest"' : ''?>>
                <th>AMS</th>
                <td><?=@($ams->price)?$ams->price:'';?></td>
                <td><?=@($ams->inventory>0)?$ams->inventory:'0';?></td>
                <td><?=@$ams->pricing_update?></td>
            </tr>
            <tr <?= $lowest == 'evike' ? 'class="lowest"' : '' ?>>
                <th>Evike</th>
                <td><?=@($evike->price)?$evike->price:'';?></td>
                <td><?=@($evike->inventory>0)?($evike->inventory<100)?$evike->inventory:'100+':'0';?></td>
                <td><?=@$evike->pricing_update?></td>
            </tr>
            <tr<?= $lowest == 'airsoftgi' ? 'class="lowest"' : '' ?>>
                <th>Airsoft GI</th>
                <td><?=@($airsoftgi->price)?$airsoftgi->price:'';?></td>
                <td><?=@($airsoftgi->inventory>0)?$airsoftgi->inventory:'0';?></td>
                <td><?=@$airsoftgi->pricing_update?></td>
            </tr>
            <?/*<tr <?= $lowest == 'airsplat' ? 'class="lowest"' : '' ?>>
                <th>Airsplat</th>
                <td><?=@($airsplat->price)?$airsplat->price:'';?></td>
                <td><?=@($airsplat->inventory>0)?'yes':'no';?></td>
                <td><?=@$airsplat->pricing_update?></td>
            </tr>
            <tr <?= $lowest == 'airrattle' ? 'class="lowest"' : '' ?>>
                <th>Airrattle</th>
                <td><?=@($airrattle->price)?$airrattle->price:'';?></td>
                <td><?=@($airrattle->inventory>0)?'yes':'no';?></td>
                <td><?=@$airrattle->pricing_update?></td>
            </tr>*/?>
        </table>
    </div>

<div class=" col-md-12">

    <p>
        Add the product urls for any of our competitors with same or similar product to compare our prices<br/>
        ( http://www.competitor.com/product_page.html )
    </p>
    <form action="/competitors/saveurls" method="post">
    <input type="hidden" name="sku" value="<?= $urls->sku; ?>" />
    <table class="table table-striped">
        <tr><td><label>Evike</label></td><td><input type="text" name="evike" value="<?=isset($urls->evike)?$urls->evike:'';?>" style="width:100%"/></td></tr>
        <tr><td><label>Airsoft GI</label></td><td><input type="text" name="airsoftgi" value="<?=isset($urls->airsoftgi)?$urls->airsoftgi:'';?>" style="width:100%" /></td></tr>
<!--        <tr><td><label>Airsplat</label></td><td><input type="text" name="airsplat" value="--><?//=isset($urls->airsplat)?$urls->airsplat:'';?><!--" style="width:100%" /></td></tr>-->
<!--        <tr><td><label>Airrattle</label></td><td><input type="text" name="airrattle" value="--><?//=isset($urls->airrattle)?$urls->airrattle:'';?><!--" style="width:100%"/></td></tr>-->
        <tr><td colspan="2"><input type="submit"name="update" value="Update Competitor Urls" class="btn btn-block btn-warning"/></td></tr>
    </table>
    </form>
</div>
</div>


<!--    <table class="table table-striped">-->
<!--        <tr>-->
<!--           <th>name</th>-->
<!--           <td><a href="#" id="name" data-type="text" data-pk="1" data-original-title="Enter Product Name" class="editable editable-click">--><?//= $item->name; ?><!--</a></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <th>Description</th>-->
<!--            <td>-->
<!--            <div class="accordion" id="accordion2">-->
<!--                <div class="accordion-group">-->
<!--                    <div class="accordion-heading">-->
<!--                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">-->
<!--                            Description-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div id="collapseOne" class="accordion-body collapse in">-->
<!--                        <div class="accordion-inner">-->
<!--                            --><?//= $item->long_description; ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="accordion-group">-->
<!--                    <div class="accordion-heading">-->
<!--                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">-->
<!--                            Features-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div id="collapseTwo" class="accordion-body collapse">-->
<!--                        <div class="accordion-inner">-->
<!--                            <a href="#" id="features" data-type="textarea" data-pk="1">--><?//= $item->features; ?><!--</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="accordion-group">-->
<!--                    <div class="accordion-heading">-->
<!--                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">-->
<!--                            Specs-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div id="collapseThree" class="accordion-body collapse">-->
<!--                        <div class="accordion-inner">-->
<!--                            <a href="#" id="specs" data-type="textarea" data-pk="1">--><?//= $item->specs; ?><!--</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->
</div>
<? endif; ?>
<?php $this->load->view('common/footer.php'); ?>
