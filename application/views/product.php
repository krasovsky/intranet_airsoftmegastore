<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <table class="table table-striped">
        <tr>
           <th>name</th>
           <td><a href="#" id="name" data-type="text" data-pk="1" data-original-title="Enter Product Name" class="editable editable-click"><?= $item["name"]; ?></a></td>
        </tr>
        <tr>
            <th>Description</th>
            <td>
            <div class="accordion" id="accordion2">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Description
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <div class="accordion-inner">
                            <?= $item["long_description"]; ?>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                            Features
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <a href="#" id="features" data-type="textarea" data-pk="1"><?= $item["features"]; ?></a>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                            Specs
                        </a>
                    </div>
                    <div id="collapseThree" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <a href="#" id="specs" data-type="textarea" data-pk="1"><?= $item["specs"]; ?></a>
                        </div>
                    </div>
                </div>
            </div>
            </td>
        </tr>
    </table>
    <div class="clear"></div>
    <div class="grid">
    
    <?php if(! empty($gridData) && ! empty($gridData[0])): ?>
        <h3>Sale Rate</h3>
        <canvas id="myChart" style="width:100%; height:400px"></canvas>
        <div id="chart_data" class="hidden"><?php echo json_encode($gridData);?></div>
        <div id="chart_header" class="hidden"><?php echo json_encode($gridHeader);?></div>
        <script>
            $(document).ready(function() {
                var gridData = JSON.parse($("#chart_data").text());
                var gridHeader = JSON.parse($("#chart_header").text())
                var data = {
                    labels: gridHeader,
                    datasets: [
                        {
                            label: "Sale_Rate",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: gridData
                        }
                    ]
                };
                var ctx = $("#myChart").get(0).getContext("2d");
                var myLineChart = new Chart(ctx).Line(data);
            });
        </script>
    <?php endif;?>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>
