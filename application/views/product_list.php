<?php $this->load->view('common/header.php'); ?>
<div class="container">
    <table class="table table-striped">
     <thead>
         <tr>
             <th>sku</th>
             <th>name</th>
         </tr>
     </thead>
     <?php foreach($result as $row):?>
     <tr>
         <td><a href="/index.php/products/item/<?=$row->id?>"><?=$row->sku?></a></td>
         <td><a href="/index.php/products/item/<?=$row->id?>"><?=$row->name?></a></td>
     </tr>
     <?php endforeach; ?>
    </table>
    <div class="text-center">
    <?php
        $path = (empty($control)) ? "/products/page/" : "/products/{$control}/"
    ?>
        <?php $this->load->view('common/pagination.php', array('path'=>$path )); ?>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>
