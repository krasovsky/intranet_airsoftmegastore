<?php $this->load->view('common/header.php'); ?>
    <div class="container">
    <style>
        .control-label{
            width:200px;
        }
    </style>
    <?php 
        if(empty($data)){
            $data = array();
            $data["start_date"] = date("m/d/Y h:i A");
            $data["end_date"] =  date("m/d/Y h:i A");
        }
    ?>
    <script type="text/javascript" src="/assets/js/markitup/jquery.markitup.js"></script>
    <script type="text/javascript" src="/assets/js/markitup/sets/product/set.js"></script>
    <script type="text/javascript" src="/assets/js/markitup/sets/cart/set.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/js/markitup/skins/markitup/style.css" />
    <link rel="stylesheet" type="text/css" href="/assets/js/markitup/sets/cart/style.css" />
    <?php //dump($errors); ?>
        <form class="form-horizontal" action="/promotions/save" method="post">
            <fieldset>

                <!-- Form Name -->
                <legend>Coupon Editor</legend>

                <!-- Text input-->
                <div class="form-group" style="float:left">
                    <label class="col-md-4 control-label" for="textinput">ID</label>

                    <div class="col-md-4">
                        <input id="textinput" name="number" type="text" placeholder="" class="form-control input-sm bfh-numbe" style="width:80px" disabled value="<?=@$data["id"]?>">
                        <input id="id" name="id" type="hidden" placeholder="" style="" value="<?=@$data["id"]?>">
                        <span class="help-block">coupon id</span>
                    </div>
                </div>


                 <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="promo_name">Promo Name</label>

                    <div class="col-md-6">
                        <?php if(! empty($errors["promo_name"])) echo showErrorMessage($errors["promo_name"]);?>
                        <input id="promo_name" name="promo_name" type="text" placeholder="" value="<?=@$data["promo_name"]?>"
                               class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group" style="float:left">
                    <label class="col-md-4 control-label" for="promo_code">Promo Code</label>
                    <div class="col-md-4">
                        <?php if(! empty($errors["promo_code"])) echo showErrorMessage($errors["promo_code"]);?>
                        <input id="promo_code" name="promo_code" type="text" placeholder="Ex: Save75" value="<?=@$data["promo_code"]?>" class="form-control input-md" required="" style="width:100px">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="discount_value">Discount Value</label>

                    <div class="col-md-2">
                        <?php if(! empty($errors["discount_value"])) echo showErrorMessage($errors["discount_value"]);?>
                        <input id="discount_value" name="discount_value" type="number" placeholder="Ex: 75" value="<?=@$data["discount_value"]?>"
                               class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group" style="float:left; width:50%">
                    <label class="col-md-4 control-label"for="description">Description</label>

                    <div class="col-md-4">
                        <?php if(! empty($errors["description"])) echo showErrorMessage($errors["description"]);?>
                        <textarea class="form-control col-xs-12" style="width:390px" rows="6" id="description" name="description" required=""><?=@$data["description"]?></textarea>
                    </div>

                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="product_list">List of Products</label>

                    <div class="col-md-4">
                        <?php if(! empty($errors["product_list"])) echo showErrorMessage($errors["product_list"]);?>
                        <textarea class="form-control col-xs-12" style="width:100%" rows="6" id="product_list" name="product_list" required=""> <?=@$data["product_list"]?> </textarea>
                    </div>
                </div>
    
                 <!-- Textarea -->
                <div class="form-group" >
                    <label class="col-md-4 control-label"for="description">Shopping Card Promo Text</label>

                    <div class="col-md-4">
                        <?php if(! empty($errors["promo_card_text"])) echo showErrorMessage($errors["promo_card_text"]);?>
                        <textarea class="form-control col-xs-12" style="width:677px" rows="6" id="promo_card_text" name="promo_card_text" required=""><?=@$data["promo_card_text"]?></textarea>
                    </div>

                </div>

                 <!-- Textarea -->
                <div class="form-group" >
                    <label class="col-md-4 control-label"for="description">Product Page Promo Text</label>

                    <div class="col-md-4">
                        <?php if(! empty($errors["promo_product_text"])) echo showErrorMessage($errors["promo_product_text"]);?>
                        <textarea class="form-control col-xs-12" style="width:677px" rows="6" id="promo_product_text" name="promo_product_text" required=""><?=@$data["promo_product_text"]?></textarea>
                    </div>

                </div>
                
                

                <!-- Text input-->
                <div class="form-group">
                    <!-- START DATE START-->
                    <label class="col-md-4 control-label" for="start_date">Start Date</label>
                    <div class="col-md-2">
                        <input id="start_date" name="start_date" type="text" placeholder="" class="form-control input-md" required="" value="<?=date("m/d/Y h:i A" , strtotime(@$data["start_date"]))?>">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                    </div>
                    <!-- START DATE END-->

                    <!-- END DATE START-->
                    <label class="col-md-4 control-label" for="end_date" style="width: 110px;">End Date</label>
                    <div class="col-md-2">
                        <input id="end_date" name="end_date" type="text" placeholder="" class="form-control input-md" required="" value="<?=date("m/d/Y h:i A" , strtotime(@$data["end_date"]))?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    
                    <!-- END DATE START-->
                    <script type="text/javascript">
                        $(function () {
                            $('#start_date').datetimepicker({
                                defaultDate: moment()
                            });
                            $('#end_date').datetimepicker({
                                defaultDate: moment(),
                                useCurrent: false //Important! See issue #1075
                            });
                            $("#start_date").on("dp.change", function (e) {
                                $('#end_date').data("DateTimePicker").minDate(e.date);
                            });
                            $("#end_date").on("dp.change", function (e) {
                                $('#start_date').data("DateTimePicker").maxDate(e.date);
                            });
                        });
                    </script>
                </div>

                <!-- Multiple Radios -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="active">Active</label>
                    <div class="col-md-4 <?php if(@$data["active"] == 1) echo "active"; elseif (@$data["active"] == 2)  echo "disabled"; ?>" style="height: 36px; max-width: 205px;">
                        <div class="radio radio-inline">
                            <label for="active-0 radio-inline">
                                <input type="radio" name="active" id="active-0" value="1" <?php if(empty($data["active"]) || $data["active"] == 1) echo "checked"; ?> >
                                Yes
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <label for="active-1 radio-inline">
                                <input type="radio" name="active" id="active-1" value="2" <?php if(! empty($data["active"]) && $data["active"] == 2) echo "checked"; ?> >
                                No
                            </label>
                        </div>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div class="col-md-4" style="float:right">
                        <button id="save" name="save" class="btn btn-primary btn-lg">SAVE</button>
                    </div>
                </div>

            </fieldset>

            <script type="text/javascript" >
               $(document).ready(function() {
                    $("#promo_card_text").markItUp(myHtmlCartSettings);
                    $("#promo_product_text").markItUp(myHtmlSettings);
               });
            </script>
        </form>

    </div>
<?php $this->load->view('common/footer.php'); ?>