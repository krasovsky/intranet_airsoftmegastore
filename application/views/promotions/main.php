<?php $this->load->view('common/header.php'); ?>
<?php //dump($data); ?>
<div class="container">
<style>
	.table > tbody > tr.active > td {
		background-color: inherit;
		max-width: 350px;
    	word-wrap: break-word;
	}
	.table > tbody > tr > td {
		text-align: center;
	    width: 300px;
	    max-width: 300px;
	    word-wrap: break-word;
	}
</style>
<h3>Coupons Manager</h3>
<div class="button_group">
	<a href="/promotions/coupon/"> <button type="button" class="btn btn-primary" style="float:right">Add New</button> </a>
</div>
<div class="clear"></div>
<table class="table-bordered table" id="coupons-table">
	<thead>
    	<tr>
	        <th>Id</th>
	        <th>Promo Name</th>
	        <th>Promo Code</th>
	        <th>Discount</th>
	        <th>Description</th>
	        <th>Start Date</th>
	        <th>End  Date</th>
	        <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    	<?php foreach(@$data as $element): ?>
    		<tr class="<?php if($element["active"] == 2) echo "disabled"; elseif (strtotime($element["start_date"]) < time() && time() < strtotime($element["end_date"]) ) echo "active"; ?>">

    			<td><?=$element["id"]?></td>

    			<td><a href="/promotions/coupon/<?=$element["id"]?>"><?=$element["promo_name"]?></a></td>
    		
    			<td><?=$element["promo_code"]?></td>
    		
    			<td><?=$element["discount_value"]?></td>
    		
    			<td><?=$element["description"]?></td>
    		
    			<td><?=date("m-d-Y h:i A", strtotime($element["start_date"]));?></td>

    			<td><?=date("m-d-Y h:i A", strtotime($element["end_date"]));?></td>

    			<td><a href="/promotions/coupon/<?=$element["id"]?>"><button type="button" class="btn btn-info">Edit</button></a></td>
    		</tr>

    	<?php endforeach; ?>
    </tbody>
</table>
</div>

<?php if($saved == true):?>
	<script>
		$.notify({
			// options
			message: 'Coupon successfully saved!' 
		},{
			// settings
			type: 'info'
		});
	</script>
<?php endif?>
<?php $this->load->view('common/footer.php'); ?>