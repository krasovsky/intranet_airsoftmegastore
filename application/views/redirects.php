<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 9/24/13
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */
$this->load->view('common/header');
?>
<div class="container">
    <h1 class="bebas">AMS 404 Redirect Tool</h1>
    <div class="span12">
        <input type="text" class="span10" name="subject" value="<?=$subject?>" />
    </div>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Product urls</th><th>Score</th><th>actions</th>
        </tr>
        <?php foreach($redirects as $item): ?>
        <tr>
            <td><?=$item['url']?></td>
            <td><?=$item['score']?></td>
            <td><span class="btn">redirect here</span></td>
        </tr>
        <?php endforeach; ?>
    </table>
    </div>
</div>
<?php
$this->load->view('common/footer');
?>