<div class="container">
        <div class="row">
            <h4>Choose date range</h4>
            <form action="/reports/process" method="post">
                <input type="hidden" value="<?php if(!empty($start)) echo $start; ?>" name="start"/>
                <input type="hidden" value="<?php if(!empty($end)) echo $end; ?>"name="end"/>
                <div class="bfh-datepicker col-md-3" style="padding-left:0;" data-format="y-m-d" data-date="<?php if(!empty($start)) echo $start; else echo "today"; ?>" data-name="start"></div>
                <div class="bfh-datepicker col-md-3" data-format="y-m-d" data-date="<?php if(!empty($end)) echo $end; else echo "today"; ?>" data-name="end"></div>
                <input type="submit" name="submit" class="btn" value="Generate" />
            </form>
        </div>
    </div>