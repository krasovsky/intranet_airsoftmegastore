<?php $this->load->view('common/header.php'); ?>
<style>
	td.highlight {
    	background-color: whitesmoke !important;
	}
</style>
<div class="container">
<h3>Master Report</h3> 
<a href="#" class="export" style="float:right">Export</a>
<div class="clear"></div>
	<table name="master-report" id="sku-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>sku</th>
			<th>product id</th>
			<th>item id</th>
			<th>MPN</th>
			<th>Inventory</th>
			<th>30day sale</th>
			<th>90day sale</th>
			<th>run rate (per day)</th>
			<th>high season rate(per day)</th>
			<th>sell through(in days)</th>
			<!-- <th>restock date</th> -->
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>sku</th>
			<th>product id</th>
			<th>item id</th>
			<th>MPN</th>
			<th>Inventory</th>
			<th>30day sale</th>
			<th>90day sale</th>
			<th>run rate (per day)</th>
			<th>high season rate(per day)</th>
			<th>sell through(in days)</th>
			<!-- <th>restock date</th> -->
		</tr>
	</tfoot>
	<tbody>
		<?php foreach($data as $sku => $element): ?>
			<?php if(!empty($element["item_id"])):?>
			<tr>
				<td><a href="/index.php/products/item/<?=$element["item_id"]?>" target="_blank"><?=$sku?></a></td>
				<td><?=$element["product_id"]?></td>
				<td><?=$element["item_id"]?></td>
				<td><?=$element["mpn"]?></td>
				<td><?=$element["inventory"]?></td>
				<td><?=$element["30_days_sales"]?></td>
				<td><?=$element["90_days_sales"]?></td>
				<td><?php $rate = ceil($element["365_days_sales"] * 10000 / 365) / 10000; echo $rate;?></td>
				<td><?php if(empty($element["high_season_sale"])) echo "N/A"; else echo  ceil($element["high_season_sale"] *100 / 90) / 100; ?></td>
				<td><?php if (! empty($element["365_days_sales"]) && $element["inventory"] > 0) echo floor($element["inventory"] / $rate); else echo "N/A" ?></td>
				<!-- <td>N|A</td> -->
			</tr>
			<?php endif;?>
		<?php endforeach; ?>
	</tbody>
	</table>
	<script>
		$(document).ready(function() {
        	var table = $("#sku-table").DataTable({
        		"pageLength":100,
        		"lengthMenu": [[ 100 , 200, 500, 1000, -1 ],[100,200,500,1000,"ALL"]]
        	});

	    // This must be a hyperlink
	    $(".export").on('click', function (event) {
	        // CSV
	        exportTableToCSV.apply(this, [$('table'), 'export.csv']);
	        
	        // IF CSV, don't do event.preventDefault() or return false
	        // We actually need this to be a typical hyperlink
	    	});
        });
	</script>
</div>
<?php $this->load->view('common/footer.php'); ?>