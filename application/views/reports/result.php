<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header.php'); ?>
<div class="container">
<div class="clear"></div>
<h1>Top 20 </h1>
<?php $this->load->view('reports/form.php', array("start" => $start, "end" => $end)); ?>
<div class="clear"></div>
    <h3>Money;</h3>
    <table class="table table-bordered" style="max-width:45%;">
    <tr>
        <th>Item Id</th>
        <th>SKU</th>
        <th>Money Amount</th>
        <th>Units Sold</th>
    </tr>
    <?php
    foreach($top20ByMoney as $row): ?>
    <tr>
        <td>
            <a href="/index.php/products/item/<?php echo $row["item_id"]; ?>" > <?php echo $row["item_id"]; ?></a>
        </td>
        <td>
            <a href="/index.php/data/sku/<?=$start?>/<?=$end?>/<?=$row["sku"];?>"><?php echo $row["sku"]; ?></a>        
        </td>
        <td><?php echo $row["money_amount"];?></td>
        <td><?php echo $row["units_sold"]; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
<?php $this->load->view('common/footer.php'); ?>
