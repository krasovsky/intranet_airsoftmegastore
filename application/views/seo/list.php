<?php $this->load->view('common/header.php'); ?>

<div class="container">
    <div class="row">
    <h1>Trending Products</h1>
    <h4>30 days of trends</h4>
    <table class="table table-bordered">
    <tr>
        <th>sku</th>
        <th>name</th>
        <th>Peak Events</th>
        <th>top day</th>
    </tr>
    <?php foreach($data as $row) : ?>
        <tr>
            <td>
                <?= $row->sku ?>
            </td>
            <td>
                <a href="/index.php/stats/trending/<?= $row->id ?>" ><?= $row->name ?></a>
            </td>
            <td>
                <?= $row->events ?>
            </td>
            <td>
                <?= $row->timestamp ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>