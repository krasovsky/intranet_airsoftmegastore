<?php $this->load->view('common/refreshHeader.php'); ?>

<div class="container">
    <div class="row">
    <h3>Start Date <?= $data['query']['start-date']?> to End Date <?= $data['query']['end-date']?> </h3>
    <table class="table table-bordered">
    <tr>
    <?php foreach($data['columnHeaders'] as $header): ?>
        <th><?=$header['name'];?></th>
    <?php endforeach; ?>
    </tr>
    <?php foreach($data['rows'] as $row) : ?>
        <tr>
            <?php foreach($row as $column):?>
            <td>
                <?php print_r($column) ?>
            </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </table>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>