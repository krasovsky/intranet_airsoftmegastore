<?php $this->load->view('common/header.php'); ?>


<div class="container">
    <div class="row margin-row">
        <a href="#" class=""><img src="<?= substr($product->img, '0', stripos($product->img, ';')); ?>" style="max-width:240px;max-height: 180px" class="pull-right img-polaroid" /></a>
        <h1><?= $product->name ?></h1>
        <h3><?= $product->sku ?></h3>
    </div>
    <div class="row text-center">
        <strong>Added to Cart vs Actual Transaction</strong>
        <div class="span-12">
            <canvas id="myChart" width="940" height="400"></canvas>
        </div>
    </div>
    <div class="row">
        <?php
        $total_transactions=0;
        $total_items=0;
        $total_events=0;
        foreach($orders as $row){
            $total_transactions+=@$row['quantity'];
            $total_items+=@$row['total'];
        }
        foreach($data as $row){
            $total_events+=@$row['events'];
        }
        ?>
        <div class="span-2">Total Transactions:<b><?= $total_transactions;?></b></div>
        <div class="span-2">Total Items Bought:<b><?= $total_items;?></b></div>
        <div class="span-2">Total Add to Cart Events:<b><?= $total_events;?></b></div>
        <div class="span-2">Avg Conversion Rate:<b><?= round($total_transactions/$total_events, 2);?></b></div>
        <div class="span-2 btn-group pull-right">
            <a href="/index.php/stats/trending/<?=$product->itemid?>/30" class="btn">30</a>
            <a href="/index.php/stats/trending/<?=$product->itemid?>/60" class="btn">60</a>
            <a href="/index.php/stats/trending/<?=$product->itemid?>/90" class="btn">90</a>
            <a href="/index.php/stats/trending/<?=$product->itemid?>/120" class="btn">120</a>
            <a href="/index.php/stats/trending/<?=$product->itemid?>/150" class="btn">150</a>
       </div>
    </div>
</div>
<div class="row">
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</div>

<script type="text/javascript">

    var data = {
        labels: [
            <?php
            $events = array();
            foreach($data as $row){
                $events[] =  $row['timestamp'];
            }
            echo '"'.implode('","',$events).'"';
            ?>

        ],
        datasets: [
            {
                label: "Add To Cart Events",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [
                    <?php
                    $events = array();
                    foreach($data as $row){
                        $events[] =  $row['events'];
                    }
                    echo implode(',',$events);
                    ?>
                ]
            }
            ,{
                label: "Actual Purchases",
                fillColor: "rgba(120,220,220,0.2)",
                strokeColor: "rgba(120,220,220,1)",
                pointColor: "rgba(120,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(120,220,220,1)",

                    <?php

                    $events = array();
                    foreach($orders as $row){
                        $events[] =  $row['quantity'];
                    }
                    echo 'data: ['.implode(',',$events).']';

                    ?>

            }
        ]
    };

    var AMS = AMS || {};
    AMS.Page = {
        ready:function(){
            var ctx = $('#myChart').get(0).getContext("2d");
            var myNewChart = new Chart(ctx).Line(data);
        }
    }
</script>

<?php $this->load->view('common/footer.php'); ?>