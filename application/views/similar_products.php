<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 9/24/13
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */
$this->load->view('common/header');
?>
<form action="/index.php/similar/airsplat" method="post">
<div class="container">
    <h1 class="bebas">AMS Similar Product Tool</h1>
    <div class="span12 input-append">
        <input type="text" class="span6" name="subject" value="<?=$subject?>" />
        <input type="submit" name="type" value="Levenschtein" class="btn span2"/>
        <input type="submit" name="type" value="similar text" class="btn span2"/>
        <input type="submit" name="type" value="alike" class="btn span2"/>
    </div>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Product name</th><th>Score</th><th>actions</th>
        </tr>
        <?php foreach($redirects as $item): ?>
        <tr>
            <td><?=$item['name']?></td>
            <td><?=$item['score']?></td>
            <td><span class="btn">redirect here</span></td>
        </tr>
        <?php endforeach; ?>
    </table>
    </div>
</div>
</form>
<?php
$this->load->view('common/footer');
?>