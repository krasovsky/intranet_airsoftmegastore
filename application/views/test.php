<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <style>
      .clear{
        clear:both;
      }
      .image-block{
        padding-left:4%;
        min-width: 300px;;
        float:left;
        padding-top:20px;
        height: 300px;
      }
      .block-text{
        text-align: center;
        font-weight: bold;
        font-size: 17px;
      }
      body{
        padding-left: 20%;
        padding-right: 20%;
      }
      .banner{
        width:100%;;
      }
      .bottom-text{
        margin-top:20px;
        width:100%;
        float:left;
      }
      img{
        display: block;
        margin: 0 auto;
      }
      .logo{
        height:200px;
      }
    </style>

    <div class="logo">
        <img src="/assets/img/jay/logo.jpg"/ style="max-width:100%; max-height:100%">
    </div>

    <div class="header-text" style="line-height:22px">
        Hi _______, <br />
        I hope you are well and prospering during this holiday season.  <br />
        I am writing once more to ask you to consider voting for my entries in the upcoming JCK Jewelers Choice Awards. The final day for voting is December 31. <br />
        You can see my entries below. <br />
        As always I truly appreciate your friendship and support. <br/>
        Fondly, <br/>
        Terry <br/>
        <a href="https://omnicontests4.com/?comp_id=04328462-3DBA-4BF1-9EA6-239AA7A3081A&s=J" target="_blank">Simply click on this link, login or register, and vote for me!</a>
    </div>

    <div class="clear"></div>
    <div class="image-block">
        <div class="block-text">Best Earring Design $2501-$10,000</div>
        <div class="image"><img src="/assets/img/jay/3.jpg" style=""/></div>
    </div>
    <div class="image-block">
        <div class="block-text">Best Necklace Design $2501-$10,000</div>
        <div class="image"><img src="/assets/img/jay/1.jpg" style=''/></div>
    </div>
    <div class="image-block">
        <div class="block-text">Best Ring Design $2501-$10,000</div>
        <div class="image"><img src="/assets/img/jay/2.jpg"  style="padding-top:30px"/></div>
    </div>
    
    <div class="clear"></div>
    <br/>
    
    <div class="bottom-text">
        <div class="banner">
            <a href="https://omnicontests4.com/?comp_id=04328462-3DBA-4BF1-9EA6-239AA7A3081A&s=J" target="_blank">
                <img src="/assets/img/jay/banner.jpg"  style="margin: 0"/>
            </a>
        </div>
    </div>

</body>
</html>