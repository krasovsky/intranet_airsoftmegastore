<?php $this->load->view('common/header');?>
<div class="container">
    <div class="row">
        <form>
            <div class="col-md-4"><h4>Choose a category:</h4>
                <select name="category">
                <?php foreach($categories as $c): ?>
                <option value="<?=$c->id?>" class="input-large">
                    <?=$c->name?>
                </option>
                <?php endforeach;?>
            </select>
            </div>
        </form>
    </div>
    <hr>
</div>
<?php $this->load->view('common/footer');?>