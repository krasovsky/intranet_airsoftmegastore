<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header_blank.php'); ?>
<div class="container">
<table class="table table-bordered">
    <tr>
        <th>Email</th>
        <th>Total Spent</th>
        <th>Transactions</th>
        <th>Last Purchase</th>
    </tr>
    <?php foreach($users as $row): ?>
        <tr>
            <td>
                <a href="/index.php/data/user/<?=$start?>/<?=$end?>/<?=urlencode($row->email)?>"><?=$row->email?></a>
            </td>
            <td>$<?=number_format($row->spent, 2, '.', ',')?></td>
            <td><?=$row->transactions?></td>
            <td><?=$row->last_purchase?></td>
        </tr>
    <?php endforeach; ?>
</table>
</div>
<?php $this->load->view('common/footer.php'); ?>
