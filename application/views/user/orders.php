<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abstraktron
 * Date: 10/7/13
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<?php $this->load->view('common/header_blank.php'); ?>
<div class="container">
<a href="/index.php/data/orders/<?=$start?>/<?=$end?>">View Orders</a>
    <?php if($user):?>
<h1><?=$user->full_name;?></h1>
<h3><?=$user->email;?></h3>

<table class="table-bordered table">
    <tr><td>Address</td><td><?=$user->address1?> <?=$user->address2?></td></tr>
    <tr><td>City</td><td><?=$user->city?></td></tr>
    <tr><td>State</td><td><a href="/index.php/data/state/<?=$start?>/<?=$end?>/<?=$user->state?>"><?=$user->state?></a></td></tr>
    <tr><td>Zip</td><td><?=$user->zip?></td></tr>
    <tr><td>Phone</td><td><?=$user->phone?></td></tr>
    <tr><td>Account Created</td><td><?=$user->account_created_on_date?></td></tr>
    <tr><td>Last Login</td><td><?=$user->last_logon_at_datetime?></td></tr>
</table>
<?php endif;?>

    <table class="table table-bordered">
        <tr>
            <th>Order Id</th>
            <th>Items</th>
            <th>Shipping</th>
            <th>Profit</th>
        </tr>
        <?php
        foreach($orders as $row): ?>
        <?php //dump($row);?>
            <tr>
                <td>
                    <a href="/index.php/data/autopsy/<?=$start?>/<?=$end?>/<?=$row['items'][0]->order_id?>"><?=$row['items'][0]->order_id?></a>
                </td>
                <td>
                    <div class="span4 pull-left"><strong>DATE :</strong> <?=$row['items'][0]->ordered_at_datetime?></div>
                    <?php if($row['items'][0]->ship_charged == 0):?>
                        <div class="span2 pull-right" style="color:#3CC44C;"><strong>Free Shipping</strong></div>
                    <?php endif;?>
                    <table class="table table-bordered">
                        <?php foreach($row['items'] as $key => $item):?>
                            <?php if(is_numeric($key)):?>
                                <tr><td <?php if($item->price == 0){ echo 'style="color:red"'; };?>><?=$item->name?><?php if($item->price == 0){ echo ' [freebie]'; };?>
                                </td><td><a href="/index.php/data/sku/<?=$start?>/<?=$end?>/<?=$item->sku?>"><?=$item->sku?></a></td>
                                <td><?=$item->quantity?></td><td>$<?=$item->revenue?></td><tr>
                            <?php endif;?>
                        <?php endforeach; ?>
                    </table>

                </td>
                <td><?=$row["items"]['shipping_price']?></td>
                <td><?=number_format($row['profit'], 2, '.', ',')?></td>
            </tr>
        <?php endforeach; ?>
</div>
<?php $this->load->view('common/footer.php'); ?>
