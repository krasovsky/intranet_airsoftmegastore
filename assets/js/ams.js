/**
 * Created with JetBrains PhpStorm.
 * User: abstraktron
 * Date: 8/29/13
 * Time: 7:34 PM
 * To change this template use File | Settings | File Templates.
 */
$(function(){
    for(Module in AMS){
        try{
            AMS[Module].ready();
        }catch(E){
            if (!!window.console && ~location.hash.search('debug')) console.log(E);
        }
    }

    $(window).load(function(){
        for(Module in AMS){
            try{
                AMS[Module].load();
            }catch(E){
                if (!!window.console && ~location.hash.search('debug')) console.log(E);
            }
        }
    })
});

function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function (i, row) {
            var $row = $(row),
                $cols = $row.find('td');

            return $cols.map(function (j, col) {
                var $col = $(col),
                    text = $col.text();

                return text.replace(/"/g, '""'); // escape double quotes

            }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"',

        // Data URI
        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    $(this)
        .attr({
        'download': filename,
            'href': csvData,
            'target': '_blank'
    });
}

var AMS = AMS || {};