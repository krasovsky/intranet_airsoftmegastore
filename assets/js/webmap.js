/**
 * Created by vlad on 11/7/2015.
 */

var webmap = new Object();

webmap.accessToken = 'pk.eyJ1Ijoia3Jhc292c2t5MjIyIiwiYSI6ImNpZ29zbnNvNTAwYmh1ZWtvbWp1ZWk4YnMifQ.xQkYr6e4Zn01Osja7GTVhg';

webmap.init = function(){
    var jsonElement = $("#mapJson");
    webmap.markersArr = JSON.parse(jsonElement.text());

    webmap.markers = [];
    webmap.display();
    document.title = "Custom AirsoftMegastore Map";
}

webmap.display = function(){
    L.mapbox.accessToken = webmap.accessToken;
    webmap.map = L.mapbox.map('map', 'mapbox.streets').setView([0, 0], 2);
    webmap.overlays = L.layerGroup().addTo(webmap.map);


//    var myIcon = L.divIcon({
//        iconUrl: 'http://webshot.airsoftmegastore.net/webshot/get/12689-14689-478-7064-468-9693.jpg',
//        iconSize: [240,120],
//        iconAnchor: [100, 94],
//        popupAnchor: [-3, -76],
//        "html": "<div style='width:100%;height:100%'><img width='100%' src='http://webshot.airsoftmegastore.net/webshot/get/12689-14689-478-7064-468-9693.jpg'/></div>"
//    });


    webmap.addLayer("customizer");
    webmap.addLayer("gunbuilder");
    //webmap.map.addLayer(webmap.overlays);

    //webmap.addLayer("gunbuilder");

}

webmap.addLayer = function(group){
    var clusterGroup  = new L.MarkerClusterGroup({
        iconCreateFunction: function(cluster) {
            return new L.DivIcon({
                iconSize: [20, 20],
                html: '<div style="text-align:center;color:#fff;background:' +
                "red" + '">' + cluster.getChildCount() + '</div>'
            });
        }
    }).addTo(webmap.overlays);

    //webmap.markers.push(markers);
    for (var i = 0; i < webmap.markersArr[group].length; i++) {
        var a = webmap.markersArr[group][i];
        var title = "<a target='_blank' href='"+a["url"]+"'><div style='width:100px;height:auto'><img class='image' width='100%' src='http://webshot.airsoftmegastore.net/webshot/get/" + a["image_name"] + "'/><div class='clear'></div><div class='marker-text'>" +a['name'] +
            "</div><div class='clear'></div><div class='text-created'>"+ a["date_added"] + "</div></a>";

        if(group == "gunbuilder"){
            var url = "/assets/images/icons/scull.jpg";
            var color = "#5820e5";
        }
        else{
            var url = "/assets/images/icons/start.gif";
            var color = "#e5208d";
        }
       //
        var marker = L.marker(new L.LatLng(a["lat"], a["lon"]), {
            icon: L.mapbox.marker.icon({
                'marker-color': color
            }),
            title: a['name']
        });
        marker.bindPopup(title);

        clusterGroup.addLayer(marker);
        //clusterGroup.on('click', function(e) {
        //    window.open(e.layer.options.url);
        //});
    }
}

webmap.toggleFilter = function(){
    webmap.overlays.clearLayers();

    var filters = document.getElementById('filterForm').filters;
    for (var i = 0; i < filters.length; i++) {
        if (filters[i].checked)  webmap.addLayer(filters[i].value);
    }
}

$( document ).ready( webmap.init );