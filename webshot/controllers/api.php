<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

const SECRET_KEY = "8a6e92e13972938d96f3961506b380f356311cb6a482c";
const API_KEY = "8a6e92e13972938d96f3961506b380f369c209a19d43625a21fb3e4f86c11ca2";

class Api extends CI_Controller{
    public function index(){
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $this->load->model('webshotModel');
//        $request = json_encode(array(
//            "website" => "http://www.airsoft.sandbox.orderdynamics.net/11072-#12689,14689,478,7064,468,9693|webshot",
//	    ));
        $data = file_get_contents('php://input');
        //$dataArr = json_decode($data,true);

//        $data = array("name" => "","platform"=>"golden-eagle-m27","price"=> "298.79","type" => "GunBuilder2", "website"=> "http://www.airsoft.sandbox.orderdynamics.net/airsoft-gun-builder/golden-eagle-m27/#13922,12788,12791,12800,2522,1339,9693|webshot");
//        $data = json_encode($data);
        try{
            $this->webshotModel->processRequest($data);
            $this->webshotModel->saveApprovedLog();
        }
        catch (Exception $e) {

            $this->webshotModel->saveDeniedLog($e->getMessage(),$data);
        }
    }
    public function createWebshotTestRequest(){
        $url = "http://webshot.airsoftmegastore.net";
        $data = array("website" => "http://www.airsoft.sandbox.orderdynamics.net/1844-#1844,13555,461,2357|webshot");

        $jsonData = json_encode($data);

        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, "data=".$jsonData);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }

    // public function updateDb(){
    //      $this->load->model('webshotModel');
    //      $this->webshotModel->fixDb();
    // }
}