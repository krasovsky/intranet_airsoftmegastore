<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class webMap extends CI_Controller{
	public function index(){
		$this->load->model('webMapModel');
		$data = $this->webMapModel->getLocations();
		$this->load->view('webmap', ["data" => $data]);
	}

	public function chat(){
		$this->load->view('chat');
	}
}
?>