<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class webshot extends CI_Controller{

	function __construct()
	{
		parent::__construct();

		$this->load->model('webshotModel');
	}

	public function process(){

		var_dump(date('Y-m-d H:i:s'). "START!");
		var_dump("get approved logs!");
		$approvedLogs = $this->webshotModel->getApprovedLogs();


		if(empty($approvedLogs)){
			var_dump("NO LOGS TO PROCESS!");
			return;
		}

		foreach($approvedLogs as $log){
			try{
				var_dump("process log ".$log["id"]);
				$imageName = $this->webshotModel->processLog($log);

				$this->webshotModel->updateApprovedLogAsComplete($log,$imageName);
				var_dump($log["id"]. " complete!");

			}
			catch (Exception $e) {
            	echo "ERROR! ".$e->getMessage();
            	$this->webshotModel->saveFailedWebshotLog($log["id"],$e->getMessage());
			}
		}
		//var_dump("creating new config file");
		//upload newest config file
		//$this->webshotModel->createNewConfigFile();
		

	}

	public function getWebshotsConfig(){
		header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
		$limitValue = $this->uri->segment(3);

		$options = array();
		if(empty($limitValue)) $limitValue = 20;

		$cacheSegment = $this->uri->segment(4);

		if(! empty($_GET["itemId"])) $options["itemId"] = $_GET["itemId"];
		if(! empty($cacheSegment) && $cacheSegment == true) $options["clearCache"] = true;

		$configArr = $this->webshotModel->pullWebshotsConfig($limitValue,$options);;

		$this->load->view("configView", ["data" => $configArr]);
	}

	public function get(){
		$imageName = $this->uri->segment(3);
		if(empty($imageName))
			return;

		$options = array();
		if(! empty($_GET['width'])){
			$options["width"] = intval($_GET['width']);
		}
		
		if(! empty($_GET['height'])){
			$options["height"] = intval($_GET['height']);
		}

		if(! empty($options["width"]) || ! empty($options["height"])){
			$options["mode"] = (! empty($_GET['mode']) && $_GET['mode']=='fit') ?'fit':'fill';
		}

	    $imageData = $this->webshotModel->getImage($imageName,$options);
 
		$this->load->view('imageView', ["data" => $imageData]);

	}

	public function fixXrefs(){
		$this->webshotModel->fixXrefs();
	}

}