<?php
class webMapModel extends CI_Model
{
    const MEMCACHEKEY = "wemap_key_beta_version_0.9.0";
    const VALUE_QUERY_CACHE_TIME = 10800; //3hours
    function __construct()
    {
        parent::__construct();
        $this->dateBase = $this->load->database('webshot',true);
    }

    public function getLocations(){

        $this->load->driver('cache',array('adapter'=>'cache_memcached'));
        $mapArr = array();

        $mapArr = $this->cache->memcached->get(md5(self::MEMCACHEKEY));

        if(empty($mapArr) || !$mapArr){

                $queryStr = <<<HEREDOC
                select a.id,a.location_json,a.type,a.date_added,a.request,a.type as is_gunbuilder,
                w.image_name,w.url
                 from ams_logs a
                inner join webshots w on w.id = a.webshot_id
                where w.id is not null and a.status = 1 and a.processed = 2
HEREDOC;
            $rez = $this->dateBase->query($queryStr);
            $result = $rez->result_array();

            $filteredArr = array();
            //filter same location
            foreach($result as $element){
                $decodedLoc = json_decode($element["location_json"],true);
                if(empty($decodedLoc["loc"])) continue;
                $filteredArr[$decodedLoc["loc"]] = $element;
            }


            
            $mapArr["gunbuilder"] = array();
            $mapArr["customizer"] = array();

            foreach($filteredArr as $element){
                $tArr = array();
                $tArr["id"] = $element["id"];
                $tArr["date_added"] = date("m-d-Y",strtotime($element["date_added"]));
                $tArr["image_name"]= $element["image_name"];

                $requestArr = json_decode($element["request"],true);
                //var_dump($requestArr);
                $urlArr = explode('|',$requestArr["website"]);
               // var_dump($requestArr);
                $tArr["url"] = $urlArr[0];

                $tArr["name"] = (empty($requestArr["name"])) ? $requestArr["platform"] : $requestArr["name"];

                if(!empty($requestArr["price"])) $tArr["price"] = $requestArr["price"];

                $locationArr = json_decode($element["location_json"],true);
                $coordinates = explode(',',$locationArr["loc"]);
                $tArr["lat"] = $coordinates[0];
                $tArr["lon"] = $coordinates[1];

                $tArr["isGunbuilder"] = $element["is_gunbuilder"];

                if($tArr["isGunbuilder"] == 1){
                    $mapArr["gunbuilder"][] = $tArr;
                }
                else{
                    $tArr["name"] = "CUSTOM BUILD";
                    $mapArr["customizer"][] = $tArr;
                }
            }
           
            $this->cache->memcached->save(md5(self::MEMCACHEKEY),$mapArr, self::VALUE_QUERY_CACHE_TIME);
        }


        

        return $mapArr;
    }
}

?>