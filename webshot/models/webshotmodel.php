<?php

class webshotModel extends CI_Model
{
    const STATUS_PENDING = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_COMPLETED = 2;

    const REQUEST_DENIED = 0;
    const REQUEST_APPROVED = 1;

    const VALUE_IMAGE_CACHE_TIME = 14400; //4*60*60 4 hours
    const VALUE_CONFIG_CACHE_TIME = 1200; // 20*60 20 mins

    const VALUE_WEBSHOT_DEFAULT_IMAGE_PATH = "assets/img/webshot-temp.png";

    const WEBSHOT_TYPE_PROSHOP = 0;
    const WEBSHOT_TYPE_GUNBUILDER = 1;
    const VALUE_WEBSHOT_TYPE_PROSHOP = "Proshop";
    const VALUE_WEBSHOT_TYPE_GUNBUILDER = "GunBuilder2";

    function __construct()
    {
        parent::__construct();
        $this->dateBase = $this->load->database('webshot',true);

        $this->webshotPath = realpath(APPPATH."assets/data/webshots/");
        $this->webshotPathResized = realpath(APPPATH."assets/data/webshots/resized/");
        $this->webshotPathCompressed = realpath(APPPATH."assets/data/webshots/compressed/");

        $this->appsConfigs = $this->config->item("appconfig");
    }
    public function processRequest($request)
    {
        $status = self::REQUEST_DENIED;
        if (empty($request))
            throw new Exception("empty request");
        $this->request = $request;
        $this->requestJson = json_decode($request, true);
        //$this->requestJson = $request;
        //var_dump($this->requestJson);
        if (!$this->requestJson)
            throw new Exception("Enable to decode!");
        $url = $this->requestJson["website"];

        $this->checkWebsiteDomain($url);
    }

    private function checkWebsiteDomain($url)
    {
        $arr = explode("/", $url);
        $domain = $arr[2];
        //var_dump($domain);
        if ($domain != "www.airsoftmegastore.com")
            throw new Exception("Wrong Domain Detected!");

        //throw new Exception("test Error");

    }

    public function saveApprovedLog($denyReason = "")
    {
        $currentDate = date('Y-m-d H:i:s');

        $sql = <<<HEREDOC
			INSERT into ams_logs (sender_ip,request,status,deny_reason,date_added,location_json,type)
			values (?,?,?,?,?,?,?)
HEREDOC;

        $ip = get_client_ip();

        $locationJson = file_get_contents("http://ipinfo.io/{$ip}/json");
        //var_dump($ip);
        //var_dump($locationJson);

        if (!empty($denyReason)) {
            $status = self::REQUEST_DENIED;
        } else {
            $status = self::REQUEST_APPROVED;
        }

        $type = self::WEBSHOT_TYPE_PROSHOP;
        if($this->requestJson["type"] == self::VALUE_WEBSHOT_TYPE_GUNBUILDER){
            $type = self::WEBSHOT_TYPE_GUNBUILDER;
        }

        $this->dateBase->query($sql, array($ip, $this->request, $status, $denyReason, $currentDate, $locationJson,$type));
    }

    public function saveDeniedLog($denyReason,$data)
    {
        $sql = <<<HEREDOC
			INSERT into ams_logs (sender_ip,request,status,deny_reason,date_added,location_json)
			values (?,?,?,?,?,?)
HEREDOC;

        $ip = get_client_ip();
        $locationJson = file_get_contents("http://ipinfo.io/{$ip}/json");
        $status = self::REQUEST_DENIED;

        $request = $this->request;

        if(!isset($this->request)) $request = "EMPTY REQUEST";
        $currentDate = date('Y-m-d H:i:s');

        $this->dateBase->query($sql, array($ip, $data, $status, $denyReason, $currentDate, $locationJson));
    }

    public function saveFailedWebshotLog($id,$message){
        $queryStr = "update ams_logs set deny_reason = ? where id = {$id}";
        $this->dateBase->query($queryStr,array($message));
    }


    public function updateApprovedLogAsComplete($log,$imageName = ""){
        $updateStr = "";
        if(! empty($imageName)){
            $queryStr = "select id from webshots where image_name like '{$imageName}'";
            $rez = $this->dateBase->query($queryStr);
            $temp = $rez->result_array();
            $webshotId = $temp[0]["id"];
            $updateStr = ", webshot_id = {$webshotId}";
        }
        $completeVal = self::STATUS_COMPLETED;
        $queryStr = "update ams_logs set processed = {$completeVal}  {$updateStr} where id = {$log["id"]}";
        $this->dateBase->query($queryStr);
    }
    
    public function getApprovedLogs(){

        $approvedVal = self::REQUEST_APPROVED;
        $queryStr = <<<HEREDOC
            SELECT * from ams_logs where processed = 0 and status = {$approvedVal}
            order by date_added asc
            limit 20
HEREDOC;

        $rez = $this->dateBase->query($queryStr);

        $processData = $rez->result_array();

        if(empty($processData)) return $processData;

        //change status to inprogress
        $idsArr = array();
        foreach($processData as $element){
            $idsArr[] = $element["id"];
        }
        $idsString = implode(',', $idsArr);
        //mark them as in process
        $queryStr = <<<HEREDOC
            UPDATE ams_logs set processed = ?
            where id in ( {$idsString} )
HEREDOC;

        $this->dateBase->query($queryStr,array(self::STATUS_IN_PROCESS));

        return $processData;
    }

    public function getImageNameFromRequest($requestArr){
        if(strrpos($requestArr["website"],'#') !== false && strrpos($requestArr["website"],'|') !== false){
            $startPos = strrpos($requestArr["website"],'#') + 1;
            $nameLength = strrpos($requestArr["website"],'|') - $startPos;
            $notFormatedName =  substr($requestArr["website"],$startPos ,$nameLength);
            $formatedName = str_replace(',', '-', $notFormatedName);
        }
        else{
            $formatedName = uniqid();
        }

        if(empty($formatedName)) throw new Exception("NO IMAGE NAME!");

        $formatedName .= ".jpg";

        return $formatedName;
    }

    public function processLog($log){

        $requestArr = json_decode($log["request"], true);

        //check if current log was processed
        $md5string = md5($this->getImageNameFromRequest($requestArr));
        $queryStr = <<<HEREDOC
            SELECT * from webshots where md5 = '{$md5string}'
HEREDOC;

        $result = $this->dateBase->query($queryStr);
        $result = $result->result_array();

        if(count($result) == 0){
            var_dump("making webshot!");
            $imageName = $this->makeWebshot($requestArr);
            dump($imageName);
            var_dump("resize image");
            $this->resizeImage($imageName);


            // var_dump("compressImage");
            // $this->compressImage($imageName); DISABLED BECAUSE WORK ONLY WITH PNG!
            if(file_exists($this->webshotPathResized."\\".$imageName)){
                $currentTime = date('Y-m-d H:i:s');
                $imgData = file_get_contents($this->webshotPathResized."\\".$imageName);

                if(filesize($this->webshotPathResized."\\".$imageName) > 64000) throw new Exception($imageName . " is more than 64kb...Please check image!");
                $queryStr = <<<HEREDOC
                    insert into webshots
                        (request,date_added,url,md5,build_name,image_name,last_date_used,image_data,type)
                    values
                        (?,?,?,?,?,?,?,?,?);
HEREDOC;
                //need to get buildname from websitestring
                $buildName = "";
                $websiteLink = $requestArr["website"];
                $arr = explode('/', $websiteLink);
                foreach($arr as $element){
                    if(strpos($element,"#") !== false){
                        $buildName = substr($element, 0, strpos($element,'-'));
                        $urlArr = explode('|', $element);
                        $url = $urlArr[0];
                    }
                }
                if(! empty($requestArr["platform"])) $buildName  = $requestArr["platform"];
                $webshotType = 0;
                if(! empty($requestArr["type"]) && $requestArr["type"] == self::VALUE_WEBSHOT_TYPE_GUNBUILDER) $webshotType = 1;

                $this->dateBase->query($queryStr,array($log["request"],$currentTime,$url,$md5string,$buildName,$imageName,$currentTime,$imgData,$webshotType));

                //create categories xref
                var_dump("add categories xref");
                $imageName1 = substr($imageName, 0, -4);
                $partsArr = explode("-", $imageName1);
                $valuesArr = array();

                foreach($partsArr as $part){
                    if($part < 1000000)
                        $valuesArr[] = "('".$part."','".$buildName."','".$imageName."')";
                }

                $values = implode(',',$valuesArr);
                if(! empty($values)){
                    $queryStr = "Insert into builds_xref (item_id,build_name,image_name) 
                        values
                            {$values}";

                    dump($queryStr);
                    $this->dateBase->query($queryStr);
                }

                return $imageName;
            }
            else{
                throw new Exception("Image wasn't processed!");
            }
        }
        else{

            var_dump("image found");
            $imageName = $result[0]["image_name"];

            $currentTime = date('Y-m-d H:i:s');
            //update last_date_used value
            $queryStr = "UPDATE webshots set last_date_used = '{$currentTime}' where id = {$result[0]["id"]}";
            $this->dateBase->query($queryStr);

            return $imageName;
        }
    }

    public function fixXrefs(){

        $rez = $this->dateBase->query("SELECT id,build_name,image_name from webshots");
        $rezArr = $rez->result_array();

        foreach($rezArr as $element){
            $imageName = $element["image_name"];
            $buildName = $element["build_name"]; 
            $imageName = substr($imageName, 0, -4);
            $partsArr = explode("-", $imageName);
            dump($partsArr);
            $valuesArr = array();

            foreach($partsArr as $part){
                if($part < 1000000)
                     $valuesArr[] = "('".$part."','".$buildName."','".$element["image_name"]."')";
            }

            $values = implode(',',$valuesArr);

            if(! empty($values)){
                $queryStr = "Insert into builds_xref (item_id,build_name,image_name) 
                values
                    {$values}";
                dump($queryStr);
                $this->dateBase->query($queryStr);
            }
        }    
    }

    private function compressImage($filename){
        $cmd = <<<HEREDOC
pngquant.exe --force --verbose --output "{$this->webshotPathCompressed}\\{$filename}" --quality=45-85 "{$this->webshotPathResized}\\{$filename}"
HEREDOC;
        dump($cmd);
        chdir($this->appsConfigs["pngquantPath"]);
        dump($this->appsConfigs["pngquantPath"]);
        system($cmd,$retval);
    }

    private function resizeImage($name){
        //$name = $this->getImageName($name);
        $img = $this->webshotPath."\\".$name;
        $img2 = $this->webshotPathResized."\\".$name;
        if(is_file($img)){
            //change directory;
            chdir($this->appsConfigs["ImageMagickPath"]);
            $cmd = 'convert '.$img.' -crop 1100x815-100-500 -trim -bordercolor transparent +antialias +repage '.$img2.' 2>&1';
            system($cmd);

            if(! file_exists($img2))
                throw new Exception("Error in image resizing");
        }
    }

    private function makeWebshot($requestArr,$options = array()){

        if(strrpos($requestArr["website"],'#') !== false && strrpos($requestArr["website"],'|') !== false){
            $startPos = strrpos($requestArr["website"],'#') + 1;
            $nameLength = strrpos($requestArr["website"],'|') - $startPos;
            $notFormatedName =  substr($requestArr["website"],$startPos ,$nameLength);
            $formatedName = str_replace(',', '-', $notFormatedName);
        }
        else{
            $formatedName = uniqid();
        }

        if(empty($formatedName)) throw new Exception("NO IMAGE NAME!");

        $formatedName .= ".jpg";
        


        $command = "wkhtmltoimage.exe --height 815 --width 1100 --javascript-delay 5000 \"{$requestArr["website"]}\" {$this->webshotPath}\\{$formatedName}";
        chdir($this->appsConfigs["wkhtmltoimagePath"]);
        //dump($command);
        system($command,$retval);
        // var_dump("{$this->webshotPath}\\{$formatedName}");
        // if(! file_exists("{$this->webshotPath}\\{$formatedName})"))
        //     die("asdas");
        //     throw new Exception("Error in creating webshot!");

        return $formatedName;
    }

    public function createNewConfigFile($options = array()){
        $queryStr = <<<HEREDOC
            select * from webshots
            order by last_date_used desc
            limit 10
HEREDOC;

        $rez = $this->db->query($queryStr);
        $webshotsArr = $rez->result_array();
        $webshotsJson = json_encode($webshotsArr);
        //update remote db
        $currentTime = date('Y-m-d H:i:s');
        $queryStr = <<<HEREDOC
            insert into ams_webshot_configs (config_json,date_created)
            values
                ('{$webshotsJson}','{$currentTime}')
HEREDOC;
        
        dump($queryStr);
        $this->dateBase->query($queryStr);


        // ///FOR NOW UPLOADING NEW CONFIG.JS
        // file_put_contents($this->webshotPath . 'forgeConfig.js', 'var forgeConfig = ' . $webshotsJson . ';');

        // $upload = ftp_put($this->ftp_conn_id, "forgeConfig.js", $this->webshotPath . 'forgeConfig.js', FTP_ASCII);
        // if($upload)
        //     var_dump("new config file is uploaded!");

    }

    public function getImage($imageName,$options = array()){
        $queryStr = <<<HEREDOC
        SELECT * from webshots where image_name like ? limit 1
HEREDOC;
        $rez = $this->dateBase->query($queryStr,array($imageName));
        $result = $rez->result_array();                  
        $result = $result[0];
        $src = imagecreatefromstring($result["image_data"]);


        $imgHeight = imagesy($src);

        //crop stupid zopim chat
        if($imgHeight > 450){
            $imgHeight = imagesy($src);
            $imgWight = imagesx($src);
            if($imgWight > 990){
                $imgWight = 990;
            }
            $new_image = imagecreatetruecolor($imgWight, 315);
            imagecopy($new_image, $src, 0, 0, 0, 0, $imgWight, $imgHeight);

            imagedestroy($src);
            $src = $new_image;   
        }

        if(! empty($options["width"]) && ! empty($options["height"])){
            $w = $options["width"]; $h = $options["height"];
            if ($w <= 1 || $w >= 1000) $w = 100;
            if ($h <= 1 || $h >= 1000) $h = 100;

            $dst = imagecreatetruecolor($w, $h);
            imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
            $this->scale_image($src, $dst, $options["mode"]);
            return $dst;
        }
              
        return $src;
    }

    function scale_image($src_image, $dst_image, $op = 'fit') {
        $src_width = imagesx($src_image);
        $src_height = imagesy($src_image);

        $dst_width = imagesx($dst_image);
        $dst_height = imagesy($dst_image);

        // Try to match destination image by width
        $new_width = $dst_width;
        $new_height = round($new_width*($src_height/$src_width));
        $new_x = 0;
        $new_y = round(($dst_height-$new_height)/2);

        // FILL and FIT mode are mutually exclusive
        if ($op =='fill')
            $next = $new_height < $dst_height;
        else
            $next = $new_height > $dst_height;

        // If match by width failed and destination image does not fit, try by height
        if ($next) {
            $new_height = $dst_height;
            $new_width = round($new_height*($src_width/$src_height));
            $new_x = round(($dst_width - $new_width)/2);
            $new_y = 0;
        }

        // Copy image on right place
        imagecopyresampled($dst_image, $src_image , $new_x, $new_y, 0, 0, $new_width, $new_height, $src_width, $src_height);
    }

    /**
     * method will generate config file
     */
    public function pullWebshotsConfig($limit = 20, $options = array()){ 
        $whereStr = "";
        if(! empty($options["itemId"])) {
            $queryStr = <<<HEREDOC
                SELECT DISTINCT w.id,w.url,w.last_date_used,w.image_name,w.build_name,w.type from 
                    builds_xref b
                inner join webshots w on w.image_name = b.image_name
                where b.item_id = ?
                ORDER by w.last_date_used desc
                limit {$limit}
HEREDOC;
            
            $rez = $this->dateBase->query($queryStr,array($options["itemId"]));
        }
        else{

            $queryStr = <<<HEREDOC
                SELECT id,url,last_date_used,image_name,build_name,type from webshots
                {$whereStr}
                ORDER by last_date_used desc
                limit {$limit}
HEREDOC;
                $rez = $this->dateBase->query($queryStr);
        }

        $result = $rez->result_array();

        //fix gunbuilder links
        foreach($result as &$element){
            if($element["type"] == 1){
                $element["url"] = "airsoft-gun-builder/{$element["build_name"]}/".$element["url"];
            }
            else{
                $element["build_name"] = self::generateGunName();
            }
        }
    

        return $result;
    }

public static function generateGunName(){  

    $nm1 = ["Abomination","Agony","Allegiance","Alpha","Amigo","Amnesia","Apocalypse","Armageddon","Ash","Ashes","Atomic Annie","Baby","Battery","Battlestar","Betrayal","Betrayer","Big Bertha","Mighty Mouse","Betty","Quick Fix","Swiftscope","Bad Medicine","Toots","Enterprise","Big Rhonda","Crowd Control","Intervention","Matilda","Pink Mist","Judas","Headhunter","Jaeger","Lupus","Big Daddy","Big Game","Birthmark","Blackout","Bloodfury","Bloodmoon","Bloodrage","Bloodspiller","Bloodweep","Bombardment","Boomer","Boomstick","Brass Rain","Brutality","Brutalizer","Bulldog","Burn","Burp","Cataclysm","Catastrophe","Chance","Chaos","Comet","Corpsemaker","Corruption","Cosmos","Crash","Crimson","Crossfire","Cruiser","Crush","Cyclone","Daddy","Damnation","Darkness","Dawn","Deadeye","Deathbringer","Deathraze","Decimation","Demise","Desolation","Despair","Destiny","Destruction","Devastation","Devotion","Devourer","Discharge","Disturbance","Divine","Dominance","Dominion","Doom","Downpour","Dragonmaw","Dragonstrike","Dreamhunter","Due Diligence","Early Retirement","Echo","Eclipse","Emergency","End of Dreams","Endbringer","Enigma","Envy","Epilogue","Equality","Equalizer","Eternal Rest","Eternity","Eveningstar","Extermination","Extinction","Falcon","Falling Star","Fate","Finality","Frankenlove","Frankenstein","Frenzy","Fury","Gov'ner","Grace","Harbinger","Harmony","Hatred","Heat","Hell's Scream","Homage","Honor's Call","Hope's End","Infamy","Infinity","Injection","Interrogator","Judged","Justice","Justifier","King of Nines","Knight's Fall","Knightfall","Lament","Last Chance","Last Laugh","Last Rites","Last Word","Last Words","Lazarus","Legacy","Legionaire","Life's Limit","Lightning","Lightningflash","Limbo","Lockjaw","Magma","Malice","Maneater","Massacre","Mercy","Midnight","Misery's End","Moonsight","Mortality","Narcoleptic","Night's Fall","Nightbane","Nightfall","Nightmare","Nighttime","Nightwatch","Nirvana","Oathbreaker","Oathkeeper","Oblivion","Ole Betsy","Omega","Optimist","Orbit","Party Pooper","Patience","Peacekeeper","Peacemaker","Pendulum","Perfect Storm","Persuasion","Piety","Popeye","Pride","Prophecy","Pulse","Punisher","Purifier","Rage","Rapture","Rattlesnake","Reckoning","Reign","Remorse","Requiem","Retirement","Rigormortis","Salvation","Salvo","Scar","Serenity","Severance","Shadowfury","Shadowmoon","Silence","Snake","Snapper","Soulburn","Sprocket","Stalker","Storm","Storm Breaker","Storm-Weaver","Stormbringer","Stormrider","Sugar","Sunshine","Supernova","Supremacy","Suspension","Swan Song","Sweetie","Syndrome","Termination","The Ambassador","The Chancellor","The End","The Judge","The Light","The Oculus","The Oracle","The Pig","The Void","Thunder","Thundercall","Trauma","Treachery","Tremor","Tribunal","Tribute","Trinity","Trouble","Twilight","Typhoon","Ubiquity","Undoing","Vacancy","Valkyrie","Vanquisher","Vengeance","Voice of Insanity","Voice of Reason","Warbringer","Warmonger","Whispers","Whisperwind","Wicked","Widow Maker","Willbreaker","Wit's End","Wolf"];
    $nm2 = ["Apocalypse","Apocalyptic","Atuned","Bandit's","Baneful","Banished","Barbaric","Battleworn","Blood Infused","Blood-Forged","Bloodied","Bloodlord's","Bloodsurge","Brutal","Brutality","Burnished","Cataclysm","Cataclysmic","Challenger","Challenger's","Champion","Cold-Forged","Commando","Commando's","Conqueror","Conqueror's","Corroded","Corrupted","Crazed","Dancing","Dark","Darkness","Defender","Defender's","Defiled","Defiling","Deluded","Deserted","Desire's","Desolation","Destiny's","Diabolical","Dire","Doom","Doom's","Eerie","Engraved","Eternal","Exiled","Extinction","Faith's","Faithful","Fancy","Fearful","Feral","Ferocious","Fierce","Fireguard","Firestorm","Forsaken","Fortune's","Foul","Frenzied","Furious","Fusion","Ghastly","Ghostly","Gladiator","Gleaming","Glinting","Greedy","Grieving","Grim","Guard's","Guardian's","Guerilla","Hateful","Heartless","Heinous","Hero","Hero's","Honed","Honor's","Hopeless","Howling","Hungering","Incarnated","Infantry","Infantry's","Infused","Inherited","Judgement","Keeper's","Legionnaire's","Liar's","Lightning","Lonely","Loyal","Lustful","Lusting","Malevolent","Malicious","Malificent","Malignant","Massive","Mended","Mercenary","Military","Misfortune's","Mourning","Nightmare","Oathkeeper's","Ominous","Peacekeeper","Peacekeeper's","Phantom","Polished","Pride's","Prideful","Primal","Prime","Primitive","Private","Promised","Protector's","Proud","Pure","Raging","Recruit's","Refined","Reforged","Reincarnated","Relentless","Remorseful","Renewed","Renovated","Replica","Restored","Retribution","Ritual","Roaring","Savage","Sentinel","Shadow","Silent","Singed","Singing","Sinister","Smooth","Soldier's","Solitude's","Sorrow's","Soulless","Spectral","Spiteful","Storm","Storm-Forged","Stormfury","Stormguard","Terror","Thirsting","Thirsty","Thunder","Thunder-Forged","Thunderfury","Thunderguard","Thundersoul","Thunderstorm","Timeworn","Trainee's","Treachery's","Trooper","Twilight","Twilight's","Twisted","Tyrannical","Vanquisher","Vengeance","Vengeful","Veteran","Veteran's","Vicious","Victor","Vile","Vindication","Vindicator","Vindictive","Void","Vowed","War","War-Forged","Warden's","Warlord's","Warmonger","Warmonger's","Warp","Warped","Warrior","Warrior's","Whistling","Wicked","Windsong","Woeful","Wrathful","Wretched","Yearning","Zealous"];
    $nm3 = ["Bronze","Bronzed","Ivory","Ebon","Glass","Golden","Gilded","Chromed","Iron","Stainless","Carbon","Brass","Fiberglass","Ironbark","Mithril","Obsidian","Silver","Steel","Titanium"];
    $nm4 = ["Rifle","Carbine","Blaster","Fusil","Launcher","Shooter","Repeater","Sniper","Longrifle"];
    $nm5 = ["Agent","Allegiance","Ambassador","Annihilation","Bearer","Betrayer","Blaster","Bond","Boomstick","Boon","Breaker","Bringer","Bruiser","Butcher","Call","Carbine","Carrier","Champion","Conqueror","Courier","Crier","Crusader","Crusher","Cry","Cunning","Dawn","Defender","Defiler","Delegate","Destroyer","Dispatcher","Disposer","Emissary","Ender","Envoy","Executioner","Favor","Ferocity","Foe","Fusil","Gift","Glory","Guardian","Harbinger","Heirloom","Herald","Hope","Incarnation","Last Hope","Last Stand","Launcher","Legacy","Longrifle","Memory","Messenger","Might","Musket","Oath","Pact","Pledge","Pounder","Promise","Prophecy","Protector","Queller","Ravager","Reach","Reaper","Repeater","Rifle","Scepter","Sculptor","Secret","Shooter","Shotgun","Slayer","Smasher","Sniper","Soul","Suppressor","Terror","Token","Tribute","Vengeance","Voice","Whisper","Wit"];
    $nm6 = ["of Agony","of Ancient Power","of Anguish","of Ashes","of Assassins","of Black Magic","of Blessed Fortune","of Blessings","of Blight","of Blood","of Bloodlust","of Broken Bones","of Broken Dreams","of Broken Families","of Burdens","of Chaos","of Closing Eyes","of Conquered Worlds","of Corruption","of Cruelty","of Cunning","of Dark Magic","of Dark Souls","of Darkness","of Decay","of Deception","of Degradation","of Delusions","of Denial","of Desecration","of Diligence","of Dismay","of Dragonsouls","of Due Diligence","of Echoes","of Ended Dreams","of Ending Hope","of Ending Misery","of Eternal Bloodlust","of Eternal Damnation","of Eternal Glory","of Eternal Justice","of Eternal Rest","of Eternal Sorrow","of Eternal Struggles","of Eternity","of Executions","of Faded Memories","of Fallen Souls","of Fools","of Frost","of Frozen Hells","of Fury","of Giants","of Giantslaying","of Grace","of Grieving Widows","of Hate","of Hatred","of Hell's Games","of Hellish Torment","of Heroes","of Holy Might","of Honor","of Hope","of Horrid Dreams","of Horrors","of Illuminated Dreams","of Illumination","of Immortality","of Inception","of Infinite Trials","of Insanity","of Invocation","of Justice","of Light's Hope","of Lost Comrades","of Lost Hope","of Lost Voices","of Lost Worlds","of Magic","of Mercy","of Misery","of Mountains","of Mourning","of Mystery","of Necromancy","of Nightmares","of Oblivion","of Perdition","of Phantoms","of Power","of Pride","of Pride's Fall","of Putrefaction","of Reckoning","of Redemption","of Regret","of Riddles","of Secrecy","of Secrets","of Shadow Strikes","of Shadows","of Shifting Sands","of Shifting Worlds","of Silence","of Slaughter","of Souls","of Stealth","of Storms","of Subtlety","of Suffering","of Suffering's End","of Summoning","of Terror","of Thunder","of Time-Lost Memories","of Timeless Battles","of Titans","of Torment","of Traitors","of Trembling Hands","of Trials","of Truth","of Twilight's End","of Twisted Visions","of Unholy Blight","of Unholy Might","of Vengeance","of Visions","of Wasted Time","of Widows","of Wizardry","of Woe","of Wraiths","of Zeal","of the Ancients","of the Banished","of the Basilisk","of the Beast","of the Blessed","of the Breaking Storm","of the Brotherhood","of the Burning Sun","of the Caged Mind","of the Cataclysm","of the Champion","of the Claw","of the Corrupted","of the Covenant","of the Crown","of the Damned","of the Daywalker","of the Dead","of the Depth","of the Dreadlord","of the Earth","of the East","of the Emperor","of the Empty Void","of the End","of the Enigma","of the Fallen","of the Falling Sky","of the Flame","of the Forest","of the Forgotten","of the Forsaken","of the Gladiator","of the Harvest","of the Immortal","of the Incoming Storm","of the Insane","of the King","of the Lasting Night","of the Leviathan","of the Light","of the Lion","of the Lionheart","of the Lone Victor","of the Lone Wolf","of the Lost","of the Moon","of the Moonwalker","of the Night","of the Night Sky","of the Nightstalker","of the North","of the Occult","of the Oracle","of the Phoenix","of the Plague","of the Prince","of the Protector","of the Queen","of the Serpent","of the Setting Sun","of the Shadows","of the Sky","of the South","of the Stars","of the Storm","of the Summoner","of the Sun","of the Sunwalker","of the Talon","of the Undying","of the Victor","of the Void","of the West","of the Whispers","of the Wicked","of the Wind","of the Wolf","of the World","of the Wretched"];
    $nm7 = ["Abomination","Agony","Allegiance","Alpha","Amigo","Amnesia","Apocalypse","Armageddon","Ash","Ashes","Atomic Annie","Baby","Battery","Battlestar","Betrayal","Betrayer","Big Bertha","Mighty Mouse","Betty","Quick Fix","Swiftscope","Bad Medicine","Toots","Enterprise","Big Rhonda","Crowd Control","Intervention","Matilda","Pink Mist","Judas","Headhunter","Jaeger","Lupus","Big Daddy","Big Game","Birthmark","Blackout","Bloodfury","Bloodmoon","Bloodrage","Bloodspiller","Bloodweep","Bombardment","Boomer","Boomstick","Brass Rain","Brutality","Brutalizer","Bulldog","Burn","Burp","Cataclysm","Catastrophe","Chance","Chaos","Comet","Corpsemaker","Corruption","Cosmos","Crash","Crimson","Crossfire","Cruiser","Crush","Cyclone","Daddy","Damnation","Darkness","Dawn","Deadeye","Deathbringer","Deathraze","Decimation","Demise","Desolation","Despair","Destiny","Destruction","Devastation","Devotion","Devourer","Discharge","Disturbance","Divine","Dominance","Dominion","Doom","Downpour","Dragonmaw","Dragonstrike","Dreamhunter","Due Diligence","Early Retirement","Echo","Eclipse","Emergency","End of Dreams","Endbringer","Enigma","Envy","Epilogue","Equality","Equalizer","Eternal Rest","Eternity","Eveningstar","Extermination","Extinction","Falcon","Falling Star","Fate","Finality","Frankenlove","Frankenstein","Frenzy","Fury","Gov'ner","Grace","Harbinger","Harmony","Hatred","Heat","Hell's Scream","Homage","Honor's Call","Hope's End","Infamy","Infinity","Injection","Interrogator","Judged","Justice","Justifier","King of Nines","Knight's Fall","Knightfall","Lament","Last Chance","Last Laugh","Last Rites","Last Word","Last Words","Lazarus","Legacy","Legionaire","Life's Limit","Lightning","Lightningflash","Limbo","Lockjaw","Magma","Malice","Maneater","Massacre","Mercy","Midnight","Misery's End","Moonsight","Mortality","Narcoleptic","Night's Fall","Nightbane","Nightfall","Nightmare","Nighttime","Nightwatch","Nirvana","Oathbreaker","Oathkeeper","Oblivion","Ole Betsy","Omega","Optimist","Orbit","Party Pooper","Patience","Peacekeeper","Peacemaker","Pendulum","Perfect Storm","Persuasion","Piety","Popeye","Pride","Prophecy","Pulse","Punisher","Purifier","Rage","Rapture","Rattlesnake","Reckoning","Reign","Remorse","Requiem","Retirement","Rigormortis","Salvation","Salvo","Scar","Serenity","Severance","Shadowfury","Shadowmoon","Silence","Snake","Snapper","Soulburn","Sprocket","Stalker","Storm","Storm Breaker","Storm-Weaver","Stormbringer","Stormrider","Sugar","Sunshine","Supernova","Supremacy","Suspension","Swan Song","Sweetie","Syndrome","Termination","The Ambassador","The Chancellor","The End","The Judge","The Light","The Oculus","The Oracle","The Pig","The Void","Thunder","Thundercall","Trauma","Treachery","Tremor","Tribunal","Tribute","Trinity","Trouble","Twilight","Typhoon","Ubiquity","Undoing","Vacancy","Valkyrie","Vanquisher","Vengeance","Voice of Insanity","Voice of Reason","Warbringer","Warmonger","Whispers","Whisperwind","Wicked","Widow Maker","Willbreaker","Wit's End","Wolf"];

    $i = rand(0,10);
    if($i < 3){
        $gunName = $nm1[rand(0,count($nm1))];
    }else if($i < 5){
        $rnd = rand(0,count($nm2)-1);
        $rnd2 = rand(0,count($nm4)-1);
        $gunName = $nm2[$rnd] . " " . $nm4[$rnd2];
    }else if($i < 7){
        $rnd = rand(0,count($nm2)-1);
        $rnd2 = rand(0,count($nm3)-1);
        $rnd3 = rand(0,count($nm4)-1);
        $gunName = $nm2[$rnd] . " " . $nm3[$rnd2] . " " . $nm4[$rnd3];
    }else{
        $rnd = rand(0,count($nm7)-1);
        $rnd2 = rand(0,count($nm5)-1);
        $rnd3 = rand(0,count($nm6)-1);
        $gunName = $nm7[$rnd] . ", " . $nm5[$rnd2] . " " . $nm6[$rnd3];
    };
    return $gunName;
}

    public function fixDb(){
        $queryStr = "SELECT id,md5,image_name from webshots";
        $rez = $this->dateBase->query($queryStr);
        $result = $rez->result_array();

        foreacH($result as $element){
            $md5string = md5($element["image_name"]);

            if($image_name !== $element["md5"]){
                $this->dateBase->query("update webshots set md5 = ? where id = ?",array($md5string,$element["id"]));
            }
        }
        die("DONE");
    }
}