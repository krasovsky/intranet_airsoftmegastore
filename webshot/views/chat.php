<!DOCTYPE html>
<html>
    <head>
        <script src="/webshot/assets/js/node_modules/socket.io-client/socket.io.js"></script>
        <script type="text/javascript">
            var socketIo = io.connect("127.0.0.1:1337");
            socketIo.on("message_to_client", function(data) {
		   		document.getElementById("chatlog").innerHTML = ("<hr/>" +
		    	data['message'] + document.getElementById("chatlog").innerHTML);
			});

			function sendMessage() {
			    var msg = document.getElementById("message_input").value;
			    socketIo.emit("message_to_server", { message : msg});
			}
        </script>
    </head>
    <body>
        <input type="text" id="message_input"/>
        <button onclick="sendMessage()">send</button>
        <div id="chatlog"></div>
    </body>
</html>