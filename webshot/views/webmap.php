<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8 />
    <title>Leaflet Markercluster</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.mapbox.com/mapbox.js/v2.2.3/mapbox.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/v2.2.3/mapbox.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />
    <link href='/webshot/assets/css/webmap.css' rel='stylesheet' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/webmap.js"></script>
    <title>Custom AirsoftMegastore Map</title>

</head>
<body>
<!-- Example data. -->

<div id='map'></div>
<div id="mapJson" style="display:none"><?php echo json_encode($data)?></div>
<form id='filterForm' class="filterForm">

    <div class="form-title">Map Filters</div>
    <div class="form-select"><input type='checkbox' name='filters' onclick='webmap.toggleFilter();' value='customizer' checked> Customizer </div>
    <div class="form-icon"> <img src="/assets/img/icons/customizer.png"/></div>

    <div class="clear"><!-- CLEAR --></div>
    <div class="form-select"><input type='checkbox' name='filters' onclick='webmap.toggleFilter();' value='gunbuilder' checked> Gunbuilder</div>
    <div class="form-icon"> <img src="/assets/img/icons/gunbuilder.png"/></div>
</form>

<script>


</script>
</body>
</html>